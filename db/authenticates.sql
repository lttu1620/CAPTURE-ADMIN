/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : cakephp

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-12-25 09:57:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `authenticates`
-- ----------------------------
DROP TABLE IF EXISTS `authenticates`;
CREATE TABLE `authenticates` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL COMMENT '`user_id/admin_id base on type',  
  `token` varchar(40) NOT NULL COMMENT 'トークン',
  `expire_date` int(11) NOT NULL COMMENT 'トークンの期限',
  `regist_type` varchar(20) NOT NULL COMMENT 'user/admin',  
  `created` int(11) DEFAULT NULL COMMENT '作成日', 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
