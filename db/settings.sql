SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,  
    `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, -- unique
    `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `data_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL, -- integer/string/boolean
    `type` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'global', -- global/user/company/group
    `default_value` text COLLATE utf8_unicode_ci,  
    `value` text COLLATE utf8_unicode_ci DEFAULT NULL, -- setting value, for global setting only
    `pattern_url_rule` varchar(100) DEFAULT NULL, -- for group setting only, to config for permission to access url
    `disable` tinyint(1) DEFAULT 0,    
    `created` int(11) DEFAULT NULL,
    `updated` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for `user_settings`
-- ----------------------------
DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE `user_settings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,  
    `user_id` int(11) NOT NULL,  
    `setting_id` int(11) NOT NULL,  
    `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
    `disable` tinyint(1) DEFAULT 0,   
    `created` int(11) DEFAULT NULL,
    `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for `company_settings`
-- ----------------------------
DROP TABLE IF EXISTS `company_settings`;
CREATE TABLE `company_settings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,  
    `company_id` int(11) NOT NULL,  
    `setting_id` int(11) NOT NULL,  
    `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
    `disable` tinyint(1) DEFAULT 0,   
    `created` int(11) DEFAULT NULL,
    `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for `group_settings`
-- ----------------------------
DROP TABLE IF EXISTS `group_settings`;
CREATE TABLE `group_settings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,  
    `setting_id` int(11) NOT NULL,  
    `group` varchar(10) COLLATE utf8_unicode_ci NOT NULL, -- admin/user/company
    `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
    `disable` tinyint(1) DEFAULT 0,   
    `created` int(11) DEFAULT NULL,
    `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;