/*
Navicat MySQL Data Transfer

Source Server         : capture_dev
Source Server Version : 50621
Source Host           : sv4.evolable-asia.z-hosts.com:3306
Source Database       : capture

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2014-11-25 15:21:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admins`
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理者ID',
  `name` varchar(40) NOT NULL COMMENT '管理者名',
  `login_id` varchar(40) NOT NULL COMMENT 'ログインID',
  `password` varchar(40) NOT NULL COMMENT 'パスワード',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created` int(11) DEFAULT NULL COMMENT '作成日',
  `updated` int(11) DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='ユーザテーブル';

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('1', 'Cao Đình Tuấn', 'caodinhtuan', '123456', '0', '1416562550', '1416810369');
INSERT INTO `admins` VALUES ('2', 'Marisa Kirisame', 'marisa', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('3', 'Reimu Hakurei', 'reimu', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('4', 'Cirno', 'cirno', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('5', 'Alice Margatroid', 'alice', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('6', 'Patchouli Knowledge', 'patchouli', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('7', 'Sakuya Izayoi', 'sakuya', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('8', 'Hong Meiling', 'meiling', '123456', '1', '1416793668', '1416811688');
INSERT INTO `admins` VALUES ('9', 'Remilia Scarlet', 'remilia', '123456', '0', '1416793668', '1416810129');
INSERT INTO `admins` VALUES ('10', 'Flandre Scarlet', 'flandre', '123456', '1', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('11', 'Youmu Konpaku', 'youmu', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('12', 'Yuyuko Saigyouji', 'yuyuko', '123456', '0', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('13', 'Yukari Yakumo', 'yukari', '123456', '1', '1416793668', '1416808569');
INSERT INTO `admins` VALUES ('14', 'Suika Ibuki', 'suika', '123456', '1', '1416793668', '1416808569');
INSERT INTO `admins` VALUES ('15', 'Reishou Kirisame', 'reishou', '123456', '1', '1416793668', '1416793668');
INSERT INTO `admins` VALUES ('16', 'Tuan Evolableasia', 'tuancd', '123456', '0', '1416889106', '1416889106');
DROP TRIGGER IF EXISTS `before_insert_admins`;
DELIMITER ;;
CREATE TRIGGER `before_insert_admins` BEFORE INSERT ON `admins` FOR EACH ROW SET 
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `before_update_admins`;
DELIMITER ;;
CREATE TRIGGER `before_update_admins` BEFORE UPDATE ON `admins` FOR EACH ROW SET 
	new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
