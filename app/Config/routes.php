<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

App::import('Component', 'Cookie');
$cookie = new CookieComponent(new ComponentCollection());
if ($cookie->read('last_login_url') == 'admins/login') {
    Router::connect('/admins/login', array('controller' => 'pages', 'action' => 'login'));
    Router::connect('/login', array('controller' => 'pages', 'action' => 'login'));
} else {
    Router::connect('/login', array('controller' => 'pages', 'action' => 'login'));
    Router::connect('/admins/login', array('controller' => 'pages', 'action' => 'login'));
}
Router::connect('/', array('controller' => 'pages', 'action' => 'index', 'index'));
Router::connect('/fblogin', array('controller' => 'pages', 'action' => 'fblogin'));
Router::connect('/logout', array('controller' => 'pages', 'action' => 'logout'));
Router::connect('/register', array('controller' => 'pages', 'action' => 'register'));
Router::connect('/register/profile', array('controller' => 'pages', 'action' => 'registerprofile'));
Router::connect('/register/company', array('controller' => 'pages', 'action' => 'registercompany'));
Router::connect('/register/active', array('controller' => 'pages', 'action' => 'registeractive'));
Router::connect('/register/approve', array('controller' => 'pages', 'action' => 'registerapprove'));
Router::connect('/pending', array('controller' => 'pages', 'action' => 'pending'));
Router::connect('/forgetpassword', array('controller' => 'pages', 'action' => 'forgetpassword'));
Router::connect('/sendmailnotification', array('controller' => 'pages', 'action' => 'sendmailnotification'));
Router::connect("/lp", array('controller' => 'pages', 'action' => 'lp'));
Router::connect("/users/companyinformation/*", array('controller' => 'companies', 'action' => 'update'));
Router::connect("/contact", array('controller' => 'pages', 'action' => 'contact'));
Router::connect("/privacypolicy", array('controller' => 'pages', 'action' => 'privacypolicy'));
Router::connect("/posts/detail/*", array('controller' => 'pages', 'action' => 'postdetail'));

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
//Router::connect('/register', array('controller' => 'pages', 'action' => 'register'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
