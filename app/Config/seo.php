<?php

Configure::write('seo', array(
    'default' => array(
        'title' => __('Capture admin'),
        'description' => '',
        'keywords' => '',
    ),
    'admins' => array(
        'index' => array(
            'title' => __('Admin list'),
        ),
        'password' => array(
            'title' => __('Change password'),
        ),
        'profile' => array(
            'title' => __('Change profile'),
        ),
        'update' => array(
            'title' => __('Add/update Admin'),
        )
    ),
    'adminsettings' => array(
        'index' => array(
            'title' => __('Admin settings'),
        )
    ),
    'campuses' => array(
        'index' => array(
            'title' => __('Campus list'),
        ),
        'update' => array(
            'title' => __('Add/update campuse'),
        )
    ),
    'categories' => array(
        'follower' => array(
            'title' => __('Category follower list'),
        ),
        'index' => array(
            'title' => __('Category list'),
        ),
        'update' => array(
            'title' => __('Add/update Category'),
        )
    ),
    'companies' => array(
        'follower' => array(
            'title' => __('Company followers'),
        ),
        'index' => array(
            'title' => __('Company list'),
        ),
        'update' => array(
            'title' => __('Add/update Company'),
        )
    ),
    'companysettings' => array(
        'index' => array(
            'title' => __('Company settings'),
        )
    ),
    'companyuserviewlogs' => array(
        'index' => array(
            'title' => __('Company user view log list'),
        )
    ),
    'companyviewlogs' => array(
        'index' => array(
            'title' => __('Company view log list'),
        )
    ),
    'contacts' => array(
        'index' => array(
            'title' => __('Contact list'),
        ),
        'update' => array(
            'title' => __('Add/update contact'),
        )
    ),
    'departments' => array(
        'index' => array(
            'title' => __('Department list'),
        ),
        'update' => array(
            'title' => __('Add/update department'),
        )
    ),
    'followcategorylogs' => array(
        'index' => array(
            'title' => __('Follow category log list'),
        )
    ),
    'followcompanies' => array(
        'index' => array(
            'title' => __('Company followers'),
        )
    ),
    'followcompanylogs' => array(
        'index' => array(
            'title' => __('Follow company log list'),
        )
    ),
    'followsubcategorylogs' => array(
        'index' => array(
            'title' => __('Follow subcategory log list'),
        )
    ),
    'helpcontents' => array(
        'index' => array(
            'title' => __('Help content list'),
        ),
        'update' => array(
            'title' => __('Add/update the helping content'),
        )
    ),
    'loginlogs' => array(
        'index' => array(
            'title' => __('Login log list'),
        )
    ),
    'newscommentlikes' => array(
        'index' => array(
            'title' => __('News comment like list'),
        )
    ),
    'newscomments' => array(
        'index' => array(
            'title' => __('News comment list'),
        ),
        'like' => array(
            'title' => __('News comment likes'),
        ),
        'update' => array(
            'title' => __('Add/update news comment'),
        )
    ),
    'newsfeedfavorites' => array(
        'index' => array(
            'title' => __('News feed favorites list'),
        )
    ),
    'newsfeedlikes' => array(
        'index' => array(
            'title' => __('News Feed Likes list'),
        )
    ),
    'newsfeedreadlogs' => array(
        'index' => array(
            'title' => __('News feed read log list'),
        )
    ),
    'newsfeeds' => array(
        'favorite' => array(
            'title' => __('News feed favorites'),
        ),
        'index' => array(
            'title' => __('NewsFeed list'),
        ),
        'like' => array(
            'title' => __('News feed likes'),
        ),
        'lists' => array(
            'title' => __('RSSホームフィード RSS news feeds'),
        ),
        'update' => array(
            'title' => __('Add/update news feed'),
        ),
        'view' => array(
            'title' => __('News feed detail'),
        ),
    ),
    'newsfeedviewlogs' => array(
        'index' => array(
            'title' => __('News feed view log list'),
        )
    ),
    'newssites' => array(
        'index' => array(
            'title' => __('News site list'),
        ),
        'update' => array(
            'title' => __('Add/update news site'),
        )
    ),
    'newssitesrss' => array(
        'index' => array(
            'title' => __('Newssitesrss list'),
        ),
        'update' => array(
            'title' => __('Add/update news site rss'),
        )
    ),
    'pages' => array(
        'index' => array(
            'title' => __('Dashboard'),
        ),
        'display' => array(
            'title' => __('Display news feed'),
        ),
        'login' => array(
            'title' => __('Sign In'),
        ),
        'fblogin' => array(
            'title' => __('Login facebook'),
        ),
        'register' => array(
            'title' => __('Register New Membership'),
        ),
        'registerprofile' => array(
            'title' => __('Update profile'),
        ),
        'registercompany' => array(
            'title' => __('Register company'),
        ),
        'registeractive' => array(
            'title' => __('Register active'),
        ),
        'registerapprove' => array(
            'title' => __('Register approve'),
        ),
        'logout' => array(
            'title' => __('Logout'),
        ),
        'pending' => array(
            'title' => __('Notice'),
        ),
        'forgetpassword' => array(
            'title' => __('Forget password'),
        ),
        'newpassword' => array(
            'title' => __('New password'),
        ),
        'lp' => array(
            'title' => 'キャプチャー｜大学生と企業の“イマ”を掴むニュースアプリ',
        ),
        'contact' => array(
            'title' => __('Contact form'),
        ),
        'privacypolicy' => array(
            'title' => 'プライバシーポリシー - キャプチャー｜大学生と企業の“イマ”を掴むニュースアプリ',
        )
    ),
    'presetcomments' => array(
        'index' => array(
            'title' => __('Presetcomment list'),
        ),
        'update' => array(
            'title' => __('Add/update presetcomment'),
        )
    ),
    'pushmessageopenlogs' => array(
        'index' => array(
            'title' => __('Push message open log list'),
        )
    ),
    'pushmessages' => array(
        'index' => array(
            'title' => __('Push message list'),
        ),
        'update' => array(
            'title' => __('Add/update the pushing message'),
        )
    ),
    'pushmessagesendlogs' => array(
        'index' => array(
            'title' => __('Push message send log list'),
        )
    ),
    'settings' => array(
        'index' => array(
            'title' => __('Settings list'),
        ),
        'update' => array(
            'title' => __('Add/update settings'),
        )
    ),
    'sharelogs' => array(
        'index' => array(
            'title' => __('Share log list'),
        )
    ),
    'startlogs' => array(
        'index' => array(
            'title' => __('Start log list'),
        )
    ),
    'statistics' => array(
        'index' => array(
            'title' => __('Statistic'),
        ),
        'users' => array(
            'title' => __('Statistic user'),
        ),
        'feeds' => array(
            'title' => __('Newsfeed share report'),
        ),
        'newsfeeds' => array(
            'title' => __('News feed statistic'),
        ),
        'newscommentlike' => array(
            'title' => __('Clap statistic'),
        )
    ),
    'subcategories' => array(
        'index' => array(
            'title' => __('Subcategory list'),
        ),
        'update' => array(
            'title' => __('Add/update subcategory'),
        ),
        'follower' => array(
            'title' => __('Subcategory follower list'),
        ),
    ),
    'tags' => array(
        'index' => array(
            'title' => __('Tag list'),
        ),
        'update' => array(
            'title' => __('Add/update tag'),
        )
    ),
    'universities' => array(
        'index' => array(
            'title' => __('University list'),
        ),
        'update' => array(
            'title' => __('Add/update university'),
        )
    ),
    'userrecruiters' => array(
        'index' => array(
            'title' => __('Recruiter list'),
        ),
        'update' => array(
            'title' => __('Add/update recruiter'),
        ),
        'waitingapprove' => array(
            'title' => __('Awaiting approval'),
        ),
    ),
    'users' => array(
        'index' => array(
            'title' => __('User list'),
        ),
        'update' => array(
            'title' => __('Register user'),
        ),
        'profile' => array(
            'title' => __('Update user profile'),
        ),
        'profileinformation' => array(
            'title' => __('Update user information'),
        ),
        'recruiterinformation' => array(
            'title' => __('Update recruiter profile'),
        ),
        'facebookinformation' => array(
            'title' => __('Update user facebook information'),
        ),
        'password' => array(
            'title' => __('Change password'),
        ),
        'newsfeedfavorites' => array(
            'title' => __('News feed favorites list'),
        )
    ),
    'usersettings' => array(
        'index' => array(
            'title' => __('User settings'),
        )
    ),
    'system' => array(
        'ps' => array(
            'title' => __('Process list')
        )
    )
));

