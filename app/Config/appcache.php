<?php

class cacheObj {

    public $key = null;
    public $seconds = 86400; // 24*60*60

    public function __construct($key = null, $seconds = null) {
        $this->key = $key;
        $this->seconds = $seconds;
    }

}

Configure::write('universities_all_key_value', new cacheObj('universities_all_key_value', 60 * 60));
Configure::write('universities_all', new cacheObj('universities_all', 60 * 60));
Configure::write('campuses_all', new cacheObj('campuses_all', 60 * 60));
Configure::write('companies_all', new cacheObj('companies_all', 60 * 60));
Configure::write('categories_all', new cacheObj('categories_all', 60 * 60));
Configure::write('subcategories_all', new cacheObj('subcategories_all', 60 * 60));
Configure::write('global_settings', new cacheObj('global_settings', 60 * 60));
Configure::write('news_sites_all', new cacheObj('news_sites_all', 60 * 60));
Configure::write('tags_all', new cacheObj('tags_all', 60 * 60));
Configure::write('company_categories_all_key_value', new cacheObj('company_categories_all_key_value', 60 * 60));
Configure::write('report_general', new cacheObj('report_general', 60 * 60));
Configure::write('count_feed_manual', new cacheObj('count_feed_manual', 60 * 60));
