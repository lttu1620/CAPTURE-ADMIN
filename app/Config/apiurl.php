<?php
Configure::write('API.Timeout', 30);

Configure::write('API.url_books_detail', 'books/detail');
Configure::write('API.url_books_all', 'books/all');
Configure::write('API.url_books_addupdate', 'books/addupdate');

Configure::write('API.url_admins_list', 'admins/list');
Configure::write('API.url_admins_detail', 'admins/detail');
Configure::write('API.url_admins_disable', 'admins/disable');
Configure::write('API.url_admins_addupdate', 'admins/addupdate');
Configure::write('API.url_admins_login', 'admins/login');
Configure::write('API.url_admins_password', 'admins/updatepassword');

Configure::write('API.url_categories_all', 'categories/all');
Configure::write('API.url_categories_list', 'categories/list');
Configure::write('API.url_categories_detail', 'categories/detail');
Configure::write('API.url_categories_addupdate', 'categories/addupdate');

Configure::write('API.url_subcategories_detail', 'subcategories/detail');
Configure::write('API.url_subcategories_addupdate', 'subcategories/addupdate');
Configure::write('API.url_subcategories_all', 'subcategories/all');
Configure::write('API.url_subcategories_list', 'subcategories/list');

Configure::write('API.url_users_list', 'users/list');
Configure::write('API.url_users_detail', 'users/detail');
Configure::write('API.url_users_disable', 'users/disable');
Configure::write('API.url_users_addupdate', 'users/addupdate');
Configure::write('API.url_users_forgetpassword', 'users/forgetpassword');
Configure::write('API.url_users_registeremail', 'users/registeremail');
Configure::write('API.url_users_registeractive', 'users/registeractive');
Configure::write('API.url_users_login', 'users/login');
Configure::write('API.url_users_login_facebook', 'users/fblogin');
Configure::write('API.url_users_registercompany', 'users/registercompany');
Configure::write('API.url_users_token', 'users/token');
Configure::write('API.url_users_resendregisteremail', 'users/resendregisteremail');
Configure::write('API.url_users_resendregistercompany', 'users/resendregistercompany');
Configure::write('API.url_users_resendforgetpassword', 'users/resendforgetpassword');

Configure::write('API.url_universities_all', 'universities/all');
Configure::write('API.url_universities_list', 'universities/list');
Configure::write('API.url_universities_detail', 'universities/detail');
Configure::write('API.url_universities_disable', 'universities/disable');
Configure::write('API.url_universities_addupdate', 'universities/addupdate');

Configure::write('API.url_userprofiles_detail', 'userprofiles/detail');
Configure::write('API.url_userprofiles_detailbyemail', 'userprofiles/detailbyemail');
Configure::write('API.url_userprofiles_updatepassword', 'userprofiles/updatepassword');
Configure::write('API.url_userprofiles_list', 'userprofiles/list');
Configure::write('API.url_userprofiles_addupdate', 'userprofiles/addupdate');
Configure::write('API.url_userprofiles_changepassword', 'userprofiles/changepassword');
Configure::write('API.url_userprofiles_companyadminemail', 'userprofiles/companyadminemail');

Configure::write('API.url_userfacebookinformations_detail', 'userfacebookinformations/detail');
Configure::write('API.url_userfacebookinformations_addupdate', 'userfacebookinformations/addupdate');

Configure::write('API.url_useractivations_detail', 'useractivations/detail');
Configure::write('API.url_useractivations_addupdate', 'useractivations/addupdate');
Configure::write('API.url_useractivations_disable', 'useractivations/disable');
Configure::write('API.url_useractivations_check', 'useractivations/check');
Configure::write('API.url_user_activation_invite', 'useractivations/sendInvite');
Configure::write('API.url_user_activation_check_token_invite', 'useractivations/checktokeninvite');
Configure::write('API.url_user_register_by_invite', 'users/registerbyinvite');
Configure::write('API.url_user_activation_list_invite', 'useractivations/listinvite');

Configure::write('API.url_recruiters_list', 'userrecruiters/list');
Configure::write('API.url_recruiters_detail', 'userrecruiters/detail');
Configure::write('API.url_recruiters_disable', 'userrecruiters/disable');
Configure::write('API.url_recruiters_addupdate', 'userrecruiters/addupdate');

Configure::write('API.url_companies_all', 'companies/all');
Configure::write('API.url_companies_list', 'companies/list');
Configure::write('API.url_companies_detail', 'companies/detail');
Configure::write('API.url_companies_disable', 'companies/disable');
Configure::write('API.url_companies_addupdate', 'companies/addupdate');
Configure::write('API.url_companies_updatecounter', 'companies/updatecounter');

Configure::write('API.url_campuses_list', 'campuses/list');
Configure::write('API.url_campuses_all', 'campuses/all');
Configure::write('API.url_campuses_detail', 'campuses/detail');
Configure::write('API.url_campuses_addupdate', 'campuses/addupdate');

Configure::write('API.url_upload_image', 'upload/image.json');
Configure::write('API.url_upload_video', 'upload/video.json');

Configure::write('API.url_newsfeeds_list', 'newsfeeds/list');
Configure::write('API.url_mobile_newsfeeds_list', 'mobile/newsfeeds/list');
Configure::write('API.url_newsfeeds_detail', 'newsfeeds/detail');
Configure::write('API.url_newsfeeds_addupdate', 'newsfeeds/addupdate');
Configure::write('API.url_newsfeedreadlogs_list', 'newsfeedreadlogs/list');
Configure::write('API.url_newsfeedviewlogs_list', 'newsfeedviewlogs/list');
Configure::write('API.url_newsfeedlikes_list', 'newsfeedlikes/list');
Configure::write('API.url_newsfeedfavorites_list', 'newsfeedfavorites/list');
Configure::write('API.url_newsfeedfavorites_disable', 'newsfeedfavorites/disable');

Configure::write('API.url_newssites_list', 'newssites/list');
Configure::write('API.url_newssites_all', 'newssites/all');

Configure::write('API.url_newssitesrss_all', 'newssitesrss/all');
Configure::write('API.url_newssitesrss_list', 'newssitesrss/list');
Configure::write('API.url_newssitesrss_detail', 'newssitesrss/detail');
Configure::write('API.url_newssitesrss_addupdate', 'newssitesrss/addupdate');

Configure::write('API.url_newscomments_list', 'newscomments/list');
Configure::write('API.url_newscomments_detail', 'newscomments/detail');
Configure::write('API.url_newscomments_add', 'newscomments/add');
Configure::write('API.url_newscomments_update', 'newscomments/update');
Configure::write('API.url_newscomments_updatecounter', 'newscomments/updatecounter');
Configure::write('API.url_newscomments_checkusercommentedfeed', 'newscomments/checkusercommentedforfeed');

Configure::write('API.url_helpcontents_list', 'helpcontents/list');
Configure::write('API.url_helpcontents_detail', 'helpcontents/detail');
Configure::write('API.url_helpcontents_addupdate', 'helpcontents/addupdate');

Configure::write('API.url_pushmessages_list', 'pushmessages/list');
Configure::write('API.url_pushmessages_detail', 'pushmessages/detail');
Configure::write('API.url_pushmessages_addupdate', 'pushmessages/addupdate');
Configure::write('API.url_pushmessages_disable', 'pushmessages/disable');

Configure::write('API.url_newssites_all', 'newssites/all');
Configure::write('API.url_newssites_list', 'newssites/list');
Configure::write('API.url_newssites_detail', 'newssites/detail');
Configure::write('API.url_newssites_disable', 'newssites/disable');
Configure::write('API.url_newssites_addupdate', 'newssites/addupdate');

Configure::write('API.url_departments_all', 'departments/all');
Configure::write('API.url_departments_list', 'departments/list');
Configure::write('API.url_departments_detail', 'departments/detail');
Configure::write('API.url_departments_disable', 'departments/disable');
Configure::write('API.url_departments_addupdate', 'departments/addupdate');

Configure::write('API.url_followcompanies_list', 'followcompanies/list');
Configure::write('API.url_followcompanies_disable', 'followcompanies/disable');

Configure::write('API.url_followcategories_list', 'followcategories/list');
Configure::write('API.url_followcategories_disable', 'followcategories/disable');
Configure::write('API.url_mobile_followcategories_list', 'mobile/followcategories/list');


Configure::write('API.url_followsubcategories_list', 'followsubcategories/list');
Configure::write('API.url_followsubcategories_disable', 'followsubcategories/disable');

Configure::write('API.url_loginlogs_list', 'loginlogs/list');
Configure::write('API.url_loginlogs_add', 'loginlogs/add');

Configure::write('API.url_presetcomments_list', 'presetcomments/list');
Configure::write('API.url_presetcomments_addupdate', 'presetcomments/addupdate');
Configure::write('API.url_presetcomments_detail', 'presetcomments/detail');

Configure::write('API.url_companyviewlogs_list', 'companyviewlogs/list');
Configure::write('API.url_companyuserviewlogs_list', 'companyuserviewlogs/list');
Configure::write('API.url_followcompanylogs_list', 'followcompanylogs/list');
Configure::write('API.url_followcategorylogs_list', 'followcategorylogs/list');
Configure::write('API.url_followsubcategorylogs_list', 'followsubcategorylogs/list');
Configure::write('API.url_sharelogs_list', 'sharelogs/list');
Configure::write('API.url_pushmessageopenlogs', 'pushmessageopenlogs/list');
Configure::write('API.url_pushmessagesendlogs', 'pushmessagesendlogs/list');

Configure::write('API.url_settings_all', 'settings/all');
Configure::write('API.url_settings_list', 'settings/list');
Configure::write('API.url_settings_detail', 'settings/detail');
Configure::write('API.url_settings_addupdate', 'settings/addupdate');
Configure::write('API.url_settings_multiupdate', 'settings/multiupdate');

Configure::write('API.url_usersettings_all', 'usersettings/all');
Configure::write('API.url_usersettings_disable', 'usersettings/disable');
Configure::write('API.url_usersettings_addupdate', 'usersettings/addupdate');
Configure::write('API.url_usersettings_multiupdate', 'usersettings/multiupdate');

Configure::write('API.url_contacts_detail', 'contacts/detail');
Configure::write('API.url_contacts_addupdate', 'contacts/addupdate');
Configure::write('API.url_contacts_list', 'contacts/list');

Configure::write('API.url_newscommentlikes_add', 'newscommentlikes/add');
Configure::write('API.url_newscommentlikes_disable', 'newscommentlikes/disable');

Configure::write('API.url_companysettings_all', 'companysettings/all');
Configure::write('API.url_companysettings_disable', 'companysettings/disable');
Configure::write('API.url_companysettings_addupdate', 'companysettings/addupdate');
Configure::write('API.url_companysettings_multiupdate', 'companysettings/multiupdate');

Configure::write('API.url_adminsettings_all', 'adminsettings/all');
Configure::write('API.url_adminsettings_disable', 'adminsettings/disable');
Configure::write('API.url_adminsettings_addupdate', 'adminsettings/addupdate');
Configure::write('API.url_adminsettings_multiupdate', 'adminsettings/multiupdate');

Configure::write('API.url_newscommentlikes_update', 'newscommentlikes/add');
Configure::write('API.url_newscommentlikes_update', 'newscommentlikes/update');
Configure::write('API.url_newscommentlikes_disable', 'newscommentlikes/disable');
Configure::write('API.url_newscommentlikes_list', 'newscommentlikes/list');

Configure::write('API.url_userrecruiters_isAdmin', 'userrecruiters/isAdmin');
Configure::write('API.url_userrecruiters_isApproved', 'userrecruiters/isApproved');
Configure::write('API.url_userrecruiters_login', 'userrecruiters/login');
Configure::write('API.url_userrecruiters_login_facebook', 'userrecruiters/fblogin');
Configure::write('API.url_userrecruiters_login_facebook_by_token', 'userrecruiters/fblogintoken');

Configure::write('API.url_reports_newsfeed_share', 'reports/newsfeedshares');
Configure::write('API.url_reports_newsfeed_favorite', 'reports/newsfeedfavorites');
Configure::write('API.url_reports_newsfeed_read', 'reports/newsfeedreads');
Configure::write('API.url_reports_general', 'reports/general');
Configure::write('API.url_reports_newscomment_like', 'reports/newscommentlikes');

Configure::write('API.url_reports_newsfeed_view', 'reports/newsfeedviews');
Configure::write('API.url_system_deletecache', 'system/deletecache');
Configure::write('API.url_newssites_updatecounter', 'newssites/updatecounter');
Configure::write('API.url_newsfeeds_updatecounter', 'newsfeeds/updatecounter');

Configure::write('API.url_tags_list', 'tags/list');
Configure::write('API.url_tags_all', 'tags/all');
Configure::write('API.url_tags_detail', 'tags/detail');
Configure::write('API.url_tags_addupdate', 'tags/addupdate');
Configure::write('API.url_tags_updatecounter', 'tags/updatecounter');

Configure::write('API.url_newsfeedtags_disable', 'newsfeedtags/disable');
Configure::write('API.url_newsfeedtags_add', 'newsfeedtags/add');

Configure::write('API.url_startlogs_list', 'startlogs/list');
Configure::write('API.url_images_crop', 'images/crop');
Configure::write('API.url_system_runbatch', 'system/runbatch');
Configure::write('API.url_system_ps', 'system/ps');

Configure::write('API.url_mobile_newsfeeds_list', 'mobile/newsfeeds/list');
Configure::write('API.url_mobile_newsfeedfavorites_list', 'mobile/newsfeedfavorites/list');
Configure::write('API.url_mobile_followcompanies_list', 'mobile/followcompanies/list');

Configure::write('API.url_reports_newsfeedspv', 'reports/newsfeedspv');
Configure::write('API.url_reports_newsfeedsuu', 'reports/newsfeedsuu');

Configure::write('API.url_posts_detail', 'posts/detail');
Configure::write('API.url_posts_update', 'posts/update');
Configure::write('API.url_posts_add', 'posts/add');
Configure::write('API.url_posts_list', 'posts/list');