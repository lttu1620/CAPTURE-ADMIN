<?php

/**
 * Login of model.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Login extends AppModel {

    public $name = 'Login';
    public $table = 'admins';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'login' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Login ID can not empty'),
                ),
            ),
            'password' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Password can not empty'),
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

}
