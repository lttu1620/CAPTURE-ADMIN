<?php

/**
 * Invites of model
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Invite extends AppModel {

    public $name = 'Invite';
    public $table = 'invites';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'company_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Company can not empty'),
                )
            ),
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 255),
                    'message' => __('Email must be no larger then 255 character long')
                ),
                'rule3' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'))
            )
        );
        return $this->validates();
    }

}
