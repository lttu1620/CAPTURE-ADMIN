<?php

/**
 * User of model.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class User extends AppModel {

    public $name = 'User';
    public $table = 'users';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateUserInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Name must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            'password' => array(
                'maxLength' => array(
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password must be not null or must be between 6 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
            ),
            'password_confirm' => array(
                'maxLength' => array(
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password confirm must be not null or must be between 6 and 40 characters'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            ),
            'email' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Email must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'rule3' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'))
            ),
            'image_url' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadImage_url", array()),
                ),
            ),
            'sex_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Sex can not empty'),
                ),
            ),
            'introduction_text' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 200),
                    'message' => __('Introduction ext must be no larger then 200 character long')
                )
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to upload image_url.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadImage_url() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.image_url");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Validate password.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function validate_passwords() {
        if ($this->data[$this->name]['password'] != '') {
            return $this->data[$this->name]['password'] === $this->data[$this->name]['password_confirm'];
        } else {
            return true;
        }
    }

    /**
     * Validate password to user profile.
     *
     * @author thailh
     * @param array $date Input array
     * @return bool Returns the boolean.
     */
    public function validate_passwords_user_profile($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'password' => array(
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password must be not null or must be between 6 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
            ),
            'password_confirm' => array(
                'maxLength' => array(
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password confirm must be not null or must be between 6 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            )
        );
        if ($this->validates()) {
            return true; //$data[$this->name]['password'] === $data[$this->name]['password_confirm'];
        } else {
            return false;
        }
    }

    /**
     * Validate data recruiter before insert or update.
     *
     * @author thailh
     * @param array $date Input array
     * @return bool Returns the boolean.
     */
    public function validateRecruiterInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'thumbnail_img' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadThumbnail_img", array()),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Check upload thumbnail of image.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadThumbnail_img() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile('User.thumbnail_img');
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Validate data company before insert or update.
     *
     * @author thailh
     * @param array $date Input array
     * @return bool Returns the boolean.
     */
    public function validateCompanyInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Company name must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Company name can not empty'),
                )
            ),
            'thumbnail_img' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadCompany_thumbnail_img", array()),
                ),
            ),
            'background_img' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadBackground_img", array()),
                ),
            )
            ,
            'introduction_movie' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadIntroduction_movie", array()),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Check data thumbnail image of company before upload.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadCompany_thumbnail_img() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.thumbnail_img");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Check data background image before upload.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadBackground_img() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.background_img");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Check the data introduction movie before upload.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadIntroduction_movie() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Video = new VideoComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.introduction_movie");
        if (!empty($file['name'])) {
            if (!$this->Video->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Video->errorMsg);
            }
        }
        return true;
    }

}
