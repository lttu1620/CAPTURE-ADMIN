<?php

/**
 * Post of model.
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Post extends AppModel
{
    public $name = 'Post';
    public $table = 'posts';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     * @author Le Tuan Tu
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'post_title' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 255),
                    'message' => __('Description must be no larger then 255 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Title can not empty'),
                )
            ),
            'post_description' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 255),
                    'message' => __('Title must be  not empty or no larger then 255 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Description can not empty'),
                )
            ),
            'post_content' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Content can not empty'),
                )
            ),
            'image_url' => array(
                'checkUpload' => array(
                    'allowEmpty' => true,
                    'rule' => array("checkUpload", array()),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
