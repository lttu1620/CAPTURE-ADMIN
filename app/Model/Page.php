<?php

/**
 * Page of model
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Page extends AppModel {

    public $name = 'Page';
    public $table = 'admins';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to login.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateLogin($data) {
        $this->set($data[$this->name]);
        $validate = array();
        if (isset($data[$this->name]['login'])) {
            $validate['login'] = array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Login ID can not empty'),
                ),
            );
        } elseif (isset($data[$this->name]['email'])) {
            $validate['email'] = array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 40),
                    'message' => __('Email must be no larger then 40 character long')
                ),
                'format' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address')
                ),
            );
        }
        $validate['password'] = array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => __('Password can not empty'),
            ),
        );
        $this->validate = $validate;
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to register.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateRegister($data) {
        $this->set($data[$this->name]);
        $this->validate = array();
        $this->validate = array(
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 40),
                    'message' => __('Email must be no larger then 40 character long')
                ),
                'validate_format' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address')
                ),
                'validate_email' => array(
                    'rule' => array("validate_email", array()),
                    'message' => __('Email address have registered already')
                )
            ),
            'password' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Password can not empty'),
                ),
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password must be not null or must be between 6 and 40 characters'),
                ),
            ),
            'password_confirm' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Confirm password can not empty'),
                ),
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password confirm must be not null or must be between 6 and 40 characters'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to register profile.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateRegisterProfile($data) {
        $this->set($data[$this->name]);
        $this->validate = array();
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 40),
                    'message' => __('Name must be no larger then 40 character long')
                ),
            ),
            'nickname' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Nickname can not empty'),
                ),
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 40),
                    'message' => __('Nickname must be no larger then 40 character long')
                ),
            ),
            'sex_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Gender can not empty'),
                )
            ),
            'accept_policy' => array(
                'notEmpty' => array(
                    'rule'     => array('multiple', array('min' => 1)),
                    'required' => true,
                    'message' => __('please accept policy'),
                )
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to register company.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateRegisterCompany($data) {
        $this->set($data[$this->name]);
        $this->validate = array();
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 100),
                    'message' => __('Name must be no larger then 100 character long')
                ),
                'consistency' => array(
                    'rule' => array('check_consistency'),
                    'message' => __('Company name or kana name is invalid')
                )
            ),
            'kana' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Kana can not empty'),
                ),
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 100),
                    'message' => __('Kana must be no larger then 100 character long')
                ),
                'consistency' => array(
                    'rule' => array('check_consistency'),
                    'message' => __('Company name or kana name is invalid')
                )
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to update company.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateUpdateCompany($data) {
        $this->set($data[$this->name]);
        $allowEmptyThumbImage = true;
        if (empty($data[$this->name]['thumbnail_img']) || is_array($data[$this->name]['thumbnail_img'])) {
            $allowEmptyThumbImage = false;
        }
        $this->validate = array(
            'thumbnail_img' => array(
                'checkUploadImage' => array(
                    'allowEmpty' => $allowEmptyThumbImage,
                    'rule' => array("checkUploadImage"),
                    'message' => __('Image can not empty')
                ),
            ),
            'address' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Address can not empty'),
                ),
            ),
            'description_short' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Short description can not empty'),
                ),
                'maxLenght' => array(
                    'rule' => array('maxlength', 255),
                    'message' => __('Short description must be no larger than 255 character long')
                ),
            ),
            'category_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Category can not empty'),
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to remind password.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateForgetPassword($data) {
        $this->set($data[$this->name]);
        $this->validate = array();
        $this->validate = array(
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'validate_format' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address')
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to reset password.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateResetPassword($data) {
        $this->set($data[$this->name]);
        $this->validate = array();
        $this->validate = array(
            'password' => array(
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password must be not null or must be between 6 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
            ),
            'password_confirm' => array(
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password confirm must be not null or must be between 6 and 40 characters'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Validate data before the processing to upload image.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadImage() {
        $argList = func_get_args();
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile('Page.thumbnail_img');
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        } elseif (empty($argList[1]['allowEmpty'])) {
            return false;
        }
        return true;
    }

    /**
     * Verify data before the processing to upload video.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadVideo() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Video = new VideoComponent(new ComponentCollection());
        $file = $this->Common->getFile('Page.introduction_movie');
        if (!empty($file['name'])) {
            if (!$this->Video->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Video->errorMsg);
            }
        }
        return true;
    }

    /**
     * Validate password.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function validate_passwords() {
        if ($this->data[$this->name]['password'] != '') {
            return $this->data[$this->name]['password'] === $this->data[$this->name]['password_confirm'];
        } else {
            return true;
        }
    }

    /**
     * Validate email.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function validate_email() {
        return true;
    }

    /**
     * Check consistency.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function check_consistency() {
        $companyList = API::call(Configure::read('API.url_companies_all'), array('disable' => 0));
        $nameList = array_values(CommonComponent::arrayKeyValue($companyList, 'name', 'name'));
        $kanaList = array_values(CommonComponent::arrayKeyValue($companyList, 'kana', 'kana'));
        $name = $this->data[$this->name]['name'];
        $pos_name = array_search($name, $nameList);
        $kana = $this->data[$this->name]['kana'];
        $pos_kana = array_search($kana, $kanaList);

        if ($name != '' && $kana != '') {
            if (($pos_name !== false) && ($kana != $kanaList[$pos_name])) {
                return false;
            }
            if (($pos_kana !== false) && ($name != $nameList[$pos_kana])) {
                return false;
            }
        }
        return true;
    }

}
