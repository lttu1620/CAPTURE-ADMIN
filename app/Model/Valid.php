<?php

/**
 * Valid of model.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Valid extends AppModel {

    var $useTable = false;
    var $validate = array();

    /*
      var $_schema = array(
      'username' => array('type' => 'string', 'length' => 255),
      'email' => array('type' => 'string', 'length' => 255),
      'website' => array('type' => 'string', 'length' => 255),
      );
     */

    /**
     * validate data.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function valid1() {
        $this->validate = array(
            "username" => array(
                "rule1" => array(
                    "rule" => "notEmpty",
                    "message" => __("Username can not empty")
                ),
                "rule2" => array(
                    "rule" => array('minLength', 4),
                    "message" => __("Username must be at least 4 characters long")
                ),
                "rule3" => array(
                    "rule" => array('maxLength', 10),
                    "message" => __("Username must be no larger than 10 characters long")
                ),
            ),
            "email" => array(
                "rule1" => array(
                    "rule" => "notEmpty",
                    "message" => __("Please enter email !")
                ),
                "rule2" => array(
                    "rule" => "email",
                    "message" => __("Email not available !")
                ),
            ),
            "website" => array(
                "rule1" => array(
                    "rule" => "notEmpty",
                    "message" => __("Please enter website !")
                ),
                "rule2" => array(
                    "rule" => array('url', true),
                    "message" => __("website is not available")
                ),
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }

}
