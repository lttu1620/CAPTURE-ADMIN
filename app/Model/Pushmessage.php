<?php

/**
 * Pushmessages of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Pushmessage extends AppModel {

    public $name = 'Pushmessage';
    public $table = 'push_messages';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'message' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Message can not empty')
                ),
                'between' => array(
                    'rule' => array('between', 2, 40),
                    'message' => __('Between 2 to 40 characters'),
                ),
            ),
            'android_icon' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Android icon can not empty')
                )
            ),
            'send_reservation_date' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Reservation date can not empty')
                )
            ),
            'send_reservation_time' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Reservation time can not empty')
                )
            ),
            'ios_sound' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('IOS sound can not empty')
                )
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }

}
