<?php

/**
 * Startlog of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Startlog extends AppModel
{
	public $name = 'Startlog';
	public $table = 'start_logs';
	public $primaryKey = 'id';
}
