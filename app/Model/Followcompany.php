<?php

/**
 * Followcompany of model.
 *
 * @package Model
 * @version 1.0
 * @author Tuancd
 * @copyright Oceanize INC
 */
class Followcompany extends AppModel
{
	public $name = 'Followcompany';
	public $table = 'follow_companies';
	public $primaryKey = 'id';
}
