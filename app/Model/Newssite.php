<?php

/**
 * Newssite of model.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Newssite extends AppModel {

    public $name = 'Newssite';
    public $table = 'news_sites';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 255),
                    'message' => __('Name must be  not empty or no larger then 255 character long')
                )
            ),
            'feed_url' => array(
                'url' => array(
                    'allowEmpty' => true,
                    'rule' => array('url', true),
                    'message' => __('Url is not available')
                ),
            ),
            'thumbnail_img' => array(
                'checkUpload' => array(
                    'allowEmpty' => true,
                    'rule' => array("checkUpload", array()),
                ),
            )
        );
        if ($this->validates())
            return true;
        return false;
    }
    /**
     * Validate data before upload.
     *
     * @author truongnn
     * @return bool Returns the boolean.
     */
    public function checkUpload() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile('Newssite.thumbnail_img');
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

}
