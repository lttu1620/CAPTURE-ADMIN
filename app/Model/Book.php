<?php
/**
 * Admin's model.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Book extends AppModel
{
	public $name = 'Book';
	public $table = 'books';
	public $primaryKey = 'book_id';

	 /**
	 * Verify data before the processing to insert or update.
	 *
	 * @author thailh
	 * @param array $data Input array.
	 * @return bool Returns the boolean.
	 */
	public function validateInsertUpdate($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'isbn'  => array(
				'notEmpty'       => array(
					'rule'    => 'notEmpty',
					'message' => __('Isbn can not empty'),
				),
				'between'        => array(
					'rule'    => array('between', 2, 13),
					'message' => __('Between 2 to 13 characters')
				),
				/*
				'isUnique' => array(
					'rule' => array('isUnique', 2, 13),
					'message' => 'Isbn must have unique value'
				),
				*
				*/
				'customValidate' => array(
					'rule'    => array("customValidate", array()),
					'message' => __("Isbn must difference \"cakephp\"")
				),
				/*
				'isbn_minLength' => array(
					'rule' => array('minLength', 2),
					'message' => 'Isbn must be at least 2 characters long',
				),
				'isbn_maxLength' => array(
					'rule' => array('maxLength', 13),
					'message' => 'Isbn must be no larger than 13 characters long',
				)
				*
				*/
			),
			'url'   => array(
				/*
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Url can not empty',
				),*/
				'maxLength' => array(
					'allowEmpty' => true,
					'rule'       => array('maxLength', 100),
					'message'    => __('Isbn must be no larger than 100 characters long')
				),
				'url'       => array(
					'allowEmpty' => true,
					'rule'       => array('url', true),
					'message'    => __('Url is not available')
				),
			),
			'image' => array(
				'checkUpload' => array(
					'allowEmpty' => true,
					'rule'       => array("checkUpload", array()),
				),
			)
		);
		if ($this->validates()) return true;
		return false;
	}
         /**
	 * Custom validate.
	 *
	 * @author thailh
	 * @return bool Returns the boolean.
	 */
	public function customValidate()
	{
		$args = func_get_args();
		if (!empty($args[0]['isbn']) && $args[0]['isbn'] == 'cakephp') {
			return false;
		}
		return true;
	}
         /**
	 * Verify data before upload.
	 *
	 * @author thailh
	 * @return bool Returns the boolean.
	 */
	public function checkUpload()
	{
		$this->Common = new CommonComponent(new ComponentCollection());
		$this->Image = new ImageComponent(new ComponentCollection());
		$file = $this->Common->getFile('Book.image');
		if (!empty($file['name'])) {
			if (!$this->Image->checkUpload($file)) {
				return $this->Common->parseArrayMessage($this->Image->errorMsg);
			}
		}
		return true;
	}
}