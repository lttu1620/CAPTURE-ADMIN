<?php

/**
 * Companys of model
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Company extends AppModel
{
	public $name = 'Company';
	public $table = 'companies';
	public $primaryKey = 'id';

	/**
	 * Verify data before the processing to insert or update.
	 *
	 * @author Truongnn
	 * @param array $data Input array.
	 * @return bool Returns the boolean.
	 */
	public function validateInsertUpdate($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'name'               => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('Name can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 2, 120),
					'message' => __('Between 2 to 120 characters')
				),
			),
			'image'              => array(
				'checkUpload' => array(
					'allowEmpty' => true,
					'rule'       => array("checkUpload", array()),
				),
			),
			'corporate_url'      => array(
				'maxLength' => array(
					'allowEmpty' => true,
					'rule'       => array('maxLength', 255),
					'message'    => __('url must be no larger than 255 characters long')
				),
				'url'       => array(
					'allowEmpty' => true,
					'rule'       => array('url', true),
					'message'    => __('Url is not available')
				),
			),
			'introduction_movie' => array(
				'maxLength' => array(
					'allowEmpty' => true,
					'rule'       => array('maxLength', 255),
					'message'    => __('url must be no larger than 255 characters long')
				),
				'url'       => array(
					'allowEmpty' => true,
					'rule'       => array('url', true),
					'message'    => __('Introduction movie url is not available'),
				),
			)
		);
		if ($this->validates())
			return true;
		return false;
	}
        /**
	 * Verify data before upload.
	 *
	 * @author truongnn
	 * @return bool Returns the boolean.
	 */
	public function checkUpload()
	{
		$this->Common = new CommonComponent(new ComponentCollection());
		$this->Image = new ImageComponent(new ComponentCollection());
		$file = $this->Common->getFile('Company.thumbnail_img');
		if (!empty($file['name'])) {
			if (!$this->Image->checkUpload($file)) {
				return $this->Common->parseArrayMessage($this->Image->errorMsg);
			}
		}
		return true;
	}
}
