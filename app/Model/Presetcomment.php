<?php

/**
 * Presetcomment of model.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Presetcomment extends AppModel {

    public $name = 'Presetcomment';
    public $table = 'preset_comments';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'comments' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Comments can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 40),
                    'message' => __('Comments must be between 2 to 40 characters')
                ),
            )
        );
        if ($this->validates())
            return true;
        return false;
    }

}
