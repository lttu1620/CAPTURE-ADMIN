<?php

/**
 * Followcategorylog of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Followcategorylog extends AppModel {

    public $name = 'Followcategorylog';
    public $table = 'follow_category_logs';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateUserInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'user_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('user_id can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 digits')
                ),
            ),
            'category_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('category_id can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 digits')
                ),
            ),
            'unfollow' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('unfollow can not empty')
                ),
                'between' => array(
                    'rule' => array('between', 1, 1),
                    'message' => __('unfollow must be only 1 digit')
                )
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

}
