<?php

/**
 * AdminSetting's model.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class AdminSetting extends AppModel
{
	public $name = 'AdminSetting';
	public $table = 'admins_settings';
	public $primaryKey = 'id';
        
        /**
	 * Verify data before the processing to insert or update.
	 *
         * @author diennvt
	 * @param array $data Input data.
	 * @return bool Returns the boolean.
	 */
	public function validateInsertUpdate($data)
	{
            $this->set($data[$this->name]);
            $this->validate = array();
            if ($this->validates()) {
                    return true;
            }
            return false;
	}

}
