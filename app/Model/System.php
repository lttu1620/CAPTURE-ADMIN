<?php

/**
 * System of model.
 *
 * @package   Model
 * @version   1.0
 * @author    Le Tuan Tu
 * @copyright Oceanize INC
 */
class System extends AppModel
{
    public $name = 'System';
}
