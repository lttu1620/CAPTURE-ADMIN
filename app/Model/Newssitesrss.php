<?php

/**
 * Newssitesrss of model
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Newssitesrss extends AppModel {

    public $name = 'Newssitesrss';
    public $table = 'news_sites_rss';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 255),
                    'message' => __('Between 2 to 255 characters')
                ),
            ),
            'news_site_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('news_site_id can not empty'),
                ),
            ),
            'url' => array(
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxLength', 255),
                    'message' => __('url must be no larger than 255 characters long')
                ),
                'url' => array(
                    'allowEmpty' => true,
                    'rule' => array('url', true),
                    'message' => __('Url is not available')
                ),
            ),
            'description' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Description can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 255),
                    'message' => __('Description must be between 2 to 255 characters')
                ),
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }

}
