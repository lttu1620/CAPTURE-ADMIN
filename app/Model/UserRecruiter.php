<?php

/**
 * UserRecruiter of model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class UserRecruiter extends AppModel {

    public $name = 'UserRecruiter';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsert($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 40),
                    'message' => __('Name must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            'password' => array(
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password must be not null or must be between 6 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
            ),
            'password_confirm' => array(
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password confirm must be not null or must be between 6 and 40 characters'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            ),
            'email' => array(
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 40),
                    'message' => __('Email must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'rule3' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'))
            ),
            'company_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Company can not empty'),
                )
            ),
            'email_invite' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'rule3' => array(
                    'allowEmpty' => false,
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'))
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Validate password.
     *
     * @author tuancd.
     * @return bool Returns the boolean.
     */
    public function validate_passwords() {
        if ($this->data[$this->name]['password'] != '') {
            return $this->data[$this->name]['password'] === $this->data[$this->name]['password_confirm'];
        } else {
            return true;
        }
    }

}
