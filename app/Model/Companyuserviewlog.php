<?php

/**
 * Companyuserviewlog of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Companyuserviewlog extends AppModel
{
	public $name = 'Companyuserviewlog';
	public $table = 'company_user_view_logs';
	public $primaryKey = 'id';

	/**
	 * Verify data before the processing to insert or update.
	 *
	 * @param array $data Input array.
	 * @return bool Returns the boolean.
	 */
	public function validateUserInsertUpdate($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'user_id'    => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('user_id can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 1, 11),
					'message' => __('Between 1 to 11 digits')
				),
			),
			'company_id' => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('company_id can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 1, 11),
					'message' => __('Between 1 to 11 digits')
				),
			),
			'share_type' => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('share_type can not empty')
				),
				'between'  => array(
					'rule'    => array('between', 1, 2),
					'message' => __('Between 1 to 2 digits')
				)
			)
		);
		if ($this->validates()) {
			return true;
		}
		return false;
	}

}
