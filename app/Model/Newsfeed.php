<?php

/**
 * Newsfeed of model.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Newsfeed extends AppModel {

    public $name = 'Newsfeed';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'title' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 255),
                    'message' => __('Title must be  not empty or no larger then 255 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Title can not empty'),
                )
            ),
            'short_content' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 255),
                    'message' => __('Short content must be not empty or no larger then 255 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Short content can not empty'),
                )
            ),
            'image_url' => array(
                'checkUpload' => array(
                    'allowEmpty' => true,
                    'rule' => array("checkUpload", array()),
                ),
            )
        );
        if ($this->validates())
            return true;
        return false;
    }

    /**
     * Verify data before upload.
     *
     * @author Truongnn
     * @return bool Returns the boolean.
     */
    public function checkUpload() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile('Newsfeed.image_url');
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Validate newsfeed before import.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateImportNewsFeed($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'detail_url' => array(
                'maxLength' => array(
                    'rule' => array('maxLength', 255),
                    'message' => __('url must be no larger than 255 characters long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Url can not empty'),
                ),
                'url' => array(
                    'allowEmpty' => true,
                    'rule' => array('url', true),
                    'message' => __('Url is not available')
                ),
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }

}
