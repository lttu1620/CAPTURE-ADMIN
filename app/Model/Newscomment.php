<?php

/**
 * Newscomment of model
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Newscomment extends AppModel {

    public $name = 'Newscomment';
    public $table = 'news_comments';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'comment' => array(
                'ruleRequired' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Comment must not be empty')
                )
            ),
            'comment_short' => array(
                'ruleRequired' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Comment short must not be empty')
                )
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

}
