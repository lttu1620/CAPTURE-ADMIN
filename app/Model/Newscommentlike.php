<?php

/**
 * Newscommentlike of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Newscommentlike extends AppModel {

    public $name = 'Newscommentlike';
    public $table = 'news_comment_likes';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'news_site_id' => array(
                'ruleRequired' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Comment must not be empty')
                )
            ),
            'user_id' => array(
                'ruleRequired' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Comment short must not be empty')
                )
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

}
