<?php

App::uses('AppModel', 'Model');

/**
 * MasterData of model
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class MasterData extends AppModel {

    /**
     * Get all universities.
     *
     * @author thailh
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function universities_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('universities_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_universities_all'));
            AppCache::write(Configure::read('universities_all')->key, $result, Configure::read('universities_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get all universities include key and value.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function universities_all_key_value() {
        $result = AppCache::read(Configure::read('universities_all_key_value')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_universities_all'));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'id', 'name');
            AppCache::write(Configure::read('universities_all_key_value')->key, $result, Configure::read('universities_all_key_value')->seconds);
        }
        return $result;
    }

    /**
     * Get all campuses.
     *
     * @author thailh
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function campuses_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('campuses_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_campuses_all'));
            AppCache::write(Configure::read('campuses_all')->key, $result, Configure::read('campuses_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get all companies.
     *
     * @author thailh
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function companies_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('companies_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_companies_all'));
            AppCache::write(Configure::read('companies_all')->key, $result, Configure::read('companies_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get all categories.
     *
     * @author thailh
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function categories_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('categories_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_categories_all'));
            AppCache::write(Configure::read('categories_all')->key, $result, Configure::read('categories_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get all subcategories.
     *
     * @author thailh
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function subcategories_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('subcategories_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_subcategories_all'));
            AppCache::write(Configure::read('subcategories_all')->key, $result, Configure::read('subcategories_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get info global_settings.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function global_settings() {
        $result = AppCache::read(Configure::read('global_settings')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_settings_all'), array('type' => 'global'));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'name', 'value');
            AppCache::write(Configure::read('global_settings')->key, $result, Configure::read('global_settings')->seconds);
        }
        return $result;
    }

    /**
     * Get all news_sites.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function news_sites_all() {
        $result = AppCache::read(Configure::read('news_sites_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_newssites_all'), array());
            AppCache::write(
                    Configure::read('news_sites_all')->key, $result, Configure::read('news_sites_all')->seconds
            );
        }
        return $result;
    }

    /**
     * Get all tags.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function tags_all() {
        $result = AppCache::read(Configure::read('tags_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_tags_all'), array());
            AppCache::write(Configure::read('tags_all')->key, $result, Configure::read('tags_all')->seconds);
        }
        return $result;
    }

    /**
     * Get report genaral.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function report_general() {
        $result = AppCache::read(Configure::read('report_general')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_reports_general'), array());
            AppCache::write(Configure::read('report_general')->key, $result, Configure::read('report_general')->seconds);
        }
        return $result;
    }
    /**
     * Count feed manual.
     *
     * @author thailh
     * @return int Returns the integer.
     */
    public static function count_feed_manual() {
        $count_feed_manual = AppCache::read(Configure::read('count_feed_manual')->key);
        if ($count_feed_manual === false) {
            $result = Api::call(Configure::read('API.url_newsfeeds_list'), array('news_site_id' => 0, 'page' => 1, 'limit' => 1));
            $count_feed_manual = $result[0];
            AppCache::write(Configure::read('count_feed_manual')->key, $count_feed_manual, Configure::read('count_feed_manual')->seconds);
        }
        return $count_feed_manual;
    }

}
