<?php

/**
 * Test of model.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Test extends AppModel {

    public $name = 'Test';
    public $table = 'admins';
    public $primaryKey = 'id';

    /**
     * validate data.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validate($data) {
        return true;
    }

}
