<?php

/**
 * Campuse's model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Campuse extends AppModel
{
	public $name = 'Campuse';
	public $table = 'campuses';
	public $primaryKey = 'id';

	/**
	 * Verify data before the processing to insert or update.
	 *
	 * @author Truongnn
	 * @param array $data Input array.
	 * @return bool Returns the boolean.
	 */
	public function validateInsertUpdate($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'name'          => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('Name can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 2, 40),
					'message' => __('Between 2 to 40 characters')
				),
			),
			'university_id' => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('University must be choose')
				),
			),
		);
		if ($this->validates())
			return true;
		return false;
	}
}
