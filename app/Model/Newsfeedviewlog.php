<?php

/**
 * Newsfeedviewlog of model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Newsfeedviewlog extends AppModel {

    public $name = 'Newsfeedviewlogs';
    public $table = 'News_feed_view_logs';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateUserInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 40),
                    'message' => __('Name must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            'image_url' => array(
                'checkUpload' => array(
                    'allowEmpty' => true,
                    'rule' => array("checkUploadImage_url", array()),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

}
