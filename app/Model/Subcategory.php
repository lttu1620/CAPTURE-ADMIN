<?php

/**
 * Subcategories of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Subcategory extends AppModel {

    public $name = 'Subcategory';
    public $table = 'subcategories';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 40),
                    'message' => __('Between 4 to 40 characters')
                ),
            ),
            'category_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('category_id can not empty'),
                ),
            ),
        );
        if (!empty($data[$this->name]['id'])) {
            $this->validate['image_url'] = array(
                'checkUploadUpdate' => array(
                    'allowEmpty' => true,
                    'rule' => array("checkUploadUpdate"),
                ),
            );
        } else {
            $this->validate['image_url'] = array(
                'checkUploadInsert' => array(
                    'allowEmpty' => false,
                    'rule' => array("checkUploadInsert"),
                    'message' => __('Image can not empty')
                ),
            );
        }
        if ($this->validates())
            return true;
        return false;
    }
    /**
     * Verify data before the processing to upload image.
     *
     * @author truongnn
     * @return bool Returns the boolean.
     */
    public function checkUploadInsert() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile('Subcategory.image_url');
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
            return true;
        }
        return false;
    }
    /**
     * Verify data before the processing to update image.
     *
     * @author truongnn
     * @return bool Returns the boolean.
     */
    public function checkUploadUpdate() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile('Subcategory.image_url');
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

}
