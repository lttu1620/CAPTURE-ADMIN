<?php 
   echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
   echo $this->Html->css('//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css');
     //JS datetimepicker 
        echo $this->Html->script('plugins/datetimepicker/moment.min.js');
        echo $this->Html->script('plugins/datetimepicker/moment-with-locales.js');
        echo $this->Html->script('//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js');
?>

<div class="container" style="height:400px">
    <div class="row">
        <div class='col-sm-6'>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker3'>
                    <input type='text' id="textvalue" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>   
    <button type="button" class="btn btn-primary " id="btnModalSave" ><?php echo __('Save'); ?></button>
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker3').datetimepicker({
           format: 'YYYY年MM月DD日 HH時mm分',
            stepping:10,
            locale: 'ja',
            minDate: 'moment',
            maxDate: new Date([ moment().subtract('years',-10)]),
            showTodayButton:true,
            showClear:true,
            showClose:true
        });
       
         // action ajax save time to DB
        $("#btnModalSave").bind('click', function () {
            var _sd  = $('#datetimepicker3').data('date') !== undefined ?
                            date_change_formart($('#datetimepicker3').data('date').trim(),1):"";
            var _startDate =_sd;// _sd[0] + "-" + _sd[1] +"-" + _sd[2] + " " + _sd[3] + ":" + _sd[4];
            console.log(_startDate);
        });        
    });
    $("#textvalue").click(function() {
        //$(this).next().click();
         $('#datetimepicker3').data('DateTimePicker').date(_startDate).change;
    });

    function date_change_formart(_string ,_type)
    {
        _type=  _type === undefined ? 1 : 2;// 1: to english; 2: to japanese
        _pattern= null;
        _replace= "";
        switch(_type)
        {
            case 1 : 
                     _pattern =/(\d{4})\/(\d{2})\/(\d{2})(\s)*(\d{2})\:(\d{2})/;
                    _replace ='$1年$2月$3日 $5時$6分';
                break;
            case 2: 
                     _pattern =/(\W)*(\d{4})\年(\d{2})\月(\d{2})\日(\s)*(\d{2})\時(\d{2})\分/;
                    _replace ='$2/$3/$4 $6:$7';
                break;
        }
        
        return _string.replace(_pattern, _replace);
    }

</script>