<div>
    <div class="checkbox">
        <label>
            <input type="checkbox" data-toggle="toggle">
            Option one is enabled
        </label>
    </div>
    <div class="checkbox disabled">
        <label>
            <input type="checkbox" disabled data-toggle="toggle">
            Option two is disabled
        </label>
    </div>
    <div>
        <input type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled">
        <input type="checkbox" id="toggle-two">
        <script>
            $(function () {
                $('#toggle-two').bootstrapToggle({
                    on: 'Enabled',
                    off: 'Disabled'
                });
            })
        </script>
    </div>
    <div>
        <div style="margin-top: 30px;"> <input id="toggle-demo" type="checkbox" data-toggle="toggle"></div>
        <script>
            $(function () {

            })
            function toggleInit() {
                $('#toggle-demo').bootstrapToggle();
            }
            function toggleDestroy() {
                $('#toggle-demo').bootstrapToggle('destroy');
            }
            function toggle() {
                $('#toggle-demo').bootstrapToggle('toggle');
            }
            function toggleEnable() {
                $('#toggle-demo').bootstrapToggle('enable');
            }
            function toggleDisable() {
                $('#toggle-demo').bootstrapToggle('disable');
            }
            function toggleOn() {
                $('#toggle-demo').bootstrapToggle('on')
            }
            function toggleOff() {
                $('#toggle-demo').bootstrapToggle('off');
            }
        </script>
        <table style="padding: 10px; margin-top: 20px;" class="table-responsive">
            <tr>
                <td><button class="btn btn-default btn-xs" onclick="toggleInit()">Initialize</button></td>
                <td><button class="btn btn-default btn-xs" onclick="toggleDestroy()">destroy</button></td>
                <td><button class="btn btn-default btn-xs" onclick="toggleOn()">on</button></td>
                <td><button class="btn btn-default btn-xs" onclick="toggleOff()">off</button></td>
                <td><button class="btn btn-default btn-xs" onclick="toggle()">toggle</button></td>
                <td><button class="btn btn-default btn-xs" onclick="toggleEnable()">enable</button></td>
                <td><button class="btn btn-default btn-xs" onclick="toggleDisable()">disable</button></td>
            </tr>
        </table>
    </div>
</div>