<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon','img/favicon.ico');
        echo $this->Html->css('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css');
        echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css');
        echo $this->Html->css('//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css');
        echo $this->Html->css('morris/morris.css');
        echo $this->Html->css('jvectormap/jquery-jvectormap-1.2.2.css');
        echo $this->Html->css('datepicker/datepicker3.css');
        echo $this->Html->css('daterangepicker/daterangepicker-bs3.css');
        echo $this->Html->css('bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
        echo $this->Html->css('AdminLTE.css');
        echo $this->Html->css('custom.css');

        /*
		echo $this->Html->css('cake.generic');
		echo $this->Html->css('custom');
        */
        
		echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js');
		echo $this->Html->script('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js');
		echo $this->Html->script('//code.jquery.com/ui/1.11.1/jquery-ui.min.js');
		echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
		      
        //echo $this->Html->script('plugins/morris/morris.min.js');
		echo $this->Html->script('plugins/sparkline/jquery.sparkline.min.js');
		echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
		echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
		echo $this->Html->script('plugins/jqueryKnob/jquery.knob.js');
		echo $this->Html->script('plugins/daterangepicker/daterangepicker.js');
		echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
		echo $this->Html->script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
		echo $this->Html->script('plugins/iCheck/icheck.min.js');
                
		echo $this->Html->script('AdminLTE/app.js');
		//echo $this->Html->script('AdminLTE/dashboard.js');		
		echo $this->Html->script('common.js');
        
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
       
	?>
</head>
<body class="skin-blue">
    <?php include("header.ctp"); ?>  
    <div class="wrapper row-offcanvas row-offcanvas-left"> 
        <?php include("menu.ctp"); ?>
        <aside class="right-side">         
            <section class="content-header"> 
                <?php if (!empty($breadcrumb)) : ?>
                <?php echo $this->Breadcrumb->render($breadcrumb, $breadcrumbTitle); ?>  
                <?php endif ?>
            </section>       
            <section class="content">                
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>                
            </section>
        </aside>
    </div>    
    <?php echo Configure::read('Google.Analyticstracking')['admin']; ?>
</body>
</html>