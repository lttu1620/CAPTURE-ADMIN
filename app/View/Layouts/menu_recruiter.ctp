
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">           
        <a href="<?php echo($this->Html->url('/')); ?>" ><?php echo $this->Html->image('logo-noascii.png'); ?></a>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
			
			<li class="<?php if ($controller=='newsfeeds' || ($controller=='pages' && $action=='index')) echo "active"?>">
                <a href="<?php echo($this->Html->url('/newsfeeds/lists')) ?>">
                    <i class="fa fa-book"></i> <span><?php echo __('News Feeds') ?></span>
                </a>

            </li>
            <li <?php if ($controller == 'newscomments') echo "class=\"active\"" ?>>
                <a href="<?php echo($this->Html->url('/newscomments')) ?>">
                    <i class="fa fa-comment"></i> <span><?php echo __('News Comments') ?></span>
                </a>
            </li>  
            
            <?php if ($AppUI->recruiter_admin) : ?>
            <li class="treeview <?php if ($controller=='userrecruiters' || $controller=='invites') echo "active"?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span><?php echo __('User Manager')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu"> 
                    <li<?php if ($controller=='userrecruiters' && $action=='index') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/userrecruiters'))?>"><i class="fa fa-anchor"></i> <span><?php echo __('Members')?></span></a>
                    </li>
                    <li<?php if ($controller=='userrecruiters' && $action=='waitingapprove') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/userrecruiters/waitingapprove'))?>"><i class="fa fa-check-circle"></i> <span><?php echo __('Awaiting approval')?></span></a>
                    </li>
                    <li<?php if ($controller=='invites' && $action=='index') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/invites/index'))?>"><i class="fa fa-paper-plane"></i> <span><?php echo __('Invited list')?></span></a>
                    </li>
                </ul>
            </li> 
            <?php endif ?>
            <?php if (!$AppUI->recruiter_admin) : ?>
            <li <?php if ($controller == 'userrecruiters') echo "class=\"active\"" ?>>
                <a href="<?php echo($this->Html->url('/userrecruiters')) ?>">
                    <i class="fa fa-user"></i> <span><?php echo __('Members') ?></span>
                </a>                
            </li>
            <?php endif ?>
            
            <!--<li <?php /*if ($controller == 'companies') echo "class=\"active\"" */?>>
                <a href="<?php /*echo($this->Html->url('/companies/follower')) */?>">
                    <i class="fa fa-building"></i> <span><?php /*echo __('Company Followers') */?></span>
                </a>                
            </li>-->
            <li <?php if ($controller=='contacts') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/contacts/update'))?>">               
                    <i class="fa fa-envelope-o"></i> <span>お問い合わせ</span>
                </a>
            </li>
            <li class="treeview <?php if ($controller=='users') echo "active"?>">           
                <a href="#">
                    <i class="fa fa-cog"></i> <span><?php echo __('Setting page') ?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">                  
                    <li <?php if ($action == 'password') echo "class=\"active\"" ?>>
                        <a href="<?php echo($this->Html->url('/users/password')) ?>">
                            <i class="fa fa-angle-double-right"> </i><span><?php echo __('Change password') ?></span>
                        </a>                
                    </li>
                </ul>
            </li> 
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>