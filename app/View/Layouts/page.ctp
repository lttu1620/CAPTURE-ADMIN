<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja">
    <head>
        <title><?php echo $meta['title'] . '｜Capture ';?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width; initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
        <title>キャプチャー｜大学生と企業の“イマ”を掴むニュースアプリ</title>
        <link rel="stylesheet" type="text/css" href="css/reset.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <header>
            <section class="sec_inner contact_form">
                <div class="left">
                    <h1><a href="/"><img src="img/logo.png" width="250"></a></h1>
                    <h2><img src="img/h2.png"  width="407"></h2>
                    <div class="dl_btns">
                        <a href=""><img src="img/appstore_link.png" width="189"></a>
                        <a href=""><img src="img/googleplay_link.png" width="189"></a>
                    </div>
                </div>
                <div class="right">
                    <div class="for_company_btn">
                        <a href="<?php echo($this->Html->url('/contact')) ?>" title="企業採用担当者の方はこちらから">企業採用担当の方はこちら</a>
                    </div>
                    <div class="iphone"></div>
                </div>
            </section>
        </header>
        <section class="media_area">
            <div class="sec_inner">
            </div>
        </section>
        <?php echo $this->fetch('content'); ?>                     
        <footer>
            <div class="fb_like_box">
                 <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcapture.news%3Ffref%3Dts&amp;width&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=674403482679977" scrolling="no" frameborder="0" style="overflow:hidden; width:100%; min-height:300px;" allowTransparency="true"></iframe>
            </div>
            <ul>
                <li><a href="http://oceanize.co.jp/" target="_blank">運営会社について</a></li>
                <li><a href="<?php echo($this->Html->url('/privacypolicy')) ?>">プライバシーポリシー</a></li>
                <li><a href="<?php echo($this->Html->url('/contact')) ?>">お問い合わせ</a></li>
            </ul>
            <address>
                Copyright (C) Capture by Oceanize,Inc. All rights reserved.
            </address>
        </footer>
<?php echo Configure::read('Google.Analyticstracking')['lp']; ?>        
    </body>
</html>