<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Capture: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'Capture %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>	
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <title><?php echo $meta['title'] . '｜Capture '; if ($AppUI->is_admin == '1') echo '管理サイト'?></title>
   <meta name="description" content="<?php echo $meta['description']; ?>" />          
   <meta name="keywords" content="<?php echo $meta['keywords']; ?>" /> 
   <?php
   echo $this->Html->meta('icon','img/favicon.ico');                 

   echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
   echo $this->Html->css('//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');

   echo $this->Html->css('jquery-ui.css');
   echo $this->Html->css('jvectormap/jquery-jvectormap-1.2.2.css');
   echo $this->Html->css('lightbox/jquery.lightbox-0.5.css');
   echo $this->Html->css('../../mobile/bootstrap/css/bootstrap.min.css');
   echo $this->Html->css('../../mobile/dist/css/AdminLTE.min.css');
   echo $this->Html->css('../../mobile/dist/css/skins/_all-skins.min.css');
   echo $this->Html->css('../../mobile/dist/datepicker/datepicker3.css');
   echo $this->Html->css('../../mobile/plugins/datepicker/datepicker3.css');
   echo $this->Html->css('../../mobile/plugins/daterangepicker/daterangepicker-bs3.css');
   echo $this->Html->css('../../mobile/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
   echo $this->Html->css('bootstrap-toggle.css');
   echo $this->Html->css('capture.css');
   foreach ($moreCss as $css) {
    echo $this->Html->css($css);
}
echo $this->Html->css('custom_sp.css'); 
   echo $this->Html->css('AdminLTE.min.css');
        // CSS for chosen
echo $this->Html->css('../chosen.css');
echo $this->Html->css('../docsupport/prism.css');
echo $this->Html->script('jquery.min.js');        
echo $this->Html->script('../../mobile/plugins/jQuery/jQuery-2.1.4.min.js');        
echo $this->Html->script('bootstrap.min.js');		
?>
<script type="text/javascript">
    var baseUrl = "<?php echo $this->html->url('/', true); ?>";
    var controller = "<?php echo $controller; ?>";
    var action = "<?php echo $action; ?>";
    var referer = "<?php echo $referer; ?>";        
    var url = "<?php echo $url; ?>";
    var imgBaseUrl = baseUrl+ "<?php echo Configure::read('App.imageBaseUrl'); ?>";
</script>
</head>
<body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <?php include("mobile_header.ctp"); ?>  
      <?php include("mobile_menu.ctp"); ?>        
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">    
          <section class="content-header" id="content_header_<?php echo $controller . '_' . $action; ?>"> 
              <?php if (!empty($breadcrumb)) : ?>
                  <?php echo $this->Breadcrumb->render($breadcrumb, $breadcrumbTitle); ?>  
              <?php endif ?>
          </section>       
          <section class="content" id="<?php echo $controller . '_' . $action; ?>">                
              <?php echo $this->Session->flash(); ?>
              <?php echo $this->fetch('content'); ?>                
          </section>
      </aside>
        <?php include("mobile_footer.ctp"); ?>    
    </div>
    <!-- JS FOR PAGE -->
    <?php        
    echo $this->Html->script('jquery-ui.min.js');
    ?>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    
    <?php
    echo $this->Html->script('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
    echo $this->Html->script('../../mobile/plugins/sparkline/jquery.sparkline.min.js');
    echo $this->Html->script('../../mobile/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
    echo $this->Html->script('../../mobile/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
    echo $this->Html->script('../../mobile/plugins/knob/jquery.knob.js');
    echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js');
    echo $this->Html->script('../../mobile/plugins/daterangepicker/daterangepicker.js');
    echo $this->Html->script('../../mobile/plugins/datepicker/bootstrap-datepicker.js');
    echo $this->Html->script('../../mobile/plugins/fastclick/fastclick.min.js');
    echo $this->Html->script('plugins/lightbox/jquery.lightbox-0.5.js');
    echo $this->Html->script('../../mobile/dist/js/app.min.js');
    echo $this->Html->script('../../mobile/iscroll.js');

    foreach ($moreScript as $script) {
        echo $this->Html->script($script);
    }
    echo $this->Html->script('bootstrap-toggle.js');
    echo $this->Html->script('common.js');

    //JS for chosen
    echo $this->Html->script('../chosen.jquery.js');
    echo $this->Html->script('../docsupport/prism.js');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script'); 

    ?> 
    <?php echo Configure::read('Google.Analyticstracking')['admin']; ?>
</body>
</html>