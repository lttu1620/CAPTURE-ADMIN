<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">           
        <!-- sidebar menu: : style can be found in sidebar.less -->

        <a href="<?php echo($this->Html->url('/')); ?>" ><?php echo $this->Html->image('logo-noascii.png'); ?></a>
        <ul class="sidebar-menu">
            <li class="">
                <a href="<?php echo($this->Html->url('/')); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo __('Dashboard')?></span>
                </a>
            </li> 
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i> <span><?php echo __('Statistics page')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">                    
                    <li>
                        <a href="<?php echo($this->Html->url('/statistics/newsfeeds'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('News feed')?></span></a>
                    </li> 
                    <li>
                        <a href="<?php echo($this->Html->url('/statistics/newscommentlike'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Clap')?></span></a>
                    </li>      
                </ul>
            </li> 
			<li class="treeview <?php if ($controller=='newsfeeds' && $action !=="import")  echo "active"?>">    
                <a href="<?php echo($this->Html->url('/newsfeeds'))?>">
                    <i class="fa fa-book"></i> <span><?php echo __('News Feed Manager')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if (empty($this->request->query['news_site_id'])) echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('All')?></span></a>
                    </li>
                    <li<?php if (empty($this->request->query['news_site_id'])) echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds?appli=all'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo '【appli】全ての記事'?></span></a>
                    </li>
                    <li<?php if (empty($this->request->query['news_site_id'])) echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds?appli=hot'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo '【appli】人気記事'?></span></a>
                    </li>
                    <li<?php if (empty($this->request->query['news_site_id'])) echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds?news_site_id=0'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Other sites');?></span></a>
                    </li>
                    <?php if (!empty($sitelist)): ?>
					<?php foreach ($sitelist as $site): ?>
						<?php if (!empty($site['feed_count'])): ?>
		                    <li <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id']==$site['id']) echo "class=\"active\""?>>
		                        <a href="<?php echo($this->Html->url('/newsfeeds?news_site_id=' . $site['id'])) ?>">
		                            <i class="fa fa-angle-double-right"></i>
									<span><?php echo $this->common->truncate($site['name'], 10) ?></span>
									<small class="badge-custom pull-right bg-red"><?php echo ($site['feed_count'] >= 1000)? '999+' : $site['feed_count'] ?></small>
		                        </a>                
		                    </li>
						<?php endif; ?>
					<?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </li>
            <li class="<?php if ($controller=='newsfeeds' && $action =="import") echo "active"?>">
                <a href="<?php echo($this->Html->url('/newsfeeds/import'))?>">
                    <i class="fa fa-bookmark-o"></i> <span><?php echo __('Edit recommend Articles added')?></span>
                </a>
            </li>
            <li class="treeview <?php if ($controller=='posts')  echo "active"?>">
                <a href="<?php echo($this->Html->url('/posts'))?>">
                    <i class="fa fa-bar-chart-o"></i> <span><?php echo __('Post Manager')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller=='posts' && $action =="index") echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/posts'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Post list');?></span></a>
                    </li>
                    <li<?php if ($controller=='posts' && $action =="update") echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/posts/update'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Add post');?></span></a>
                    </li>
                </ul>
            </li>
            <li <?php if ($controller=='newscomments') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/newscomments'))?>">
                    <i class="fa fa-comment"></i> <span><?php echo __('News Comment Manager')?></span>
                </a>
            </li>
            <li <?php if ($controller=='pushmessages') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/pushmessages'))?>">               
                    <i class="fa fa-bullhorn"></i> <span><?php echo __('Message Manager')?></span>
                </a>
            </li>
            <li <?php if ($controller=='contacts') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/contacts'))?>">               
                    <i class="fa fa-envelope-o"></i> <span><?php echo __('Contact Manager')?></span>
                </a>
            </li>
            <li class="treeview <?php if (($controller == 'admins' && $action != 'password') || $controller=='users' || $controller=='userrecruiters' || $controller=='companies' || $controller=='invites') echo "active"?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span><?php echo __('User Manager')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller == 'admins' && $action != 'password') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/admins'))?>"><i class="fa fa-wrench"></i> <span><?php echo __('Admin Manager')?></span></a>
                    </li>
                    <li<?php if ($controller=='users') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/users'))?>"><i class="fa fa-user"></i> <span><?php echo __('User Manager')?></span></a>
                    </li>   
                    <li<?php if ($controller=='userrecruiters') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/userrecruiters'))?>"><i class="fa fa-anchor"></i> <span><?php echo __('Recruiter Manager')?></span></a>
                    </li>
                    <li<?php if ($controller=='invites' && $action=='index') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/invites/index'))?>"><i class="fa fa-paper-plane"></i> <span><?php echo __('Invited list')?></span></a>
                    </li>
                    <li<?php if ($controller=='companies') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/companies'))?>"><i class="fa fa-building"></i> <span><?php echo __('Company Manager')?></span></a>
                    </li>                    
                </ul>
            </li>            
            <li class="treeview <?php if ($controller=='settings' || ($controller == 'admins' && $action == 'password')) echo "active"?>">
                <a href="#">      
                    <i class="fa fa-gear"></i> <span><?php echo __('Settings')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'global') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=global'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Global settings')?></a>
                    </li>
                    <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'user') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=user'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Default user settings')?></a>
                    </li>
                    <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'company') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=company'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Default company settings')?></a>
                    </li>
                    <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'admin') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=admin'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Default admin settings')?></a>
                    </li>
                    <li<?php if ($controller == 'admins' && $action == 'password') echo " class=\"active\""?>>
                        <a href="<?php echo $this->Html->Url("/admins/password") ?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Change password'); ?></a>                              
                    </li>
                </ul>   
            </li>
            <li class="treeview <?php if (  in_array($controller, array( 
                                                'startlogs',
                                                'loginlogs',                                                
                                                'newsfeedviewlogs',
                                                'newsfeedreadlogs',
                                                'companyviewlogs',
                                                'companyuserviewlogs',
                                                'followcompanylogs',
                                                'followcategorylogs',
                                                'followsubcategorylogs',
                                                'pushmessageopenlogs',
                                                'pushmessagesendlogs'))
                                ) echo "active"?>">
                <a href="#">
                    <i class="fa fa-files-o"></i> <span><?php echo __('Logs')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller=='startlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/startlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Start logs')?></a></li>
                    <li<?php if ($controller=='loginlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/loginlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Login logs')?></a></li>
                    <li<?php if ($controller=='newsfeedviewlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/newsfeedviewlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('View feed logs')?></a></li>
                    <li<?php if ($controller=='newsfeedreadlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/newsfeedreadlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Read feed logs')?></a></li>
                    <li<?php if ($controller=='companyviewlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/companyviewlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('View company logs')?></a></li>
                    <li<?php if ($controller=='companyuserviewlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/companyuserviewlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('User view company logs')?></a></li>
                    <li<?php if ($controller=='followcompanylogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/followcompanylogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Follow company logs')?></a></li>
                    <li<?php if ($controller=='followcategorylogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/followcategorylogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Follow category logs')?></a></li>
                    <li<?php if ($controller=='followsubcategorylogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/followsubcategorylogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Follow subcategory logs')?></a></li>
                    <li<?php if ($controller=='pushmessageopenlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/pushmessageopenlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Open message logs')?></a></li>
                    <li<?php if ($controller=='pushmessagesendlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/pushmessagesendlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Send message logs')?></a></li>
                    <li<?php if ($controller=='sharelogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/sharelogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Share logs')?></a></li>
                </ul>
            </li>
            <li class="treeview <?php if (in_array($controller, array(                
                                                'tags',
                                                'universities',
                                                'campuses',
                                                'departments',
                                                'categories',
                                                'subcategories',
                                                'helpcontents',
                                                'newssites',
                                                'newssitesrss'))
                                ) echo "active"?>">
                <a href="#">
                    <i class="fa fa-gears"></i> <span><?php echo __('Master')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller=='universities') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/universities'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Universities')?></a></li>
                    <li<?php if ($controller=='campuses') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/campuses'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Campuses')?></a></li>
                    <li<?php if ($controller=='departments') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/departments'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Departments')?></a></li>
                    <li<?php if ($controller=='categories') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/categories'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Categories')?></a></li>
                    <li<?php if ($controller=='subcategories') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/subcategories'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('SubCategories')?></a></li>
                    <li<?php if ($controller=='helpcontents') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/helpcontents'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Help')?></span></a></li>
                    <li<?php if ($controller=='newssites') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/newssites'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('News sites')?></span></a></li>
                    <li<?php if ($controller=='newssitesrss') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/newssitesrss'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('News sites Rss')?></span></a></li>
                    <li<?php if ($controller=='tags') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/tags'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Tags')?></span></a></li>
                </ul>
            </li> 
            <li class="treeview <?php if ($controller=='system') echo "active"?>">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span><?php echo __('System') ?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($action=='runbatch') echo " class=\"active\""?>><a onclick="return runbatch();" href="#"><i class="fa fa-angle-double-right"></i> <?php echo __('Start batch')?></a></li>
                    <li<?php if ($action=='deletecache') echo " class=\"active\""?>><a href="<?php echo $this->Html->url('/system/deletecache')?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Delete cache')?></a></li>
                    <li<?php if ($action=='ps') echo " class=\"active\""?>><a href="<?php echo $this->Html->url('/system/ps')?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Process')?></a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
