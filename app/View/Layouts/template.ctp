<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Capture: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'Capture %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>	
    <title><?php echo $meta['title'] . '｜Capture '; if ($AppUI->is_admin == '1') echo '管理サイト'?></title>
    <meta name="description" content="<?php echo $meta['description']; ?>" />          
    <meta name="keywords" content="<?php echo $meta['keywords']; ?>" /> 
	<?php
		echo $this->Html->meta('icon','img/favicon.ico');                 
                         
        echo $this->Html->css('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css');       
        echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css');
        echo $this->Html->css('//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css');
              
        //echo $this->Html->css('bootstrap.min.css');
        //echo $this->Html->css('font-awesome.min.css');
        //echo $this->Html->css('ionicons.min.css');
         
        echo $this->Html->css('jquery-ui.css');
        echo $this->Html->css('jvectormap/jquery-jvectormap-1.2.2.css');
        echo $this->Html->css('lightbox/jquery.lightbox-0.5.css');
        echo $this->Html->css('datepicker/datepicker3.css');
        echo $this->Html->css('daterangepicker/daterangepicker-bs3.css');
        echo $this->Html->css('bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
        echo $this->Html->css('AdminLTE.css');
        echo $this->Html->css('bootstrap-toggle.css');
        echo $this->Html->css('capture.css');
        echo $this->Html->css('/adminlte/js/plugins/imageresize/css/component.css');
        foreach ($moreCss as $css) {
            echo $this->Html->css($css);
        }
        echo $this->Html->css('custom.css'); 
        // CSS for chosen
        echo $this->Html->css('../chosen.css');
        echo $this->Html->css('../docsupport/prism.css');

		echo $this->Html->script('jquery.min.js');        
		echo $this->Html->script('bootstrap.min.js');		
	?>
    
    <script type="text/javascript">
        var baseUrl = "<?php echo $this->html->url('/'); ?>";
        var controller = "<?php echo $controller; ?>";
        var action = "<?php echo $action; ?>";
        var referer = "<?php echo $referer; ?>";        
        var url = "<?php echo $url; ?>";
        var imgBaseUrl = baseUrl+ "<?php echo Configure::read('App.imageBaseUrl'); ?>";
    </script>
</head>
<body class="skin-capture fixed" id="<?php echo $controller . '_' . $action; ?>">
    <?php include("header.ctp"); ?>  
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <?php include("menu.ctp"); ?>        
        <aside class="right-side">           
            <section class="content-header"> 
                <?php if (!empty($breadcrumb)) : ?>
                <?php echo $this->Breadcrumb->render($breadcrumb, $breadcrumbTitle); ?>  
                <?php endif ?>
            </section>       
            <section class="content" id="<?php echo $controller . '_' . $action; ?>">                
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>                
            </section>
        </aside>
    </div>
    
    <div id="modal-resize" class="modal fade" role="dialog">
        <div class="modal-dialog modal-resize" style="z-index: 1060; width: 1270px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="resize-image-content">
                        <div class="component">
                            <div class="input-group my-colorpicker2">
                                <input type="text" class="form-control" id="txtColor"/>
                                <div class="input-group-addon">
                                    <i></i>
                                </div>
                            </div>
                            <div class="overlay-background"></div>
                            <div class="overlay">
                                <div class="overlay-inner"></div>
                            </div>
                            <img class="resize-image" />
                            <button type="button" class="btn btn-primary btn-crop js-crop"><?php echo __("Crop")?></button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <?php        
        echo $this->Html->script('jquery-ui.min.js');
		     
        echo $this->Html->script('raphael-min.js');	 
        
		echo $this->Html->script('plugins/sparkline/jquery.sparkline.min.js');
		echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
		echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
		echo $this->Html->script('plugins/jqueryKnob/jquery.knob.js');
		echo $this->Html->script('plugins/daterangepicker/daterangepicker.js');
		echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
		echo $this->Html->script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); 
        echo $this->Html->script('plugins/lightbox/jquery.lightbox-0.5.js');
        echo $this->Html->script('plugins/imageresize/js/component.js');
        
        echo $this->Html->script('autocomplete.js');  	        
		echo $this->Html->script('AdminLTE/app.js');
        foreach ($moreScript as $script) {
            echo $this->Html->script($script);
        }
        echo $this->Html->script('bootstrap-toggle.js');
        echo $this->Html->script('common.js');
        	
        //JS for chosen
        echo $this->Html->script('../chosen.jquery.js');
        echo $this->Html->script('../docsupport/prism.js');        
        
        echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script'); 

	?> 
    <?php echo Configure::read('Google.Analyticstracking')['admin']; ?>
</body>
</html>