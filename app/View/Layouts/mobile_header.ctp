<header class="main-header">
   <!-- 
  <a href="<?php echo($this->Html->url('/')); ?>" class="logo">    
    <span class="logo-mini">     
       <?php echo $this->Html->image('logo-ascii.png'); ?>
    </span>   
    <span class="logo-lg">
        <?php echo $this->Html->image('logo-ascii.png'); ?>
    </span>   
  </a>
 -->
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">  
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span> 
    </a>
    <span class="logo-mini">   
        <a href="<?php echo($this->Html->url('/')); ?>"> 
            <?php echo $this->Html->image('logo-ascii.png'); ?>
        </a>
    </span> 
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <?php if ($AppUI) : ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="glyphicon glyphicon-user"></i>
              <span><?php echo $AppUI->display_name; ?> <i class="caret"></i></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->                        
              <li class="user-header">
                <?php if (!empty($AppUI->display_image)) : ?>
                  <?php echo $this->Html->image($this->Common->thumb($AppUI->display_image), array('class' => 'img-circle'))?>                                
                <?php endif ?>
                <p>
                  <?php echo $AppUI->display_name ?>            
                  <small><?php echo __('Member since') . date('Y-m', $AppUI->created) ?></small>
                </p>
              </li>                       
              <!-- Menu Body -->
              <li class="user-body">
                <div class="col-xs-4 text-center"><a href="userrecruiters"><i class="fa fa-building-o"></i>
                  <?php echo __('Company')?></a>
                </div>
            </li>
            <!-- Menu Footer-->                        
            <li class="user-footer">    
              <?php if ($AppUI->is_admin) : ?>
                <div class="pull-left">
                  <a href="<?php echo $this->Html->Url("/admins/profile") ?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i>&nbsp;<?php echo __('Profile'); ?></a>
                </div>
              <?php endif ?>
              <?php if (!$AppUI->is_admin) : ?>
                <div class="pull-left">
                  <a href="<?php echo $this->Html->Url("/users/profileinformation") ?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i>&nbsp;<?php echo __('Profile'); ?></a>
                </div>
              <?php endif ?>
              <div class="pull-right">
                <a href="<?php echo $this->Html->Url("/pages/logout") ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>&nbsp;<?php echo __('Sign out'); ?></a>
              </div>                            
            </li>
          </ul>
        </li>
      <?php endif; ?>
    </ul>
  </div>
</nav>  
</header>

<?php if ($controller=='newsfeeds' || ($controller=='pages' && $action=='index')): ?>
<?php 
    $tag_id = $this->request->query('tag_id', 0); 
    $appli = $this->request->query('appli', 0);
    $news_site_id = $this->request->query('news_site_id', 0);
?>
<div id="scroller_wapper">   
    <div id="scroller">   
        <ul id="thelist">           
            <li><a <?php if ($tag_id) echo "class=\"active\""; ?> href="#" data-menu="rss-menu-1"><i class="fa fa-newspaper-o"></i> おすすめ <i class="fa fa-angle-down"></i></a></li>            
            <li><a <?php if ($news_site_id) echo "class=\"active\""; ?> href="#" data-menu="rss-menu-3"><i class="fa fa-newspaper-o"></i> 媒体別 <i class="fa fa-angle-down"></i></a></li>             
            <li><a <?php if ($appli) echo "class=\"active\""; ?> href="#" data-menu="rss-menu-2"><i class="fa fa-newspaper-o"></i> コメントのある記事 <i class="fa fa-angle-down"></i></a></li>            
        </ul>   
    </div>
</div>

<div class="box box-solid box-primary rss-menu" id="rss-menu-1">
    <div class="box-header with-border">
        <h3 class="box-title">おすすめ</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div> 
    <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li <?php if (isset($this->request->query['tag_id']) && $this->request->query['tag_id'] == 'all') echo "class='active'"; ?>>
                <a href="<?php echo($this->Html->url('/newsfeeds/lists?tag_id=all')) ?>" class="">
                    <i class="fa fa-angle-double-right"></i>
                    <span class="badge bg-capture-red"></span>
                    <?php echo __('ALL') ?>
                </a>
            </li>
            <?php foreach ($tagList as $tag): ?>
                <?php if (!empty($tag['feed_count'])): ?>
                    <li <?php if (isset($this->request->query['tag_id']) && $this->request->query['tag_id'] == $tag['id'])  echo "class='active'"; ?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds/lists?tag_id=' . $tag['id'])) ?>" >
                            <i class="fa fa-angle-double-right"></i>
                            <span class="label label-primary pull-right"><?php echo ($tag['feed_count'] >= 1000) ? '999+' : $tag['feed_count'] ?></span>
                            <?php echo $this->common->truncate($tag['name'], 10) ?>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id'] == '0') echo "class='active'"; ?>>
                <a href="<?php echo($this->Html->url('/newsfeeds/lists?news_site_id=0')) ?>" >
                    <i class="fa fa-angle-double-right"></i>
                    <span class="label label-primary pull-right"><?php echo !empty($count_feed_manual) ? $count_feed_manual : 0;?></span>
                    その他おすすめ記事
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="box box-solid box-primary rss-menu" id="rss-menu-2">
    <div class="box-header with-border">
        <h3 class="box-title">コメントのある記事</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li <?php if (isset($this->request->query['appli']) && $this->request->query['appli'] == 'all') echo "class='active'"; ?>>
                <a href="<?php echo($this->Html->url('/newsfeeds/lists?appli=all')) ?>" >
                    <i class="fa fa-angle-double-right"></i>
                    全ての記事
                </a>
            </li>
            <li <?php if (isset($this->request->query['appli']) && $this->request->query['appli'] == 'hot') echo "class='active'"; ?>>
                <a href="<?php echo($this->Html->url('/newsfeeds/lists?appli=hot')) ?>" >
                    <i class="fa fa-angle-double-right"></i>
                    人気記事
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="box box-solid box-primary rss-menu" id="rss-menu-3">
    <div class="box-header with-border">
        <h3 class="box-title">媒体別</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id'] == 'all') echo "class='active'"; ?>>
                <a href="<?php echo($this->Html->url('/newsfeeds/lists?news_site_id=all')) ?>">
                    <span class="badge bg-capture-red"></span><i class="fa fa-angle-double-right"></i> <?php echo __('All') ?>
                </a>
            </li>

            <?php if (!empty($sitelist)): ?>
                <?php foreach ($sitelist as $site): ?>
                    <?php if (!empty($site['feed_count'])): ?>
                        <li <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id'] == $site['id']) echo "class='active'";?>> 
                            <a href="<?php echo($this->Html->url('/newsfeeds/lists?news_site_id=' . $site['id'])) ?>">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="badge bg-capture-red"><?php echo ($site['feed_count'] >= 1000) ? '999+' : $site['feed_count'] ?></span>
                                <?php echo $this->common->truncate($site['name'], 10) ?>
                            </a>
                        </li>          
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>

<?php endif; ?>

<?php if ($controller=='users' && $action=='profileinformation'): ?>
<div id="scroller_wapper">   
    <div id="scroller">   
        <ul id="thelist">           
            <li><a class="active">ユーザ情報</a></li>            
            <li><a href="<?php echo $this->Html->url('/users/companyinformation') ?>" >企業プロフィール</a></li>            
            <li><a href="<?php echo $this->Html->url('/users/newscomments') ?>" >ニュースコメント一覧</a></li>             
        </ul>   
    </div>
</div>
<?php endif; ?>

<?php if ($controller=='companies' && $action=='update'): ?>
<div id="scroller_wapper">   
    <div id="scroller">   
        <ul id="thelist">           
            <li><a href="<?php echo $this->Html->url('/users/profileinformation') ?>" >ユーザ情報</a></li>            
            <li><a class="active">企業プロフィール</a></li>            
            <li><a href="<?php echo $this->Html->url('/users/newscomments') ?>" >ニュースコメント一覧</a></li>             
        </ul>   
    </div>
</div>
<?php endif; ?>

<?php if ($controller=='users' && $action=='newscomments'): ?>
<div id="scroller_wapper">   
    <div id="scroller">   
        <ul id="thelist">           
            <li><a href="<?php echo $this->Html->url('/users/profileinformation') ?>" >ユーザ情報</a></li>            
            <li><a href="<?php echo $this->Html->url('/users/companyinformation') ?>" >企業プロフィール</a></li>            
            <li><a class="active">ニュースコメント一覧</a></li>             
        </ul>   
    </div>
</div>
<?php endif; ?>

<script type="text/javascript"> 
    var myScroll;
    function loaded() {       
        myScroll = new iScroll('scroller_wapper');       
    }   
    if ($('#scroller_wapper').length > 0) {
        document.addEventListener('DOMContentLoaded', loaded, false);
        $(function(){
            $('#thelist a').click(function() {
                // $('.rss-menu').hide();
                $('#thelist a').removeClass('active');
                var menu = $(this).data('menu'); 
                // $('#'+menu).show();
                var display = $('#'+menu).css('display');
                if(display == 'none'){
                    $('.rss-menu').hide();
                    $('#'+menu).css('display','block');
                }else{
                    $('#'+menu).css('display','none');
                }    
                $(this).addClass('active');
            });
        });   
    }
</script>

