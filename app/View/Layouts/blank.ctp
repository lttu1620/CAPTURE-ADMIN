<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html class="bg-capture">
    <head>
        <title><?php echo $meta['title']; ?></title> 
        <meta name="description" content="<?php echo $meta['description']; ?>" />          
        <meta name="keywords" content="<?php echo $meta['keywords']; ?>" />      
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <?php
            echo $this->Html->meta('icon','img/favicon.ico');
            echo $this->Html->css('bootstrap.min.css');
            echo $this->Html->css('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css');
            echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css');
            echo $this->Html->css('bootstrap-toggle.css');
            echo $this->Html->css('jquery-ui.css');
            echo $this->Html->css('lightbox/jquery.lightbox-0.5.css');
            echo $this->Html->css('AdminLTE.css');
            foreach ($moreCss as $css) {
                echo $this->Html->css($css);
            }
            echo $this->Html->css('custom.css');
            echo $this->Html->script('jquery.min.js');        
            echo $this->Html->script('bootstrap.min.js');
        ?>
        <script type="text/javascript">
            var baseUrl = "<?php echo $this->html->url('/'); ?>";
            var controller = "<?php echo $controller; ?>";
            var referer = "<?php echo $referer; ?>";        
            var url = "<?php echo $url; ?>";
            var imgBaseUrl = baseUrl+ "<?php echo Configure::read('App.imageBaseUrl'); ?>";
        </script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-capture" id="<?php echo $controller . '_' . $action; ?>">
        <div class="form-box" id="login-box">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?> 
        </div>
        <?php echo Configure::read('Google.Analyticstracking')['admin']; ?>
    </body>
    <?php       
               
        echo $this->Html->script('jquery-ui.min.js');
        echo $this->Html->script('plugins/lightbox/jquery.lightbox-0.5.js');
        echo $this->Html->script('AdminLTE/app.js');        
        foreach ($moreScript as $script) {
            echo $this->Html->script($script);
        } 
        echo $this->Html->script('bootstrap-toggle.js');
        echo $this->Html->script('common.js');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');  
    ?>
</html>