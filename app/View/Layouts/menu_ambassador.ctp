<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->

        <a href="<?php echo($this->Html->url('/')); ?>" ><?php echo $this->Html->image('logo-noascii.png'); ?></a>
        <ul class="sidebar-menu">
            <li class="">
                <a href="<?php echo($this->Html->url('/')); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo __('Dashboard')?></span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i> <span><?php echo __('Statistics page')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo($this->Html->url('/statistics/newsfeeds'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('News feed')?></span></a>
                    </li>
                    <li>
                        <a href="<?php echo($this->Html->url('/statistics/newscommentlike'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Clap')?></span></a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php if ($controller=='newsfeeds' && $action !=="import")  echo "active"?>">
                <a href="<?php echo($this->Html->url('/newsfeeds'))?>">
                    <i class="fa fa-book"></i> <span><?php echo __('News Feed Manager')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if (empty($this->request->query['news_site_id'])) echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('All')?></span></a>
                    </li>
                    <li<?php if (empty($this->request->query['news_site_id'])) echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds?appli=all'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo '【appli】全ての記事'?></span></a>
                    </li>
                    <li<?php if (empty($this->request->query['news_site_id'])) echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/newsfeeds?appli=hot'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo '【appli】人気記事'?></span></a>
                    </li>
                    <?php if (!empty($sitelist)): ?>
                        <?php foreach ($sitelist as $site): ?>
                            <?php if (!empty($site['feed_count'])): ?>
                                <li <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id']==$site['id']) echo "class=\"active\""?>>
                                    <a href="<?php echo($this->Html->url('/newsfeeds?news_site_id=' . $site['id'])) ?>">
                                        <i class="fa fa-angle-double-right"></i>
                                        <span><?php echo $this->common->truncate($site['name'], 10) ?></span>
                                        <small class="badge-custom pull-right bg-red"><?php echo ($site['feed_count'] >= 1000)? '999+' : $site['feed_count'] ?></small>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </li>
            <li class="<?php if ($controller=='newsfeeds' && $action =="import") echo "active"?>">
                <a href="<?php echo($this->Html->url('/newsfeeds/import'))?>">
                    <i class="fa fa-bookmark-o"></i> <span><?php echo __('Edit recommend Articles added')?></span>
                </a>
            </li>
            <li class="treeview <?php if ($controller=='admins') echo "active"?>">           
                <a href="#">
                    <i class="fa fa-cog"></i> <span><?php echo __('Setting page') ?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">                  
                    <li <?php if ($action == 'password') echo "class=\"active\"" ?>>
                        <a href="<?php echo($this->Html->url('/admins/password')) ?>">
                            <i class="fa fa-angle-double-right"> </i><span><?php echo __('Change password') ?></span>
                        </a>                
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
