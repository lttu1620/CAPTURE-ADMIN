<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <?php
                echo $this->SimpleForm->render($updateForm);
                ?>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="other-header">
                    <div class="other-logo">
                        <a href="<?php echo($this->Html->url('/')); ?>">
                            <?php echo $this->Html->image('logo-noascii.png'); ?>
                        </a>
                    </div>
                    <div class="other-logo-text">
                        <?php echo $this->Html->image('logo-ascii.png'); ?>
                    </div>
                    <div class="other-left-text">オリジナル</div>
                </div>
                <div class="other-body">
                    <div class="other-title"></div>
                    <div class="other-image"><img id="load-image" src="#" alt=""></div>
                    <div class="other-content"></div>
                </div>
                <div class="other-footer">
                    copyright (c) 2015 Capture
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function (){
        var image = '';
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    image = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#image_url").change(function(){
            readURL(this);
        });
        if (image === '') {
            image = $('.js-thumb').attr('href');
        }
        $(".preview").click(function(){
            var editFrame = document.getElementsByClassName('cke_wysiwyg_frame');
            $('.other-title').html($('#post_title').val());
            $('#load-image').attr('src', image);
            $('.other-content').html($(editFrame).contents().find("body").html());
            $('div#myModal div.modal-dialog')
                .css('width', '1024')
                .css('overflow-y', 'initial !important');
            $('div#myModal').modal('show');
            $('div#myModal div.modal-backdrop').css('z-index', '0');
            $('div#myModal div.other-body').css('min-height', '400px');
        });
    });

</script>