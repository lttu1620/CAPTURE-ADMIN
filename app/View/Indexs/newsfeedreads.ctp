<div class="row">
    <div class="col-xs-120">  
        <div class="box box-primary">  
            <div data-original-title="Header tooltip" title="" data-toggle="tooltip" class="box-header">
                <h3 class="box-title"><?php echo __('Search') ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-primary btn-xs search-collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div>  
            <div class="box-body search-body">          
                <?php
                echo $this->SimpleForm->render($searchForm);
                ?>                              
            </div>   
        </div>
    </div>
</div>
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">               
                <li class="active"><a href="#chart-result" data-toggle="tab" id="tabChart">Line Chart</a></li>
            </ul>
            <div class=" no-padding">
                <div class="chart" id="chart-result" style="min-height:500px;"></div>               
            </div>
        </div><!-- /.nav-tabs-custom -->
    </section>
</div>
<script type="text/javascript">
    $(function () {
        var _data = <?php echo $data; ?>;
        var _xkey = <?php echo $xkey; ?>;
        var _ykeys = <?php echo $ykeys; ?>;
        var _labels = <?php echo $labels; ?>;
        var _type =<?php echo $type; ?>;
        var _lineColors = ['#00a65a', '#f56954', 'yellow'];
        _ykeys = eval(_ykeys);
        _labels = eval(_labels);
        $('#tabChart').text($('#type option:selected').text());
        DrawChart('chart-result', _data, _xkey, _ykeys, _labels, _lineColors, _type);
    });
</script>