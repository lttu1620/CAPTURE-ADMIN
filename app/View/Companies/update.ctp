<div class="mailbox row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <?php if (!empty($profileTab)) : ?> 
                    <div class="col-md-3 col-sm-4">                        
                        <?php echo $profileTab; ?>                          
                    </div>
                    <?php endif; ?>
                    
                    <div class="col-md-9 col-sm-8">
                        <div class="box box-primary">  
                            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                                <h3 class="box-title"><?php echo __('Common information company') ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                                </div>
                            </div> 
                            <div class="box-body" >                
                                <?php
                                    echo $this->SimpleForm->render($commonForm);
                                ?>
                            </div>
                        </div>
                        
                        <?php if (!empty($introWhatForm)) : ?>                            
                        <div class="box box-primary">  
                            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                                <h3 class="box-title"><?php echo __('Introduction what') ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                                </div>
                            </div>
                            <div class="box-body" >                
                                <?php
                                    echo $this->SimpleForm->render($introWhatForm);
                                ?>
                            </div>
                        </div>                               
                        <?php endif; ?>
                        
                        <?php if (!empty($introWhyForm)) : ?>                      
                        <div class="box box-primary"> 
                            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                                <h3 class="box-title"><?php echo __('Introduction why') ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                                </div>
                            </div>
                            <div class="box-body" >                
                                <?php
                                    echo $this->SimpleForm->render($introWhyForm);
                                ?>
                            </div>
                        </div>                           
                        <?php endif; ?>
                        
                        <?php if (!empty($introHowForm)) : ?>                                   
                        <div class="box box-primary">  
                            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                                <h3 class="box-title"><?php echo __('Introduction how') ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                                </div>
                            </div>
                            <div class="box-body" >                
                                <?php
                                    echo $this->SimpleForm->render($introHowForm);
                                ?>
                            </div>
                        </div>
                       <?php endif; ?>
                        
                        
                        <?php if (!empty($introlikethisForm)) : ?>
                        <div class="box box-primary">  
                            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                                <h3 class="box-title"><?php echo __('Introduction likethis') ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                                </div>
                            </div>
                            <div class="box-body" >                
                                <?php
                                    echo $this->SimpleForm->render($introlikethisForm);
                                ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        
                        <?php if (!empty($otherForm)) : ?>
                        <div class="box box-primary">  
                            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                                <h3 class="box-title"><?php echo __('Other information') ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                                </div>
                            </div>
                            <div class="box-body" >                
                                <?php
                                    echo $this->SimpleForm->render($otherForm);
                                ?>
                            </div>
                        </div>
                        <?php endif; ?>                        
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>