<!-- Main row -->
<div class="row">
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-12">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title alignC textBlue">
                    <?php echo(!empty($company_info['kana']) ? $company_info['kana'] : '' ) . " | " . (!empty($company_info['name']) ? $company_info['name'] : ''); ?>
                </h3>
            </div>
            <div class="box-body rssnews-feeds clearfix">
                <!-- RSS News Feeds -->
                <div class="news-feed-detail">
                    <div class="row top-img">
                        <div class="pull-left mT5">
                            <?php if (!empty($company_info['establishment_date'])) : ?>
                                <span class="date-time"><i class="fa fa-calendar"></i>
                                    <?php echo!empty($company_info['establishment_date']) ? $this->common->dateFormat($company_info['establishment_date']) : ''; ?>
                                </span>
                            <?php endif ?>

                            <?php if (!empty($company_info['category_name'])) : ?>
                                <span class="press"><i class="fa fa-book"></i>
                                    <?php echo!empty($company_info['category_name']) ? $company_info['category_name'] : '' ?>
                                </span>
                            <?php endif ?>
                        </div>
                        <div class="pull-right comment-like-favourite">
                            <span class="co-newsfeedfavorites">
                                <?php echo __('Number of employee') ?>: 
                                </i><?php echo!empty($company_info['employees']) ? $company_info['employees'] : 0 ?>
                            </span>
                        </div>
                    </div>
                    <div class="imgs"><img src="<?php echo $this->common->thumb(!empty($company_info['thumbnail_img']) ? $company_info['thumbnail_img'] : '', null, 'company') ?>" alt=""></div>
                    <div class="news-feeds-preview-detail">
                        <p>
                            <?php if (!empty($company_info['description'])) {
                                echo $company_info['description']; ?>...
                                <a href="<?php echo $this->Html->url('/users/companyinformation') ?>" class="viewmore">続きを読む</a>
                            <?php } ?>
                        </p>
                    </div>
                    <!-- Chat box -->
                    <div class="box-success">
                        <div class="box-body chat full" id="chat-box">
                            <!-- chat item -->
                            <?php if (!empty($followcompanies)): ?>
    <?php foreach ($followcompanies as $item): ?>
                                    <div class="item news-feed-item-detail">
                                        <img src="<?php echo $this->Common->thumb($item['image_url'], '60x60'); ?>" alt="user image" class="online"/>
                                        <p class="message">
                                            <b><?php echo (isset($item['display_username'])? $item['display_username']:'')?> </b> <br>
                                            <i><?php echo $this->Common->dateFormat($item['created']) ?></i>
                                        </p>
                                    </div><!-- /.item -->
                                <?php endforeach; ?>
<?php endif; ?>
                        </div><!-- /.chat -->
                    </div><!-- /.box (chat box) -->
                    <!-- paginations -->
                    <div class="clearfix">
                        <?php
                        echo $this->Paginate->render($total, $limit);
                        ?>                
                    </div>
                </div>
                <!-- /.RSS News Feeds -->
            </div>
        </div>
    </section><!-- right col -->
</div><!-- /.row (main row) -->


