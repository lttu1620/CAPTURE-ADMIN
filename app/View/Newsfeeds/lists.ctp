<?php
    $this->layout = 'template';
 ?>
<div class="row">
    <section class="col-md-3">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">編集部おすすめ</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <ul class="list-group">
                    <a href="<?php echo($this->Html->url('/newsfeeds/lists?tag_id=all')) ?>" class="list-group-item <?php if (isset($this->request->query['tag_id']) && $this->request->query['tag_id'] == 'all') echo 'active'; ?>">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="badge bg-capture-red"></span>
                        <?php echo __('ALL') ?>
                    </a>
                    <?php foreach ($tagList as $tag): ?>
                        <?php if (!empty($tag['feed_count'])): ?>
                            <a href="<?php echo($this->Html->url('/newsfeeds/lists?tag_id=' . $tag['id'])) ?>" class="list-group-item <?php if (isset($this->request->query['tag_id']) && $this->request->query['tag_id'] == $tag['id']) echo 'active'; ?>">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="badge bg-capture-red"><?php echo ($tag['feed_count'] >= 1000) ? '999+' : $tag['feed_count'] ?></span>
                                <?php echo $this->common->truncate($tag['name'], 10) ?></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <a href="<?php echo($this->Html->url('/newsfeeds/lists?news_site_id=0')) ?>" class="list-group-item <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id'] == '0') echo 'active'; ?>">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="badge bg-capture-red"><?php echo !empty($count_feed_manual) ? $count_feed_manual : 0;?></span>
                        その他おすすめ記事
                    </a>
                    <a href="<?php echo($this->Html->url('/newsfeeds/lists?original=1')) ?>" class="list-group-item <?php if (isset($this->request->query['original']) && $this->request->query['original'] == '1') echo 'active'; ?>">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="badge bg-capture-red"><?php echo !empty($count_feed_original) ? $count_feed_original : 0;?></span>
                        オリジナル記事
                    </a>
                </ul>
            </div><!-- /.box-body -->
        </div>
        
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">コメントのある記事</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <ul class="list-group">
                    <a href="<?php echo($this->Html->url('/newsfeeds/lists?appli=all')) ?>" class="list-group-item <?php if (isset($this->request->query['appli']) && $this->request->query['appli'] == 'all') echo 'active'; ?>">
                        <i class="fa fa-angle-double-right"></i>
                        全ての記事
                    </a>
                    <a href="<?php echo($this->Html->url('/newsfeeds/lists?appli=hot')) ?>" class="list-group-item <?php if (isset($this->request->query['appli']) && $this->request->query['appli'] == 'hot') echo 'active'; ?>">
                        <i class="fa fa-angle-double-right"></i>
                        人気記事
                    </a>
                </ul>
            </div><!-- /.box-body -->
        </div>
        
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">媒体RSS</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <ul class="list-group">

                    <a href="<?php echo($this->Html->url('/newsfeeds/lists?news_site_id=all')) ?>" class="list-group-item <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id'] == 'all') echo 'active'; ?>">
                        <span class="badge bg-capture-red"></span><i class="fa fa-angle-double-right"></i> <?php echo __('All') ?></a>

                    <?php if (!empty($sitelist)): ?>
                    <?php foreach ($sitelist as $site): ?>
                        <?php if (!empty($site['feed_count'])): ?>
                            <a href="<?php echo($this->Html->url('/newsfeeds/lists?news_site_id=' . $site['id'])) ?>" class="list-group-item <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id'] == $site['id']) echo 'active'; ?>">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="badge bg-capture-red"><?php echo ($site['feed_count'] >= 1000) ? '999+' : $site['feed_count'] ?></span>
                                <?php echo $this->common->truncate($site['name'], 10) ?></a>                
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div><!-- /.box-body -->
        </div>
    </section>

    <section class="col-md-9">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title">ニュース一覧 </h3>
                <div class="box-tools">
                    <?php
                    echo $this->Form->Create("Search", array('type' => 'get'));
                    ?>
                    <div class="input-group">
                        <?php
                        Echo $this->Form->input('title', array(
                            'class' => 'form-control input-sm pull-right',
                            'div' => FALSE,
                            'label' => FALSE,
                            'placeholder' => __('Search'),
                            'style' => 'width: 150px;height:30px',
                            'value' => $title
                        ));
                        ?>
                        <div class="input-group-btn"> 
                            <?php
                            echo $this->Form->button("<i class=\"fa fa-search\" ></i>", array(
                                'type' => 'submit',
                                'id' => 'btnSearch',
                                'class' => 'btn btn-sm btn-default',
                                'escape' => false));
                            ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body rssnews-feeds clearfix">              
                <!-- RSS News Feeds -->
                <?php
                foreach ($data as $item) {
                    echo $this->Item->newsFeedItem($item);
                }
                ?>
                <!-- /.RSS News Feeds -->
            </div><!-- /.box-body -->
            <div class="box-footer clearfix">
                <?php
                echo $this->Paginate->render($total, $limit);
                ?>                
            </div>
        </div><!-- /.box -->
    </section>
</div>

<script>
    viewMore();
    function viewMore() {
        $.each($('.shortContent'), function (_index, _obj) {
            var _height = $(this).height();
            if (_height > 38)
            {
                $(this).next().removeClass('Hidden');
                $(this).height(38).css({'max-height': '38px', 'overflow': 'hidden'});
            } else
            {
                $(this).attr('style', '');
                $(this).next().addClass('Hidden');
            }           
        });
    }
</script>
