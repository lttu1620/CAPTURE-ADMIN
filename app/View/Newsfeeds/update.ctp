<div class="row">
    <div class="col-md-6">    
        <div class="box box-primary">   
            <div class="box-body">                
                <?php
                echo $this->SimpleForm->render($updateForm);
                ?>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('iCheck/all.css');
?>
<link rel="stylesheet" type="text/css" href="/adminlte/js/plugins/imageresize/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/adminlte/js/plugins/imageresize/css/demo.css" />





<script type="text/javascript">
    $(function () {
        $("#short_content").closest("div").after(<?php echo $htmlTags; ?>);

        $(".my-colorpicker2").colorpicker().on('changeColor.colorpicker', function (event) {
            $('.overlay-background').css('backgroundColor', event.color.toHex());
        });
        $("#txtColor").click(function () {
            $('div.input-group-addon').click();
        });
        $('#txtColor').change(function () {
            var color = $(this).val();
            if (color === '') {
                $('.overlay-background').css('backgroundColor', '');
            }
        });
    });
</script>

<?php

echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min.js');
echo $this->Html->script('plugins/iCheck/icheck.min.js');
?>