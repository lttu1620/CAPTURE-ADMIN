<style>
    .daterangepicker {
        width: 785px !important;
    }
    .daterangepicker .ranges{
        width: 280px
    }
    .daterangepicker .ranges .daterangepicker_start_input {
        width: 130px;
     }
     .daterangepicker .ranges .daterangepicker_end_input{
        width: 130px;
     }
     .daterangepicker .ranges .input-mini 
     {
        width: 120px !important;
     }
    .clsDatePicker{
        width: 150px !important;
    }
</style>
<?php 
    echo $this->Html->css('//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css');
?>

<div class="row">
    <div class="col-xs-120">
        <div class="box box-primary collapsed-box">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('Search') ?></h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs search-collapse" data-widget="collapse"><i
                            class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body search-body" style="display:none;">
                <?php
                echo $this->SimpleForm->render($searchForm);
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <?php
            echo $this->SimpleTable->render($table);
            echo $this->Paginate->render($total, $limit);
            ?>
        </div>
    </div>
</div>
<!-- Modal -->
<div>
    <div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only"><?php echo __('Close')?></span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo __('Set time schedule'); ?></h4>
                </div>
                <div class="modal-body">
                    <!-- Date and time range start_date-->
                    <div class="form-group col-md-6">
                        <label><?php echo __('Start date')?></label>
                        <div class='input-group date schedule' id='start_date'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
                    </div>
                    <!-- /.input group -->

                    <!-- Date and time range finish_date -->
                    <div class="form-group col-md-6">
                        <label><?php echo __('Finish date')?></label>

                        <div class='input-group date schedule' id='finish_date'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <!-- /.input group -->
                    <div class="cls"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
                    <button type="button" class="btn btn-primary " id="btnModalSave" ><?php echo __('Save'); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var _textStart = "<?php echo __('Start date');?>";
    var _textFinish ="<?php echo __('Finish date');?>";
</script>
<?php 
    //JS datetimepicker
    
    //js_schedule.js

    echo $this->Html->script('plugins/datetimepicker/moment.min.js');
    echo $this->Html->script('plugins/datetimepicker/moment-with-locales.js');
    echo $this->Html->script('plugins/datetimepicker/bootstrap-datetimepicker.min.js');
    /*echo $this->Html->script('//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js');*/
    echo $this->Html->script('js_schedule.js');
?>
