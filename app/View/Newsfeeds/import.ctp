
<div class="row">
    <div class="col-md-6">    
        <div class="box box-primary">   
            <div class="box-body">  
                <?php
                echo $this->Form->label('detail_url', __('New recommended news articles '), array('class' => 'margin'));
                echo $this->Form->Create($modelName, array('type' => 'post'));
                echo $this->Form->label('detail_url', __('Detail url'), array('class' => 'margin'));
                ?>
                <div class="input-group margin">
                    <?php
                    Echo $this->Form->input('detail_url', array(
                        'class' => 'form-control ',
                        'div' => FALSE,
                        'label' => FALSE,
                    ));
                    ?>
                    <span class="input-group-btn">
                        <?php
                        echo $this->Form->button(__('Go!'), array(
                            'type' => 'submit',
                            'id' => 'btnSearch',
                            'class' => 'btn btn-info btn-flat',
                            'style'=> "height:36px",
                            ));
                        ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>