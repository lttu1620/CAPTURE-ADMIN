<!-- Main row -->
<div class="row">
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-12">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title alignC textBlue">
                    <?php echo!empty($feed_info['title']) ? $feed_info['title'] : ''; ?>
                </h3>
            </div>
            <div class="box-body rssnews-feeds clearfix">
                <!-- RSS News Feeds -->
                <div class="news-feed-detail">
                    <div class="row top-img">
                        <div class="pull-left mT5">
                            <span class="date-time"><i class="fa fa-calendar"></i>
                                <?php echo !empty($feed_info['distribution_date']) ? $this->common->dateFormat($feed_info['distribution_date']) : ''; ?>
                            </span>
                            <span class="press"><i class="fa fa-book"></i>
                                <?php echo !empty($feed_info['site_name']) ? $feed_info['site_name'] : '' ?>
                            </span>
                        </div>
                        <div class="pull-right comment-like-favourite">
                            <a href="<?php echo($this->Html->url('/newsfeeds/view/' . $feed_info['id']))?>">
                            <span class="co-newsfeedfavorites">
                                <i class="fa fa-comment-o"></i>
                                <?php echo !empty($feed_info['comments_count']) ? $feed_info['comments_count'] : 0 ?>
                            </span>
                            </a>
                            <a href="<?php echo($this->Html->url('/newsfeeds/like/' . $feed_info['id']))?>" class="co-likes">
                                <i class="fa fa-likes"></i>
                                <?php echo !empty($feed_info['likes_count']) ? $feed_info['likes_count'] : 0 ?>
                            </a>
                            <a href="javascript:void(0);" class="co-favourites">
                                <i class="fa fa-favourites"></i>
                                <?php echo !empty($feed_info['favorite_count']) ? $feed_info['favorite_count'] : 0 ?>
                            </a>
                        </div>
                    </div>
                    <div class="imgs"><img src="<?php echo!empty($feed_info['image_url']) ? $feed_info['image_url'] : '' ?>" alt=""></div>
                    <div class="news-feeds-preview-detail">
                        <p><?php echo!empty($feed_info['short_content']) ? $feed_info['short_content'] : '' ?>...
                            <a href="<?php echo!empty($feed_info['detail_url']) ? $feed_info['detail_url'] : '' ?>" class="viewmore">続きを読む</a>
                        </p>
                    </div>

                    <!-- Chat box -->
                    <div class="box-success">
                        <div class="box-body chat full" id="chat-box">
                            <!-- chat item -->
                            <?php if (!empty($newsfeedfavorites)): ?>
                                <?php foreach ($newsfeedfavorites as $item): ?>
                            <div class="item news-feed-item-detail">
                                <img src="<?php
                                        echo $this->Common->thumb($item['image_url'], '60x60');?>" alt="user image" class="online"/>
                                <p class="message">
                                    <b><?php echo $item['display_username']?></b><br>
                                    <i><?php echo $this->Common->dateFormat($item['created'])?></i>
                                </p>
                            </div><!-- /.item -->
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div><!-- /.chat -->
                    </div><!-- /.box (chat box) -->
                    <!-- paginations -->
                    <div class="clearfix">
                        <?php
                        echo $this->Paginate->render($total, $limit);
                        ?>                
                    </div>
                </div>
                <!-- /.RSS News Feeds -->
            </div>
        </div>
    </section><!-- right col -->
</div><!-- /.row (main row) -->


