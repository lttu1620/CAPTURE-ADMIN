<?php

/**
 * 
 * ItemHelper Helper - render a item (news feed, comment, ...)
 * @package View.Helper
 * @created 2015-03-12
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class ItemHelper extends AppHelper {

    /** @var array $helpers Use helpers */
    public $helpers = array('Html');

    /**
     * Render a news feed item
     *     
     * @author thailvn   
     * @param array $item Item informaition        
     * @return string Html 
     */
    function newsFeedItem($item = array()) {
        return $this->fetch('newsfeeditem', array('item' => $item));
    }
     /**
     * Render a news feed item
     *     
     * @author thailvn   
     * @param array $item Item informaition        
     * @return string Html 
     */
    function mobileNewsFeedItem($item = array()) {
        return $this->fetch('mobile_newsfeeditem', array('item' => $item));
    }

    /**
     * Render a comment item
     *
     * @author thailvn
     * @param array $item Item informaition
     * @return string Html
     */
    function commentItem($item = array()) {
        return $this->fetch('commentitem', array('item' => $item));
    }

    /**
     * Render news comment item
     *
     * @author Le Tuan Tu
     * @param array $item News comment item
     * @return string Html
     */
    function newsCommentItem($item = array()) {
        return $this->fetch('mobile_news_comment_item', array('item' => $item));
    }
}
