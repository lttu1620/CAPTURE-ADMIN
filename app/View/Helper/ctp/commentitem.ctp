<div class="item news-feed-item-detail">
    <img src="<?php                                        
    $image = $item['display_image'];
    if ($item['is_company']) {
        $imageType = 'company';
    } else {
        $imageType = 'user';
    }
    echo $this->Common->thumb($image, '60x60', $imageType);?>" alt="user image" class="online"/>
    <span class="co-likes">
        <i class="fa fa-likes"></i>
        <?php echo $item['like_count'] ?>
    </span>
    <p class="message">
        <a href="#" class="name">
            <?php echo $item['nickname'] ?>
        </a>
        <span class="comment"><?php echo $item['comment'] ?></span>
        <span class="date"><?php echo $this->Common->dateFormat($item['created'])?></span>
    </p>
</div>