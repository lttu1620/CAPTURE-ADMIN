<div class="item">
    <div class="sp_news_comment_item">
        <a href="#">
            <?php echo $item['news_feed_title']; ?>
        </a>

        <div class="sp_news_comment_content">
            <?php echo $item['comment']; ?>
        </div>
        <div class="sp_news_comment_info">
        <span class="label label-primary sp_news_comment_like">
            拍手 <?php echo $item['like_count']; ?>
        </span>
        <span class="sp_news_comment_date">
            作成日：<?php echo $this->Common->dateFormat($item['created']) ?>
        </span>
        <span class="sp_news_comment_nickname">
            投稿者：<a href="#"><?php echo $item['nickname'] ?></a>
        </span>
        </div>
    </div>
</div>