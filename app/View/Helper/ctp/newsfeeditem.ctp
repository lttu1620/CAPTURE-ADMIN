<div class="row news-feed-items">
    <div class="news-feeds-image">
        <div class="imgs">
            <!--                                <a href="javascript:void(0);">-->
            <?php
            echo $this->Html->image($item['image_url'], array(
                "alt" => $item['site_name'],
                'url' => array('controller' => 'newsfeeds', 'action' => 'view', $item['news_feed_id'])
            ));
            ?>
            <!--                                </a>-->
        </div>
        <div class="comment-like-favourite"> 
            <a href="<?php echo($this->Html->url('/newsfeeds/view/' . $item['news_feed_id'])) ?>">
                <span class="co-comments">
                        <i class="fa fa-comment-o"></i>
                        <?php
                        echo !empty($item['comments_count']) ? $item['comments_count'] : 0;?>
                </span>
            </a>

            <a href="<?php echo($this->Html->url('/newsfeeds/like/' . $item['news_feed_id'])) ?>" class="co-likes">
                    <i class="fa fa-likes"></i>
                    <?php echo !empty($item['likes_count']) ? $item['likes_count'] : 0;?>
            </a>
            <a href="<?php echo($this->Html->url('/newsfeeds/favorite/' . $item['news_feed_id'])) ?>" class="co-favourites">
                    <i class="fa fa-favourites"></i>
                    <?php echo !empty($item['favorite_count']) ? $item['favorite_count'] : 0;?>
            </a>
        </div>
    </div>
    <div class="preview-content">
        <div class="preview-content-inner">
            <?php
            echo $this->Html->link($item['title'], array(
                'controller' => 'newsfeeds',
                'action' => 'view',
                $item['news_feed_id']
                ), array(
                'class' => 'linkDetail'
            ));
            ?> 
            <div class="bottom-nf-title">
                <span class="date-time"><i class="fa fa-calendar"></i>
                    <?php
                    //echo $this->Time->format($item['distribution_date'], '%Y.%m.%e %H:%M'); 
                    echo $this->Common->dateFormat($item['distribution_date']);
                    ?> 
                </span>
                <span class="press"><i class="fa fa-book"></i>
                    <?php echo $item['site_name'] ?>
                </span>
            </div>
            <table class=" table news-feeds-preview">
                <tr>
                    <td>
                        <div class="shortContent">
                            <?php
                            echo $item['short_content']
                            ?>
                        </div>
                        <?php
                        echo $this->Html->link('続きを読む', array(
                            'controller' => 'newsfeeds',
                            'action' => 'view',
                            $item['news_feed_id']
                            ), array(
                            'class' => 'viewmore Hidden',
                        ));
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="news-feeds-bottom pull-right">
            <span class="avatars-cmts"> 
                <span class="countusercmts">
                    <?php
                    $userCount = $item['listUser'][0];
                    echo $userCount . ' 人のユーザがコメントしています。';
                    ?>
                </span>                                                   
                <?php
                $lstUser = $item['listUser'][1];
                foreach ($lstUser as $user) {
                    echo $this->Html->image($user['display_image'], array(
                        "title" => $user['name'],
                    ));
                    echo '&nbsp;';
                }
                ?>
            </span>
            <a id="btnShare" href="<?php echo $this->Html->url('/newsfeeds/view/' . $item['news_feed_id'])?>" class="btn btn-success">
                <i class="fa fa-comment-o"></i> コメント
            </a>
            <button data-id="<?php echo $item['news_feed_id'];?>" class="quick-comment btn btn-warning<?php if($item['isCommented']==1):?> disabled<?php endif;?>">
                <i class="fa fa-comment"></i>読んでね！
            </button>
        </div>
    </div>
</div>
<?php unset($item) ?>