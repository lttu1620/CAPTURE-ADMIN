<?php

/**
 * Render table html
 * 
 * @package View.Helper
 * @created 2014-11-17
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class SimpleTableHelper extends AppHelper {

    /** @var array $helpers Use helpers */
    public $helpers = array('Form', 'Html', 'Common');

    /**
     * Create input text/textarea/select/file    
     * @param array $item   
     * @param int $option (0: normail; 1: generate dynamic input base on data type)  
     * @return string Html
     */
    function input($item, $option = 0) {
        $attr = array();
        if (!isset($item['name'])) {
            $item['name'] = $item['id'];
        }
        $attr[] = "name=\"{$item['name']}\"";
        if (in_array($item['type'], array('text', 'checkbox'))) {
            $attr[] = "value=\"{$item['value']}\"";
            if ($item['type'] == 'checkbox' && $item['value'] == 1 && $option == 1) {
                $attr[] = "checked=\"checked\"";
            }
        }
        if (in_array($item['type'], array('image', 'video'))) {
            $attr[] = "type=\"file\"";
        } else {
            $attr[] = "type=\"{$item['type']}\"";
        }
        foreach ($item as $k => $v) {
            if (is_string($v) && in_array($k, array('id', 'name', 'value', 'class', 'style', 'width', 'height', 'rows', 'cols'))) {
                $attr[] = "{$k}=\"{$v}\"";
            }
        }
        $attr = implode(' ', $attr);
        if ($item['type'] == 'text') {
            return "<div class=\"td_input_text\"><input {$attr}/></div>";
        } elseif ($item['type'] == 'textarea') {
            return "<div class=\"td_textarea\"><textarea {$attr}>{$item['value']}</textarea></div>";
        } elseif ($item['type'] == 'checkbox') {
            return "<div class=\"td_input_checkbox\"><input {$attr} /></div>";
        } elseif ($item['type'] == 'image') {
            $html = "<div class=\"td_file\"><input {$attr} /></div>";
            if (!empty($item['value'])) {
                $html .= "<div class=\"td_img\"><img style=\"margin-top:5px;width:100px;\" src=\"{$this->Common->thumb($item['value'], '100x100')}\" /></div>";
            }
            return $html;
        } elseif ($item['type'] == 'select') {
            $select = "<div class=\"td_select\"><select {$attr}>";
            foreach ($item['options'] as $optionVal => $optionTxt) {
                $selected = '';
                if ($optionVal == $item['value']) {
                    $selected = "selected=\"selected\"";
                }
                $select .= "<option {$selected} value=\"{$optionVal}\">{$optionTxt}</option>";
            }
            return "{$select}</select></div>";
        }
        return '';
    }

    /**
     * Render table html    
     * @author thailh 
     * @param array $table Table information   
     * @return string Html
     */
    function render($table) {
        //$controller = $this->request->params['controller'];
        $modelName = $table['modelName'];
        $columns = $table['columns'];
        $dataset = $table['dataset'];
        if (empty($columns)) {
            return false;
        }
        $html = "<div class=\"form-body\">";
        $html .= $this->Form->create($modelName, array(
            'class' => 'form-table',
            'enctype' => 'multipart/form-data',
            'id' => 'dataForm',
        ));
        $html .= "<table class=\"table table-hover\">";
        foreach ($columns as $i => $item) {
            if (empty($item['id'])) {
                $columns[$i]['id'] = 'ID' . time() . rand(1000, 9999);
            }
            if (empty($item['type'])) {
                $columns[$i]['type'] = '';
            }
            if (empty($item['title'])) {
                $columns[$i]['title'] = '';
            }
            if (empty($item['value'])) {
                $columns[$i]['value'] = '';
            }
            if ($columns[$i]['type'] == 'link' && !isset($item['href'])) {
                $columns[$i]['href'] = '#';
            }
            if (empty($item['link'])) {
                $columns[$i]['link'] = '';
            }
            if (!empty($item['href'])) {
                $columns[$i]['href'] = $this->Html->Url($item['href']);
            }
        }       
        
        $hidden = array();
        foreach ($columns as &$item) {
            $value = "";
            if ($item['type'] == 'hidden') {
                continue;
            }
            if ($item['type'] == 'checkbox' && empty($item['title'])) {
                $value .= "<th class=\"checkbox_{$item['id']}\"><input type=\"checkbox\" onclick=\"checkAll('{$item['name']}', this.checked ? 1 : 0)\" /></th>";
                $html .= $value;
                continue;
            }
            $options = array();
            $td_options = array();
            foreach ($item as $attrKey => $attrVal) {
                if ($attrKey == 'width') {
                    $options[] = "{$attrKey}=\"{$attrVal}\"";
                }
                if (in_array($attrKey, array('align'))) {
                    $td_options[] = "{$attrKey}=\"{$attrVal}\"";
                    unset($item[$attrKey]);
                }
            }
            $options = !empty($options) ? implode(' ', $options) : '';
            $item['td_options'] = !empty($td_options) ? implode(' ', $td_options) : '';
            $thTitle = $item['title'];
            if (isset($item['th_title'])) {
                $thTitle = $item['th_title'];
                unset($item['th_title']);
            }
            $value .= "<th {$options} {$item['td_options']}>{$thTitle}</th>";
            if (!empty($item['hidden'])) {
                $hidden[$item['id']] = true;
            } else {
                $html .= $value;
            }
        }       
        unset($item);
        $rows = array();      
        foreach ($dataset as $data) {
            $row = array();
            foreach ($columns as $item) {        
                if (isset($item['empty']) && empty($data[$item['id']])) {
                    $data[$item['id']] = $item['empty'];
                }
                $search = $replace = array();
                foreach ($data as $fld => $val) {
                    if (is_array($val)) {
                        continue;
                    }
                    $search[] = '{' . $fld . '}';
                    $replace[] = $val;
                }
                if ($item['type'] == 'hidden') {
                    continue;
                }
                $options = array();
                foreach ($item as $attrKey => $attrVal) {
                    if (!in_array($attrKey, array(
                                'id',
                                'title',
                                'type',
                                'link',
                                'rules',
                                'options',
                                'td_options',
                                'hidden',
                            ))) {
                        $attrVal = str_replace($search, $replace, $attrVal);
                        if (is_scalar($attrVal)) {
                            if ($attrKey == 'src') {
                                $attrVal = $this->Common->thumb($attrVal, '60x60');
                            }
                            if (!empty($attrVal)) {
                                $options[] = "{$attrKey}=\"{$attrVal}\"";
                            }
                        }
                    }
                }
                if (!empty($item['name'])) {
                    $item['name'] = str_replace($search, $replace, $item['name']);
                }
                if (!empty($item['value'])) {
                    $item['value'] = str_replace($search, $replace, $item['value']);
                }
                if (!isset($data[$item['id']])) {
                    $data[$item['id']] = !empty($item['empty']) ? $item['empty'] : $item['title'];
                }
                if (!empty($item['rules']) && is_array($item['rules'])) {
                    // support for setting, generate dynamic inputs     
                    foreach ($item['rules'] as $ruleKey => $ruleValue) {
                        if (is_array($ruleValue)) {
                            $ruleValue['name'] = $item['name'];
                            $ruleValue['value'] = $data[$item['id']];
                            $item['rules'][$ruleKey] = $this->input($ruleValue, 1);
                        }
                    }
                    $data[$item['id']] = str_replace(
                            array_keys($item['rules']), array_values($item['rules']), $data[!empty($item['value']) ? $item['value'] : $item['id']]
                    );
                }
                $value = $data[$item['id']];
                if ($item['type'] == 'url' && $value != '' && Validation::url($value)) {
                    $item['type'] = 'link';
                    $options['href'] = "href=\"{$value}\"";
                }
                $options = !empty($options) ? implode(' ', $options) : '';
                if ($item['type'] == 'link') {
                    if ($data[$item['id']] != '') {
                        if (isset($item['button'])) {
                            $btnValue = $data[$item['id']];
                            if (!empty($item['value'])) {
                                $btnValue = $item['value'];
                            }
                            $value = "<a {$options}><span class=\"label label-primary\">{$btnValue}</span></a>";
                        } else {
                            $value = "<a {$options}>{$data[$item['id']]}</a>";
                        }
                    }
                }
                if ($item['type'] == 'image') {
                    $value = "<img {$options} />";
                }
                if ($item['type'] == 'date') {
                    $value = self::getCommonComponent()->dateFormat($value);
                }
                if ($item['type'] == 'datetime') {
                    $value = self::getCommonComponent()->dateTimeFormat($value);
                }
                if (in_array($item['type'], array('text', 'checkbox', 'select', 'video'))) {
                    if ($item['type'] == 'checkbox' && !empty($item['toggle'])) {
                        if (!isset($data['id'])) {
                            $data['id'] = '0';
                        }
                        if (!isset($item['class'])) {
                            $item['class'] = "toggle-event";
                        }
                        $checked = $data[$item['id']];
                        $value = "<input value=\"{$data['id']}\" class=\"{$item['class']}\" {$checked} type=\"checkbox\" data-toggle=\"toggle\" data-onstyle=\"primary\" data-style=\"ios\" data-size=\"mini\">";
                    } else {
                        $value = $this->input($item);
                    }
                }
                $row[$item['id']] = array(
                    'options' => !empty($item['td_options']) ? $item['td_options'] : '',
                    'value' => $value
                );
            }
            $rows[] = $row;
        }

        foreach ($rows as $i => $row) {
            $html .= "<tr>";
            foreach ($row as $field => $rowItem) {               
                if (!empty($hidden[$field])) {
                    continue;
                }
                $value = $rowItem['value'];               
                if (isset($table['merges'][$field])) {
                    $value .= '<div>';
                    foreach ($table['merges'][$field] as $merge) {  
                        if (empty($row[$merge['field']]['value'])) continue;
                        $value .= '<div class="mt5 '. (!empty($merge['class'])? $merge['class']: '') .' ">';
                        
                        if (is_string($merge)) {
                            $value .= $row[$merge];
                        } else {
                            if (!empty($merge['before'])) {
                                $value .= $merge['before'];
                            }
                            $value .= $row[$merge['field']]['value'];
                            if (!empty($merge['after'])) {
                                $value .= $merge['after'];
                            }
                        }
                        $value .= '</div>';
                    }
                    $value .= '</div>';
                }
                $html .= "<td {$rowItem['options']}>{$value}</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</table>";
        unset($row);
        unset($rows);
        unset($hidden);
        if (!empty($table['buttons'])) {
            $html .= "<div class=\"form-group button-group\">";
            foreach ($table['buttons'] as $control) {
                if (empty($control['type']) || $control['type'] != 'submit') {
                    continue;
                }
                if (isset($control['type'])) {
                    unset($control['type']);
                }
                $html .= $this->Form->submit($control['value'], $control);
            }
            $html .= "</div>";
        }
        if (!empty($table['hiddens'])) {
            foreach ($table['hiddens'] as $control) {
                if (empty($control['name'])) {
                    $control['name'] = $control['id'];
                }
                if (empty($control['type'])) {
                    $control['type'] = 'hidden';
                }
                $html .= $this->Form->input($control['id'], $control);
            }
        }
        unset($table);
        $html .= $this->Form->input('action', array(
            'type' => 'hidden',
            'name' => 'action',
            'id' => 'action',
        ));
        $html .= $this->Form->input('action', array(
            'type' => 'hidden',
            'name' => 'actionId',
            'id' => 'actionId',
        ));
        $html .= $this->Form->end();
        $html .= "</div>";        
        return $html;
    }

}
