<!--
<div class="row">
    <section class="col-lg-12  ui-sortable">        
        <div class="nav-tabs-custom" style="cursor: move;">                
            <?php if (!empty($profileTab)) : ?>
                <?php echo $profileTab; ?>  
            <?php endif ?>
        </div>
    </section>
    <div class="col-md-6">    
        <div class="box box-primary">   
            <div class="box-body">                
                <?php
                echo $this->SimpleForm->render($updateForm);
                ?>
            </div>
        </div>
    </div>
</div>
-->

<div class="mailbox row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-4">                        
                        <?php echo $profileTab; ?>                          
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="box box-primary">  
                            <div class="box-body"> 
                            <?php        
                                echo $this->SimpleForm->render($updateForm);
                            ?>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
