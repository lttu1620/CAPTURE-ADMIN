<?php 
echo $this->Html->css('imgareaselect.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('iCheck/all.css');
//
?>
<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                    class="sr-only"><?php echo __('Close')?></span></button>
                    <h4 class="modal-title" id="myModalLabel">クロップ画像</h4>
                </div>
                <div class="modal-body">              
                    <div class="box-body">
                        <div class="row mr10">
                            <div class="col-md-9 col-sm-4">
                                <img src="<?php echo $imageUrl ?>" id="image" style="border:solid 1px;margin-bottom:10px">  
                                <div class='imageDescription '> 
                                    <span class='width'></span>
                                    <span> - </span>
                                    <span class='height'> </span>
                                </div>                                   
                                <div class="cls"></div>
                            </div>
                            <div class="col-md-3 col-sm-4">
                                <div class="row">
                                  <!-- checkbox -->
                                  <div class="form-group">
                                    <label>
                                      <input type="checkbox" class="minimal scale_image" />
                                      <?php echo __(' Use scale 16:9'); ?>
                                  </label>
                              </div>
                          </div>
                          <div class="row">
                             <div class="form-group">
                                <label>
                                <input type="checkbox" class="minimal background_color" />
                                <?php echo __('Background color: ');?>
                                </label>

                                <div class="input-group my-colorpicker2">
                                  <input type="text" class="form-control" id="txtColor"/>
                                  <div class="input-group-addon">
                                    <i></i>
                                </div>
                            </div><!-- /.input group -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="cls"></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
        <button type="button" class="btn btn-primary " id="btncrop" ><?php echo __('Save'); ?></button>
    </div>
</div>
</div>
</div> 
<?php
echo $this->Html->script('plugins/imgareaselect/jquery.imgareaselect.pack.js');
echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min.js');
echo $this->Html->script('plugins/iCheck/icheck.min.js');
echo $this->Html->script('js_crop-image.js');
 //
?>     
<script type="text/javascript">
    var _textW  = "<?php echo __('Width')?>",
    _textH      = "<?php echo __('Height')?>",
    _msg1       = "<?php echo __('Please choose image area to crop'); ?>",
    _msg2       = "<?php echo __('Please choose background color'); ?>"
    _info       =  "<?php echo $info ?>";
</script>
