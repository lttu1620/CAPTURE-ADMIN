<div class="other-header">
    <div class="other-logo">
<!--        <a href="<?php echo($this->Html->url('/')); ?>"> -->
            <?php echo $this->Html->image('logo-noascii.png'); ?>
<!--        </a>-->
    </div>
    <div class="other-logo-text">
        <?php echo $this->Html->image('logo-ascii.png'); ?>
    </div>
    <div class="other-left-text">オリジナル</div>
</div>
<div class="other-body">
    <div class="other-title"><?php echo !empty($data['post_title']) ? $data['post_title'] : ''?></div>
    <div class="other-image"><?php echo !empty($data['image_url']) ? $this->Html->image($data['image_url']) : "<img alt='No image'>"; ?></div>
    <div class="other-content"><?php echo !empty($data['post_content']) ? $data['post_content'] : '' ?></div>
</div>
<div class="other-footer">
    copyright (c) 2015 Capture
</div>

