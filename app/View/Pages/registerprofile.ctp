
<div class="header"><?php echo __('Update profile')?></div>
    <div class="body bg-gray">
        <p>初めてのご利用にはユーザー登録が必要になります。</p>
       <?php 
			echo $this->SimpleForm->render($updateForm); 
		?>
    </div>
</div> 
<!--
<div class="margin text-center">
    <span><?php echo __('Register using social networks')?></span>
    <br/>
    <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
    <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
    <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>
</div>-->