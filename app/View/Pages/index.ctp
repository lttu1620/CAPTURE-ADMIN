<!-- Main content -->
<h2>
	総会員数 <small>[<?php echo date('Y-m-d H:i'); ?>]
	</small>
</h2>
<ol class="breadcrumb">
	<li>
		<a href="#"><i class="fa fa-user"></i> 会員</a>
	</li>
	<li class="active">総会員数</li>
</ol>

<div class="col-lg-6 col-xs-12">
	<!-- users number -->
	<div class="small-box bg-yellow">
		<div class="inner">
			<h3>
				<?php echo $data['user_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在のアプリ会員数</p>
		</div>
		<div class="icon">
			<i class="ion ion-person"></i>
		</div>
		<div class="small-box-footer">アプリ合計数</div>
	</div>
</div>

<div class="col-lg-3 col-xs-6">
	<!-- ios users number -->
	<div class="small-box bg-blue">
		<div class="inner">
			<h3>
				<?php echo $data['user_ios_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在のiPhoneアプリ会員数</p>
		</div>
		<div class="icon">
			<i class="fa fa-apple"></i>
		</div>
		<div class="small-box-footer"><?php echo __('iPhone')?></div>
	</div>
</div>

<div class="col-lg-3 col-xs-6">
	<!-- android users number -->
	<div class="small-box bg-green">
		<div class="inner">
			<h3>
				<?php echo $data['user_android_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在のAndroidアプリ会員数</p>
		</div>
		<div class="icon">
			<i class="fa fa-android"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Android')?></div>
	</div>
</div>

<div class="col-lg-3 col-xs-6">	
	<div class="small-box bg-teal">        
		<div class="inner">
			<h3>
				<?php echo !empty($data['user_study_year_1']) ? $data['user_study_year_1'] : 0 ?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在の1年生会員数</p>
		</div>
		<div class="icon">
			<i class="ion ion-person"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Captures')?></div>
	</div>      
</div>

<div class="col-lg-3 col-xs-6">	
	<div class="small-box bg-teal">        
		<div class="inner">
			<h3>
				<?php echo !empty($data['user_study_year_2']) ? $data['user_study_year_2'] : 0 ?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在の2年生会員数</p>
		</div>
		<div class="icon">
			<i class="ion ion-person"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Captures')?></div>
	</div>      
</div>

<div class="col-lg-3 col-xs-6">	
	<div class="small-box bg-teal">        
		<div class="inner">
			<h3>
				<?php echo !empty($data['user_study_year_3']) ? $data['user_study_year_3'] : 0 ?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在の3年生会員数</p>
		</div>
		<div class="icon">
			<i class="ion ion-person"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Captures')?></div>
	</div>      
</div>

<div class="col-lg-3 col-xs-6">	
	<div class="small-box bg-teal">        
		<div class="inner">
			<h3>
				<?php echo !empty($data['user_study_year_4']) ? $data['user_study_year_4'] : 0 ?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在の4年生会員数</p>
		</div>
		<div class="icon">
			<i class="ion ion-person"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Captures')?></div>
	</div>      
</div>

<div class="col-lg-3 col-xs-6">
	<!-- men users number -->
	<div class="small-box bg-aqua">
		<div class="inner">
			<h3>
				<?php echo $data['user_male_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在のアプリ男性会員数</p>
		</div>
		<div class="icon">
			<i class="fa fa-male"></i>
		</div>
		<div class="small-box-footer">男性ユーザ</div>
	</div>
</div>

<div class="col-lg-3 col-xs-6">
	<!-- female users number -->
	<div class="small-box bg-red">
		<div class="inner">
			<h3>
				<?php echo $data['user_female_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在のアプリ女性会員数</p>
		</div>
		<div class="icon">
			<i class="fa fa-female"></i>
		</div>
		<div class="small-box-footer">女性ユーザ</div>
	</div>
</div>

<div class="col-lg-3 col-xs-6">	
	<div class="small-box bg-maroon">        
		<div class="inner">
			<h3>
				<?php echo !empty($data['user_department_humanities_count']) ? $data['user_department_humanities_count'] : 0 ?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在の文系学部会員数</p>
		</div>
		<div class="icon">
			<i class="ion ion-person"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Captures')?></div>
	</div>      
</div>

<div class="col-lg-3 col-xs-6">	
	<div class="small-box bg-maroon">        
		<div class="inner">
			<h3>
				<?php echo !empty($data['user_department_sciences_count']) ? $data['user_department_sciences_count'] : 0 ?>&nbsp;<sup style="font-size: 20px">人</sup>
			</h3>
			<p>現在の理系学部学生数</p>
		</div>
		<div class="icon">
			<i class="ion ion-person"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Captures')?></div>
	</div>      
</div>

<div class="col-lg-3 col-xs-6">
	<!-- total clap number -->
	<div class="small-box bg-purple">
		<div class="inner">
			<h3>
				<?php echo $data['news_comment_like_count']?>&nbsp;<sup style="font-size: 20px">回</sup>
			</h3>
			<p>現在の総クラップ数</p>
		</div>
		<div class="icon">
			<i class="fa fa-thumbs-o-up"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Claps')?></div>
	</div>
</div>
<div class="col-lg-3 col-xs-6">
	<!-- total capture number -->
	<div class="small-box bg-blue">
		<div class="inner">
			<h3>
				<?php echo $data['news_feed_favorite_count']?>&nbsp;<sup style="font-size: 20px">回</sup>
			</h3>
			<p>現在の総キャプチャー数</p>
		</div>
		<div class="icon">
			<i class="fa fa-paperclip"></i>
		</div>
		<div class="small-box-footer"><?php echo __('Captures')?></div>
	</div>
</div>

<?php // echo $this->render("/Helper/ctp/test", 'ajax'); ?>