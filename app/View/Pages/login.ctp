<?php
     $this->layout = 'blank';
    echo $this->Html->image('logo.png');
?>
<br />
<div class="header"><?php echo __('Sign In') ?></div>

<div class="body bg-gray">
    <?php 
		if($isAdmin == true){
			echo $this->SimpleForm->render($adminLoginForm); 
		}else{
			echo $this->SimpleForm->render($userLoginForm);
		}
    ?>
</div>

<?php if (!$isAdmin) : ?>
<div class="footer">
    <a href="<?php echo $this->html->url('/forgetpassword'); ?>" class="text-center"><i class="fa fa-exclamation-circle"></i>&nbsp;<?php echo __('if you Forget password.')?></a><br />
    <a href="<?php echo $this->html->url('/recruiter-policy.html'); ?>" class="text-center" target="_blank"><i class="fa fa-file-text"></i>&nbsp;<?php echo __('if you want to read policy.')?></a>
</div>
<?php endif ?>

<?php if($isAdmin == false): ?>
<div class="margin text-center">
    <span>Login with Facebook</span>
    <br/>
    <button class="btn bg-blue btn-circle" id="fblogin"><i class="fa fa-facebook"></i></button>
</div>
<?php endif;?>


<!--
<div class="margin text-center">
    <span>Sign in using social networks</span>
    <br/>
    <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
    <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
    <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>
</div>
-->
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
    var fb_app_id = '<?php echo Configure::read('Facebook.appId') ?>';
    $(function () {
        FB.init({
            appId: fb_app_id,
            cookie: true,
            status: true,
            oauth: true,
            xfbml: true
        });
    });
</script>