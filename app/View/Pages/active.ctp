<script>
    var iOSAppID = "<?php echo Configure::read('Config.iOSAppID'); ?>";
    var AndroidAppID = "<?php echo Configure::read('Config.AndroidAppID'); ?>";   
    if (navigator.appVersion.indexOf('iPhone') > -1) {
        link = iOSAppID + '://';
    } else if (navigator.userAgent.indexOf('Android') > -1) {
        link = "https://play.google.com/store/apps/details?id=co.jp.oceanize.capture";
    }   
    location.href = link;  
</script>
