<section class="sec_content_01">
    <div class="sec_inner">
         <div class="right">
            <h3><img src="img/cup.png" width="70"></h3>
            <h2>興味のある業界・業種の社会人が厳選したニュースをお届け！</h2>
            <p>興味がある・就職したい業界のカテゴリーを選択すると、その業界の社会人が選んだ必読ニュースや、Capture編集部のオリジナルコンテンツがあなただけのタイムラインに流れてきます！</p>
        </div>
        <div class="left">
            <!-- <img src="img/iPhone_white.png" width="336"> -->
            <img src="img/iphone_second.png" width="336">
        </div>
    </div>
</section>
 <section class="sec_content_02">
    <div class="sec_inner clearFix">
        <div class="left">
            <h3><img src="img/cup.png" width="70"></h3>
           <h2>様々な企業のコメント付きニュースを読んで、就活や将来に役立てよう！</h2>
            <p>社会人が選ぶのは、「この業界/業種を志望するならぜひ読んで欲しい！」「大学生のうちに読んだら将来のヒントになるでは？」というニュース記事が中心で、このアプリでしか読めないコメント付きで配信されます！</p>
        </div>
        <div class="right">
            <img src="img/iphone_third.png" width="300">
        </div>
</section>
<section class="dl_sec">
    <h2>Captureをダウンロードしてニュースを読む</h2>
    <div class="dl_btns">
            <a href=""><img src="img/appstore_link.png" width="189"></a>
            <a href=""><img src="img/googleplay_link.png" width="189"></a>
    </div>
</section>
<section class="news_area">
    <div class="sec_inner">
        <h2>サービスについてのお知らせ</h2>
        <dl>
            <dt>2015.01.15</dt>
            <dd>定期メンテナンスを12/29〜1/5に実施いたします。</dd>
        </dl>
        <dl>
            <dt>2015.01.15</dt>
            <dd>今回のアップデートにおいて、新機能「おすすめ業界のレコメンド機能」を追加しました。その他バグ修正も含まれています。</dd>
        </dl>
        <dl>
            <dt>2015.01.15</dt>
            <dd>ここにニュースのタイトルが入ります。</dd>
        </dl>
    </div>
</section>
