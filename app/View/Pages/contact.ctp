<?php 
/*
function h($str) {
    return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
}
 * 
 */
function e($msg, Exception &$previous = null) {
    return new RuntimeException($msg, 0, $previous);
}
function exception_to_array(Exception $e) {
    do {
        $msgs[] = $e->getMessage();
    } while ($e = $e->getPrevious());
    return array_reverse($msgs);
}
/* 変数の初期化 */
foreach (array('name', 'email', 'contents', 'token', 'confirm', 'execute') as $v) {
    $$v = trim(filter_input(INPUT_POST, $v));
}

/* セッションの初期化 */
session_name('ContactForm');
@session_start();
if (!isset($_SESSION['token'])) {
    $_SESSION['token'] = array();
}

/* 「確認」か「送信」のときのみ実行 */
if ($confirm or $execute) {
    try {
        // トークンをチェック
        if (!isset($_SESSION['token'][$token])) {
            throw e('フォームの有効期限が切れています。', $e);
        }
        // トークンを消費させる
        unset($_SESSION['token'][$token]);
        // 各項目チェック
        if ($name === '') {
            $e = e('名前を入力してください。', $e);
        }
        if ($email === '') {
            $e = e('メールアドレスを入力してください。', $e);
        } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $e = e('メールアドレスが不正です。', $e);
        }
        // 例外がここまでに1つでも発生していればスローする
        if (!empty($e)) {
            throw $e;
        }
        // 「送信」のとき
        $to = 'info@capture-news.jp';
        if (            
            $execute and
            !mb_internal_encoding('utf-8') ||
            !mail(
                $to,
                mb_encode_mimeheader('Capture公式サイト問い合わせフォームからの送信', 'ISO-2022-JP-MS'),
                mb_convert_encoding($contents, 'ISO-2022-JP-MS'),
                implode("\r\n", array(
                    'Content-Type: text/plain; charset=ISO-2022-JP',
                    'From: ' . mb_encode_mimeheader($name, 'ISO-2022-JP-MS')
                             . ' <' . $email . '>',
                )),
                '-f ' . $to
            )
        ) {
            throw e('メール送信でエラーが発生しました。', $e);
        }
    } catch (Exception $e) {
        // 最初の画面に戻す
        $confirm = $execute = '';
    }
}

/* 「最初」か「確認」のときのみ実行 */
if (!$execute) {
    // 値をダミーにしてトークンをキー部分に生成(最大10個まで保持)
    $_SESSION['token'] = array_slice(
        array($token = sha1(mt_rand()) => true) + $_SESSION['token'],
        0,
        10
    );
}
// ヘッダー送信
// header('Content-Type: application/xhtml+xml; charset=utf-8');
?>

<section class="sec_inner_contact">
    <h1 style="color:#1f489d; font-size:20px;">コンタクトフォーム</h1>
    <?php if (!empty($e)): ?>
        <div id="errmsg">
          <ul>
        <?php foreach (exception_to_array($e) as $msg): ?>
            <li><?=$msg?></li>
        <?php endforeach; ?>
          </ul>
        </div>
    <?php endif; ?>

    <?php if ($execute): ?>
        <pstyle="margin: 20px 0;">送信が完了しました。<br>ご意見・お問い合わせありがとうございました。<br /></p>
        <a href="index.html" style="font-size: 16px;">トップに戻る</a>
    <?php elseif ($confirm): ?>
        <p style="font-size: 20px; margin:20px 0;">この内容で送信しますか？</p>
        <p>
          <form action="" method="post">
            <table id="contact-form" border="1" cellpadding="0" cellspacing="0">
              <tr>
                <th>名前(必須)</th>
                <td><?=h($name)?></td>
              </tr>
              <tr>
                <th>メールアドレス(必須)</th>
                <td><?=h($email)?></td>
              </tr>
              <tr>
                <th>内容</th>
                <td><pre><?=
                  $contents !== '' ?
                  h($contents) :
                  '<span style="color:red;">【未入力】</span>'
                ?></pre></td>
              </tr>
              <tr>
                <th colspan="2">
                  <input type="hidden" name="name" value="<?=h($name)?>" />
                  <input type="hidden" name="email" value="<?=h($email)?>" />
                  <input type="hidden" name="contents" value="<?=h($contents)?>" />
                  <input type="hidden" name="token" value="<?=h($token)?>" />
                  <input type="submit" name="execute" value="送信" />
                  <input type="submit" value="修正" />
                </th>
              </tr>
            </table>
          </form>
        </p>
    <?php else: ?>
        <div class="contact_form">
          <form action="" method="post">
            <div><input type="hidden" name="token" value="<?=h($token)?>" /></div>
            <dl>
                <dt>名前</dt>
                <dd><input type="text" name="name" value="<?=h($name)?>" /></dd>
            </dl>
            <dl>
                <dt>メールアドレス</dt>
                <dd><input type="text" name="email" value="<?=h($email)?>" /></dd>
            </dl>
            <dl>
                <dt>内容</dt>
                <dd><textarea name="contents" rows="10"><?=h($contents)?></textarea></dd>
            </dl>
            <dl>
                <dd><input type="submit" name="confirm" value="確認" /></dd>
            </dl>
          </form>
        </div>
    <?php endif; ?>
</section>