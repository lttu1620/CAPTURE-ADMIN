<!-- Main row -->
<div class="row">
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-12">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title alignC textBlue">
                    <?php echo!empty($feed_info['title']) ? $feed_info['title'] : ''; ?>
                </h3>
            </div>
            <div class="box-body rssnews-feeds clearfix">
                <!-- RSS News Feeds -->
                <div class="news-feed-detail">
                    <div class="row top-img">
                        <div class="pull-left mT5">
                            <span class="date-time"><i class="fa fa-calendar"></i>
                                <?php echo !empty($feed_info['distribution_date']) ? $this->common->dateFormat($feed_info['distribution_date']) : ''; ?>
                            </span>
                            <span class="press"><i class="fa fa-book"></i>
                                <?php echo !empty($feed_info['site_name']) ? $feed_info['site_name'] : '' ?>
                            </span>
                        </div>
                        <div class="pull-right comment-like-favourite">
                            <span class="co-comments"><i class="fa fa-comment-o"></i>
                                <?php echo !empty($feed_info['comments_count']) ? $feed_info['comments_count'] : 0 ?>
                            </span>
                            <span class="co-likes"><i class="fa fa-likes"></i>
                                <?php echo !empty($feed_info['likes_count']) ? $feed_info['likes_count'] : 0 ?>
                            </span>
                            <span class="co-favourites"><i class="fa fa-favourites"></i>
                                <?php echo !empty($feed_info['favorite_count']) ? $feed_info['favorite_count'] : 0 ?>
                            </span>
                        </div>
                        <!--HTML For Tags-->
                        <?php echo $feed_info['htmlTags']; ?>
                    </div>
                    <div class="imgs"><img src="<?php echo!empty($feed_info['image_url']) ? $feed_info['image_url'] : '' ?>" alt=""></div>
                    <div class="news-feeds-preview-detail">
                        <p><?php echo !empty($feed_info['short_content']) ? $feed_info['short_content'] : '' ?>...
                            <a target="_blank" href="<?php echo!empty($feed_info['detail_url']) ? $feed_info['detail_url'] : '' ?>" class="viewmore">続きを読む</a>
                        </p>
                    </div>

                    <!-- Chat box -->
                    <div class="box-success">
                        <div class="box-body chat full" id="chat-box">
                        <?php
                        foreach ($comments as $item) {
                            echo $this->Item->commentItem($item);
                        }
                        ?>                            
                        </div><!-- /.chat -->
                    </div><!-- /.box (chat box) -->
                    <div style="height: 45px;">
                        <button data-id="<?php echo!empty($feed_info['id']) ? $feed_info['id'] : ''; ?>" class="quick-comment-reload btn btn-warning pull-right mL5 <?php if($feed_info['isCommented']==1):?> disabled<?php endif;?>">
                            <i class="fa fa-comment"></i>読んでね！
                        </button>
                    </div>
                    <!-- paginations -->
                    <div class="clearfix">
                    <?php
                        echo $this->Paginate->render($total, $limit);
                    ?>                
                    </div>
                    <!-- /.paginations -->
                    <?php if($is_admin == 0): //just comment if not admin?>
                    <!-- input comments -->
                    <form id="frm_feedcomment" method="post" name="frm_feedcomment" role="form" action="<?php echo $this->request->here;?>">
                        <div class="form-group aLeft">
                            <label>コメント</label>
                            <textarea id="txt_comment" name="txt_comment" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                        </div>
                        <div class="form-group aRight">
                            <button type="submit" class="btn btn-primary"><?php echo __('Submit') ?></button>
                        </div>
                    </form>
                    <!-- /.input comments -->
                    <?php endif;?>
                </div>
                <!-- /.RSS News Feeds -->
            </div>
        </div>
    </section><!-- right col -->
</div><!-- /.row (main row) -->


