
<div class="row" >
    <div class="col-md-3" id="page_conditions">
        
    </div>

    <div class="col-md-9">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title">ニュース一覧 </h3>
                <div class="box-tools">
                    <?php
                    echo $this->Form->Create("Search", array('type' => 'get'));
                    ?>
                    <div class="input-group">
                        <?php
                        Echo $this->Form->input('title', array(
                            'class' => 'form-control input-sm pull-right',
                            'div' => FALSE,
                            'label' => FALSE,
                            'placeholder' => __('Search'),
                            'style' => 'width: 150px;height:30px',
                            'value' => $title
                            ));
                            ?>
                            <div class="input-group-btn"> 
                                <?php
                                echo $this->Form->button("<i class=\"fa fa-search\" ></i>", array(
                                    'type' => 'submit',
                                    'id' => 'btnSearch',
                                    'class' => 'btn btn-sm btn-default',
                                    'escape' => false));
                                    ?>
                            </div>
                        </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body rssnews-feeds clearfix no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <!-- RSS News Feeds -->
                    <?php
                    foreach ($data as $item) {
                        echo $this->Item->mobileNewsFeedItem($item);
                    }
                    ?>
                    <!-- /.RSS News Feeds -->
                </ul>  
            </div><!-- /.box-body -->
            <div class="box-footer clearfix">
                <?php
                echo $this->Paginate->render($total, $limit, 5);
                ?>                
            </div>
        </div><!-- /.box -->
    </div>
</div>
<script type="text/javascript">
    $(function() {
        viewMore();
        // Check a has class 'active' => show ul content this
        setTimeout(function(){ 
                 $("#page_conditions .box-body ul li[class='active']").closest("div.box").find("div.box-header button").click();
            },100);
    });
    function viewMore() {
        $.each($('.shortContent'), function (_index, _obj) {
            var _height = $(this).height();
            if (_height > 38)
            {
                $(this).next().removeClass('Hidden');
                $(this).height(38).css({'max-height': '38px', 'overflow': 'hidden'});
            } else
            {
                $(this).attr('style', '');
                $(this).next().addClass('Hidden');
            }           
        });
    }
</script>
