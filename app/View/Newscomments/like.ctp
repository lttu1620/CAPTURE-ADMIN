<!-- Main row -->
<div class="row">
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-12">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title alignC textBlue">
                    <?php echo!empty($comment_info['news_feed_title']) ? $comment_info['news_feed_title'] : ''; ?>
                </h3>
            </div>
            <div class="box-body rssnews-feeds clearfix">
                <!-- RSS News Comments -->
                <div class="news-feed-detail">

                    <div class="row top-img">
                        <div class="pull-left mT5">
                            <span class="date-time"><i class="fa fa-calendar"></i>
                                <?php echo!empty($comment_info['created']) ? $this->common->dateFormat($comment_info['created']) : ''; ?>
                            </span>
                            <span class="press"><i class="fa fa-book"></i>
                                <?php echo!empty($comment_info['news_site_name']) ? $comment_info['news_site_name'] : '' ?>
                            </span>
                        </div>
                        <div class="pull-right feed-like-favourite">
                            <a href="javascript:void(0);" class="co-likes"><i class="fa fa-likes"></i>
                                <?php echo!empty($comment_info['like_count']) ? $comment_info['like_count'] : 0 ?>
                            </a>
                        </div>
                    </div>
                    <div class="news-feeds-preview-detail">
                        <p><?php echo!empty($comment_info['comment_short']) ? $comment_info['comment_short'] : '' ?>...
                            <a href="<?php echo!empty($comment_info['detail_url']) ? $comment_info['detail_url'] : '' ?>" class="viewmore">続きを読む</a>
                        </p>
                    </div>

                    <!-- Chat box -->
                    <div class="box-success">
                        <div class="box-body chat full" id="chat-box">
                            <!-- chat item -->
                            <?php if (!empty($newscommentlikes)): ?>
                                <?php foreach ($newscommentlikes as $item): ?>
                            <div class="item news-feed-item-detail">
                                <img src="<?php
                                        echo $this->Common->thumb($item['image_url'], '60x60');?>" alt="user image" class="online"/>
                                <div class="pull-right feed-like-favourite">
                                    <i class="fa fa-likes"></i>
                                    <?php echo!empty($item['like_count']) ? $item['like_count'] : 0 ?>
                                </div>          

                                <p class="message">
                                    <b><?php echo $item['display_username']?></b><br>
                                    <i><?php echo $this->Common->dateFormat($item['created'])?></i>
                                </p>
                            </div><!-- /.item -->
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div><!-- /.chat -->
                    </div><!-- /.box (chat box) -->
                    <!-- paginations -->
                    <?php if ($total > 0) : ?>
                        <!-- paginations -->
                        <div class="clearfix">
                            <?php
                            echo $this->Paginate->render($total, $limit);
                            ?>                
                        </div>
                    <?php endif ?>
                </div>
                <!-- /.RSS News Comments -->
            </div>
        </div>
    </section><!-- right col -->
</div><!-- /.row (main row) -->


