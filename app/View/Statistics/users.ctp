<!-- Main content -->       
<h2>
    会員数推移
</h2>
<ol class="breadcrumb">
    <li>
        <a href="#"><i class="fa fa-user"></i> 統計</a>
    </li>
    <li class="active">会員数推移</li>
</ol>

<div class="row">
    <div class="col-md-6">
        <!-- AREA CHART -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo __('Area Chart')?></h3>
            </div>
            <div class="box-body chart-responsive">
                <div class="chart" id="revenue-chart" style="height: 300px;">
                    <svg height="300" version="1.1" width="556" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                        <text x="54.8125" y="261.421875" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                            <tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text>
                        <path fill="none" stroke="#aaaaaa" d="M67.3125,261.421875H531" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                        <text x="54.8125" y="202.31640625" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                            <tspan dy="4.16015625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">7,500</tspan></text>
                        <path fill="none" stroke="#aaaaaa" d="M67.3125,202.31640625H531" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                        <text x="54.8125" y="143.2109375" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                            <tspan dy="4.15625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15,000</tspan></text>
                        <path fill="none" stroke="#aaaaaa" d="M67.3125,143.2109375H531" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                        <text x="54.8125" y="84.10546875" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                            <tspan dy="4.16015625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">22,500</tspan></text>
                        <path fill="none" stroke="#aaaaaa" d="M67.3125,84.10546875H531" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                        <text x="54.8125" y="25.00000000000003" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal">
                            <tspan dy="4.164062500000028" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30,000</tspan></text>
                        <path fill="none" stroke="#aaaaaa" d="M67.3125,25.00000000000003H531" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                        <text x="445.9248936816525" y="273.921875" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,6.7891)">
                            <tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan></text>
                        <text x="239.71635783718105" y="273.921875" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,6.7891)">
                            <tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan></text>
                        <path fill="#74a5c1" stroke="none" d="M67.3125,219.40182708333333C80.27095990279466,219.91407447916666,106.18787970838396,222.9767228515625,119.14633961117862,221.45081666666667C132.10479951397326,219.92491048177084,158.02171931956258,209.46474437329238,170.98017922235724,207.19457760416668C183.79778630012152,204.94908656079235,209.43300045565007,205.2037083984375,222.25060753341435,203.38818541666666C235.06821461117863,201.57266243489582,260.7034287667072,195.2166551905453,273.52103584447144,192.67039375000002C286.4794957472661,190.09615141450365,312.3964155528554,182.7987953776042,325.3548754556501,182.90617031250002C338.31333535844476,183.01354524739585,364.230255164034,204.5008754724499,377.1887150668287,193.52939322916666C390.00632214459296,182.67716622765823,415.6415363001215,102.08149905904696,428.45914337788577,95.61133333333333C441.13589763061964,89.2122683298803,466.4894061360875,135.33490646033655,479.16616038882137,142.05247031250002C492.124620291616,148.91931336137822,518.0415400972054,147.97483828125002,531,149.9489609375L531,261.421875L67.3125,261.421875Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path>
                        <path fill="none" stroke="#3c8dbc" d="M67.3125,219.40182708333333C80.27095990279466,219.91407447916666,106.18787970838396,222.9767228515625,119.14633961117862,221.45081666666667C132.10479951397326,219.92491048177084,158.02171931956258,209.46474437329238,170.98017922235724,207.19457760416668C183.79778630012152,204.94908656079235,209.43300045565007,205.2037083984375,222.25060753341435,203.38818541666666C235.06821461117863,201.57266243489582,260.7034287667072,195.2166551905453,273.52103584447144,192.67039375000002C286.4794957472661,190.09615141450365,312.3964155528554,182.7987953776042,325.3548754556501,182.90617031250002C338.31333535844476,183.01354524739585,364.230255164034,204.5008754724499,377.1887150668287,193.52939322916666C390.00632214459296,182.67716622765823,415.6415363001215,102.08149905904696,428.45914337788577,95.61133333333333C441.13589763061964,89.2122683298803,466.4894061360875,135.33490646033655,479.16616038882137,142.05247031250002C492.124620291616,148.91931336137822,518.0415400972054,147.97483828125002,531,149.9489609375" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                        <circle cx="67.3125" cy="219.40182708333333" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="119.14633961117862" cy="221.45081666666667" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="170.98017922235724" cy="207.19457760416668" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="222.25060753341435" cy="203.38818541666666" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="273.52103584447144" cy="192.67039375000002" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="325.3548754556501" cy="182.90617031250002" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="377.1887150668287" cy="193.52939322916666" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="428.45914337788577" cy="95.61133333333333" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="479.16616038882137" cy="142.05247031250002" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="531" cy="149.9489609375" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <path fill="#eaf2f5" stroke="none" d="M67.3125,240.41185104166667C80.27095990279466,240.191190625,106.18787970838396,241.74172408854167,119.14633961117862,239.529209375C132.10479951397326,237.3166946614583,158.02171931956258,223.6913122751594,170.98017922235724,222.71173333333334C183.79778630012152,221.74280198870105,209.43300045565007,233.60487122395836,222.25060753341435,231.73516822916667C235.06821461117863,229.86546523437502,260.7034287667072,209.6184939542065,273.52103584447144,207.754109375C286.4794957472661,205.86923705316485,312.3964155528554,214.77780924479165,325.3548754556501,216.738140625C338.31333535844476,218.69847200520834,364.230255164034,232.75018892019582,377.1887150668287,223.4367604166667C390.00632214459296,214.22456483165416,415.6415363001215,148.44710512704305,428.45914337788577,142.63564427083332C441.13589763061964,136.8880456218347,466.4894061360875,170.73042209964515,479.16616038882137,177.20052239583333C492.124620291616,183.8144026986035,518.0415400972054,190.52880559895834,531,194.97156666666666L531,261.421875L67.3125,261.421875Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path>
                        <path fill="none" stroke="#a0d0e0" d="M67.3125,240.41185104166667C80.27095990279466,240.191190625,106.18787970838396,241.74172408854167,119.14633961117862,239.529209375C132.10479951397326,237.3166946614583,158.02171931956258,223.6913122751594,170.98017922235724,222.71173333333334C183.79778630012152,221.74280198870105,209.43300045565007,233.60487122395836,222.25060753341435,231.73516822916667C235.06821461117863,229.86546523437502,260.7034287667072,209.6184939542065,273.52103584447144,207.754109375C286.4794957472661,205.86923705316485,312.3964155528554,214.77780924479165,325.3548754556501,216.738140625C338.31333535844476,218.69847200520834,364.230255164034,232.75018892019582,377.1887150668287,223.4367604166667C390.00632214459296,214.22456483165416,415.6415363001215,148.44710512704305,428.45914337788577,142.63564427083332C441.13589763061964,136.8880456218347,466.4894061360875,170.73042209964515,479.16616038882137,177.20052239583333C492.124620291616,183.8144026986035,518.0415400972054,190.52880559895834,531,194.97156666666666" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                        <circle cx="67.3125" cy="240.41185104166667" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="119.14633961117862" cy="239.529209375" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="170.98017922235724" cy="222.71173333333334" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="222.25060753341435" cy="231.73516822916667" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="273.52103584447144" cy="207.754109375" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="325.3548754556501" cy="216.738140625" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="377.1887150668287" cy="223.4367604166667" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="428.45914337788577" cy="142.63564427083332" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="479.16616038882137" cy="177.20052239583333" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                        <circle cx="531" cy="194.97156666666666" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 175.750607533414px; top: 135px; display: none;"><div class="morris-hover-row-label">2011 Q4</div><div class="morris-hover-point" style="color: #a0d0e0">
                    Item 1:
                    3,767
                </div>
                <div class="morris-hover-point" style="color: #3c8dbc">
                    Item 2:
                    3,597
                </div>
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
