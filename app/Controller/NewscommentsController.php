<?php

App::uses('AppController', 'Controller');

/**
 * NewscommentsController class of Newscomments Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class NewscommentsController extends AppController {

    /**
     * Initializes components for NewscommentsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Newscomments.
     * 
     * @return void
     */
    public function index() {
        include ('Newscomments/index.php');
    }

    /**
     * Handles user interaction of view update Newscomments.
     * @param object $id ID value of NewsComments. Default value is 0.
     * 
     * @return void
     */
    public function update($id = 0) {
        include ('Newscomments/update.php');
    }

    /**
     * Handles user interaction of view like Newscomments.
     * @param object $commentId ID value of NewsComments. Default value is 0.
     * 
     * @return void
     */
    public function like($commentId = 0) {
        include('Newscomments/like.php');
    }
    
     /**
     * Update static for Table by ModelName.
      * 
     * @param object $modelName Name of Model will update stataic.
     * 
     * @return void
     */
    public function doUpdateStatisticAction($modelName) {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId2'])) {
                $data['items'] = array($data['actionId2']);
            }
            if (!empty($data['items'])) {
                $param['news_comment_id'] = implode(',', $data['items']);
                if (!Api::call(Configure::read('API.url_newscomments_updatecounter'), $param)) {
                    AppLog::warning("Can not update", __METHOD__, $data);
                    $this->Common->setFlashErrorMessage(__("Can not update"));
                }
                $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));
                if (empty($this->getParams())) {
                    return $this->redirect($this->request->here(false));
                }
                return $this->redirect($this->request->here . '?' . http_build_query($this->getParams(), null, '&'));
            }
        }
    }
}
