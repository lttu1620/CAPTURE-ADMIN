<?php

$modelName = $this->Category->name;
// process delete / active / inactive 
$this->doGeneralAction($modelName);
$isAdmin = $this->AppUI->is_admin == 1;

if ($isAdmin) {
    $listCategory = MasterData::categories_all();
    $listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');
}


// create breadcrumb
$pageTitle = __('Category follower list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/categories',
            'name' => __('Category list'),
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Username'),
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname'),
        ));
if ($isAdmin) {
    $this->SearchForm
            ->addElement(array(
                'id' => 'category_id',
                'label' => __('Category'),
                'options' => $listCategory,
                'empty' => Configure::read('Config.StrChooseOne'),
                'autocomplete_combobox' => true,
    ));
}
$this->SearchForm->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => __('All')
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'user_name-asc' => __('Name Asc'),
                'user_name-desc' => __('Name Desc'),
                'user_nickname-asc' => __('Nickname Asc'),
                'user_nickname-desc' => __('Nickname Desc'),
                'email-asc' => __('Email Asc'),
                'email-desc' => __('Email Desc'),
                'updated-asc' => __('Follow date Asc'),
                'updated-desc' => __('Follow date Desc'),
                'category_name-asc' => __('Category name Asc'),
                'category_name-desc' => __('Category name Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
if (!$isAdmin) {
    $param['category_id'] = $id;
}
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_followcategories_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

if ($isAdmin) {
    $this->SimpleTable->addColumn(array(
        'id' => 'user_name',
        'title' => __('Username'),
        'width' => '200'
    ));
} else {
    $this->SimpleTable->addColumn(array(
        'id' => 'user_name',
        'title' => __('Username'),
    ));
}
$this->SimpleTable->addColumn(array(
            'id' => 'user_nickname',
            'title' => __('Nickname'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
            'width' => '200'
        ));
if ($isAdmin) {
    $this->SimpleTable->addColumn(array(
        'id' => 'category_name',
        'title' => __('Category name'),
    ));
}
$this->SimpleTable->addColumn(array(
            'id' => 'updated',
            'title' => __('Follow date'),
            'width' => '120'
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'link',
            'title' => __('Status'),
            'rules' => array(
                '0' => $this->SimpleTable->enabledTemplate(),
                '1' => $this->SimpleTable->disabledTemplate()
            ),
            'onclick' => 'return disableItem({id}, {disable});',
            'empty' => 0,
            'width' => '80'
        ))
        ->setDataset($data);
