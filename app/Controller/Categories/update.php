<?php

$modelName = $this->Category->name;
$model = $this->{$modelName};
$data = $cropImageInfo = array();
$pageTitle = __('Add Category');
if (!empty($id)) {
    $cropImageInfo = array(
        'category_id' => $id,
        'field' => 'image_url',        
    );
    $pageTitle = __('Edit category');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_categories_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/categories',
            'name' => __('Category list'),
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('Id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'image_url',
            'type' => 'file',
            'image' => true,
            'label' => __('Image'),
            'required' => true,
            'crop' => $cropImageInfo,                       
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));
    
// process when submit form
if ($this->request->is('post')) {    
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $model->data[$modelName]['image_url'] = $this->Image->uploadImage(
                    "{$modelName}.image_url", 'categories'
            );
        } elseif (isset($model->data[$modelName]['image_url']['remove'])) {
            $model->data[$modelName]['image_url'] = '';
        } else {
            unset($model->data[$modelName]['image_url']);
        }
        $id = Api::call(Configure::read('API.url_categories_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());        
    }
    // show validation error    
    $this->Common->setFlashErrorMessage($model->validationErrors);
}