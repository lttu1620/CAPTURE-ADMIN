<?php

$this->AppHtml->script('plugins/hightchart/highcharts.js');

// create breadcrumb
$pageTitle = __('News feed statistic');
$this->Breadcrumb->setTitle($pageTitle)->add(array(
    'name' => $pageTitle,
));

$param = $this->getParams(array(
        'type' => 'line',
        'mode' => 'day',
    ));
if (!isset($param['date_from'])) {
    $param['date_from'] = date('Y-m-d', strtotime('last month'));
    $this->setParam('date_from', $param['date_from']);
}
if (!isset($param['date_to'])) {
    $param['date_to'] = date('Y-m-d');
    $this->setParam('date_to', $param['date_to']);
}
// create search form 
$this->SearchForm
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'date_from',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date from'),
        'value' => $param['date_from']
    ))
    ->addElement(array(
        'id' => 'date_to',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date to'),
        'value' => $param['date_to']
    ))
    ->addElement(array(
        'id' => 'type',
        'label' => __('Chart type'),
        'options' => Configure::read('Config.searchChartType'),
    ))
    ->addElement(array(
        'id' => 'mode',
        'label' => __('View mode'),
        'options' => Configure::read('Config.searchChartMode'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$newsfeedFavorite = Api::call(Configure::read('API.url_reports_newsfeed_favorite'), $param); 
$newsfeedShare = Api::call(Configure::read('API.url_reports_newsfeed_share'), $param); 
$newsfeedLike = Api::call(Configure::read('API.url_reports_newsfeed_like'), $param);
$newsfeedView = Api::call(Configure::read('API.url_reports_newsfeed_view'), $param);
$newsfeedRead = Api::call(Configure::read('API.url_reports_newsfeed_read'), $param);

if (Api::getError()) {
    return $this->Common->handleException(Api::getError()); 
}
switch ($param['mode']) {
    case 'week':
        $newsfeedFavorite = $this->Common->weekChartData(array(
            'data' => $newsfeedFavorite,
            'date_field' => 'date',
            'count_field' => array('favorite_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedShare = $this->Common->weekChartData(array(
            'data' => $newsfeedShare,
            'date_field' => 'date',
            'count_field' => array('facebook_count', 'twitter_count', 'google_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedLike = $this->Common->weekChartData(array(
            'data' => $newsfeedLike,
            'date_field' => 'date',
            'count_field' => array('like_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedView = $this->Common->weekChartData(array(
            'data' => $newsfeedView,
            'date_field' => 'date',
            'count_field' => array('view_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedRead = $this->Common->weekChartData(array(
            'data' => $newsfeedRead,
            'date_field' => 'date',
            'count_field' => array('read_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        break;
    case 'month':
        $newsfeedFavorite = $this->Common->monthChartData(array(
            'data' => $newsfeedFavorite,
            'date_field' => 'date',
            'count_field' => array('favorite_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedShare = $this->Common->monthChartData(array(
            'data' => $newsfeedShare,
            'date_field' => 'date',
            'count_field' => array('facebook_count', 'twitter_count', 'google_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedLike = $this->Common->monthChartData(array(
            'data' => $newsfeedLike,
            'date_field' => 'date',
            'count_field' => array('like_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedView = $this->Common->monthChartData(array(
            'data' => $newsfeedView,
            'date_field' => 'date',
            'count_field' => array('view_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        $newsfeedRead = $this->Common->monthChartData(array(
            'data' => $newsfeedRead,
            'date_field' => 'date',
            'count_field' => array('read_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        break;  
}

$this->set('newsfeedFavorite', $this->Chart->render(array(
    'id' => 'newsfeedFavorite',
    'type' => $param['type'],
    'title' => __('News feed favorite statistic'),
    'data' => $this->Common->arrayDateForChart($newsfeedFavorite, 'date'),
    'x' => 'date',
    'y' => array(
        'favorite_count' => __('Favorite')
    )
)));
$this->set('newsfeedShare', $this->Chart->render(array(
    'id' => 'newsfeedShare',
    'type' => $param['type'],
    'title' => __('News feed share statistic'),
    'data' => $this->Common->arrayDateForChart($newsfeedShare, 'date'),
    'x' => 'date',
    'y' => array(
        'facebook_count' => __('Facebook'),
        'google_count' => __('Google'),
        'twitter_count' => __('Twitter'),
    )
)));
$this->set('newsfeedLike', $this->Chart->render(array(
    'id' => 'newsfeedLike',
    'type' => $param['type'],
    'title' => __('News feed like statistic'),
    'data' => $this->Common->arrayDateForChart($newsfeedLike, 'date'),
    'x' => 'date',
    'y' => array(
        'like_count' => __('Like')
    )
)));
$this->set('newsfeedView', $this->Chart->render(array(
    'id' => 'newsfeedView',
    'type' => $param['type'],
    'title' => __('News feed view statistic'),
    'data' => $this->Common->arrayDateForChart($newsfeedView, 'date'),
    'x' => 'date',
    'y' => array(
        'view_count' => __('View')
    )
)));
$this->set('newsfeedRead', $this->Chart->render(array(
    'id' => 'newsfeedRead',
    'type' => $param['type'],
    'title' => __('News feed read statistic'),
    'data' => $this->Common->arrayDateForChart($newsfeedRead, 'date'),
    'x' => 'date',
    'y' => array(
        'read_count' => __('Read')
    )
)));