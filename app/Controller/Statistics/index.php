<?php

$modelName = $this->Setting->name;

// process delete / visiable / invisiable 
$this->doGeneralAction($modelName);

// create search conditions         
$page = $this->getParam('page', 1);
$limit = $this->getParam('limit', Configure::read('Config.pageSize'));
$type = $this->getParam('type', 'global');

// create breadcrumb
$this->Breadcrumb->setTitle(__('Settings list'))
        ->add(array(
            'link' => '/' . $this->request->url,
            'name' => __('Settings list'),
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
        ))
        ->addElement(array(
            'id' => 'type',
            'label' => __('Type'),
            'options' => Configure::read('Config.SettingType'),
        ))
        ->addElement(array(
            'id' => 'data_type',
            'label' => __('Data type'),
            'options' => Configure::read('Config.SettingDataType'),
            'empty' => __('All'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => 'ID Asc',
                'id-desc' => 'ID Desc',
                'name-asc' => 'Name Asc',
                'name-desc' => 'Name Desc',
            ),
            'empty' => Configure::read('Config.StrChooseOne'),

        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
            'onchange' => 'javascript: $(\'#btnSearch\').click();',
        ))
        ->addElement(array(
            'type' => 'submit',
            'id' => 'btnSearch',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

$param = $this->getParams();
$param['page'] = $page;
$param['limit'] = $limit;
$param['type'] = $type;

list($total, $data) = Api::call(Configure::read('API.url_settings_list'), $param);
$this->set('total', $total);
$this->set('limit', $limit);
$this->SimpleTable
        ->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '20'
        ))
        ->addColumn(array(
            'id' => 'name',
            'title' => __('Name'),
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '300',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'description',
            'title' => __('Description'),
            'width' => '300',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'value',
            'name' => 'value[{id}]',
            'title' => __('Value'),
            'rules' => array(
                'number' => array('type' => 'text', 'class' => 'text_number'),
                'string' => array('type' => 'text'),
                'textarea' => array('type' => 'textarea', 'rows' => 2),
                'boolean' => array('type' => 'select', 'options' => Configure::read('Config.BooleanValue')),
                'image' => array('type' => 'image'),
                'checkbox' => array('type' => 'checkbox'),
            ),
            'value' => 'data_type',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'link',
            'title' => __('Status'),
            'rules' => array(
                '0' => $this->SimpleTable->enabledTemplate(),
                '1' => $this->SimpleTable->disabledTemplate()
            ),
            'onclick' => 'return disableItem({id}, {disable});',
            'empty' => 0,
            'width' => '80'
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'id' => 'btnSaveDataTable',
            'onclick' => 'return $(\'#dataForm\').submit();',
            'value' => _('Save setting'),
            'class' => 'btn btn-primary btn-saveTable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => _('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));

// process when submit form
if ($this->request->is('post')) {

    $data = $this->request->data;
    unset($data['action']);
    unset($data['actionId']);
   
    // Processing data normal
    $jsonData = array();
    if (!empty($data['value'])) {
        foreach ($data['value'] as $settingId => $settingVal) {
            $jsonData[] = array(
                'id' => $settingId,
                'value' => $settingVal
            );
        }
    }
    // Processing Data image    
    if (!empty($this->request->form['value'])) {
        $files = array();
        foreach ($this->request->form['value'] as $attrKey => $attrVal) {
            foreach ($attrVal as $settingId => $settingVal) {
                $files[$settingId][$attrKey] = $settingVal;
            }
        }
        if (!empty($files)) {
            foreach ($files as $settingId => $file) {
                if ($imageUrl = $this->Image->upload2($file)) {
                    $jsonData[] = array(
                        'id' => $settingId,
                        'value' => $imageUrl
                    );
                }
            }
        }
    }
    if(empty($jsonData)){
        return $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
    }
    $jsonData = json_encode($jsonData);    
    if (Api::call(Configure::read('API.url_settings_multiupdate'), array('value' => $jsonData))) {
        if (Api::getError()) {
            $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        }
        $this->redirect($this->referer()); //$this->request->here);
    } else {
        AppLog::info("Can not update", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
    }
}