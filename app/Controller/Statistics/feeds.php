<?php

$this->AppHtml->css('morris/morris.css');
$this->AppHtml->script('plugins/morris/morris.min.js');

$pageTitle = __('Newsfeed share report');
$modelName = 'Newsfeedshare';
//Create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)->add(array(
    'name' => $pageTitle,
));

// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'date_from',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date from'),
    ))
    ->addElement(array(
        'id' => 'date_to',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date to'),
    ))
    ->addElement(array(
        'id' => 'type',
        'label' => __('Chart Type'),
        'options' => array(
            'line_chart' => 'Line-Chart',
            'bar_chart' => 'Bar Chart',
            'sales_chart' => 'Sales Chart',
        ),
    ))
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));
if ($this->request->is('get')) {
    $param = $this->getParams();
    $result = Api::call(Configure::read('API.url_reports_newsfeed_share'), $param);
    $param['type']= !empty($param['type']) ? $param['type'] : "line_chart";
    if (Api::getError()) {
        $this->set('data', '\'\'');
    } else {
        $this->set('data', !empty($result) ? json_encode($result) : '\'\'');
        $this->set('xkey', '\'date_report\'');
        $this->set('ykeys', '\'' . json_encode(array('facebook_count', 'twitter_count', 'google_count')) . '\'');
        $this->set('labels', '\'' . json_encode(array('Facebook', 'Twitter', 'Google+')) . '\'');
        $this->set('type', '\'' . $param['type'] . '\'');
    }
}
?>