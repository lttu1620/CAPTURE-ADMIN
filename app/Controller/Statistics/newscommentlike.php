<?php

$this->AppHtml->script('plugins/hightchart/highcharts.js');

$pageTitle = __('Clap statistic');
$modelName = 'Newscommentlike';
//Create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)->add(array(
    'name' => $pageTitle,
));
$param = $this->getParams(array(
    'type' => 'line',
    'mode' => 'day',
        ));
if (!isset($param['date_from'])) {
    $param['date_from'] = date('Y-m-d', strtotime('last month'));
    $this->setParam('date_from', $param['date_from']);
}
if (!isset($param['date_to'])) {
    $param['date_to'] = date('Y-m-d');
    $this->setParam('date_to', $param['date_to']);
}
// Create search form 
$this->SearchForm
        ->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
            'value' => $param['date_from']
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
            'value' => $param['date_to']
        ))
        ->addElement(array(
            'id' => 'type',
            'label' => __('Chart type'),
            'options' => Configure::read('Config.searchChartType'),
        ))
        ->addElement(array(
            'id' => 'mode',
            'label' => __('View mode'),
            'options' => Configure::read('Config.searchChartMode'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'id' => 'btnSearch',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param['news_comment_id'] = $id;
if ($id > 0) {
    $newscomment = Api::call(Configure::read('API.url_newscomments_detail'), array('id' => $id));
    if(!Api::getError()){
        if(isset($newscomment)){
            $param['news_feed_id'] = $newscomment['news_feed_id'];
            $newsfeedspv = Api::call(Configure::read('API.url_reports_newsfeedspv'), $param);
            $newsfeedsuu = Api::call(Configure::read('API.url_reports_newsfeedsuu'), $param);
            $newsfeedpvuu = StatisticsController::mergeTwoArrayByKey($newsfeedspv, $newsfeedsuu, 'date', 'pv', 'uu');
        }else{
            $newsfeedpvuu = array();
        }
    }else{
        $newsfeedpvuu = array();
    }
}
$newscommentLike = Api::call(Configure::read('API.url_reports_newscomment_like'), $param);
if (Api::getError()) {
    return $this->Common->handleException(Api::getError());
}
switch ($param['mode']) {
    case 'week':
        $newscommentLike = $this->Common->weekChartData(array(
            'data' => $newscommentLike,
            'date_field' => 'date',
            'count_field' => array('like_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        if ($id > 0) {
            $newsfeedpvuu = $this->Common->weekChartData(array(
                'data' => $newsfeedpvuu,
                'date_field' => 'date',
                'count_field' => array('pv', 'uu'),
                'date_from' => $param['date_from'],
                'date_to' => $param['date_to'],
            ));
        }
        break;
    case 'month':
        $newscommentLike = $this->Common->monthChartData(array(
            'data' => $newscommentLike,
            'date_field' => 'date',
            'count_field' => array('like_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        if ($id > 0) {
            $newsfeedpvuu = $this->Common->monthChartData(array(
                'data' => $newsfeedpvuu,
                'date_field' => 'date',
                'count_field' => array('pv', 'uu'),
                'date_from' => $param['date_from'],
                'date_to' => $param['date_to'],
            ));
        }
        break;
}
$this->set('id',$id);
$this->set('newscommentLike', $this->Chart->render(array(
            'id' => 'newscommentLike',
            'type' => $param['type'],
            'title' => __('Clap statistic'),
            'data' => $this->Common->arrayDateForChart($newscommentLike, 'date'),
            'x' => 'date',
            'y' => array(
                'like_count' => __('Like')
            )
)));
if($id > 0){
    $this->set('newsfeedpvuu', $this->Chart->render(array(
            'id' => 'newsfeedpvuu',
            'type' => $param['type'],
            'title' => __('Clap statistic'),
            'data' => $this->Common->arrayDateForChart($newsfeedpvuu, 'date'),
            'x' => 'date',
            'y' => array(
                'pv' => __('page view'),
                'uu' => __('user unique')
            )
)));
}
