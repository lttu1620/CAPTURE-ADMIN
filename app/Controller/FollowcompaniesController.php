<?php

App::uses('AppController', 'Controller');

/**
 * FollowcompaniesController class of Followcompanies Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class FollowcompaniesController extends AppController {

        
    /**
     * Initializes components for FollowcompaniesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Followcompanies.
     * 
     * @param object $id ID value of Followcompanies. Default value is 0.
     */
    public function index($id = 0) {
        include ('Followcompanies/index.php');
    }

}
