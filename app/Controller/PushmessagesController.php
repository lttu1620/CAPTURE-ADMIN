<?php

App::uses('AppController', 'Controller');

/**
 * Pushmessages controller
 * 
 * @package Controller
 * @created 2014-12-04
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class PushmessagesController extends AppController {

    public $uses = array('User', 'Pushmessage');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     * 
     * @author Truongnn 
     * @return void
     */
    public function index() {
        include ('Pushmessages/index.php');
    }

    /**
     * update action
     * 
     * @author Truongnn 
     * @param $id 
     * @return void
     */
    public function update($id = 0) {
        $this->AppHtml->css('timepicker/bootstrap-timepicker.css');
        $this->AppHtml->script('plugins/timepicker/bootstrap-timepicker.js');
        include ('Pushmessages/update.php');
    }

}
