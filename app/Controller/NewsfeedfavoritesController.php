<?php

App::uses('AppController', 'Controller');

/**
 * NewsFeedFavoritesController class of NewsFeedFavorites Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class NewsFeedFavoritesController extends AppController {

    /**
     * Initializes components for NewsFeedFavoritesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Newscomments.
     * 
     *  updat@param object $feedId. ID value of NewsFeeds. Default value is 0.
     * @return void
     */
    public function index($feedId = 0) {
        include ('Newsfeedfavorites/index.php');
    }

}
