<?php

/**
 * Sharelog controller
 * 
 * @package Controller
 * @created 2014-12-17
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class SharelogsController extends AppController {

    public $uses = array('User','Sharelog');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     * 
     * @author Truongnn 
     * @return void
     */
    public function index() {
        include ('Sharelogs/index.php');
    }

}
