<?php

$modelName = $this->Newsfeedlike->name;
//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('News Feed Likes list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// Create search form 
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'user_name',
            'label' => __('Username')
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname')
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email')
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatusLike'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'name-asc' => __('Username Asc'),
                'name-desc' => __('Username Desc'),
                'nickname-asc' => __('Nick Name Asc'),
                'nickname-desc' => __('Nick Name Desc'),
                'created-asc' => __('Date Asc'),
                'created-desc' => __('Date Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),            
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['news_feed_id'] = $feedId;
list($total, $data) = Api::call(Configure::read('API.url_newsfeedlikes_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable    
        ->addColumn(array(
            'id' => 'image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{image}',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'user_name',
            'title' => __('Username'),
            'width' => 200
        ))
        ->addColumn(array(
            'id' => 'user_nickname',
            'title' => __('Nickname'),
            'width' => 200
        ))        
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),           
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Date'),
            'type' => 'date',
            'width' => 120
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'link',
            'title' => __('Status'),
            'rules' => array(
                '1' => $this->SimpleTable->disabledTemplate(__('Unliked')),
                '0' => $this->SimpleTable->enabledTemplate(__('Liked'))
            ),
            'onclick' => 'return disableItem ({id},{disable});',
            'empty' => 0,
            'width' => 80
        ))
        ->setDataset($data);
