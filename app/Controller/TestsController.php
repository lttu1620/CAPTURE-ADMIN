<?php

App::import('Vendor', 'facebook', array(
    'file' => 'facebook' . DS . 'autoload.php'
));
App::uses('AppController', 'Controller');

/**
 * Tests Controller for test and reference only
 * 
 * @package Controller
 * @created 2015-01-15
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class TestsController extends AppController {

    public $layout = "test";       
    var $paginate = array();

    /**
     * smtp action
     * 
     * @author thailh 
     */
    public function smtp() {
        AppEmail::test();
        exit;
    }

    /**
     * dialog action
     * 
     * @author thailh 
     */
    public function dialog() {
        
    }

    /**
     * upload action
     * 
     * @author thailh
     */
    public function upload() {

        $modelName = $this->Test->name;
        $model = $this->{$modelName};
        $this->UpdateForm->setModelName($modelName)
                ->addElement(array(
                    'id' => 'video',
                    'type' => 'file',
                    'label' => __('Video'),
                ))
                ->addElement(array(
                    'type' => 'submit',
                    'value' => __('Upload'),
                    'class' => 'btn bg-olive btn-block',
        ));
        if ($this->request->is('post')) {
            if ($model->validate($this->getData($modelName))) {
                if (!empty($_FILES['data']['name'][$modelName]['video'])) {
                    $result = $this->Video->upload("{$modelName}.video");
                    if ($this->Video->errorMsg) {
                        p($this->Video->errorMsg, 1);
                    }
                    d($result);
                }
            } else {
                $this->Common->setFlashErrorMessage($model->validationErrors);
            }
        }
    }

    /**
     * uploadimage action
     * 
     * @author thailh
     */
    public function uploadimage() {

        $modelName = $this->Test->name;
        $model = $this->{$modelName};
        $this->UpdateForm->setModelName($modelName)
                ->addElement(array(
                    'id' => 'image',
                    'type' => 'file',
                    'label' => __('Image'),
                ))
                ->addElement(array(
                    'type' => 'submit',
                    'value' => __('Upload'),
                    'class' => 'btn bg-olive btn-block',
        ));

        if ($this->request->is('post')) {
            if ($model->validate($this->getData($modelName))) {
                if (!empty($_FILES['data']['name'][$modelName]['image'])) {
                    /*
                      $filetype = $_FILES['data']['type'][$modelName]['image'];
                      $filedata = $_FILES['data']['tmp_name'][$modelName]['image'];
                      $filename = $_FILES['data']['name'][$modelName]['image'];
                      $filesize = $_FILES['data']['size'][$modelName]['image'];
                      $headers = array("Content-Type:multipart/form-data"); // cURL headers for file uploading
                      $cfile = new CurlFile($filedata, $filetype, $filename);
                      $postfields = array('file' => $cfile);
                      $url = Configure::read('API.Host') . Configure::read('API.url_upload_image');
                      $ch = curl_init();
                      $options = array(
                      CURLOPT_URL => $url,
                      CURLOPT_HEADER => false,
                      CURLOPT_POST => true,
                      CURLOPT_HTTPHEADER => $headers,
                      CURLOPT_POSTFIELDS => $postfields,
                      CURLOPT_INFILESIZE => $filesize,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_SAFE_UPLOAD => false,
                      CURLOPT_SSL_VERIFYPEER => false,
                      );
                      curl_setopt_array($ch, $options);  p($options);
                      $jsonResponse = curl_exec($ch);

                      echo 'Curl error: ' . curl_error($ch);

                      d($jsonResponse, 1);
                     */

                    $result = $this->Image->uploadImage("{$modelName}.image");
                    if ($this->Image->errorMsg) {
                        p($this->Image->errorMsg, 1);
                    }
                    d($result);
                }
            } else {
                $this->Common->setFlashErrorMessage($model->validationErrors);
            }
        }
    }

    /**
     * index action
     * 
     * @author thailh 
     */
    public function index() {
        $this->AppHtml->css('//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css');
        $this->AppHtml->script('//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/master/src/js/bootstrap-datetimepicker.js');
        $l = $this->getParam('l', 'eng');
        if ($l != 'jpn') $l = 'eng'; 
        $this->Session->write('language', $l);        
        $this->redirect('/');
    }

    /*
      throw new NotFoundException();
      throw new BadRequestException();
      throw new InternalErrorException();
      throw new ForbiddenException();
     */

    /**
     * component action
     * 
     * @author thailh 
     */
    public function component() {
        echo $this->Common->generalRandomId();
        exit;
    }

    /**
     * validation action
     * 
     * @author thailh 
     */
    public function validation() {
        $this->loadModel('Valid');
        if (!empty($this->data)) {
            $this->Valid->set($this->data['test']);
            if ($this->Valid->valid1()) {
                $this->Session->setFlash("Data is avaliable !");
            } else {
                //debug($this->Valid->invalidFields());
                debug($this->Valid->validationErrors);
                $errors = $this->Valid->validationErrors;
                $this->set("errors", $errors);
                $this->Session->setFlash("Data is not avaliable !");
            }
        }
    }

    /**
     * bookAll action
     * 
     * @author thailh 
     */
    public function bookAll() {
        $this->loadModel('Book');
        $data = $this->Book->find("all");
        $this->set("data", $data);
    }

    /**
     * paging action
     * 
     * @author thailh 
     */
    public function paging() {
        $this->loadModel('Book');
        $this->paginate = array(
            'field' => array('isbn', 'title', 'info'),
            'limit' => 4,
            'order' => array('title' => 'desc'),
        );
        $data = $this->paginate("Book");
        $this->set("data", $data);
    }

    /**
     * facebooksdk action
     * 
     * @author thailh 
     */
    public function facebooksdk() {
        @session_start(); 
        $tokenName = 'fb_token';        
        if ($this->getParam('reset', 0)) {
            $this->Cookie->delete($tokenName);
        }
        \Facebook\FacebookSession::setDefaultApplication(Configure::read('Facebook.appId'), Configure::read('Facebook.appSecret'));
        if ($this->Cookie->read($tokenName)) {
            $session = new \Facebook\FacebookSession($this->Cookie->read($tokenName));
        }
        if (!isset($session)) {
            $helper = new \Facebook\FacebookRedirectLoginHelper(Router::url($this->here, true));
            try {
                $session = $helper->getSessionFromRedirect();
                if (isset($session)) {
                    $this->Cookie->write($tokenName, $session->getToken(), true, '2 weeks');           
                }
            } catch (\Facebook\FacebookRequestException $ex) {
                p($ex);
                // When Facebook returns an error
            } catch (\Exception $ex) {
                p($ex);
                // When validation fails or other local issues
            }
        }
        if (isset($session)) {
            $request = new \Facebook\FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            $graphObject = $response->getGraphObject();
            echo '<pre>' . print_r($graphObject, 1) . '</pre>';
        } else {
            echo '<a href="' . $helper->getLoginUrl(array('scope' => 'email')) . '">Login</a>';
        }     
    }

    /**
     * autocomplete action
     * 
     * @author thailh 
     */
    public function autocomplete() {
        
    }
    
    /**
     * cropimage action
     * 
     * @author thailh 
     */
    public function cropimage($imageUrl = '') {
        
    }
    
    /**
     * imgareaselect action
     * 
     * @author thailh 
     */
    public function imgareaselect($imageUrl = '') {
        if (empty($imageUrl)) {
            $imageUrl = 'aHR0cDovL2wuZjE0LmltZy52bmVjZG4ubmV0LzIwMTUvMDEvMTUvY2h1LWNoby1oYW5oLXBodWMtbmhhdC10aGUtZ2lvaS1kYW5oLWJhaS1iZW5oLXVuZy10aHUtMTQyMTI5Mjk0MV80OTB4Mjk0LmpwZw==';
        }
        //$imageUrl = base64_encode('http://l.f14.img.vnecdn.net/2015/01/15/chu-cho-hanh-phuc-nhat-the-gioi-danh-bai-benh-ung-thu-1421292941_490x294.jpg');
        $this->AppHtml->script('jquery.imgareaselect.pack.js');       
        $this->AppHtml->css('imgareaselect.css');
        $this->set('imageUrl', $imageUrl);
        //print_r($this->request);
    }  
   
}
