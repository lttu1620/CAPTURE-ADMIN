<?php

$modelName = $this->Newsfeedviewlog->name;

// create breadcrumb
$pageTitle = __('News feed view log list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Username')
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname')
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email')
        ))
        ->addElement(array(
            'id' => 'title',
            'label' => __('News feed title')
        ))
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'user_name-asc' => __('Name Asc'),
                'user_name-desc' => __('Name Desc'),
                'nickname-asc' => __('Nickname Asc'),
                'nickname-desc' => __('Nickname Desc'),
                'email-asc' => __('Email Asc'),
                'email-desc' => __('Email Desc'),
                'news_feeds_title-asc' => __('News feed Asc'),
                'news_feeds_title-desc' => __('News feed Desc'),
                'created-asc' => __('Created Asc'),
                'created-desc' =>__('Created Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
            'onchange' => 'javascript: $(\'input[type=submit]\').click();'
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['share_type'] = 0;
//d($param, 1);
list($total, $data) = Api::call(Configure::read('API.url_newsfeedviewlogs_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
        ->addColumn(array(
            'id' => 'user_name',
            'title' => __('Username'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'nickname',
            'title' => __('Nickname'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'news_feeds_title',
            'title' => __('News feeds title'),
        ))
        ->addColumn(array(
            'id' => 'share_type',
            'title' => __('Share type'),
            'rules' => array(
                '0' => __('Not share'),
                '1' => __('Facebook'),
                '2' => __('Twitter'),
                '3' => __('Google plus'),
            ),
            'width' => '120'
        ))
        ->addColumn(array(
            'id' => 'created',
            'type' => 'date',
            'title' => __('Created'),
            'width' => '120'
        ))
        ->setDataset($data);
