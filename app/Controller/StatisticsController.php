<?php

/**
 * Statistics Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class StatisticsController extends AppController {

    public $components = array('Chart');

    /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author tuancd 
     * @return void
     */
    public function index() {
        include ('Statistics/index.php');
    }

    /**
     * users action
     * 
     * @author tuancd 
     * @return void
     */
    public function users($id = 0) {
        include ('Statistics/users.php');
    }

    /**
     * feeds action
     * 
     * @author tuancd 
     * @return void
     */
    public function feeds($id = 0) {
        include ('Statistics/feeds.php');
    }

    /**
     * newsfeeds action
     * 
     * @author tuancd 
     * @return void
     */
    public function newsfeeds() {
        include ('Statistics/newsfeeds.php');
    }
    
    /**
     * newscommentlike action
     * 
     * @author tuancd 
     * @return void
     */
    public function newscommentlike($id = 0) {
        include ('Statistics/newscommentlike.php');
    }
   /**
    * Merge two arrays to one array by key key and value
    * 
    * @author Truongnn
    * @param type $array1 Array 1
    * @param type $array2 Array 2
    * @param type $key Key
    * @param type $value1 Value 1
    * @param type $value2 Value 2
    * @return array an Array after merging
    */
    public static function mergeTwoArrayByKey($array1 = array(), $array2 = array(), $key= '', $value1 = '', $value2 = '') {
        //Convert to array by key and value
        $arr1 = CommonComponent::arrayKeyValue($array1, $key, $value1);
        $arr2 = CommonComponent::arrayKeyValue($array2, $key, $value2);
        //Merge two arrays by key 
        $arr = $arr1 + $arr2;
        $keys = array();
        //Get all keys from array
        $keys = array_keys($arr);
        //Sort value by key
        sort($keys);
        $result = array();
        $i = 0;
        foreach ($keys as $item) {
            $result[$i] = array(
                $key => $item,
                $value1 => empty($arr1[$item])? 0 : $arr1[$item],
                $value2 => empty($arr2[$item])? 0 : $arr2[$item]
            );
            $i++;
        }
        return $result;
    }
}
