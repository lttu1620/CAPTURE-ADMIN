<?php

App::uses('AppController', 'Controller');

/**
 * Tags of controller
 * @package Controller
 * @created 2015-01-15
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class TagsController extends AppController {

    public $uses = array('User', 'Tag');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Action: index
     * 
     * @author Truongnn
     * @return void
     */
    public function index() {
        include ('Tags/index.php');
    }

    /**
     * Action: update
     * 
     * @author Truongnn
     * @return void
     */
    public function update($id = 0) {
        include ('Tags/update.php');
    }

    /**
     * Action: doUpdateStatisticAction
     * 
     * @author Truongnn
     * @return void
     */
    public function doUpdateStatisticAction($modelName) {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId2'])) {
                $data['items'] = array($data['actionId2']);
            }
            if (!empty($data['items'])) {
                $param['id'] = implode(',', $data['items']);
                if (!Api::call(Configure::read('API.url_tags_updatecounter'), $param)) {
                    AppLog::warning("Can not update", __METHOD__, $data);
                    $this->Common->setFlashErrorMessage(__("Can not update"));
                }
                $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));

                if (empty($this->getParams())) {
                    return $this->redirect($this->request->here(false));
                }
                return $this->redirect($this->request->here. '?' . http_build_query($this->getParams(), null, '&'));
            }
        }
    }

}
