<?php


App::uses('AppController', 'Controller');

/**
 * Userrecruiters Controller
 * 
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class UserrecruitersController extends AppController {

    public $uses = array('UserRecruiter');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Action: index  
     * 
     * @author Truongnn
     * @param int id 
     * @return void
     */
    public function index() {
        include ('Userrecruiters/index.php');
    }

    /**
     * Action: update
     *
     * @author Truongnn
     * @param int id
     * @return void
     */
    public function update() {
        include ('Userrecruiters/update.php');
    }

    /**
     * Action: doIsAdminIsApprovedAction
     * 
     * @author Truongnn
     * @return void
     */
    public function doIsAdminIsApprovedAction($modelName) {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId2'])) {
                $data['items'] = array($data['actionId2']); 
            }
            if (!empty($data['action2']) && !empty($data['items'])) {
                $optionId2 = $data['optionId2'];
                $param['id'] = implode(',', $data['items']);
                switch ($optionId2) {
                    case 0:
                        $param['is_admin'] = ($data['action2'] == 'is_admin' ? 1 : 0);
                        if (!Api::call(Configure::read('API.url_userrecruiters_isAdmin'), $param)) {
                            AppLog::warning("Can not update", __METHOD__, $data);
                            $this->Common->setFlashErrorMessage(__("Can not update"));
                        }
                        $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));
                        break;
                    case 1:
                        $param['is_approved'] = ($data['action2'] == 'is_approved' ? 1 : 0);
                        if (!Api::call(Configure::read('API.url_userrecruiters_isApproved'), $param)) {
                            AppLog::warning("Can not update", __METHOD__, $data);
                            $this->Common->setFlashErrorMessage(__("Can not update"));
                        }
                        $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));
                        break;
                }
                if (empty($this->getParams())) {
                    return $this->redirect($this->request->here(false));
                }
                return $this->redirect($this->request->here(false) . '?' . http_build_query($this->getParams(), null, '&'));
            }
        }
    }
    
    /**
     * Action: waitingapprove
     * 
     * @author Truongnn
     * @return void
     */
    public function waitingapprove() {
        include ('Userrecruiters/waitingapprove.php');
    }

}
