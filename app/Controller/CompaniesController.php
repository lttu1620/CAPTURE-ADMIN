<?php

App::uses('AppController', 'Controller');

/**
 * CompaniesController class of Companies Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class CompaniesController extends AppController {

    /**
     * Initializes components for CompaniesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view update Companies.
     * 
     * @return void
     */
    public function index() {
        include ('Companies/index.php');
    }

    /**
     * Handles user interaction of view update Companies.
     * 
     * @param integer $id ID value of Companies. Default value is 0.
     * @param integer $user_id ID value of Users. Default value is -1.
     * 
     * @return void
     */
    public function update($id = 0, $user_id = -1) {
        include ('Companies/update.php');
    }

   /**
     * Handles user interaction of view follower Companies.
     * 
     * @param integer $id ID value of Companies. Default value is 0.
     * 
     * @return void
     */
    public function follower($id = 0) {
        if (empty($id) && !$this->AppUI->is_admin) {
            $id = $this->AppUI->company_id;
        }
        include ('Companies/follower.php');
    }
    
    /**
     * Handles user interaction of view doUpdateStatisticAction Companies.
     * 
     * @param integer $modelName Model name.
     * 
     * @return void
     */
    public function doUpdateStatisticAction($modelName) {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId2'])) {
                $data['items'] = array($data['actionId2']);
            }
            if (!empty($data['items'])) {
                $param['company_id'] = implode(',', $data['items']);
                if (!Api::call(Configure::read('API.url_companies_updatecounter'), $param)) {
                    AppLog::warning("Can not update", __METHOD__, $data);
                    $this->Common->setFlashErrorMessage(__("Can not update"));
                }
                $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));
                if (empty($this->getParams())) {
                    return $this->redirect($this->request->here(false));
                }
                return $this->redirect($this->request->here . '?' . http_build_query($this->getParams(), null, '&'));
            }
        }
    }
}
