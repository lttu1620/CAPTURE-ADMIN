<?php

$modelName = $this->Newssite->name;

// Process disable/enable
$this->doGeneralAction($modelName);
// Process update statistics
$this->doUpdateStatisticAction($modelName);
// create breadcrumb
$pageTitle = __('News site list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );

// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'name',
            'label' => __('Name'),
        )
    )
    ->addElement(
        array(
            'id'    => 'feed_url',
            'label' => __('Feed url'),
        )
    )
    ->addElement(
        array(
            'id'      => 'is_tadacopy',
            'label'   => __('Is tadacopy'),
            'options' => Configure::read('Config.IsTadacopy'),
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'disable',
            'label'   => __('Status'),
            'options' => array(
                1 => __('Disable'),
                0 => __('Active'),
            ),
            'empty'   => __('All'),
        )
    )
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => array(
                'id-asc'         => __('ID Asc'),
                'id-desc'        => __('ID Desc'),
                'name-asc'       => __('Name Asc'),
                'name-desc'      => __('Name Desc'),
                'rss_count-asc'  => __('Rss count Asc'),
                'rss_count-desc' => __('Rss count Desc'),
            ),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_newssites_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());

$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable->addColumn(
    array(
        'id'    => 'item',
        'name'  => 'items[]',
        'type'  => 'checkbox',
        'value' => '{id}',
        'witd'  => '20',
    )
)
    ->addColumn(
        array(
            'id'    => 'newssites_thumbnail_img',
            'type'  => 'image',
            'title' => __('Image'),
            'src'   => '{thumbnail_img}',
            'width' => '60',
        )
    )
    ->addColumn(
        array(
            'id'    => 'id',
            'type'  => 'link',
            'title' => __('ID'),
            'href'  => '/'.$this->controller.'/update/{id}',
            'width' => '30',
        )
    )
    ->addColumn(
        array(
            'id'    => 'name',
            'type'  => 'link',
            'href'  => '/'.$this->controller.'/update/{id}',
            'title' => __('Name'),
            'width' => '350',
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'feed_url',
            'title' => __('Url'),
            'empty' => '',
        )
    )
    ->addcolumn(
        array(
            'id'     => 'feed_count',
            'title'  => __('Feed count'),
            'width'  => '100',
            'button' => true,
            'empty'  => '0',
        )
    )
    ->addcolumn(
        array(
            'id'     => 'rss_count',
            'type'   => 'link',
            'title'  => __('Rss count'),
            'href'   => '/newssitesrss?news_site_id={id}',
            'width'  => '100',
            'button' => true,
            'empty'  => '0',
        )
    )
    ->addColumn(
        array(
            'id'     => 'is_tadacopy',
            'type'   => 'checkbox',
            'title'  => __('Is tadacopy'),
            'toggle' => true,
            'rules'  => array(
                '1' => 'checked',
                '0' => '',
            ),
            'class'  => 'toggle-event is_tadacopy',
            'empty'  => 0,
            'width'  => 80,
        )
    )
    ->addColumn(
        array(
            'id'     => 'disable',
            'type'   => 'checkbox',
            'title'  => __('Status'),
            'toggle' => true,
            'rules'  => array(
                '0' => 'checked',
                '1' => '',
            ),
            'empty'  => 0,
            'width'  => 90,
        )
    )
    ->setDataset($data)
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Update statistic'),
            'class' => 'btn btn-primary btn-statistic',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        )
    )
    ->addHidden(
        array(
            'type' => 'hidden',
            'id'   => 'actionId2',
        )
    );

