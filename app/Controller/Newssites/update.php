<?php

$modelName = $this->Newssite->name;
$model = $this->{$modelName};
$data = $cropImageInfo = array();
$pageTitle = __('Add news site');
if (!empty($id)) {
    $pageTitle = __('Edit news site');
    $cropImageInfo = array(
        'newssite_id' => $id,
        'field'       => 'thumbnail_img',
    );
    $param['id'] = $id;
    $param['full'] = "0";
    $data[$modelName] = Api::call(Configure::read('API.url_newssites_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(
        array(
            'link' => $this->request->base.'/newssites',
            'name' => __('newssites list'),
        )
    )
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
// Create Update form 
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(
        array(
            'id'    => 'id',
            'type'  => 'hidden',
            'label' => __('id'),
        )
    )
    ->addElement(
        array(
            'id'       => 'name',
            'label'    => __('Name'),
            'type'     => 'textarea',
            'rows'     => '2',
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'    => 'introduction_text',
            'label' => __('Introduction'),
            'type'  => 'textarea',
            'rows'  => '5',
        )
    )
    ->addElement(
        array(
            'id'    => 'feed_url',
            'label' => __('Feed url'),
        )
    )
    ->addElement(
        array(
            'id'    => 'thumbnail_img',
            'type'  => 'file',
            'image' => true,
            'label' => __('Image'),
            'crop'  => $cropImageInfo,
        )
    )
    ->addElement(
        array(
            'id'      => 'is_tadacopy',
            'label'   => __('Is tadacopy'),
            'options' => Configure::read('Config.IsTadacopy'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Cancel'),
            'class'   => 'btn btn-primary pull-left',
            'onclick' => 'return back();',
        )
    );

if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (!empty($_FILES['data']['name'][$modelName]['thumbnail_img'])) {
            $model->data[$modelName]['thumbnail_img'] = $this->Image->uploadImage(
                "{$modelName}.thumbnail_img",
                'news_sites'
            );
        } elseif (isset($model->data[$modelName]['thumbnail_img'])) {
            unset($model->data[$modelName]['thumbnail_img']);
        }
        $error = array(//'name' => array(1010 => __('The name is required and must contain a value')),
        );
        $id = Api::call(Configure::read('API.url_newssites_addupdate'), $model->data[$modelName]);
        if (!Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            $this->redirect($this->request->here(false));
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError(), $error);
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}
    