<?php

App::uses('AppController', 'Controller');

/**
 * NewsfeedsController class of Newsfeeds Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class NewsfeedsController extends AppController {

    /**
     * Initializes components for NewsfeedsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Newsfeeds.
     * 
     * 
     * @return void
     */
    public function index() {
        include ('Newsfeeds/index.php');
    }

     /**
     * Handles user interaction of view update Newsfeeds.
     * 
      * @param object $id ID value of NewsFeeds. Default value is 0.
     * @return void
     */
    public function update($id = 0) {
        include ('Newsfeeds/update.php');
    }

     /**
     * Handles user interaction of view lists Newsfeeds.
     * 
     * 
     * @return void
     */
    public function lists() {
        include ('Newsfeeds/lists.php');
    }

     /**
     * Handles user interaction of view view Newsfeeds.
     * 
     * @param object $feedId ID value of NewsFeeds. Default value is 0.
     * @return void
     */
    public function view($feedId = 0) {
        include ('Newsfeeds/view.php');
    }

    /**
     * Handles user interaction of view favorites Newsfeeds.
     * 
     * @param object $feedId ID value of NewsFeeds. Default value is 0.
     * @return void
     */
    public function favorite($feedId = 0) {
        include ('Newsfeeds/favorite.php');
    }

    /**
     * Handles user interaction of view like Newsfeeds.
     * 
     * @param object $feedId ID value of NewsFeeds. Default value is 0.
     * @return void
     */
    public function like($feedId = 0) {
        include ('Newsfeeds/like.php');
    }

    /**
     * Handles user interaction of view import Newsfeeds.
     *
     * @return void
     */
    public function import() {
        include ('Newsfeeds/import.php');
    }

   /**
     * Action update statistic for NewsFeeds Newsfeeds.
     * 
     * @param object $modelName Model name is update static.
     * @return void
     */
    public function doUpdateStatisticAction($modelName) {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId2'])) {
                $data['items'] = array($data['actionId2']);
            }
            if (!empty($data['items'])) {
                $param['news_site_id'] = implode(',', $data['items']);
                if (!Api::call(Configure::read('API.url_newsfeeds_updatecounter'), $param)) {
                    AppLog::warning("Can not update", __METHOD__, $data);
                    $this->Common->setFlashErrorMessage(__("Can not update"));
                }
                $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));
                if (empty($this->getParams())) {
                    return $this->redirect($this->request->here(false));
                }
                return $this->redirect($this->request->here . '?' . http_build_query($this->getParams(), null, '&'));
            }
        }
    }
}
