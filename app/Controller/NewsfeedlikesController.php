<?php

App::uses('AppController', 'Controller');

/**
 * NewsFeedLikesController class of NewsFeedLikes Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class NewsFeedLikesController extends AppController {

    /**
     * Initializes components for NewsFeedLikesController class.
     */
	public function __construct($request = null, $response = null) {
		parent::__construct($request, $response);
	}

    /**
     * Handles user interaction of view index NewsFeedLikes.
     * 
     *  @param object $feedId ID value of NewsFeeds. Default value is 0.
     * @return void
     */
	public function index($feedId = 0) {
		include ('Newsfeedlikes/index.php');
	}
	
}
