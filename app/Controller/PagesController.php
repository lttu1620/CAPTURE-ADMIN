<?php

/**
 * PagesController class of Pages Controller
 *
 * @package	Controller  
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
App::uses('AppController', 'Controller');

/**
 * Initializes components for PagesController class.
 */
class PagesController extends AppController {

    /**
     * Index Action
     *
     * @return void
     */
    public function index() {
        if (!$this->AppUI->is_admin) {
            $this->setParams(array('tag_id' => 'all'));
            include ('Newsfeeds/lists.php');
        } else {
            include ('Pages/index.php');
        }
    }

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     * 	or MissingViewException in debug mode.
     */
    public function display() {

        if ($this->Auth->user()->is_admin == 0) { // if is recruiter then go to newsfeeds/lists
            return $this->redirect('/newsfeeds/lists');
        }

        include ('Pages/index.php');

        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Action login   
     *  
     * @author thailh 
     * @return void
     */
    public function login() {
        include ('Pages/login.php');
    }

    /**
     * Action fblogin   
     *  
     * @author thailh 
     */
    public function fblogin() {
        include ('Pages/fblogin.php');
    }

    /**
     * Action register   
     *  
     * @author thailh 
     * @return void
     */
    public function register() {
        include ('Pages/register.php');
    }

    /**
     * Action registerprofile   
     *  
     * @author thailh 
     * 
     * @param string $token
     */
          
    public function registerprofile($token = null) {
        include ('Pages/register-profile.php');
    }

    /**
     * Action registercompany   
     *  
     * @author thailh 
     * @return void
     */
    public function registercompany() {
        include ('Pages/register-company.php');
    }

    /**
     * Action active   
     *  
     * @author thailh 
     * @param string $token
     * @return void
     */
    public function active($token = null) {
        include ('Pages/register-active.php');
    }

    /**
     * Action approve   
     *  
     * @author thailh 
     * @param string $token
     */
    public function approve($token = null) {
        include ('Pages/register-approve.php');
    }

    /**
     * Action logout   
     *  
     * @author thailh 
     * @return void
     */
    public function logout() {
        include ('Pages/logout.php');
    }

    /**
     * Action pending   
     *  
     * @author thailh 
     */
    public function pending() {
        include ('Pages/pending.php');
    }

    /**
     * Action forgetpassword   
     *  
     * @author thailh 
     * @return void
     */
    public function forgetpassword() {
        include ('Pages/forget-password.php');
    }

    /**
     * Action newpassword   
     *  
     * @author thailh 
     */
    public function newpassword($token = null) {
        include ('Pages/new-password.php');
    }

    /**
     * Action sendmailsuccess   
     *  
     * @author thailh 
     * @return void
     */
    public function sendmailsuccess() {
        // include ('Pages/new-password.php');
    }

    /**
     * Action sendmailnotification   
     *  
     * @author thailh 
     */
    public function sendmailnotification() {
        // do not delete
    }

    /**
     * Action lp   
     *  
     * @author thailh 
     * @return void
     */
    public function lp() { // landing page (appli's introduction page)
        include ('Pages/lp.php');
    }

    /**
     * Action contact   
     *  
     * @author thailh 
     * @return void
     */
    public function contact() {
        include ('Pages/contact.php');
    }

    /**
     * Action privacypolicy   
     *  
     * @author thailh 
     * @return void
     */
    public function privacypolicy() {
        include ('Pages/privacypolicy.php');
    }
    
    /**
     * Action postdetail   
     *  
     * @author thailh 
     * @return void
     */
    public function postdetail($id = 0) {
        include ('Pages/postdetail.php');
    }
    

}
