<?php

/**
 * NewsfeedviewlogsController class of Newsfeedviewlogs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class NewsfeedviewlogsController extends AppController {
    
    /**
     * Initializes components for NewsfeedviewlogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

     /**
     * Handles user interaction of view index Newsfeedviewlogs.
     * 
     * @return void
     */
    public function index() {
        include ('Newsfeedviewlogs/index.php');
    }

}
