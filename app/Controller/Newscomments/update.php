<?php

$modelName = $this->Newscomment->name;
$model = $this->{$modelName};

if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_newscomments_detail'), $param);
    $this->Common->handleException(Api::getError());
}

$this->setPageTitle(__('Edit comment'));

// create breadcrumb
$this->Breadcrumb->setTitle(__('Edit comment'))
        ->add(array(
            'link' => $this->request->base . '/newscomments',
            'name' => __('Comment list'),
        ))
        ->add(array(
            'name' => __('Edit comment'),
        ));

// create Update form 
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'news_feed_title',
            'label' => __('Feed title'),
            'disabled' => true,
        ))
        ->addElement(array(
            'id' => 'comment',
            'rows' => 8,
            'label' => __('Comment')
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_newscomments_update'), $data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not update comment", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            $this->redirect($this->request->here(false));
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}