<?php

// create search conditions         
$page = $this->getParam('page', 1);
$limit = Configure::read('Config.pageSize');
if (empty($commentId)) {
    AppLog::info("Comment id not found", __METHOD__, $commentId);
    throw new NotFoundException("Comment id not found", __METHOD__, $commentId);
}
// create breadcrumb
$this->Breadcrumb->setTitle(__('News comment likes'))
	->add(array(
	    'link' => $this->request->base . '/newscomments',
	    'name' => __('RSS news comments'),
	))
	->add(array(
	    'name' => __('News comment likes'),
	));
// get detail comment
$detail = Api::call(Configure::read('API.url_newscomments_detail'), array('id' => $commentId));
if (Api::getError()) {
    AppLog::info("API.url_newscomments_detail failed", __METHOD__, $commentId);
    $this->Common->handleException(Api::getError());
    return $this->Common->setFlashErrorMessage(Api::getError());
}
//d($detail,1);
// get newscommentlikes of comment
$param = array(
    'page' => $page,
    'limit' => $limit,
    'news_comment_id' => $commentId,
    'sort' => 'created-desc',
    'disable' => 0
);
//d($param,1);
list($total, $newscommentlikes) = Api::call(Configure::read('API.url_newscommentlikes_list'), $param);
if (Api::getError()) {
    AppLog::info("API.url_newsnewscommentlikes_list failed", __METHOD__, $commentId);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
if (empty($newscommentlikes[0])) {
    AppLog::info("API.url_newscommentlikes_list return empty", __METHOD__, $commentId);
}
// set data to view
$this->set('comment_info', $detail);
$this->set('newscommentlikes', $newscommentlikes);
$this->set('total', $total);
$this->set('limit', $limit);
$this->set('page', $page);
$this->set('is_admin', $this->AppUI->is_admin);
$this->set('user_id', $this->AppUI->id);