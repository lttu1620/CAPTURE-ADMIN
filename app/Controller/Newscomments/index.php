<?php

$modelName = $this->Newscomment->name;

//Process disable/enable
$this->doGeneralAction($modelName);
if ($this->AppUI->is_admin) {
    // Process update statistics
    $this->doUpdateStatisticAction($modelName);
}
// create breadcrumb
$pageTitle = __('News comment list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// Create search form 
$this->SearchForm
        ->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'comment',
            'type' => 'text',
            'label' => __('Comment'),
        ))
        ->addElement(array(
            'id' => 'news_feed_title',
            'type' => 'text',
            'label' => __('Feed title'),
        ))
        ->addElement(array(
            'id' => 'user_name',
            'type' => 'text',
            'label' => __('Username'),
        ))
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
        ));
if ($this->AppUI->is_admin) {
        $this->SearchForm->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => Configure::read('Config.StrAll')
        ));
}
        $this->SearchForm->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'comment_short-asc' => __('Comment Asc'),
                'comment_short-desc' => __('Comment Desc'),
                'like_count-asc' => __('Clap count Asc'),
                'like_count-desc' => __('Clap count Desc'),
                'title-asc' => __('Feed title Asc'),
                'title-desc' => __('Feed title Desc'),
                'created-asc' => __('Comment time Asc'),
                'created-desc' => __('Comment time Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if (!$this->AppUI->is_admin) {
    $param['user_id'] = $this->AppUI->id;
}
list($total, $data) = Api::call(Configure::read('API.url_newscomments_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => 20
        ))
        ->addColumn(array(
            'id' => 'id',
            'title' => __('ID'),
            'type' => $this->AppUI->is_admin ? 'link' : '',
            'href' => $this->AppUI->is_admin ? '/' . $this->controller . '/update/{id}' : '',
            'width' => 60
        ))
        ->addColumn(array(
            'id' => 'display_image',
            'type' => 'image',
            'title' => __('Comment'),
            'src' => '{display_image}',
            'image_type' => 'user',
            'width' => 68
        ))
        ->addColumn(array(
            'id' => 'display_username',
            
            'type' => 'link',
            'href' => '/users/profileinformation/{user_id}',
            'target' => 'blank'
        ))
        ->addColumn(array(
            'id' => 'comment_short',
            'title' => __('Comment'),    
            'hidden'=> true
        ))
        ->addColumn(array(
            'id' => 'news_feed_title',
            'title' => __('Feed title'),
            'type' => 'link',
            'href' => '/newsfeeds/view/{news_feed_id}',            
            'width' => 250,
            'target' => 'blank'
        ))
        ->addColumn(array(
            'id' => 'like_count',
            'type' => 'link',
            'title' => __('Clap count'),
            'href' => $this->AppUI->is_admin ? '/newscommentlikes/index/{id}' : '/newscomments/like/{id}',
            'width' => 80,
            'button' => true,
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Created'),
            'type' => 'date',
            'width' => 120, 
            'hidden'=> true
        ))
        ->addColumn(array(
            'id' => 'graph',
            'type' => 'link',
            'href' => '/statistics/newscommentlike/{id}',
            'class' => 'graph',
            'th_title' => __('Graph'),
            'title' => '<button type="button" class="btn btn-info"><i class="fa fa-align-left"></i></button>',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'edit_link',
            'type' => 'link',
            'th_title' => __('Edit'),
            'title' => __('Edit'),
            'href' => '/' . $this->controller . '/update/{id}',
            'button' => true,
            'width' => '50'
        ))
        ->addColumn(array(          
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),            
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),           
            'empty' => 0,
            'width' => 90,            
        ))
        ->addColumn(array(
            'id' => 'delete_link',
            'type' => 'link',
            'th_title' => __('Delete'),
            'title' => __('Delete'),
            'href' => '/ajax/disable?id={id}',
            'class' => 'btn-recruiter-disable',
            'data-id' => '{id}',
            'button' => true,
        ))
        ->setDataset($data)
        ->setMergeColumn(array(
            'display_username' => array(
                array(
                    'field' => 'comment_short',                   
                ),   
                array(
                    'field' => 'created',
                    'before' => __('Created'). " : ",
                    'after' => ''
                ),               
            )
        ))
        ->addButton(array(
            'id' => 'btn-statistic',
            'type' => 'submit',
            'value' => __('Update statistic'),
            'class' => 'btn btn-primary btn-statistic',
            'title' => __('Reupdate clap count'),
        ))
        ->addButton(array(
            'id' => 'btn-disable',
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'id' => 'btn-enable',
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));

if (!$this->AppUI->is_admin) {
    $this->SimpleTable->removeColumn('item');
    $this->SimpleTable->removeColumn('edit_link');
    $this->SimpleTable->removeColumn('disable');
    $this->SimpleTable->removeButton('btn-statistic');
    $this->SimpleTable->removeButton('btn-disable');
    $this->SimpleTable->removeButton('btn-enable');
    $this->SearchForm->removeElement('news_feed_title');
} else {
    $this->SimpleTable->removeColumn('delete_link');
}