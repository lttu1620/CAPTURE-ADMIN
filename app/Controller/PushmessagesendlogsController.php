<?php

/**
 * Pushmessagesendlog controller
 * 
 * @package Controller
 * @created 2014-12-19
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class PushmessagesendlogsController extends AppController {

    public $uses = array('User','Pushmessagesendlog');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     * 
     * @author Truongnn 
     * @return void
     */
    public function index() {
        include ('Pushmessagesendlogs/index.php');
    }

}
