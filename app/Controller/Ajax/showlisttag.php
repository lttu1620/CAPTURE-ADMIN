<?php
$modelName 	= $this->Ajax->name;
$tagType	= $_POST['tagType'];
$tags       =  $this->Common->arrayFilter(MasterData::tags_all(), 'type', $tagType);

if ($tags) {
	$html = "<select id=\"select-tag-id-{$_POST['feedId']}\" onchange=\"addfeedtag({$_POST['feedId']},this.value)\" class=\"chosen-select\" data-placeholder=\". __('Plear choose tag'). \">";
	$html .= "<option value=\"\">".__("-- Select tag --")."</option>";
	foreach($tags as $tag){
		$html .= "<option value=\"{$tag['id']}-{$tag['color']}\">{$tag['name']}</option>";
	}
	$html .= "</select>";
	echo $html;
}else{
	echo 'empty';
}
exit;
