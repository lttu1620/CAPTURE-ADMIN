<?php
$modelName = $this->Ajax->name;
$request = array(
	'tag_id' => $_POST['tag_id'],
	'news_feed_id' => $_POST['news_feed_id'],
	'disable' => 1
    );
$result = Api::call(Configure::read('API.url_newsfeedtags_disable'), $request);
if (Api::getError()) {
	AppLog::info("API.url_newsfeedtags_disable failed", __METHOD__, $param);
	return $this->Common->setFlashErrorMessage(Api::getError());
}else{
	echo 'OK';
}
exit;
