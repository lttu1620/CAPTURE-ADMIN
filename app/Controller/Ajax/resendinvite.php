<?php
AppLog::info("Resend invite email", __METHOD__);
$param = $this->data;
Api::call(Configure::read('API.url_user_activation_invite'), $param);
if (Api::getError()) {
    AppLog::info("Can not resent invite", __METHOD__, $param);
    echo __('System error, please try again');
} else {
    $this->Common->setFlashSuccessMessage(__('Invite has been resent successfully'));
}
exit;