<?php

$valid = FALSE;
$param = $this->data;
$is_public = $param['value'];

unset($param['value']);

// Check news_feed with id has tags | comment when set  [0] -> [1].
if ($is_public == 1) {
    $jsonData = array(
        'id' => $param['id'],
    );
    $result = Api::call("{$param['controller']}/isvalidpublic", array('news_feed_id' => $param['id']));
    $errors = Api::getError();
    if (!empty($errors)) {
        AppLog::warning("API error while requesting . . .", __METHOD__, $param);
        $jsonData['message'] = __("System error, please try again.");
    } elseif ($result == 0) {
        $jsonData['message'] = __("Please insert comment or tag first");
    } else {
        $valid = TRUE;
    }
} else {
    $valid = TRUE;
}

if ($valid) {
    $param['is_public'] = $is_public;
    $result = Api::call("{$param['controller']}/public", $param);
    if (!empty(Api::getError())) {
        AppLog::warning("Can not update", __METHOD__, $param);
        $jsonData = array(
            'id'      => $param['id'],
            'message' => __("System error, please try again.")
        );
        echo json_encode($jsonData);
    }
} else {
    echo json_encode($jsonData);
}