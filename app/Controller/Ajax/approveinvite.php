<?php
AppLog::info("Approve invite email", __METHOD__);
$param = $this->data;
Api::call(Configure::read('API.url_invite_add_update'), $param);
if (Api::getError()) {
    AppLog::info("Can not approve invite", __METHOD__, $param);
    echo __('System error, please try again');
} else {
    $this->Common->setFlashSuccessMessage(__('Invite has been approve successfully'));
}
exit;