<?php

$valid = FALSE;
$param = $this->data;

// Check news_feed with id has tags | comment when set  [0] -> [1].
$jsonData = array(
    'id'      => $param['news_feed_id'],
    'success' => FALSE
);
$result = Api::call("newsfeeds/isvalidpublic", array('news_feed_id' => $param['news_feed_id']));
$errors = Api::getError();
if (!empty($errors)) {
    AppLog::warning("API error while requesting . . .", __METHOD__, $param);
    $jsonData['message'] = __("System error, please try again.");
} elseif ($result == 0) {
    $jsonData['message'] = __("Please insert comment or tag first");
} else {
    $valid = TRUE;
}

if ($valid) {
    $param['start_date'] = !empty($param['start_date']) ? strtotime($param['start_date']) : "0";
    $param['finish_date'] = !empty($param['finish_date']) ? strtotime($param['finish_date']) : "0";
    $result = Api::call("newsfeeds/schedule", $param);
    $errors = Api::getError();
    if (!empty($errors)) {
        AppLog::info("Can not update", __METHOD__, $param);
        $jsonData['message'] = __("System error, please try again.");
    } else {
        $jsonData['success'] = TRUE;
    }
}

echo json_encode($jsonData);
