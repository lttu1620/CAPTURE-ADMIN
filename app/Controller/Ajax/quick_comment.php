<?php

if ($this->request->is('post')) {

    $jsonData = array(
        'id'      => $this->request->data['news_feed_id'],
        'success' => FALSE
    );

    $param = array(
        'news_feed_id' => $this->request->data['news_feed_id'],
        'user_id'     => $this->AppUI->id,
        'comment'     => 'この記事を「読んでね！」と言っています。',
    );

    $result = Api::call(Configure::read('API.url_newscomments_add'), $param);
    $errors = Api::getError();
    if (!empty($errors)) {
        AppLog::info("Can not add quick comment", __METHOD__, $param);
        $jsonData['message'] = __("System error, please try again.");
    } else {
        $jsonData['success'] = TRUE;
        $jsonData['message'] = __("Instant comment was added.");
    }
    echo json_encode($jsonData);
}
