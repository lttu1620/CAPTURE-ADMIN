<?php
/*
$this->Cookie->httpOnly = true;
$facebookInfo = $this->getParams();
$userInfo = Api::call(Configure::read('API.url_userrecruiters_login_facebook'), $facebookInfo); 
if (Api::getError()) {
    $this->Common->setFlashErrorMessage(Api::getError());
    echo $this->Auth->redirect('/login');
    exit;
}  
if ($this->createLoginSession($userInfo)) {
	$this->Cookie->write('last_login_url', "login", true, '2 weeks');
    echo $this->Auth->redirect();
}
 * */

$this->Cookie->httpOnly = true;
$params = $this->getParams();
if (isset($params['token'])) {
    $userInfo = Api::call(Configure::read('API.url_userrecruiters_login_facebook_by_token'), $params);
} else {
    $userInfo = Api::call(Configure::read('API.url_userrecruiters_login_facebook'), $params);
}
$result = array(
    'error' => 0,
    'message' => __('Login facebook success')
);
if (Api::getError()) {
    //$this->Common->setFlashErrorMessage(Api::getError());
    $result = array(
        'error' => 1010,
        'message' => __('Email is Invalid')
    );
}elseif (!$this->createLoginSession($userInfo)) {
    $result = array(
        'error' => 1010,
        'message' => __('System error, please try again')
    );
}
echo json_encode($result);
exit;