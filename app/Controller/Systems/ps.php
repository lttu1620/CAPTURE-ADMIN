<?php

$modelName = $this->System->name;

// create breadcrumb
$pageTitle = __('Process list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'search',
        'label' => __('Keyword'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right',
    ));

$param['id'] = $this->getParam('id', '');
$param['search'] = $this->getParam('search', '');
$param['all'] = $this->getParam('all', 0);
$data = Api::call(Configure::read('API.url_system_ps'), $param);
if (!empty($param['id'])) {
    if (!empty($data) && !Api::getError()) {
        $this->Common->setFlashSuccessMessage(__("Process killed successfuly"));
    } else {
        $this->Common->setFlashErrorMessage(Api::getError());
    }    
    return $this->redirect('/system/ps');
}

// create data table
$this->SimpleTable   
    ->addColumn(array(
        'id' => 'id',        
        'title' => __('ID'),      
        'width' => '50'
    ))
    ->addColumn(array(
        'id' => 'name',
        'title' => __('Name')
    ))
    ->addColumn(array(
        'title' => __('Kill'),
        'type' => 'link',
        'width' => '40',
        'onclick' => 'return killProcess({id});',
        'class' => 'btn-kill',
        'button' => true
    ))
    ->setDataset($data);  