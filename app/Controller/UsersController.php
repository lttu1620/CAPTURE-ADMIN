<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class UsersController extends AppController {

    /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author tuancd 
     * @return void
     */
    public function index() {
        include ('Users/index.php');
    }

    /**
     * profile action
     * 
     * @author tuancd 
     * @return void
     */
    public function profile($id = 0) {
        include ('Users/profile.php');
    }

    /**
     * profileinformation action
     * 
     * @author tuancd 
     * @return void
     */
    public function profileinformation($id = -1) {
        include ('Users/profileinformation.php');
    }

    /**
     * recruiterinformation action
     * 
     * @author tuancd 
     * @return void
     */
//    public function recruiterinformation($id = -1) {
//        include ('Users/recruiterinformation.php');
//    }

    /**
     * facebookinformation action
     * 
     * @author tuancd 
     * @return void
     */
    public function facebookinformation($id = -1) {
        include ('Users/facebookinformation.php');
    }

    /**
     * update action
     * 
     * @author tuancd 
     */
    public function update() {
        include ('Users/update.php');
    }

    /**
     * password action
     * 
     * @author tuancd 
     * @return void
     */
    public function password($id = 0) {
        if (empty($id)) {
            $id = $this->AppUI->id;
        }
        include ('Users/password.php');
    }

    /**
     * newsfeedfavorites action
     * 
     * @author tuancd
     * @return void 
     */
    public function newsfeedfavorites($user_id = 0) {
        include('Users/newsfeedfavorites.php');
    }

    /**
     * followCompanies action
     * 
     * @author tuancd 
     */
    public function followCompanies($id = 0)
    {
        include('Users/followcompanies.php');
    }
    
    /**
     * followcategories action
     * 
     * @author tuancd 
     * @return void
     */
    public function followcategories($user_id = 0) {
        include ('Users/followcategories.php');
    }

    /**
     * newscomments action
     * 
     * @author tuancd 
     * @return void
     */
    public function newscomments($user_id = 0) {
        include ('Users/newscomments.php');
    }
}
