<?php

/**
 * UserSettings Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class UserSettingsController extends AppController{
    public $uses = array('User', 'UserSetting');

     /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     * 
     * @author tuancd 
     * @return void
     */
    public function index($user_id = 0) {
        include ('Usersettings/index.php'); 
    }
}
