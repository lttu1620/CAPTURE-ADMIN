<?php

$modelName = $this->University->name;

// process delete / active / inactive 
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('University list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
        ))
        ->addElement(array(
            'id' => 'kana',
            'label' => __('Kana'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'id-desc' => __('ID Desc'),
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_universities_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '20'
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '40'
        ))
        ->addColumn(array(
            'id' => 'name',
            'type' => 'link',
            'title' => __('Name'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '250'
        ))
        ->addColumn(array(
            'id' => 'kana',
            'title' => __('Kana'),
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
        ))
        ->addColumn(array(
            'type' => 'link',
            'th_title' => __('Department'),
            'title' => __('View'),
            'href' => '/departments?university_id={id}',
            'width' => '120',
            'button' => true,
        ))
        ->addColumn(array(
            'type' => 'link',
            'th_title' => __('Campuse'),
            'title' => __('View'),
            'href' => '/campuses?university_id={id}',
            'width' => '120',
            'button' => true,
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 90,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
