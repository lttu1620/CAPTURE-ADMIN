<?php

// create search conditions         
$page = $this->getParam('page', 1);
$limit = Configure::read('Config.pageSize');
$modelName = $this->Company->name;

// create breadcrumb
$this->Breadcrumb->setTitle(__('Company followers'))
    ->add(array(
        'link' => '/' . $this->request->url,
        'name' => __('Company followers'),
    ));
// Get detail companyinfo
$companyinfo = Api::call(Configure::read('API.url_companies_detail'), array('id' => $id));

if (Api::getError()) {
    AppLog::info("API.url_companies_detail failed", __METHOD__, $id);
    $this->Common->handleException(Api::getError());
    return $this->Common->setFlashErrorMessage(Api::getError());
}
if (empty($companyinfo)) {
    AppLog::info("API.url_companies_detail return empty", __METHOD__, $id);
    throw new NotFoundException('Company unavailable', __METHOD__, $param);
}
$this->set('company_info', $companyinfo);

// Get follow company of user login
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['company_id'] = $id;
list($total, $data) = Api::call(Configure::read('API.url_followcompanies_list'), $param);
$this->set('followcompanies', $data);
$this->set('total', $total);
$this->set('limit', $param['limit']);

//d($data,1);
