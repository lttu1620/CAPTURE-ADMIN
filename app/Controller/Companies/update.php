<?php

$modelName = $this->Company->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add company');
$flagUserId = true;
if (!$this->AppUI->is_admin) {
    if ($user_id <= 0) {
        $id = $this->AppUI->company_id;
        $user_id = $this->AppUI->id;
        $flagUserId = false;
    }
} else {
    $user_id = $user_id == 0 ? -1 : $user_id;
}
if (!empty($id)) {
    $pageTitle = __('Edit company');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_companies_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$isAdminCompany = true;
if ($user_id > 0) {
    $param['id'] = $user_id == 0 ? -1 : $user_id;
    $param['full'] = true;
    $dataUser = Api::Call(Configure::read('API.url_users_detail'), $param);
    if (!empty($dataUser)) {
        if (!$this->AppUI->is_admin) {
            if ($this->AppUI->company_id != $dataUser['company_id']) {
                AppLog::info("User not same company", __METHOD__, $dataUser['company_id']);
                throw new NotFoundException("User not same company");
            }
        }

        $isAdminCompany = $this->AppUI->is_admin || $dataUser['recruiter_admin'] == 1 && $dataUser['is_company'] == 1;
        if(!$this->isMobile()){
            $this->set('profileTab', $this->Common->renderProfileTab($dataUser));
        }
    }
}
// Get list category for company
$_lst_category = MasterData::categories_all(array('id', 'name'));
$category_id = !empty($data[$modelName]) ? (!empty($data[$modelName]['company_category'][1]) ? $data[$modelName]['company_category'][1][0]['category_id'] : 0) : 0;
$category_name = "";
foreach ($_lst_category as $key => $value) {
    if ($category_id == $key) {
        $category_name = $value;
        break;
    }
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/companies',
        'name' => __('Company list'),
    ))
    ->add(array(
        'name' => $pageTitle,
    ));
// create update form
$commonForm = $this->UpdateForm
    ->reset()
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('Id'),
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name'),
        'required' => true,
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'kana',
        'label' => __('Kana'),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'name_english',
        'label' => __('Name english'),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'address',
        'label' => __('Address'),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'employees',
        'label' => __('Employees count'),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'establishment_date',
        'calendar' => true,
        'label' => __('Establishment date'),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'description_short',
        'type' => 'textarea',
        'rows' => '4',
        'label' => __('Short description'),
        'autocomplete' => 'off',
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'description',
        'type' => 'textarea',
        'rows' => '8',
        'autocomplete' => 'off',
        'label' => __('Description'),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'thumbnail_img',
        'type' => 'file',
        'image' => true,
        'label' => __('Thumbnail img'),
        'allowEmpty' => $isAdminCompany,
        'crop' => ($id && $isAdminCompany ? array('company_id' => $id, 'field' => 'thumbnail_img') : false),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
        'id' => 'introduction_movie',
        'label' => __('Introduction movie'),
        'readonly' => !$isAdminCompany
    ))
    ->addElement(array(
    'id' => 'category_name',
    'label' => __('Category'),
    'value' => $category_name,
    'readonly' => true,
    ))
;
//    ->addElement(array(
//        'id' => 'category_id',
//        'label' => __('Category'),
//        'options' => $_lst_category,
//        'selected' =>$category_id,
//        'empty' => Configure::read('Config.StrChooseOne'),
//        'readonly' => true //!$isAdminCompany
//    ));

if ($isAdminCompany) {
    $this->UpdateForm
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
    ));
}
$this->set('commonForm', $commonForm->get());
$this->UpdateForm->reset();

/*if ($id > 0) {
    $introWhyForm = $this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        // Introduction Why
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('Id'),
        ))
        ->addElement(array(
            'id' => 'introduction_why_txt',
            'rows' => 8,
            'label' => __('Introduction why txt'),
            'readonly' => !$isAdminCompany
        ))
        ->addElement(array(
            'id' => 'introduction_why_img1',
            'type' => 'file',
            'image' => true,
            'label' => __('introduction why image1'),
            'allowEmpty' => $isAdminCompany,
            'crop' => ($id && $isAdminCompany ? array('company_id' => $id, 'field' => 'introduction_why_img1') : false)
        ))
        ->addElement(array(
        'id' => 'introduction_why_img2',
        'type' => 'file',
        'image' => true,
        'label' => __('introduction why image2'),
        'allowEmpty' => $isAdminCompany,
        'crop' => ($id && $isAdminCompany ? array('company_id' => $id, 'field' => 'introduction_why_img2') : false)
    ));

    if ($isAdminCompany) {
        $this->UpdateForm
            ->addElement(array(
                'type' => 'submit',
                'value' => __('Save'),
                'class' => 'btn btn-primary pull-left',
        ));
    }
    $this->set('introWhyForm', $introWhyForm->get());
    $this->UpdateForm->reset();
}

if ($id > 0) {
    $introlikethisForm = $this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)

        // Introduction likethis
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('Id'),
        ))
        ->addElement(array(
            'id' => 'introduction_likethis_txt',
            'rows' => 8,
            'label' => __('Introduction likethis txt'),
            'readonly' => !$isAdminCompany
        ))
        ->addElement(array(
            'id' => 'introduction_likethis_img1',
            'type' => 'file',
            'image' => true,
            'label' => __('introduction likethis image1'),
            'allowEmpty' => $isAdminCompany,
            'crop' => ($id && $isAdminCompany ? array('company_id' => $id, 'field' => 'introduction_likethis_img1') : false)
        ))
        ->addElement(array(
        'id' => 'introduction_likethis_img2',
        'type' => 'file',
        'image' => true,
        'label' => __('introduction likethis image2'),
        'allowEmpty' => $isAdminCompany,
        'crop' => ($id && $isAdminCompany ? array('company_id' => $id, 'field' => 'introduction_likethis_img2') : false)
    ));

    if ($isAdminCompany) {
        $this->UpdateForm
            ->addElement(array(
                'type' => 'submit',
                'value' => __('Save'),
                'class' => 'btn btn-primary pull-left',
        ));
    }
    $this->set('introlikethisForm', $introlikethisForm->get());
    $this->UpdateForm->reset();
}
if ($id > 0) {
    $otherForm = $this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        // Other
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
        ))
        ->addElement(array(
            'id' => 'map_url',
            'rows' => 2,
            'label' => __('Map url'),
            'readonly' => !$isAdminCompany
        ))
        ->addElement(array(
            'id' => 'corporate_url',
            'label' => __('Corporate url'),
            'readonly' => !$isAdminCompany
        ))
        ->addElement(array(
            'id' => 'corporate_facebook',
            'label' => __('Corporate facebook'),
            'readonly' => !$isAdminCompany
        ))
        ->addElement(array(
            'id' => 'corporate_twitter',
            'label' => __('Corporate twitter'),
            'readonly' => !$isAdminCompany
        ))
        ->addElement(array(
        'id' => 'corporate_entry',
        'label' => __('Corporate entry'),
        'readonly' => !$isAdminCompany
    ));
    if ($isAdminCompany) {
        $this->UpdateForm
            ->addElement(array(
                'type' => 'submit',
                'value' => __('Save'),
                'class' => 'btn btn-primary pull-left',
        ));
    }
    $this->set('otherForm', $otherForm->get());
}*/

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (isset($model->data[$modelName]['establishment_date'])) {
            $model->data[$modelName]['establishment_date'] = strtotime($model->data[$modelName]['establishment_date']);
        }

        // Processing upload Image
        if (!empty($_FILES['data']['name'][$modelName]['thumbnail_img'])) {
            $model->data[$modelName]['thumbnail_img'] = $this->Image->uploadImage(
                "{$modelName}.thumbnail_img", 'companies'
            );
            if ($this->Image->errorMsg) {
                $this->Common->setFlashErrorMessage($this->Image->errorMsg);
                $this->redirect($this->request->here(false));
            }
        } elseif (!empty($model->data[$modelName]['thumbnail_img']['remove'])) {
            $model->data[$modelName]['thumbnail_img'] = '';
        } elseif (isset($model->data[$modelName]['thumbnail_img'])) {
            unset($model->data[$modelName]['thumbnail_img']);
        }

        // WHY
        if (!empty($_FILES['data']['name'][$modelName]['introduction_why_img1'])) {
            $model->data[$modelName]['introduction_why_img1'] = $this->Image->uploadImage(
                "{$modelName}.introduction_why_img1", 'companies'
            );
            if ($this->Image->errorMsg) {
                $this->Common->setFlashErrorMessage($this->Image->errorMsg);
                $this->redirect($this->request->here(false));
            }
        } elseif (!empty($model->data[$modelName]['introduction_why_img1']['remove'])) {
            $model->data[$modelName]['introduction_why_img1'] = '';
        } elseif (isset($model->data[$modelName]['introduction_why_img1'])) {
            unset($model->data[$modelName]['introduction_why_img1']);
        }

        if (!empty($_FILES['data']['name'][$modelName]['introduction_why_img2'])) {
            $model->data[$modelName]['introduction_why_img2'] = $this->Image->uploadImage(
                "{$modelName}.introduction_why_img2", 'companies'
            );
            if ($this->Image->errorMsg) {
                $this->Common->setFlashErrorMessage($this->Image->errorMsg);
                $this->redirect($this->request->here(false));
            }
        } elseif (!empty($model->data[$modelName]['introduction_why_img2']['remove'])) {
            $model->data[$modelName]['introduction_why_img2'] = '';
        } elseif (isset($model->data[$modelName]['introduction_why_img2'])) {
            unset($model->data[$modelName]['introduction_why_img2']);
        }

        // LIKETHIS
        if (!empty($_FILES['data']['name'][$modelName]['introduction_likethis_img1'])) {
            $model->data[$modelName]['introduction_likethis_img1'] = $this->Image->uploadImage(
                "{$modelName}.introduction_likethis_img1", 'companies'
            );
            if ($this->Image->errorMsg) {
                $this->Common->setFlashErrorMessage($this->Image->errorMsg);
                $this->redirect($this->request->here(false));
            }
        } elseif (!empty($model->data[$modelName]['introduction_likethis_img1']['remove'])) {
            $model->data[$modelName]['introduction_likethis_img1'] = '';
        } elseif (isset($model->data[$modelName]['introduction_likethis_img1'])) {
            unset($model->data[$modelName]['introduction_likethis_img1']);
        }

        if (!empty($_FILES['data']['name'][$modelName]['introduction_likethis_img2'])) {
            $model->data[$modelName]['introduction_likethis_img2'] = $this->Image->uploadImage(
                "{$modelName}.introduction_likethis_img2", 'companies'
            );
            if ($this->Image->errorMsg) {
                $this->Common->setFlashErrorMessage($this->Image->errorMsg);
                $this->redirect($this->request->here(false));
            }
        } elseif (!empty($model->data[$modelName]['introduction_likethis_img2']['remove'])) {
            $model->data[$modelName]['introduction_likethis_img2'] = '';
        } elseif (isset($model->data[$modelName]['introduction_likethis_img2'])) {
            unset($model->data[$modelName]['introduction_likethis_img2']);
        }
        $id = Api::call(Configure::read('API.url_companies_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->request->url}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }

// show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}