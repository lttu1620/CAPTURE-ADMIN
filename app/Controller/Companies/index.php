<?php

$modelName = $this->Company->name;

// process delete / active / inactive 
$this->doGeneralAction($modelName);
// Process update statistics
$this->doUpdateStatisticAction($modelName);
// create breadcrumb
$pageTitle = __('Company list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );

// create search form
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'name',
            'label' => __('Name'),
        )
    )
    ->addElement(
        array(
            'id'    => 'kana',
            'label' => __('Kana'),
        )
    )
    ->addElement(
        array(
            'id'    => 'address',
            'label' => __('Address'),
        )
    )
    ->addElement(
        array(
            'id'      => 'category_id',
            'label'   => __('Category'),
            'options' => \MasterData::categories_all(array('id', 'name')),
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'disable',
            'label'   => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => array(
                'id-asc'       => __('ID Asc'),
                'id-desc'      => __('ID Desc'),
                'name-asc'     => __('Name Asc'),
                'name-desc'    => __('Name Desc'),
                'kana-asc'     => __('Kana Asc'),
                'kana-desc'    => __('Kana Desc'),
                'address-asc'  => __('Address Asc'),
                'address-desc' => __('Address Desc'),
            ),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );
// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));

list($total, $data) = Api::call(Configure::read('API.url_companies_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(
        array(
            'id'    => 'item',
            'name'  => 'items[]',
            'type'  => 'checkbox',
            'value' => '{id}',
            'width' => '20',
        )
    )
    ->addColumn(
        array(
            'id'         => 'thumbnail_img',
            'type'       => 'image',
            'title'      => __('Image'),
            'src'        => '{thumbnail_img}',
            'image_type' => 'company',
            'width'      => '60',
        )
    )
    ->addColumn(
        array(
            'id'    => 'id',
            'type'  => 'link',
            'title' => __('ID'),
            'href'  => '/'.$this->controller.'/update/{id}',
            'width' => '30',
        )
    )
    ->addColumn(
        array(
            'id'    => 'name',
            'type'  => 'link',
            'title' => __('Name'),
            'href'  => '/'.$this->controller.'/update/{id}',
            'width' => '200',
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'kana',
            'title' => __('Kana'),
            'width' => '200',
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'address',
            'title' => __('Address'),
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'category_name',
            'title' => __('Category name'),
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'     => 'follow_count',
            'type'   => 'link',
            'title'  => __('Followers'),
            'href'   => '/'.($this->AppUI->is_admin == 1 ? 'followcompanies/index/{id}' : $this->controller.'/follower/{id}'),
            'width'  => '100',
            'button' => true,
            'empty'  => '0',
        )
    )
    ->addColumn(
        array(
            'id'     => 'created',
            'title'  => __('Created'),
            'type'   => 'date',
            'empty'  => '',
            'hidden' => true,
        )
    )
    ->addColumn(
        array(
            'type'   => 'link',
            'title'  => __('Setting'),
            'href'   => '/companysettings/index/{id}',
            'button' => true,
            'width'  => 60,
        )
    )
    ->addColumn(
        array(
            'id'     => 'disable',
            'type'   => 'checkbox',
            'title'  => __('Status'),
            'toggle' => true,
            'rules'  => array(
                '0' => 'checked',
                '1' => '',
            ),
            'empty'  => 0,
            'width'  => 90,
        )
    )
    ->addColumn(
        array(
            'type'   => 'link',
            'title'  => __('Invite'),
            'href'   => '/invites/update/{id}',
            'button' => true,
            'width'  => 70,
        )
    )
    ->setDataset($data)
    ->setMergeColumn(
        array(
            'name' => array(
                array(
                    'field'  => 'created',
                    'before' => __('Created')." : ",
                    'after'  => '',
                ),
            ),
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Invite'),
            'class' => 'btn btn-primary btn-invite',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Update statistic'),
            'class' => 'btn btn-primary btn-statistic',
            'title' => __('Reupdate comment count, like count and favorite count'),
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        )
    );
