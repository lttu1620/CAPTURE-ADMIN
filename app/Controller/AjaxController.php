<?php

App::uses('AppController', 'Controller');

/**
 * AjaxController class of Ajax Controller
 *
 * @package	Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class AjaxController extends AppController {

    /** @var string $layout layout name for view */
	public $layout = "ajax";

     /**
     * Handles user interaction of view fblogin Ajax.
     *
     * @return void
     */
	public function fblogin() {
		include ('Ajax/fblogin.php');
	}

    /**
     * Handles user interaction of view fblogintoken Ajax.
     *
     * @return void
     */
	public function fblogintoken() {
		include ('Ajax/fblogintoken.php');
	}

    /**
     * Handles user interaction of view university Ajax.
     *
     * @param integer $id ID value of university default value is 0.
     *
     * @return void
     */
	public function university($id = 0) {
		include ('Ajax/university.php');
	}

    /**
     * Handles user interaction of view likecomment Ajax.
     *
     * @param integer $comment_id ID value of news_comments, default value is 0.
     * @param integer $user_id ID value of user, default value is 0.
     * 
     * @return void
     */
	public function likecomment($comment_id = 0, $user_id = 0) {
		include ('Ajax/likecomment.php');
	}

    /**
     * Handles user interaction of view newscommentlikes Ajax.
     *
     * @param integer $id ID value of news_comments default value is 0.
     *
     * @return void
     */
	public function newscommentlikes($id = 0) {
		include ('Ajax/newscommentlikes.php');
	}

    /**
     * Handles user interaction of view autocompleteuniversity Ajax.
     *
     * @return void
     */
	public function autocompleteuniversity() {
		$search = $this->getParam('q', '');
		$data = MasterData::universities_all_key_value();
		$search = strtolower($search);

		$result = array();
		foreach ($data as $key => $value) {
			if (strpos(strtolower($value), $search) !== false) {
				$result[] = array(
					'id' => $key,
					'label' => $value
				);
			}
		}
		echo $this->getParam('callback') . '(' . json_encode($result) . ')';

		exit;
	}

    /**
     * Handles user interaction of view autocomplete Ajax.
     *
     * @return void
     */
	public function autocomplete() {
		$search = $this->getParam('q', '');
		$data = array(
			array('id' => 1, 'label' => 'Thai'),
			array('id' => 2, 'label' => 'Quan'),
			array('id' => 3, 'label' => 'Manh'),
			array('id' => 4, 'label' => 'Kino'),
			array('id' => 5, 'label' => 'Dien'),
			array('id' => 6, 'label' => 'Tu'),
			array('id' => 7, 'label' => 'Tuan'),
			array('id' => 8, 'label' => 'Truong'),
			array('id' => 9, 'label' => 'Huy'),
			array('id' => 10, 'label' => 'Thang'),
			array('id' => 11, 'label' => 'Hieu'),
		);
		$search = strtolower($search);
		$result = array();
		foreach ($data as $item) {
			if (strpos(strtolower($item['label']), $search) !== false) {
				$result[] = $item;
			}
		}
		echo $this->getParam('callback') . '(' . json_encode($result) . ')';
		exit;
	}

	public function dialog() {
		
	}

    /**
     * Handles user interaction of view deletefeedtag Ajax.
     *
     * @return void
     */
	public function deletefeedtag() {
		include ('Ajax/deletefeedtag.php');
	}

    /**
     * Handles user interaction of view showlisttag Ajax.
     *
     * @return void
     */
	public function showlisttag() {
		include ('Ajax/showlisttag.php');
	}

    /**
     * Handles user interaction of view addfeedtag Ajax.
     *
     * @return void
     */
	public function addfeedtag() {
		include ('Ajax/addfeedtag.php');
	}
    
    public function cropimagedialog() {
		
	}

    /**
     * Handles user interaction of view cropimage Ajax.
     *
     * @return void
     */
    public function cropimage() {
		include ('Ajax/cropimage.php');
	}

    /**
     * Handles user interaction of view resendregisteremail Ajax.
     *
     * @return void
     */
    public function resendregisteremail() {
        include ('Ajax/resendregisteremail.php');
    }

    /**
     * Handles user interaction of view resendforgetpassword Ajax.
     *
     * @return void
     */
    public function resendforgetpassword() {
        include ('Ajax/resendforgetpassword.php');
    }
    
    /**
     * Handles user interaction of view resendregistercompany Ajax.
     *
     * @return void
     */
    public function resendregistercompany() {
		include ('Ajax/resendregistercompany.php');
	}
    
    /**
     * Processing batch.
     *
     * @return void
     */
    public function runbatch() {
		include ('Ajax/runbatch.php');
	}
    
    /**
     * Handles user interaction of view disable Ajax.
     *
     * @return void
     */
    public function disable() {
		include ('Ajax/disable.php');
	}


    /**
     * Handles user interaction of view admin Ajax.
     *
     * @return void
     */
    public function admin() {
		include ('Ajax/admin.php');
	}  
    
    /**
     * Handles user interaction of view approved Ajax.
     *
     * @return void
     */
    public function approved() {
		include ('Ajax/approved.php');
	}

	/**
	 * Handles user interaction of view disable Ajax.
	 *
	 * @return void
	 */
	public function ispublic() {
		include ('Ajax/public.php');
	}

	/**
	 * Handles user interaction of view post public Ajax.
	 *
	 * @return void
	 */
	public function postpublic() {
		include ('Ajax/postpublic.php');
	}

	/**
	 * Handles user interaction of view tadacopy public Ajax.
	 *
	 * @return void
	 */
	public function istadacopy() {
		include ('Ajax/istadacopy.php');
	}

	/**
	 * Handles user interaction of view disable Ajax.
	 *
	 * @return void
	 */
	public function resendInvite() {
		include ('Ajax/resendinvite.php');
	}

	/**
	 * Handles user interaction of view disable Ajax.
	 *
	 * @return void
	 */
	public function approveInvite() {
		include ('Ajax/approveinvite.php');
	}

    /**
     * Handles user interaction of view disable Ajax.
     *
     * @return void
     */
    public function newsfeedschedule() {
        include ('Ajax/newsfeedschedule.php');
    }

    /**
     * Handles user interaction of view disable Ajax.
     *
     * @return void
     */
    public function quick_comment() {
        include ('Ajax/quick_comment.php');
    }
}
