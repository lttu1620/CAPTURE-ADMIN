<?php

$modelName = $this->UserSetting->name;

$this->doGeneralAction($modelName);

if (empty($user_id)) {
    AppLog::info("User not available", __METHOD__, $user_id);
    throw new NotFoundException("User not available", __METHOD__, $user_id);
}

// create breadcrumb
$this->Breadcrumb->setTitle(__('User settings'))
    ->add(array(
        'link' => $this->request->base . "/users",
        'name' => __('User list'),
    ))
    ->add(array(
        'name' => __('User settings'),
    ));

// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'name',
        'type' => 'text',
        'label' => __('Name'),
    ))
    ->addElement(array(
        'id' => 'data_type',
        'label' => __('Data type'),
        'options' => Configure::read('Config.SettingDataType'),
        'empty' => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => 'ID Asc',
            'id-desc' => 'ID Desc',
            'name-asc' => 'Name Asc',
            'name-desc' => 'Name Desc',
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right',
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['user_id'] = $user_id;
$param['disable'] = 0;
list($total, $data) = Api::call(Configure::read('API.url_usersettings_all'), $param);
if (Api::getError()) {
    AppLog::info("API.url_usersettings_all failed", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable->addColumn(array(
        'id' => 'name',
        'title' => __('Name'),
        'width' => '300',
    ))
    ->addColumn(array(
        'id' => 'description',
        'title' => __('Description'),
        'width' => '300',
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'value',
        'name' => 'value[{id}]',
        'title' => __('Value'),
        'rules' => array(
            'number' => array('type' => 'text'),
            'string' => array('type' => 'text'),
            'textarea' => array('type' => 'textarea', 'rows' => 2),
            'boolean' => array('type' => 'select', 'options' => Configure::read('Config.BooleanValue')),
            'image' => array('type' => 'image'),
            'checkbox' => array('type' => 'checkbox'),
        ),
        'value' => 'data_type',
        'empty' => ''
    ))
    ->setDataset($data)
    ->addButton(array(
        'type' => 'submit',
        'id' => 'btnSaveDataTable',
        'onclick' => 'return $(\'#dataForm\').submit();',
        'value' => __('Save setting'),
        'class' => 'btn btn-primary btn-saveTable',
    ));


// process when submit form
if ($this->request->is('post')) {
    $data = $this->request->data;
    unset($data['action']);
    unset($data['actionId']);
    // Processing data normal
    $jsonData = array();
    if (!empty($data['value'])) {
        foreach ($data['value'] as $settingId => $settingVal) {
            $jsonData[] = array(
                'id' => $settingId,
                'user_id' => $user_id,
                'setting_id' => $settingId,
                'value' => $settingVal
            );
        }
    }
    // Processing Data image    
    if (!empty($this->request->form['value'])) {
        $files = array();
        foreach ($this->request->form['value'] as $attrKey => $attrVal) {
            foreach ($attrVal as $settingId => $settingVal) {
                $files[$settingId][$attrKey] = $settingVal;
            }
        }
        if (!empty($files)) {
            foreach ($files as $settingId => $file) {
                if ($imageUrl = $this->Image->upload2($file)) {
                    $jsonData[] = array(
                        'id' => $settingId,
                        'user_id' => $user_id,
                        'setting_id' => $settingId,
                        'value' => $imageUrl
                    );
                }
            }
        }
    }
    if (empty($jsonData)) {
        return $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
    }
    $jsonData = json_encode($jsonData);
    Api::call(Configure::read('API.url_usersettings_multiupdate'), array('value' => $jsonData));
    if (Api::getError()) {
        AppLog::info("API.url_usersettings_multiupdate failed", __METHOD__, $jsonData);
        return $this->Common->setFlashErrorMessage(Api::getError());
    } else {
        $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        $this->redirect($this->request->here);
    }
}
