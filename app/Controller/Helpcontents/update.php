<?php

$modelName = $this->Helpcontent->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add help');
if (!empty($id)) {
    $pageTitle = __('Edit help');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_helpcontents_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/helpcontents',
            'name' => __('helpcontent list'),
        ))
        ->add(array(
            'name' => $pageTitle,
        ));
// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('Id'),
        ))
        ->addElement(array(
            'id' => 'title',
            'label' => __('Title'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'description',
            'type' => 'textarea',
            'escapse' => false,
            'rows' => '3',
            'label' => __('Description'),
        ))
        ->addElement(array(
            'id' => 'body',
            'type' => 'editor',
            'escapse' => false,
            'height' => '250',
            'label' => __('Body'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_helpcontents_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}