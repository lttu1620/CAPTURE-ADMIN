<?php

$modelName = $this->Presetcomment->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add presetcomment');
if (!empty($id)) {
    // call API get department detail
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_presetcomments_detail'), $param);
    // check available id
    if (empty($data[$modelName])) {
        AppLog::info("Presetcomment unavailable", __METHOD__, $param);
        throw new NotFoundException("Presetcomment unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit presetcomment');
}

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/presetcomments',
            'name' => __('Presetcomment list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('Id'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'comments',
            'label' => __('Comments'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if ($id = Api::call(Configure::read('API.url_presetcomments_addupdate'), $model->data[$modelName])) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            if (!empty($id)) {
                $this->redirect("/{$this->controller}/update/{$id}");
            }
            $this->redirect($this->request->here);
        }
    } else {
        AppLog::info("Can not update", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
} else {
    $this->data = $data;
}