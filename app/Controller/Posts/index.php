<?php

$modelName = $this->Post->name;

// Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Post list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );

// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'post_title',
            'label' => __('Title'),
        )
    )
    ->addElement(
        array(
            'id'    => 'post_description',
            'label' => __('Description'),
        )
    )
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => array(
                'id-asc'       => __('ID Asc'),
                'id-desc'      => __('ID Desc'),
                'created-asc'  => __('Date Asc'),
                'created-desc' => __('Date Desc'),
            ),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'disable',
            'label'   => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_posts_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(
        array(
            'id'    => 'image_url',
            'type'  => 'image',
            'title' => __('Image'),
            'src'   => '{image_url}',
            'width' => 60,
        )
    )
    ->addColumn(
        array(
            'id'    => 'id',
            'type'  => 'link',
            'title' => __('ID'),
            'href'  => '/'.$this->controller.'/update/{id}',
            'width' => '40',
        )
    )
    ->addColumn(
        array(
            'id'    => 'post_title',
            'type'  => 'link',
            'href'  => '/'.$this->controller.'/update/{id}',
            'title' => __('Title'),
            'empty' => '',
            'width' => 300,
        )
    )
    ->addColumn(
        array(
            'id'    => 'post_description',
            'title' => __('Description'),
            'empty' => '',
        )
    )
    /*
    ->addColumn(
        array(
            'id'     => 'short_url',
            'type'   => 'url',
            'target' => '_blank',
            'width'  => 180,
            'title'  => __('Short url'),
            'empty'  => '',
        )
    )
    * 
    */
    ->addColumn(
        array(
            'id'     => 'comments_count',
            'type'   => 'link',
            'title'  => __('Comment count'),
            'href'   => '/newsfeeds/view/{news_feed_id}',
            'width'  => 100,
            'empty'  => '0',
            'button' => true,
        )
    )
    ->addColumn(
        array(
            'id'     => 'likes_count',
            'type'   => 'link',
            'title'  => __('Like count'),
            'href'   => '/newsfeedlikes/index/{news_feed_id}',
            'width'  => 100,
            'empty'  => '0',
            'button' => true,
        )
    )
    ->addColumn(
        array(
            'id'    => 'created',
            'type'  => 'date',
            'title' => __('Created'),
            'width' => 140,
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'     => 'short_url',
            'type'   => 'url',
            'title'  => __('Short url button'),
            'width'  => 100,
            'button' => true,
            'value'  => __('Short url button'),
            'target' => '_blank',
        )
    )
    ->addColumn(
        array(
            'id'     => 'is_public',
            'type'   => 'checkbox',
            'title'  => __('Is public'),
            'toggle' => true,
            'rules'  => array(
                '1' => 'checked',
                '0' => '',
            ),
            'class'  => 'toggle-event post_public',
            'empty'  => 0,
            'width'  => 80,
        )
    )
    ->addColumn(
        array(
            'id'     => 'disable',
            'type'   => 'checkbox',
            'title'  => __('Status'),
            'toggle' => true,
            'rules'  => array(
                '0' => 'checked',
                '1' => '',
            ),
            'empty'  => 0,
            'width'  => 90,
        )
    )
    ->setDataset($data);

