<?php
$modelName = $this->Post->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add post');
if (!empty($id)) {
    $pageTitle = __('Edit post');
    $param['id'] = $id;
    $param['from_admin'] = 1;
    $data[$modelName] = Api::call(Configure::read('API.url_posts_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(
        array(
            'id'    => 'id',
            'type'  => 'hidden',
            'label' => __('ID'),
        )
    )
    ->addElement(
        array(
            'id'    => 'news_feed_id',
            'type'  => 'hidden',
            'label' => __('News feed Id'),
        )
    )
    ->addElement(
        array(
            'id'       => 'post_title',
            'label'    => __('Title'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'post_description',
            'label'    => __('Description'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'post_content',
            'label'    => __('Content'),
            'type'     => 'editor',
            'escapse'  => false,
            'height'   => '250',
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'         => 'image_url',
            'type'       => 'file',
            'image'      => true,
            'label'      => __('Post image'),
            'allowEmpty' => true,
        )
    )
    ->addElement(
        array(
            'id'      => 'is_public',
            'label'   => __('Is public'),
            'options' => Configure::read('Config.BooleanValue'),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Cancel'),
            'class'   => 'btn btn-primary pull-left',
            'onclick' => 'return back();',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Preview'),
            'class'   => 'btn btn-primary pull-left preview',
            'onclick' => 'return false;',
        )
    );

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $model->data[$modelName]['image_url'] = $this->Image->uploadImage("{$modelName}.image_url");
        } elseif (isset($model->data[$modelName]['image_url'])) {
            unset($model->data[$modelName]['image_url']);
        }
        if (!empty($id)) {
            $id = Api::call(Configure::read('API.url_posts_update'), $model->data[$modelName]);
        } else {
            $id = Api::call(Configure::read('API.url_posts_add'), $model->data[$modelName]);
        }
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        AppLog::info("Can not add or update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}