<?php

$modelName = $this->Invite->name;
$model = $this->{$modelName};

// Create breadcrumb 
$pageTitle = __('Invite recruiter');
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(
        array(
            'link' => $this->request->base.'/invites',
            'name' => __('Invited list'),
        )
    )
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$listCompany = MasterData::companies_all();
$listCompany = $this->Common->arrayKeyValue($listCompany, 'id', 'name');
$this->UpdateForm->setModelName($modelName);
if ($this->AppUI->is_admin == 1) {
    $this->UpdateForm->addElement(
        array(
            'id'       => 'company_id',
            'label'    => __('Company'),
            'options'  => $listCompany,
            'selected' => $id ? $id : 0,
            'empty'    => Configure::read('Config.StrChooseOne'),
            'required' => true,
        )
    );
} elseif ($this->AppUI->recruiter_admin == 1) {
    $this->UpdateForm->addElement(
        array(
            'id'       => 'company_id',
            'label'    => __('Company Id'),
            'value'    => $this->AppUI->company_id,
            'type'  => 'hidden',
        )
    );
}

$this->UpdateForm->addElement(
    array(
        'id'       => 'email',
        'label'    => __('Email'),
        'required' => true,
    )
)
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Invite'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Cancel'),
            'class'   => 'btn btn-primary pull-left',
            'onclick' => 'return back();',
        )
    );

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validate($data)) {
        if ($this->AppUI->is_admin == 1) {
            $param['admin_id'] = $this->AppUI->id;
            $param['regist_type'] = 'recruiter_admin';
        } else {
            $param['user_id'] = $this->AppUI->user_id;
            $param['regist_type'] = 'recruiter_member';
        }
        $param['email'] = $data[$modelName]['email'];
        $param['company_id'] = $data[$modelName]['company_id'];
        $invite = Api::call(Configure::read('API.url_user_activation_invite'), $param);
        if (Api::getError()) {
            AppLog::info("API.url_invite_add_update failed", __METHOD__, $param);
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Recruiter invited successfully'));
            $this->redirect("/invites/update");
        }
    } else {
        AppLog::info("Can not invite recruiter", __METHOD__, $data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}