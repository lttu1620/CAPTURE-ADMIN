<?php

$modelName = $this->Invite->name;
// General action disable/enable
$this->doGeneralAction($modelName);
// create breadcrumb
$pageTitle = __('Invited list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );

$sortOptions = array(
    'id-asc'     => __('ID Asc'),
    'id-desc'    => __('ID Desc'),
    'email-asc'  => __('Email Asc'),
    'email-desc' => __('Email Desc'),
);

// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'user_id',
            'type'  => 'text',
            'label' => __('User Id'),
        )
    )
    ->addElement(
        array(
            'id'    => 'email',
            'label' => __('Email'),
        )
    )
    ->addElement(
        array(
            'id'      => 'disable',
            'label'   => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => $sortOptions,
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );

$param = $this->getParams(
    array(
        'page'  => $this->getParam('page', 1),
        'limit' => Configure::read('Config.pageSize'),
    )
);
if ($this->AppUI->is_admin == 1) {
    $param['admin_id'] = $this->AppUI->id;
} else {
    $param['user_id'] = $this->AppUI->user_id;
}
list($total, $data) = Api::call(Configure::read('API.url_user_activation_list_invite'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addcolumn(
        array(
            'id'    => 'company_name',
            'title' => __('Company'),
            'value' => '{company_name}',
            'width' => 30,
        )
    )
    ->addColumn(
        array(
            'id'    => 'email',
            'title' => __('Email'),
            'width' => 150,
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'created',
            'title' => __('Created'),
            'type'  => 'date',
            'width' => '120',
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'      => 'disable',
            'type'    => 'link',
            'title'   => __('Status'),
            'button'  => true,
            'width'   => 150,
            'rules'   => array(
                '0' => __('Resend'),
                '1' => __('Approved'),
            ),
            'onclick' => 'invite(\'{email}\', {user_id}, {admin_id}, {company_id}, \'{regist_type}\', {disable}); return false;',
        )
    )
    ->setDataset($data)
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Invite'),
            'class' => 'btn btn-primary btn-invite',
        )
    );