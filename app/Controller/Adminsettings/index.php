<?php

$modelName = $this->AdminSetting->name;
$model = $this->{$modelName};
$this->doGeneralAction($modelName);

if (empty($admin_id)) {
    AppLog::info("Admin not available", __METHOD__, $admin_id);
    throw new NotFoundException("Admin not available", __METHOD__, $admin_id);
}

// create breadcrumb
$this->Breadcrumb->setTitle(__('Admin settings'))
    ->add(array(
        'link' => '/' . $this->request->url,
        'name' => __('Admin settings'),
    ));

// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name')
    ))
    ->addElement(array(
        'id' => 'data_type',
        'label' => __('Data type'),
        'options' => Configure::read('Config.SettingDataType'),
        'empty' => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => 'ID Asc',
            'id-desc' => 'ID Desc',
            'name-asc' => 'Name Asc',
            'name-desc' => 'Name Desc',
        ),
        'empty' => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
        'onchange' => 'javascript: $(\'#btnSearch\').click();',
        //'selected' => $limit
    ))
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right',
    ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['admin_id'] = $admin_id;
$param['disable'] = 0;
list($total, $data) = Api::call(Configure::read('API.url_adminsettings_all'), $param);
if (Api::getError()) {
    AppLog::info("API.url_adminsettings_all failed", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(array(
        'id' => 'name',
        'title' => __('Name'),
        'width' => '300',
    ))
    ->addColumn(array(
        'id' => 'description',
        'title' => __('Description'),
        'width' => '300',
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'value',
        'name' => 'value[{setting_id}]',
        'title' => __('Value'),
        'rules' => array(
            'number' => array('type' => 'text'),
            'string' => array('type' => 'text'),
            'textarea' => array('type' => 'textarea', 'rows' => 2),
            'boolean' => array('type' => 'select', 'options' => Configure::read('Config.BooleanValue')),
            'image' => array('type' => 'image'),
            'checkbox' => array('type' => 'checkbox'),
        ),
        'value' => 'data_type',
        'empty' => ''
    ))
    ->setDataset($data)
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Save setting'),
        'class' => 'btn btn-primary btn-saveTable',
    ));


// process when submit form
if ($this->request->is('post')) {
    $data = $this->request->data;
    unset($data['action']);
    unset($data['actionId']);
    if (empty($data)) {
        $this->Common->setFlashErrorMessage(__('Data is empty'));
        return;
    }
    // Processing data normal
    $jsonData = array();
    if (!empty($data['value'])) {
        foreach ($data['value'] as $settingId => $settingVal) {
            $jsonData[] = array(
                'admin_id' => $admin_id,
                'setting_id' => $settingId,
                'value' => $settingVal
            );
        }
    }
    // Processing Data image    
    if (!empty($this->request->form['value'])) {
        $files = array();
        foreach ($this->request->form['value'] as $attrKey => $attrVal) {
            foreach ($attrVal as $settingId => $settingVal) {
                $files[$settingId][$attrKey] = $settingVal;
            }
        }
        if (!empty($files)) {
            foreach ($files as $settingId => $file) {
                if ($imageUrl = $this->Image->upload2($file)) {
                    $jsonData[] = array(
                        'admin_id' => $admin_id,
                        'setting_id' => $settingId,
                        'value' => $imageUrl
                    );
                }
            }
        }
    }
    $jsonData = json_encode($jsonData);
    if (Api::call(Configure::read('API.url_adminsettings_multiupdate'), array('value' => $jsonData))) {
        if (Api::getError()) {
            $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            $this->redirect($this->request->here);
        }
    } else {
        AppLog::info("Can not update", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
    }
}
