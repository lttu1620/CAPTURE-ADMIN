<?php

$modelName = $this->Subcategory->name;

// Process disable/enable
$this->doGeneralAction($modelName);
$listCategory = array();

// Get list category
$listCategory = MasterData::categories_all();
$listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');

// create breadcrumb
$pageTitle = __('Subcategory list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
        ))
        ->addElement(array(
            'id' => 'category_id',
            'label' => __('Category Name'),
            'options' => $listCategory,
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'id-desc' => __('ID Desc'),
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_subcategories_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '20'
        ))
        ->addColumn(array(
            'id' => 'image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{image_url}',
            'width' => '60'
        ))
        ->addcolumn(array(
            'id' => 'id',
            'title' => __('ID'),
            'value' => '{id}',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'subcategory_name',
            'title' => __('Name'),
            'value' => 'name',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '250'
        ))
        ->addColumn(array(
            'id' => 'category_name',
            'title' => __('Category name'),
        ))
        ->addColumn(array(
            'type' => 'link',
            'th_title' => __('Followers'),
            'title' => __('View'),
            'href' => '/' . $this->controller . '/follower/{id}',
            'width' => '100',
            'button' => true,
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 90,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
