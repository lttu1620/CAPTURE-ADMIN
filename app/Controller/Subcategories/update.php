<?php

$modelName = $this->Subcategory->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Subcategory');

if (!empty($id)) {
    $pageTitle = __('Edit Subcategory');
    $cropImageInfo = array(
        'subcategory_id' => $id,
        'field' => 'image_url',
    );
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_subcategories_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$listCategory = MasterData::categories_all();
$listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');
$this->setPageTitle($pageTitle);
//Create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)->add(array(
            'link' => $this->request->base . '/subcategories',
            'name' => __('Subcategory list'),
        ))
        ->add(array(
            'name' => $pageTitle
        ));

//Create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('Id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'category_id',
            'label' => __('Categories'),
            'options' => $listCategory,
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'image_url',
            'type' => 'file',
            'image' => true,
            'label' => __('Image'),
            'required' => true,
            'crop' => $cropImageInfo,
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

//Process when submit form
if ($this->request->is('post')) { //p($this->getData($modelName), 1);
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $model->data[$modelName]['image_url'] = $this->Image->uploadImage(
                "{$modelName}.image_url", 'subcategories'
            );
        } elseif (isset($model->data[$modelName]['image_url']['remove'])) {
            $model->data[$modelName]['image_url'] = '';
        } else {
            unset($model->data[$modelName]['image_url']);
        }
        $id = $id = Api::call(Configure::read('API.url_subcategories_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}
