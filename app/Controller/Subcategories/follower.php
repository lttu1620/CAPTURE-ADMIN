<?php

$modelName = $this->Subcategory->name;
// process delete / active / inactive 
$this->doGeneralAction($modelName);
$isAdmin = $this->AppUI->is_admin == 1;

if ($isAdmin) {
    $listSubcategory = MasterData::subcategories_all();
    $listSubcategory = $this->Common->arrayKeyValue($listSubcategory, 'id', 'name');
}


// create breadcrumb
$this->Breadcrumb->setTitle(__('Subcategory follower list'))
        ->add(array(
            'link' => '/' . $this->request->url,
            'name' => __('Subcategory follower list'),
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Username'),
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname'),
        ));
if ($isAdmin) {
    $this->SearchForm
            ->addElement(array(
                'id' => 'subcategory_id',
                'label' => __('Subcategory'),
                'options' => $listSubcategory,
                'empty' => Configure::read('Config.StrChooseOne'),
                'autocomplete_combobox' => true,
    ));
}
$this->SearchForm->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => Configure::read('Config.StrAll')
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'user_name-asc' => 'Name Asc',
                'user_name-desc' => 'Name Desc',
                'user_nickname-asc' => 'Nickname Asc',
                'user_nickname-desc' => 'Nickname Desc',
                'email-asc' => 'Email Asc',
                'email-desc' => 'Email Desc',
                'updated-asc' => 'Follow date Asc',
                'updated-desc' => 'Follow date Desc',
                'subcategory_name-asc' => 'Subcategory name Asc',
                'subcategory_name-desc' => 'Subcategory name Desc',
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if (!$isAdmin) {
    $param['subcategory_id'] = $id;
}
list($total, $data) = Api::call(Configure::read('API.url_followsubcategories_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

if ($isAdmin) {
    $this->SimpleTable->addColumn(array(
        'id' => 'user_name',
        'title' => __('Username'),
        'width' => '200'
    ));
} else {
    $this->SimpleTable->addColumn(array(
        'id' => 'user_name',
        'title' => __('Username'),
    ));
}
$this->SimpleTable->addColumn(array(
            'id' => 'user_nickname',
            'title' => __('Nickname'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
            'width' => '200'
        ));
if ($isAdmin) {
    $this->SimpleTable->addColumn(array(
        'id' => 'subcategory_name',
        'title' => __('Subcategory name'),
    ));
}
$this->SimpleTable->addColumn(array(
            'id' => 'updated',
            'title' => __('Follow date'),
            'width' => '120'
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'link',
            'title' => __('Status'),
            'rules' => array(
                '0' => $this->SimpleTable->enabledTemplate(),
                '1' => $this->SimpleTable->disabledTemplate()
            ),
            'onclick' => 'return disableItem({id}, {disable});',
            'empty' => 0,
            'width' => '80'
        ))
        ->setDataset($data);
