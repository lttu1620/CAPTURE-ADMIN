<?php

/**
 * FollowcategorylogsController class of Followcategorylogs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class FollowcategorylogsController extends AppController {

    /**
     * Initializes components for FollowcategorylogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Followcategorylogs.
     * 
     * @return void
     */
    public function index() {
        include ('Followcategorylogs/index.php');
    }

}
