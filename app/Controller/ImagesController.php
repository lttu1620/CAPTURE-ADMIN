<?php

App::uses('AppController', 'Controller');

/**
 * ImagesController class of Images Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class ImagesController extends AppController {

     /** @var string $layout layout Name */
    public $layout = "ajax";

    /**
     * Initializes components for HelpcontentsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Helpcontents.
     * 
     * @param string $info Infomation of Image (Jsondecode [id element and url of image]).
     * @return void
     */
    public function crop($info = null) {
        if (empty($info) && !empty($this->data['info'])) {
            $info = $this->data['info'];
        }
        if (empty($info)) {
            AppLog::info("Parameters unavailable", __METHOD__, null);
        }
        $this->set('info', $info);
        $info = json_decode(base64_decode($info), true);
        // default is field image_url
        if (empty($info['field'])) {
            $info['field'] = 'image_url';
        }
        // for user and recruiter
        if (isset($info['user_id'])) {
            if ($info['field'] == 'image_url') {
                $param['id'] = $info['user_id'];
                $detail = Api::Call(Configure::read('API.url_users_detail'), $param);
            } elseif ($info['field'] == 'thumbnail_img') {
                $param['user_id'] = $info['user_id'];
                $param['unauthorize'] = true;
                $detail = Api::Call(Configure::read('API.url_recruiters_detail'), $param);
            }
        }

        // for company 
        if (isset($info['company_id'])) {
            $param['id'] = $info['company_id'];
            $detail = Api::Call(Configure::read('API.url_companies_detail'), $param);
        }

        // for Newsfeed
        if (isset($info['newsfeed_id'])) {
            if ($info['field'] == 'image_url') {
                $param['id'] = $info['newsfeed_id'];
                $detail = Api::Call(Configure::read('API.url_newsfeeds_detail'), $param);
            }
        }

        // for Newssite
        if (isset($info['newssite_id'])) {
            if ($info['field'] == 'thumbnail_img') {
                $param['id'] = $info['newssite_id'];
                $detail = Api::Call(Configure::read('API.url_newssites_detail'), $param);
            }
        }
        // for Subcategory
        if (isset($info['subcategory_id'])) {
            if ($info['field'] == 'image_url') {
                $param['id'] = $info['subcategory_id'];
                $detail = Api::Call(Configure::read('API.url_subcategories_detail'), $param);
            }
        }
        // for Subcategory
        if (isset($info['category_id'])) {
            if ($info['field'] == 'image_url') {
                $param['id'] = $info['category_id'];
                $detail = Api::Call(Configure::read('API.url_categories_detail'), $param);
            }
        }
        if (!empty($detail[$info['field']])) {
            $info['image_url'] = $detail[$info['field']];
        } else {
            echo __('Can\'t drop image !');
            exit;
        }

        if (empty($info['image_url'])) {
            $result = array(
                'status' => 400,
                'message' => __('Parameter is invalid'),
            );
            echo $this->getParam('callback') . '(' . json_encode($result) . ')';
            exit;
        }
       /* $this->AppHtml->script('/adminlte/js/plugins/imgareaselect/jquery.imgareaselect.pack.js');
        $this->AppHtml->css('imgareaselect.css');*/
        $this->set('imageUrl', $detail[$info['field']]);

        // process when submit form 
        if ($this->request->is('post')) {
            $data = array_merge($this->data, $info);
            $result = Api::call(Configure::read('API.url_images_crop'), $data);
            if ($result == false && !Api::getError()) {
                $result = array(
                    'status' => 400,
                    'message' => __('System error, pls try again'),
                );
            } else {
                $result = array(
                    'status' => 200,
                    'element' => $info['field'],
                    'message' => $result//__('Image cropped successfuly'),
                );
            }
            echo $this->getParam('callback') . '(' . json_encode($result) . ')';
            exit;
        }
    }

    public function upload() {
        $param['id'] = $this->data['id'];
        $newsfeed = Api::call(Configure::read('API.url_newsfeeds_detail'), $param);

        $url = $this->Image->uploadImageBase64($this->data['imageData']);
        $newsfeed['image_url'] = $url;
        unset($newsfeed['detail_url']);
        $result = Api::call(Configure::read('API.url_newsfeeds_addupdate'), $newsfeed);
        echo json_encode($url);
    }

    public function get_image() {
        $getInfo = getimagesize($this->request->query['img']);
        header('Content-type: ' . $getInfo['mime']);
        readfile($this->request->query['img']);
    }

}
