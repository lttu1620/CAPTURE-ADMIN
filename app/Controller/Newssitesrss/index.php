<?php

$modelName = $this->Newssitesrss->name;

// Process disable/enable
$this->doGeneralAction($modelName);

// Get list newssites
$listNewssite = MasterData::news_sites_all();
$listNewssite = $this->Common->arrayKeyValue($listNewssite, 'id', 'name');

// create breadcrumb
$pageTitle = __('Newssitesrss list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));
// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('RSS name'),
        ))
        ->addElement(array(
            'id' => 'news_site_id',
            'label' => __('Site Name'),
            'options' => $listNewssite,
            'empty' => __('All'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => __('All'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'id-desc' => __('ID Desc'),
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_newssitesrss_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '20'
        ))
        ->addcolumn(array(
            'id' => 'id',
            'title' => __('ID'),
            'value' => '{id}',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'name',
            'title' => __('Name'),
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '300',
        ))
        ->addcolumn(array(
            'id' => 'url',
            'title' => __('Url'),
            'value' => '{url}',
        ))
        ->addColumn(array(
            'id' => 'news_site_name',
            'title' => __('Site name'),
            'width' => '300',
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 90,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
