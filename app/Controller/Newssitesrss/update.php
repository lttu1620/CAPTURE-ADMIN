<?php

$modelName = $this->Newssitesrss->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add news site rss');

if (!empty($id)) {
    $pageTitle = __('Edit news site rss');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_newssitesrss_detail'), $param);
    $this->Common->handleException(Api::getError());
}

$listNewssite = array();
// Get list newssites
$listresult = Api::call(Configure::read('API.url_newssites_all'));

foreach ($listresult as $item) {
    $listNewssite[$item['id']] = (string) $item['name'];
}
$this->setPageTitle($pageTitle);
//Create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)->add(array(
            'link' => $this->request->base . '/newssitesrss',
            'name' => __('newssitesrss list'),
        ))
        ->add(array(
            'name' => $pageTitle
        ));

//Create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('ID'),
        ))
        ->addElement(array(
            'id' => 'news_site_id',
            'label' => __('News sites'),
            'options' => $listNewssite,
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true,
            'empty' => ''
        ))
        ->addElement(array(
            'id' => 'description',
            'type' => 'textarea',
            'escape' => false,
            'rows' => '8',
            'label' => __('Description'),
            'required' => true,
            'empty' => ''
        ))
        ->addElement(array(
            'id' => 'url',
            'label' => __('Url'),
            'required' => true,
            'empty' => ''
        ))
        ->addElement(array(
            'id'=>'tag_item_title',
            'label'=>__('Tag item title'),
        ))
        ->addElement(array(
            'id'=>'tag_item_link',
            'label'=>__('Tag item link'),
        ))
        ->addElement(array(
            'id'=>'tag_item_description',
            'label'=>__('Tag item description'),
        ))
        ->addElement(array(
            'id'=>'tag_item_date',
            'label'=>__('Tag item date'),
        ))
        ->addElement(array(
            'id'=>'tag_item_image',
            'label'=>__('Tag item Image'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => "return back();"
        ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_newssitesrss_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}
