<?php

/**
 * Pushmessageopenlog controller
 * 
 * @package Controller
 * @created 2014-12-18
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class PushmessageopenlogsController extends AppController {

    public $uses = array('User','Pushmessageopenlog');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Index action
     * 
     * @author Truongnn 
     * @return void
     */
    public function index() {
        include ('Pushmessageopenlogs/index.php');
    }

}
