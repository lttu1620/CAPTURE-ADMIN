<?php

$modelName = $this->Pushmessage->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Push message');
$send_reservation_time = '';
if (!empty($id)) {
    $pageTitle = __('Edit push message');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_pushmessages_detail'), $param);
    if(!empty($data[$modelName])){
        if(!empty($data[$modelName]['send_reservation_date'])){
            $send_reservation_time = date('G:i', $data[$modelName]['send_reservation_date']);
        }
    }
    $this->Common->handleException(Api::getError());
}

$this->setPageTitle($pageTitle);

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/pushmessages',
            'name' => __('push message list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('Id'),
        ))
        ->addElement(array(
            'id' => 'company_id',
            'type' => 'hidden',
            'label' => __('Company id'),
        ))
        ->addElement(array(
            'id' => 'send_reservation_date',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Reservation date'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'send_reservation_time',
            'label' => __('Reservation time'),
            'div' => array('class' => 'bootstrap-timepicker'),
            'before' => '<div class="form-group">',
            'between' => '<div class="input-group">',
            'after' => '</div></div>',
            'timepicker' => true,
            'value' => $send_reservation_time,
            'required' => true
        ))
        ->addElement(array(
            'id' => 'message',
            'label' => __('Message'),
            'type' => 'textarea',
            'rows' => '3',
            'required' => true
        ))
        ->addElement(array(
            'id' => 'android_icon',
            'label' => __('Android icon'),
            'options' => Configure::read('Config.andriodIcon'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'ios_sound',
            'label' => __('IOS sound'),
            'options' => Configure::read('Config.BooleanValue'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));
// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $dateTime = $model->data[$modelName]['send_reservation_date'] . ' ' . $model->data[$modelName]['send_reservation_time'];
        $model->data[$modelName]['send_reservation_date'] = strtotime($dateTime); 
        unset($model->data[$modelName]['send_reservation_time']);
        $id = Api::call(Configure::read('API.url_pushmessages_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}