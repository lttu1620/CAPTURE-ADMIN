<?php

$modelName = $this->Newscommentlike->name;

//General action model
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('News comment like list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// Create search form 
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'user_name',
            'type' => 'text',
            'label' => __('Username'),
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email')
        ))
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'email-asc' => __('Email Asc'),
                'email-desc' => __('Email Desc'),
                'like_count-asc' => __('Clap count Asc'),
                'like_count-desc' => __('Clap count Desc'),
                'created-asc' => __('Comment time Asc'),
                'created-desc' => __('Comment time Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

// call api to query data
$param = $this->getParams(array(
        'news_comment_id' => $news_comment_id,
        'disable' => 0, 
        'page' => 1, 
        'limit' => Configure::read('Config.pageSize')
    )
);
list($total, $data) = Api::call(Configure::read('API.url_newscommentlikes_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable
        ->addColumn(array(
            'id' => 'image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{image_url}',
            'image_type' => 'user',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'display_username',
            'title' => __('Username'),
            'width' => 120,
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
            'width' => '250',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Date'),
            'type' => 'date',
            'width' => '150'
        ))
        ->addColumn(array(
            'id' => 'like_count',
            'title' => __('Clap count'),
            'width' => '100'
        ))
        ->setDataset($data);


