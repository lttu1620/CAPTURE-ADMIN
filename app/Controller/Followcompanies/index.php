<?php

if (!$this->AppUI->is_admin) {
    return;
}
$modelName = $this->Followcompany->name;
// process disable / enable
$this->doGeneralAction($modelName);

// create breadcrumb
$this->Breadcrumb->setTitle(__('Company followers'))
    ->add(array(
        'link' => '/' . $this->request->url,
        'name' => __('Company followers'),
    ));

$sortOptions = array(
    'name-asc' => 'Username Asc',
    'name-desc' => 'Username Desc',
    'nickname-asc' => 'Nickname Asc',
    'nickname-desc' => 'Nickname Desc',
    'updated-asc' => 'Date Asc',
    'updated-desc' => 'Date Desc',
    'company_name-asc' => 'Company name Asc',
    'company_name-desc' => 'Company name Desc'
);
$listCompany = MasterData::companies_all();
$listCompany = $this->Common->arrayKeyValue($listCompany, 'id', 'name');
// create search form
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'name',
        'label' => __('Username'),
    ))
    ->addElement(array(
        'id' => 'nickname',
        'label' => __('Nickname'),
    ))
    ->addElement(array(
        'id' => 'company_id',
        'label' => __('Company'),
        'options' => $listCompany,
        'empty' => Configure::read('Config.StrAll'),
        'autocomplete_combobox' => true,
    ))
    ->addElement(array(
        'id' => 'email',
        'label' => __('Email'),
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => array(
            0 => __('Followed'),
            1 => __('Unfollowed')
        ),
        'empty' => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => $sortOptions,
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right',
    ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['company_id'] = $id; 
//d($param,1);
list($total, $data) = Api::call(Configure::read('API.url_followcompanies_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(array(
        'id' => 'image_url',
        'type' => 'image',
        'title' => __('Image'),
        'src' => '{image_url}',
        'width' => '80'
    ))
    ->addColumn(array(
        'id' => 'user_name',
        'title' => __('Username'),
        'width' => '180'
    ))
    ->addColumn(array(
        'id' => 'user_nickname',
        'title' => __('Nickname'),
    ))
    ->addColumn(array(
        'id' => 'email',
        'title' => __('Email'),
    ))
    ->addColumn(array(
        'id' => 'company_name',
        'title' => __('Company name'),
        'width' => '400'
    ))
    ->addColumn(array(
        'id' => 'updated',
        'type' => 'date',
        'title' => __('Date'),
        'width' => '150',
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'link',
        'title' => __('Status'),
        'rules' => array(
            '1' => $this->SimpleTable->disabledTemplate(__('Unfollowed')),
            '0' => $this->SimpleTable->enabledTemplate(__('Followed'))
        ),
        'onclick' => 'return disableItem ({id},{disable});',
        'empty' => 0,
        'width' => '90'
    ))
    ->setDataset($data);

if (!$this->AppUI->is_admin) {
    $this->SimpleTable->removeButton('btn-disable');
    $this->SimpleTable->removeButton('btn-enable');
    $this->SimpleTable->updateColumnAttr('disable', 'type', '');
}