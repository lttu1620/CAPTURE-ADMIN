<?php

$this->AppHtml->css('morris/morris.css');
$this->AppHtml->script('plugins/morris/morris.min.js');
$this->AppHtml->script('plugins/hightchart/highcharts.js');

// create breadcrumb
$pageTitle = __('News feed favorite chart');
$this->Breadcrumb->setTitle($pageTitle)->add(array(
    'name' => $pageTitle,
));

$param = $this->getParams(array(
        'type' => 'line',
        'mode' => 'day',
    ));
if (!isset($param['date_from'])) {
    $param['date_from'] = date('Y-m-d', strtotime('last month'));
    $this->setParam('date_from', $param['date_from']);
}
if (!isset($param['date_to'])) {
    $param['date_to'] = date('Y-m-d');
    $this->setParam('date_to', $param['date_to']);
}
// create search form 
$this->SearchForm
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'date_from',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date from'),
        'value' => $param['date_from']
    ))
    ->addElement(array(
        'id' => 'date_to',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date to'),
        'value' => $param['date_to']
    ))
    ->addElement(array(
        'id' => 'type',
        'label' => __('Chart type'),
        'options' => Configure::read('Config.searchChartType'),
    ))
    ->addElement(array(
        'id' => 'mode',
        'label' => __('View mode'),
        'options' => Configure::read('Config.searchChartMode'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$result = Api::call(Configure::read('API.url_reports_newsfeed_favorite'), $param);
if (Api::getError()) {
    return $this->Common->handleException(Api::getError()); 
}
switch ($param['mode']) {
    case 'week':
        $result = $this->Common->weekChartData(array(
            'data' => $result,
            'date_field' => 'date',
            'count_field' => array('favorite_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        break;
    case 'month' :
        $result = $this->Common->monthChartData(array(
            'data' => $result,
            'date_field' => 'date',
            'count_field' => array('favorite_count'),
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        break;  
}
$chart = $this->Chart->render(array(
    'id' => 'chart',
    'type' => $param['type'],
    'title' => __('News feed favorite report'),
    'data' => $result,
    'x' => 'date',
    'y' => array(
        'favorite_count' => __('Favorite')
    )
));
$this->set('chart', $chart);