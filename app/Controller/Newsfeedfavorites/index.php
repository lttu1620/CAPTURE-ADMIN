<?php

$modelName = $this->Newsfeedfavorite->name;
//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('News feed favorites list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// Create search form 
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'user_name',
            'label' => __('Username')
        ))
        ->addElement(array(
            'id' => 'user_nickname',
            'label' => __('Nickname')
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email')
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => array(
                0 => __('Favorite'),
                1 => __('Unfavorite')
            ),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'name-asc' => __('Username Asc'),
                'name-desc' => __('Username Desc'),
                'nickname-asc' => __('Nickname Asc'),
                'nickname-desc' => __('Nickname Desc'),
                'created-asc' => __('Date Asc'),
                'created-desc' => __('Date Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['news_feed_id'] = $feedId;
list($total, $data) = Api::call(Configure::read('API.url_newsfeedfavorites_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
        ->addColumn(array(
            'id' => 'image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{image_url}',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'user_name',
            'title' => __('Username'),
            'width' => 200
        ))
        ->addColumn(array(
            'id' => 'user_nickname',
            'title' => __('Nickname'),
            'width' => 200
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Date'),
            'type' => 'date',
            'width' => 120
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'link',
            'title' => __('Status'),
            'rules' => array(
                '0' => $this->SimpleTable->enabledTemplate('Favorite'),
                '1' => $this->SimpleTable->disabledTemplate('Unfavorite')
            ),
            'onclick' => 'return disableItem ({id},{disable});',
            'empty' => 0,
            'width' => '100'
        ))
        ->setDataset($data);
