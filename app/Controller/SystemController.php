<?php

App::uses('AppController', 'Controller');

/**
 * System Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class SystemController extends AppController
{
    public $uses = array('User', 'System');

    /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * deletecache action
     * 
     * @author tuancd
     * @return void 
     */
    public function deletecache() {
        $files = array();
        $files = array_merge($files, glob(CACHE . '*')); // remove cached data
        $files = array_merge($files, glob(CACHE . 'css' . DS . '*')); // remove cached css
        $files = array_merge($files, glob(CACHE . 'js' . DS . '*'));  // remove cached js           
        $files = array_merge($files, glob(CACHE . 'models' . DS . '*'));  // remove cached models           
        $files = array_merge($files, glob(CACHE . 'persistent' . DS . '*'));  // remove cached persistent           
        foreach ($files as $f) {
            if (is_file($f)) {
                unlink($f);
            }
        }        
        Api::call(Configure::read('API.url_system_deletecache'));        
        $this->Common->setFlashSuccessMessage(__('Cacche deleted successfuly'));
        return $this->redirect($this->request->referer());
    }
    
    /**
     * runbatch action
     * 
     * @author tuancd
     * @return void 
     */
    public function runbatch() {
        $this->layout = "ajax";
        Api::call(Configure::read('API.url_system_runbatch'));
        if (Api::getError()) {
            AppLog::info("Can not run batch", __METHOD__, Api::getError());
            $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Batch started successfuly'));
        }
        return $this->redirect($this->request->referer());
    }

    /**
     * ps action
     * 
     * @author tuancd
     * @return void 
     */
    public function ps()
    {
        include('Systems/ps.php');
    }
}
