<?php

$modelName = $this->UserRecruiter->name;
$model = $this->{$modelName};
// Create breadcrumb 
$pageTitle = __('Register recruiter');
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/userrecruiters',
        'name' => __('Recruiter list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

$companyList = Api::call(Configure::read('API.url_companies_all'), array('disable' => 0));
$nameList = $this->Common->arrayKeyValue($companyList, 'id', 'name');
$this->UpdateForm->setModelName($modelName)
    ->addElement(array(
        'id' => 'email',
        'label' => __('Email'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'password',
        'type' => 'password',
        'label' => __('Password'),
        'autocomplete' => 'off',
        'required' => true
    ))
    ->addElement(array(
        'id' => 'password_confirm',
        'type' => 'password',
        'label' => __('Confirm password'),
        'autocomplete' => 'off',
        'required' => true
    ))
    ->addElement(array(
        'id' => 'company_id',
        'label' => __('Company'),
        'options' => $nameList,
        'autocomplete_combobox' => true,
        'empty' => '',
        'required' => true
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsert($data)) {
        $param['email'] = $data[$modelName]['email'];
        $dataProfile = Api::call(Configure::read('API.url_userprofiles_detailbyemail'), $param);
        if (Api::getError()) {
            AppLog::info("API.url_userprofiles_detailbyemail failed", __METHOD__, $param);
            return $this->Common->setFlashErrorMessage(Api::getError());
        }
        if (empty($dataProfile)) {
            $data[$modelName]['is_company'] = 1;
            $id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName]);
            if (Api::getError()) {
                AppLog::info("Can not register recruiter", __METHOD__, $this->data);
                return $this->Common->setFlashErrorMessage(Api::getError());
            } else {
                $this->Common->setFlashSuccessMessage(__('Recruiter registered successfuly'));
                $this->redirect("/users/profile/{$id}");
            }
        } else {
            AppLog::info("Can not register recruiter", __METHOD__, $data);
            $this->Common->setFlashErrorMessage(__('Email address have registered already'));
        }
    } else {
        AppLog::info("Can not register recruiter", __METHOD__, $data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}