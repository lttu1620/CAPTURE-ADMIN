<?php

$modelName = $this->UserRecruiter->name;
$model = $this->{$modelName};
// Create breadcrumb 
$pageTitle = __('Invite recruiter');
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/userrecruiters',
        'name' => __('Recruiter list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

$this->UpdateForm->setModelName($modelName)
    ->addElement(array(
        'id'       => 'email_invite',
        'label'    => __('Email'),
        'required' => true
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Invite'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsert($data)) {
        $param['user_id'] = $this->AppUI->user_id;
        $param['email'] = $data[$modelName]['email_invite'];
        $invite = Api::call(Configure::read('API.url_invite_add_update'), $param);
        if (Api::getError()) {
            AppLog::info("API.url_invite_add_update failed", __METHOD__, $param);
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Recruiter invited successfully'));
            $this->redirect("/userrecruiters/invite");
        }
    } else {
        AppLog::info("Can not invite recruiter", __METHOD__, $data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}