<?php

$modelName = $this->UserRecruiter->name;
// General action disable/enable
$this->doGeneralAction($modelName);
// Execute action change admin or change approve
$this->doIsAdminIsApprovedAction($modelName);

if (!$this->AppUI->is_admin) {
    $company = $this->AppUI->company_id;
}
// create breadcrumb
$pageTitle = __('Recruiter list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

$sortOptions = array(
    'user_id-asc' => __('ID Asc'),
    'user_id-desc' => __('ID Desc'),
    'email-asc' => __('Email Asc'),
    'email-desc' => __('Email Desc'),
);

// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
            'id' => 'user_name',
            'type' => 'text',
            'label' => __('Username'),
        ))
    ->addElement(array(
        'id' => 'email',
        'label' => __('Email')
    ))
    ->addElement(array(
        'id' => 'sex',
        'label' => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty' => Configure::read('Config.StrAll'),
    ));
if ($this->AppUI->is_admin) {
    $sortOptions['company_name-asc'] = 'Company name Asc';
    $sortOptions['company_name-desc'] = 'Company name Desc';
    $listCompany = MasterData::companies_all();
    $listCompany = $this->Common->arrayKeyValue($listCompany, 'id', 'name');
    $this->SearchForm->addElement(array(
        'id' => 'company_id',
        'label' => __('Company'),
        'options' => $listCompany,
        'empty' => Configure::read('Config.StrAll'),
    ));
}
$this->SearchForm->addElement(array(
        'id' => 'is_admin',
        'label' => __('Is admin'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty' => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id' => 'is_approved',
        'label' => __('Is approved'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty' => Configure::read('Config.StrAll'),
    ));
if ($this->AppUI->is_admin) {
    $this->SearchForm->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => Configure::read('Config.StrAll'),
    ));
}
    $this->SearchForm->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => $sortOptions,
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right',
    ));

$param = $this->getParams(array(
    'page' => $this->getParam('page', 1), 
    'limit' => Configure::read('Config.pageSize'))
);
if (!$this->AppUI->is_admin && !empty($this->AppUI->company_id)) {
    $param['company_id'] = $this->AppUI->company_id;
    $param['disable'] = 0;
}
if (!$this->AppUI->is_admin) {
    $param['is_approved'] = 1;
}
list($total, $data) = Api::call(Configure::read('API.url_recruiters_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => 20
    ))
    ->addColumn(array(
        'id' => 'thumbnail_img',
        'type' => 'image',
        'title' => __('Image'),
        'src' => '{thumbnail_img}',        
        'width' => '60'
    ))
    ->addcolumn(array(
        'id' => 'user_id',
        'title' => __('ID'),
        'value' => '{id}',
        'type' => 'link',
        'href' => '/users/profileinformation/{user_id}',
        'width' => 30
    ))
     ->addColumn(array(
        'id' => 'display_username',
        'title' => __('Username'),
        'type' => 'link',
        'href' => '/users/profileinformation/{user_id}',
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'user_email',
        'title' => __('Email'),
        'width' => 150,
        'empty' => '',
        'hidden'=> true
    ))
    ->addColumn(array(
        'id' => 'user_sex_id',
        'title' => __('Gender'),
        'rules' => Configure::read('Config.searchGender'),
        'empty' => '',
        'width' => 80,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'company_name',
        'title' => __('Company name'),
        'width' => 250,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'created',
        'title' => __('Created'),
        'type' => 'date',
        'width' => '120',
        'empty' => '',
        'hidden'=> true
    ))
    ->addColumn(array(
        'id' => 'is_admin',
        'type' => 'checkbox',
        'title' => __('Admin'),
        'toggle' => true,
        'class'=>'admin',
        'rules' => array(
            '1' => 'checked',
            '0' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->addColumn(array(
        'id' => 'is_approved',
        'type' => 'checkbox',
        'title' => __('Approved'),
        'toggle' => true,
        'class'=>'approved',
        'rules' => array(
            '1' => 'checked',
            '0' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->addColumn(array(
        'id' => 'setting',
        'type' => 'link',
        'title' => __('Setting'),
        'href' => '/usersettings/index/{user_id}',
        'button' => true,
        'width' => '80'
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '1' => '',
            '0' => 'checked'
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'display_username' => array(
            array(
                'field' => 'user_email',
                'before' => __('Email') . " : ",
                'after' => ''
            ),
            array(
                'field' => 'created',
                'before' => __('Created') . " : ",
                'after' => ''
            ),
        )
    ))
    ->addButton(array(
        'id' => 'btn-addnew',
        'type' => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'id' => 'btn-disable',
        'type' => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'id' => 'btn-enable',
        'type' => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id' => 'action2',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id' => 'actionId2',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id' => 'optionId2',
    ));
if ($this->AppUI->recruiter_admin == '1') {
    $this->SimpleTable
        ->addButton(array(
                'type'  => 'submit',
                'value' => __('Invite'),
                'href'  => '/invites',
                'class' => 'btn btn-primary btn-invite',
            )
        );
}

if (!$this->AppUI->is_admin) {
    $this->SimpleTable->removeColumn('item');
    $this->SimpleTable->removeColumn('setting');
    $this->SimpleTable->removeColumn('disable');
    $this->SimpleTable->removeColumn('is_approved');
    $this->SimpleTable->removeColumn('company_name');
    $this->SimpleTable->removeButton('btn-addnew');
    $this->SimpleTable->removeButton('btn-disable');
    $this->SimpleTable->removeButton('btn-enable');    
    $this->SimpleTable->updateColumnAttr('disable', 'type', '');
    $this->SearchForm->removeElement('is_approved');
}
if($this->isMobile()){ 
  $this->SimpleTable->removeColumn('user_id'); 
  $this->SimpleTable->removeColumn('user_sex_id'); 
}