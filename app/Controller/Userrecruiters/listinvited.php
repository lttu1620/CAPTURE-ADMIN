<?php

$modelName = $this->UserRecruiter->name;
// General action disable/enable
$this->doGeneralAction($modelName);
// Execute action change admin or change approve
$this->doIsAdminIsApprovedAction($modelName);
// create breadcrumb
$pageTitle = __('Invited list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

$sortOptions = array(
    'id-asc' => __('ID Asc'),
    'id-desc' => __('ID Desc'),
    'email-asc' => __('Email Asc'),
    'email-desc' => __('Email Desc'),
);

// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'user_id',
        'type' => 'text',
        'label' => __('User Id'),
    ))
    ->addElement(array(
        'id' => 'email',
        'label' => __('Email')
    ))
    ->addElement(array(
        'id' => 'status',
        'label' => __('Status Invite'),
        'options' => Configure::read('Config.statusInvite'),
        'empty' => Configure::read('Config.StrAll'),
    ));

if ($this->AppUI->is_admin) {
    $this->SearchForm->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => Configure::read('Config.StrAll'),
    ));
}
$this->SearchForm->addElement(array(
    'id' => 'sort',
    'label' => __('Sort'),
    'options' => $sortOptions,
    'empty' => Configure::read('Config.StrChooseOne'),
))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right',
    ));

$param = $this->getParams(array(
        'page' => $this->getParam('page', 1),
        'limit' => Configure::read('Config.pageSize'))
);

list($total, $data) = Api::call(Configure::read('API.url_invite_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addcolumn(array(
        'id' => 'id',
        'title' => __('ID'),
        'value' => '{id}',
        'width' => 30
    ))
    ->addcolumn(array(
        'id' => 'user_id',
        'title' => __('User Id'),
        'value' => '{user_id}',
        'width' => 30
    ))
    ->addColumn(array(
        'id' => 'email',
        'title' => __('Email Invite'),
        'width' => 150,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'date',
        'width' => '120',
        'empty' => ''
    ));
if ($this->AppUI->recruiter_admin == '1') {
    $this->SimpleTable->addColumn(array(
        'id'           => 'status',
        'type'         => 'link',
        'title'        => __('Status Invite'),
        'button'       => true,
        'width'        => 150,
        'rules' => array(
            '0' => 'Resend',
            '1' => 'Approve'
        ),
        'onclick' => 'invite(\'{email}\', {user_id}, {status}); return false;',
    ));
}
$this->SimpleTable->setDataset($data);