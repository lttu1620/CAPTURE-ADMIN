<?php
if (!$this->AppUI->recruiter_admin || empty($this->AppUI->company_id)) {   
    throw new NotFoundException('No permission', 404);
}

$modelName = $this->UserRecruiter->name;

// create breadcrumb
$pageTitle = __('Awaiting approval');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

$param = $this->getParams(array(
    'is_approved' => 0,
    'disable' => 0,
    'company_id' => $this->AppUI->company_id,
    'page' => $this->getParam('page', 1), 
    'limit' => Configure::read('Config.pageSize'))
); 
list($total, $data) = Api::call(Configure::read('API.url_recruiters_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id' => 'thumbnail_img',
        'type' => 'image',
        'title' => __('Image'),
        'src' => '{thumbnail_img}',        
        'width' => '60'
    ))
    ->addcolumn(array(
        'id' => 'user_id',
        'title' => __('ID'),
        'value' => '{id}',
        'type' => 'link',
        'href' => '/users/profileinformation/{user_id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id' => 'display_username',
        'title' => __('Username'),
        'type' => 'link',
        'href' => '/users/profileinformation/{user_id}',
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'user_email',
        'title' => __('Email'),
        'width' => '150',
        'empty' => '',
        'hidden'=> true
    ))
    ->addColumn(array(
        'id' => 'user_sex_id',
        'title' => __('Gender'),
        'rules' => Configure::read('Config.searchGender'),
        'empty' => '',
        'width' => '80',
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'is_approved',
        'type' => 'checkbox',
        'title' => __('Approved'),
        'toggle' => true,
        'class'=>'approved',
        'rules' => array(
            '1' => 'checked',
            '0' => ''
        ),
        'empty' => 0,
        'width' => '90',
    ))    
    ->setDataset($data)
    ->setMergeColumn(array(
        'display_username' => array(
            array(
                'field' => 'user_email',
                'before' => __('Email') . " : ",
                'after' => ''
            ),
            array(
                'field' => 'created',
                'before' => __('Created') . " : ",
                'after' => ''
            ),
        )
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id' => 'action2',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id' => 'actionId2',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id' => 'optionId2',
    ));

if($this->isMobile()){
  $this->SimpleTable->removeColumn('user_id'); 
}