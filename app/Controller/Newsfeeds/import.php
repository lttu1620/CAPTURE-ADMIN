<?php

include (ROOT . "/app/Lib/Html/simple_html_dom.php");

$modelName = $this->Newsfeed->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('New recommended news articles ');
$this->setPageTitle($pageTitle);
$this->set("modelName", $modelName);
// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/newsfeeds',
        'name' => __('NewsFeed list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateImportNewsFeed($data)) {
        $error = array(
            'short_content' => array(1010 => __('Get short content of feed fail')),
            'detail_url' => array(1011 => __('This feed have been already'))
        );
        // Get File HTML and check "short_content|title| image|
        $image_url = "";
        $title = "";
        $short_content = "";
        $site_name = "";
        $str = @file_get_contents($data[$modelName]['detail_url']);
        if ($str !== FALSE && !empty($str)) {
            $str = html_by_tag($str, "<head", "</head>");
            preg_match_all('~charset=\"{0,1}([-a-z0-9_]+)\"{0,1}~i', $str, $charset);
            $encoding = $charset[1];
            if (!empty($encoding) && is_string($encoding) && strtolower($encoding) !== "utf-8") {
                $str = mb_convert_encoding($str, 'utf-8', $encoding);
            }
            $html = str_get_html($str);
            foreach ($html->find('meta[property=og:image]') as $element) {
                $image_url = !empty($element->attr['content']) ? $element->attr['content'] : "";
                break;
            }
            // get site name
            foreach ($html->find('meta[property=og:site_name]') as $element) {
                $site_name = !empty($element->attr['content']) ? $element->attr['content'] : "";
                break;
            }
            // Get title
            foreach ($html->find('meta[property=og:title]') as $element) {
                $title = !empty($element->attr['content']) ? $element->attr['content'] : "";
                break;
            }
            if (empty($title)) {
                foreach ($html->find('title') as $element) {
                    $title = !empty($element->innertext) ? $element->innertext : "";
                    break;
                }
            }
            // get short_content
            foreach ($html->find('meta[property=og:description]') as $element) {
                $short_content = !empty($element->attr['content']) ? $element->attr['content'] : "";
                break;
            }
            if (empty($short_content)) {
                foreach ($html->find('meta[name=description]') as $element) {
                    $short_content = !empty($element->attr['content']) ? $element->attr['content'] : "";
                    break;
                }
            }

            $data[$modelName]['short_content'] = !empty($short_content) ? $short_content : "";
            $data[$modelName]['title'] = !empty($title) ? $title : "";
            $data[$modelName]['image_url'] = !empty($image_url) ? $image_url : "";
            $data[$modelName]['likes_count'] = 0;
            $data[$modelName]['comments_count'] = 0;
            $data[$modelName]['favorite_count'] = 0;
            $data[$modelName]['news_site_name'] = $site_name;
            $id = Api::call(Configure::read('API.url_newsfeeds_addupdate'), $data[$modelName]);
            if (Api::getError()) {
                AppLog::info("Can not add new", __METHOD__, Api::getError());
                return $this->Common->setFlashErrorMessage(Api::getError(), $error);
            } else {
                $this->Common->setFlashSuccessMessage(__('This feed import successfuly') . " <br/>" .
                    __('You can %s Or %s this feed', "<a href=\"/newsfeeds/view/{$id}\" class=\"resendUrl\">" . __("View") . "</a>", "<a href=\"/newsfeeds/update/{$id}\" class=\"resendUrl\">" . __("Edit") . "</a>"));
                AppCache::delete(Configure::read('count_feed_manual')->key);
            }
        } else {
            $this->Common->setFlashErrorMessage('Load data fail');
        }
    } else {
        AppLog::info("Can not add new", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}