<?php

// create search conditions         
$page = $this->getParam('page', 1);
$limit = Configure::read('Config.pageSize');
if (empty($feedId)) {
    AppLog::info("Feed id not found", __METHOD__, $feedId);
    throw new NotFoundException("Feed id not found", __METHOD__, $feedId);
}
// create breadcrumb
$this->Breadcrumb->setTitle(__('News feed favorites'))
	->add(array(
	    'link' => $this->AppUI->is_admin ? $this->request->base . '/newsfeeds' : $this->request->base . '/newsfeeds/lists',
	    'name' => __('RSS news feeds'),
	))
	->add(array(
	    'name' => __('News feed favorites'),
	));
// get detail feed
$detail = Api::call(Configure::read('API.url_newsfeeds_detail'), array('id' => $feedId));
if (Api::getError()) {
    AppLog::info("API.url_newsfeeds_detail failed", __METHOD__, $feedId);
    $this->Common->handleException(Api::getError());
    return $this->Common->setFlashErrorMessage(Api::getError());
}
// get newsfeedfavorites of feed
$param = array(
    'page' => $page,
    'limit' => $limit,
    'news_feed_id' => $feedId,
    'sort' => 'created-desc',
    'disable' => 0
);
//d($param,1);
list($total, $newsfeedfavorites) = Api::call(Configure::read('API.url_newsfeedfavorites_list'), $param);
if (Api::getError()) {
    AppLog::info("API.url_newsnewsfeedfavorites_list failed", __METHOD__, $feedId);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
if (empty($newsfeedfavorites[0])) {
    AppLog::info("API.url_newsfeedfavorites_list return empty", __METHOD__, $feedId);
}
// set data to view
$this->set('feed_info', $detail);
$this->set('newsfeedfavorites', $newsfeedfavorites);
$this->set('total', $total);
$this->set('limit', $limit);
$this->set('page', $page);
$this->set('is_admin', $this->AppUI->is_admin);
$this->set('user_id', $this->AppUI->id);
