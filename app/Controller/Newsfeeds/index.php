<?php

$modelName = $this->Newsfeed->name;

// Process disable/enable
$this->doGeneralAction($modelName);
// Process update statistics
$this->doUpdateStatisticAction($modelName);
// Get list newssites
$listNewssite = MasterData::news_sites_all();
$listNewssite = $this->Common->arrayKeyValue($listNewssite, 'id', 'name');
// Add new item Other Site
$listNewssite[0] = __('Other sites');

// Get list category
$unCategory = array(-1 => __('Uncategorized'));
$listCategory = MasterData::categories_all();
$listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');
$listCategory = $unCategory + $listCategory;

// Get list tag
$unTag = array(-1 => __('No tag'));
$listTag = MasterData::tags_all();
$listTag = $this->Common->arrayKeyValue($listTag, 'id', 'name');
$listTag = $unTag + $listTag;
$appli = $this->getParam('appli', '');
// reate breadcrumb
$pageTitle = __('NewsFeed list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );

// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'title',
            'label' => __('Title'),
        )
    )
    ->addElement(
        array(
            'id'   => 'appli',
            'type' => 'hidden',
        )
    )
    ->addElement(
        array(
            'id'      => 'news_site_id',
            'label'   => __('Site Name'),
            'options' => $listNewssite,
            'empty'   => __('All'),
        )
    )
    ->addElement(
        array(
            'id'      => 'category_id',
            'label'   => __('Category'),
            'options' => $listCategory,
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'tag_id',
            'label'   => __('Tag'),
            'options' => $listTag,
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'is_public',
            'label'   => __('Is public'),
            'options' => Configure::read('Config.BooleanValue'),
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
    ->addElement(
        array(
            'id'      => 'is_tadacopy',
            'label'   => __('Is tadacopy'),
            'options' => Configure::read('Config.IsTadacopy'),
            'empty'   => Configure::read('Config.StrAll'),
        )
    );
if (!($appli == 'all' || $appli == 'hot')) {
    $this->SearchForm->addElement(
        array(
            'id'      => 'disable',
            'label'   => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty'   => Configure::read('Config.StrAll'),
        )
    )
        ->addElement(
            array(
                'id'      => 'sort',
                'label'   => __('Sort'),
                'options' => array(
                    'title-asc'           => __('Title Asc'),
                    'title-desc'          => __('Title Desc'),
                    'likes_count-asc'     => __('Like count Asc'),
                    'likes_count-desc'    => __('Like count Desc'),
                    'comments_count-asc'  => __('Comment count Asc'),
                    'comments_count-desc' => __('Comment count Desc'),
                    'favorite_count-asc'  => __('Favorite count Asc'),
                    'favorite_count-desc' => __('Favorite count Desc'),
                    'created-asc'         => __('Created Asc'),
                    'created-desc'        => __('Created Desc'),
                    'updated-asc'         => __('Updated Asc'),
                    'updated-desc'        => __('Updated Desc'),
                ),
                'empty'   => Configure::read('Config.StrChooseOne'),
            )
        );
}
$this->SearchForm->addElement(
    array(
        'id'       => 'limit',
        'label'    => __('Limit'),
        'options'  => Configure::read('Config.searchPageSize'),
        'onchange' => 'javascript: $(\'#btnSearch\').click();',
    )
)
    ->addElement(
        array(
            'type'  => 'submit',
            'id'    => 'btnSearch',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['appli'] = $appli;
if ($param['appli'] == 'hot' || $param['appli'] == 'all') {
    $param['user_id'] = 0;
    $param['order'] = ($param['appli'] == 'hot' ? 1 : 3);
    $result = Api::call(
        Configure::read('API.url_mobile_newsfeeds_list'),
        $param,
        false,
        array('total' => 0, 'data' => array())
    );
    $total = $result['total'];
    $data = $result['data'];
} else {
    list($total, $data) = Api::call(Configure::read('API.url_newsfeeds_list'), $param, false, array(0, array()));
}
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
//add tag for each feed
foreach ($data as &$item) {
    if (isset($item['best_comments'])) {
        unset($item['best_comments']);
    }
    $item['id'] = $item['news_feed_id'];
    $link = Router::url("/{$this->controller}/view/{$item['news_feed_id']}");
    $item['title'] = "<a href=\"{$link}\">{$item['title']}</a>";
    $item['title'] .= "<br/><i>".__('Created').': '.$this->Common->dateFormat($item['created'])."</i>";
    $tags = $item['tags'];
    if (!empty($tags)) {
        $item['title'] .= "<br/><div rel=\"{$item['news_feed_id']}\" class=\"tag-wrapper\">";
        foreach ($tags as $tag) {
            $style = "";
            if ($tag['tag_color']) {
                $style = "style=\"background-color:{$tag['tag_color']}\"";
            }
            $tagHtml = "<span class=\"post-wrap tag-{$item['news_feed_id']}-{$tag['id']}\" {$style}>
						{$tag['tag_name']}&nbsp&nbsp&nbsp&nbsp&nbsp
						<div class=\"delete-button\" {$style}>
						   <a onclick=\"tagDelete({$tag['id']},{$item['news_feed_id']});\" class=\"button-delete\" {$style}>
                            <i class=\"fa fa-fw fa-times\"></i>
                           </a>
						</div>
					</span>";
            $item['title'] .= $tagHtml;
        }
    } else {
        $item['title'] = "<a href=\"/".$this->controller."/update/{$item['news_feed_id']}\">{$item['title']}</a>";
        $item['title'] .= "<br/><div class=\"tag-wrapper\">";
    }

    $item['title'] .= "<span class=\"tag-select tag-select-{$item['news_feed_id']}\"></span>
                          <span class=\"tag-plus\">
                            <div>
                                  <a onclick=\"showListTag({$item['news_feed_id']},1);\" >
                                  <span> ".__("Add tag category")."</span>
                                      <i class=\"fa fa-fw fa-plus-square\"></i>
                                  </a>
                              </div>
                              <div>
                                   <a onclick=\"showListTag({$item['news_feed_id']},2);\" >
                                     <span> ".__("Add tag company")."</span>
                                      <i class=\"fa fa-fw fa-plus-square\"></i>
                                  </a>
                              </div>
                          </span>"
        ."</div>";

    unset($item['tags']);
}

$this->SimpleTable
    ->addColumn(
        array(
            'id'    => 'item',
            'name'  => 'items[]',
            'type'  => 'checkbox',
            'value' => '{news_feed_id}',
            'width' => 20,
        )
    )
    ->addColumn(
        array(
            'id'    => 'image_url',
            'type'  => 'image',
            'title' => __('Image'),
            'src'   => '{image_url}',
            'width' => 60,
        )
    )
    ->addColumn(
        array(
            'id'    => 'news_feed_id',
            'type'  => 'link',
            'title' => __('ID'),
            'href'  => '/'.$this->controller.'/update/{news_feed_id}',
            'width' => 40,
        )
    )
    ->addColumn(
        array(
            'id'    => 'title',
            'type'  => 'link',
            'href'  => '/'.$this->controller.'/view/{id}',
            'title' => __('Title'),
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'     => 'short_url',
            'type'   => 'url',
            'target' => '_blank',
            'width'  => 180,
            'title'  => __('Short url'),
            'empty'  => '',
        )
    )
    ->addColumn(
        array(
            'id'     => 'comments_count',
            'type'   => 'link',
            'title'  => __('Comment count'),
            'href'   => '/newsfeeds/view/{news_feed_id}',
            'width'  => 100,
            'empty'  => '0',
            'button' => true,
        )
    )
    ->addColumn(
        array(
            'id'     => 'likes_count',
            'type'   => 'link',
            'title'  => __('Like count'),
            'href'   => '/newsfeedlikes/index/{news_feed_id}',
            'width'  => 100,
            'empty'  => '0',
            'button' => true,
        )
    )
    ->addColumn(
        array(
            'id'      => 'time_schedule',
            'type'    => 'link',
            'class'   => '{news_feed_id}___date_time_rank',
            'title'   => __('Time schedule'),
            'href'    => 'javascript:void(0);',
            'onclick' => 'return showdatetimerank(this);',
            'width'   => 190,
            'empty'   => __('Set schedule'),
            'button'  => true,
        )
    )
    ->addColumn(
        array(
            'id'     => 'is_public',
            'type'   => 'checkbox',
            'title'  => __('Is public'),
            'toggle' => true,
            'rules'  => array(
                '1' => 'checked',
                '0' => '',
            ),
            'class'  => 'toggle-event toggle-is_public',
            'empty'  => 0,
            'width'  => 80,
        )
    )
    ->addColumn(
        array(
            'id'     => 'is_tadacopy',
            'type'   => 'checkbox',
            'title'  => __('Is tadacopy'),
            'toggle' => true,
            'rules'  => array(
                '1' => 'checked',
                '0' => '',
            ),
            'class'  => 'toggle-event is_tadacopy',
            'empty'  => 0,
            'width'  => 80,
        )
    )
    ->addColumn(
        array(
            'id'     => 'disable',
            'type'   => 'checkbox',
            'title'  => __('Status'),
            'toggle' => true,
            'rules'  => array(
                '0' => 'checked',
                '1' => '',
            ),
            'empty'  => 0,
            'width'  => 85,
        )
    )
    ->addColumn(
        array(
            'id'       => 'edit_link',
            'type'     => 'link',
            'th_title' => __('Edit'),
            'title'    => __('Edit'),
            'href'     => '/'.$this->controller.'/update/{id}',
            'button'   => true,
            'width'    => '50',
        )
    )
    ->addColumn(
        array(
            'id'     => 'start_date',
            'type'   => 'datetime',
            'empty'  => '',
            'hidden' => true,
        )
    )
    ->addColumn(
        array(
            'id'     => 'finish_date',
            'type'   => 'datetime',
            'empty'  => '',
            'hidden' => true,
        )
    )
    ->setDataset($data)
    ->setMergeColumn(
        array(
            'time_schedule' => array(
                array(
                    'field'  => 'start_date',
                    'before' => __('Start date')." : ",
                    'after'  => '',
                    'class'  => 'start_date',
                ),
                array(
                    'field'  => 'finish_date',
                    'before' => __('Finish date')." : ",
                    'after'  => '',
                    'class'  => 'finish_date',
                ),
            ),
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Update statistic'),
            'class' => 'btn btn-primary btn-statistic',
            'title' => __('Reupdate comment count, like count and favorite count'),
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        )
    )
    ->addHidden(
        array(
            'type' => 'hidden',
            'id'   => 'actionId2',
        )
    );

