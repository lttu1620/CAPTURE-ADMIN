<?php

// create breadcrumb
$this->Breadcrumb->setTitle(__('News feed detail'))
    ->add(array(
        'link' => $this->request->base . '/newsfeeds/lists',
        'name' => __('RSS news feeds'),
    ))
    ->add(array(
        'name' => __('News feed detail'),
    ));

// get detail feed
$detail = Api::call(Configure::read('API.url_newsfeeds_detail'), array('id' => $feedId, 'log' => 1));
$this->Common->handleException(Api::getError());

$isCommented = Api::call(Configure::read('API.url_newscomments_checkusercommentedfeed'), array('feed_id' => $feedId, 'user_id' => $this->AppUI->is_admin ? 0 : $this->AppUI->id));
$this->Common->handleException(Api::getError());
$detail['isCommented'] = $isCommented;

if ($this->AppUI->is_admin) {
// Handler Tags for Feed
    $tags = $detail['tags'];
    $detail['htmlTags'] = "<div style=\"clear:both; height:10px\"> </div>";
    if (!empty($tags)) {
        $detail['htmlTags'] .= "<div rel=\"{$feedId}\" class=\"tag-wrapper\">";
        foreach ($tags as $tag) {
            $style = "";
            if ($tag['tag_color']) {
                $style = "style=\"background-color:{$tag['tag_color']}\"";
            }
            $tagHtml = "<span class=\"post-wrap tag-{$feedId}-{$tag['id']}\" {$style}>
                    {$tag['tag_name']}&nbsp&nbsp&nbsp&nbsp&nbsp
                    <div class=\"delete-button\" {$style}>
                       <a onclick=\"tagDelete({$tag['id']},{$feedId});\" class=\"button-delete\" {$style}><i class=\"fa fa-fw fa-times\"></i></a>
                    </div>
                </span>";
            $detail['htmlTags'] .= $tagHtml;
        }
        
    } else {
        $detail['htmlTags'] .= "<div class=\"tag-wrapper\">";
    }
    $detail['htmlTags'] .= "<span class=\"tag-select tag-select-{$feedId}\"></span>";
    if ($this->isMobile()) {
        $detail['htmlTags'] .= "<div class=\"clearfix\"></div>";
    }
    $detail['htmlTags'] .= "<span class=\"tag-plus\">
                           <div>
                                <a onclick=\"showListTag({$feedId},1);\">
                                    <span> ". __("Add tag category"). "</span>
                                    <i class=\"fa fa-fw fa-plus-square\"></i>
                                </a>
                            </div>
                            <div>
                                 <a onclick=\"showListTag({$feedId},2);\">
                                    <span> ". __("Add tag company"). "</span>
                                    <i class=\"fa fa-fw fa-plus-square\"></i>
                                </a>
                             </div>
                        </span>"
            . "</div>";
} else {
    $detail['htmlTags'] = "";
}

// get comments of feed
$page = $this->getParam('page', 1);
$limit = Configure::read('Config.pageSize');
$param = array(
    'page' => $page,
    'limit' => $limit,
    'user_id' => $this->AppUI->is_admin ? 0 : $this->AppUI->id,
    'news_feed_id' => $feedId,
    'sort' => 'created-desc',
);

list($total, $comments) = Api::call(Configure::read('API.url_newscomments_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
// set data to view
$this->set('feed_info', $detail);
$this->set('comments', $comments);
$this->set('total', $total);
$this->set('limit', $limit);
$this->set('page', $page);
$this->set('is_admin', $this->AppUI->is_admin);
$this->set('user_id', $this->AppUI->id);

if ($this->request->is('post')) {
    $data = $this->request->data;
    $param['new_feed_id'] = $feedId;
    $param['user_id'] = $this->AppUI->id;
    $param['comment'] = $data['txt_comment'];

    Api::call(Configure::read('API.url_newscomments_add'), $param);
    if (Api::getError()) {
        AppLog::info("API.url_newscomments_add failed", __METHOD__, $param);
        return $this->Common->setFlashErrorMessage(Api::getError());
    } else {
        $this->redirect($this->request->here(false));
    }
}





