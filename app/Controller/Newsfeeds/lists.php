<?php

$this->setPageTitle(__('RSS news feeds'));

// create breadcrumb
$this->Breadcrumb->setTitle(__('RSSニュースフィード'))
        ->add(array(
            'name' => __('RSS news feeds'),
        ));

// create search conditions         
$page = $this->getParam('page', 1);
$limit = Configure::read('Config.pageSize');
$title = $this->getParam('title', '');
$site_id = $this->getParam('news_site_id', '');
$tagId = $this->getParam('tag_id', '');
$appli = $this->getParam('appli','');
$original = $this->getParam('original', '');
$this->set('title', $title);
// Call API get data news feed
$param = array(
        'page' => $page,
        'limit' => $limit,
        'title' => $title,
        'news_site_id' => ($site_id != 'all')? $site_id : '',
        'tag_id' => $tagId ? $tagId : '',
        'original' => $original ? $original : 0,
        'full' => true
    );
if ($appli != '') { 
    $param['user_id'] = ($this->AppUI->is_admin  ? 0 : $this->AppUI->id);
    $param['order'] = ($appli == 'hot') ? 1 : 3;
    $result = Api::call(Configure::read('API.url_mobile_newsfeeds_list'), $param, false, array(0, array()));
    $total = !empty($result['total']) ? $result['total'] : 0;
    $data = !empty($result['data']) ? $result['data'] : array();
} else {
    $param['user_id'] = ($this->AppUI->is_admin  ? 0 : $this->AppUI->id);
    list($total, $data) = Api::call(Configure::read('API.url_newsfeeds_list'), $param, false, array(0, array()));
}
$this->Common->handleException(Api::getError());

$tagList = $this->Common->arrayFilter(MasterData::tags_all(), 'type', 1);

// get count_feed_manual
$count_feed_manual = MasterData::count_feed_manual();
list($total_original, $data_original) = Api::call(Configure::read('API.url_posts_list'), array(), false, array(0, array()));
$this->set('data', $data);
$this->set('total', $total);
$this->set('limit', $limit);
$this->set('tagList', $tagList);
$this->set('count_feed_manual',$count_feed_manual);
$this->set('count_feed_original',$total_original);
//$this->layout = 'template';
$this->viewPath = 'Newsfeeds';
$this->view = 'lists';
