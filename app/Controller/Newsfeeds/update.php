<?php

$modelName = $this->Newsfeed->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add NewsFeed');
if (!empty($id)) {
    $pageTitle = __('Edit NewsFeed');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_newsfeeds_detail'), $param);
    $this->Common->handleException(Api::getError());
}

if ($this->AppUI->is_admin) {
// Handler Tags for Feed
    $tags = $data[$modelName]['tags'];
    $htmlTags = "<div style=\"clear:both; height:10px\"> </div>";
    if (!empty($tags)) {
        $htmlTags .= "<div rel=\"{$id}\" class=\"tag-wrapper\">";
        foreach ($tags as $tag) {
            $style = "";
            if ($tag['tag_color']) {
                $style = "style=\"background-color:{$tag['tag_color']}\"";
            }
            $tagHtml = "<span class=\"post-wrap tag-{$id}-{$tag['id']}\" {$style}>
                    {$tag['tag_name']}&nbsp&nbsp&nbsp&nbsp&nbsp
                    <div class=\"delete-button\" {$style}>
                       <a onclick=\"tagDelete({$tag['id']},{$id});\" class=\"button-delete\" {$style}><i class=\"fa fa-fw fa-times\"></i></a>
                    </div>
                </span>";
            $htmlTags .= $tagHtml;
        }
        
    } else {
        $htmlTags .= "<div class=\"tag-wrapper\">";
    }
    $htmlTags .= "<span class=\"tag-select tag-select-{$id}\"></span>
                          <span class=\"tag-plus\">
                           <div>
                                <a onclick=\"showListTag({$id},1);\">
                                    <span> ". __("Add tag category"). "</span>
                                    <i class=\"fa fa-fw fa-plus-square\"></i>
                                </a>
                            </div>
                            <div>
                                 <a onclick=\"showListTag({$id},2);\">
                                    <span> ". __("Add tag company"). "</span>
                                    <i class=\"fa fa-fw fa-plus-square\"></i>
                                </a>
                             </div>
                        </span>"
            . "</div>";
} else {
    $htmlTags = "";
}
$htmlTags = json_encode($htmlTags);
$this->set(compact('htmlTags'));
$this->setPageTitle($pageTitle);
// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/newsfeeds',
        'name' => __('NewsFeed list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

// Create Update form 
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id' => 'news_site_id',
        'type' => 'hidden',
        'required' => true
    ))
    ->addElement(array(
        'id' => 'site_name',
        'label' => __('Site name'),
        'readonly' => !empty($data[$modelName]['news_site_id']) ? true : false
    ))
    ->addElement(array(
        'id' => 'detail_url',
        'label' => __('Detail url'),
        'readonly' => true
    ))
    ->addElement(array(
        'id' => 'short_url',
        'label' => __('Short url'),
        'readonly' => true
    ))
    ->addElement(array(
        'id' => 'title',
        'type' => 'text',
        'label' => __('Title'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'short_content',
        'rows' => 8,
        'label' => __('Short Content'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'image_url',
        'type' => 'file',
        'image' => true,
        'label' => __('Image'),
        'allowEmpty' => true,
        'crop' => array(
            'newsfeed_id' => $id,
            'field' => 'image_url',
        )
    ))
    ->addElement(
        array(
            'id'      => 'is_tadacopy',
            'label'   => __('Is tadacopy'),
            'options' => Configure::read('Config.IsTadacopy'),
        )
    )
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus')
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Cancel'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $model->data[$modelName]['image_url'] = $this->Image->uploadImage(
                "{$modelName}.image_url", 'news_feeds'
            );
            if ($this->Image->errorMsg) {
                $this->Common->setFlashErrorMessage($this->Image->errorMsg);
            }
        } elseif (!empty($model->data[$modelName]['image_url']['remove'])) {
            $model->data[$modelName]['image_url'] = '';
        } else {
            unset($model->data[$modelName]['image_url']);
        }
        if (isset($model->data[$modelName]['detail_url'])) {
            unset($model->data[$modelName]['detail_url']);
        }
        if ($model->data[$modelName]['news_site_id'] == 0) {
            $model->data[$modelName]['news_site_name'] = $model->data[$modelName]['site_name'];
            unset($model->data[$modelName]['site_name']);
        }
        $id = Api::call(Configure::read('API.url_newsfeeds_addupdate'), $model->data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not add new", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        }
        // redirect page when done       
        $this->redirect($this->request->here(false));
    } else {
        AppLog::info("Can not add new", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
