<?php

$modelName = $this->Setting->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Setting');
if (!empty($id)) {
    // call API get campuse detail
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_settings_detail'), $param);
    // check available id
    if (empty($data[$modelName])) {
        AppLog::info('Setting unavailable', __METHOD__, $param);
        throw new NotFoundException('Setting unavailable', __METHOD__, $param);
    }
    $pageTitle = __('Edit Setting');
    switch ($data[$modelName]['data_type']) {
        case "number":
        case "string":
            $data[$modelName]["value_string"] = $data[$modelName]['value'];
            break;
        default:
            $data[$modelName]["value_{$data[$modelName]['data_type']}"] = $data[$modelName]['value'];
            break;
    }
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/Settings',
            'name' => __('Setting list'),
        ))
        ->add(array(
            //'link' => '/' . $this->request->url,
            'name' => $pageTitle,
        ));

$dataType = !empty($data[$modelName]['data_type']) ? $data[$modelName]['data_type'] : '';
// create update form
$this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'required' => true,
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true,
        ))
        ->addElement(array(
            'id' => 'type',
            'label' => __('Type'),
            'options' => Configure::read('Config.SettingType'),
        ))
        ->addElement(array(
            'id' => 'data_type',
            'label' => __('Data type'),
            'options' => Configure::read('Config.SettingDataType'),
            'onchange' => 'return ChangeDataType(this);',
        ))
        //Create group for string and number
        ->addElement(array(
            'id' => "value_string",
            'type' => 'input',
            'label' => __('Value'),
            'class' => 'DynamicElement form-control ',
            'div' => array('class' => 'form-group divstring divnumber DivEffect ' . (($dataType == 'string' || $dataType == 'number' || $dataType == '' ) ? 'Show' : 'Hidden')),
        ))
        //Create group for textarea
        ->addElement(array(
            'id' => "value_textarea",
            'type' => 'input',
            'rows' => 2,
            'label' => __('Value'),
            'class' => 'DynamicElement form-control ',
            'div' => array('class' => 'form-group divtextarea DivEffect ' . (($dataType == 'textarea') ? 'Show' : 'Hidden')),
        ))
        //Create group for boolen
        ->addElement(array(
            'id' => "value_boolean",
            'label' => __('Value'),
            'div' => array('class' => 'form-group divboolean DivEffect ' . (($dataType == 'boolean') ? 'Show' : 'Hidden')),
            'class' => 'DynamicElement form-control ',
            'options' => Configure::read('Config.BooleanValue'),
        ))
        //Create group for image
        ->addElement(array(
            'id' => "value_image",
            'type' => 'file',
            'image' => true,
            'label' => __('Value'),
            'class' => 'DynamicElement form-control ',
            'div' => array('class' => 'form-group divimage DivEffect ' . (($dataType == 'image') ? 'Show' : 'Hidden')),
        ))
        ->addElement(array(
            'id' => 'pattern_url_detail',
            'label' => __('Pattern url detail'),
        ))
        ->addElement(array(
            'id' => 'description',
            'label' => __('Description'),
            'rows' => 4,
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    $dataType = $data[$modelName]['data_type'];
    switch ($dataType) {
        case "number":
        case "string":
            $data[$modelName]['value'] = $data[$modelName]['value_string'];
            break;
        default:
            $data[$modelName]['value'] = $data[$modelName]["value_{$dataType}"];
            break;
    }
    $result = false;
    // If datatype is image ==> check and upload Image
    if ($dataType == "image") {
        $result = $model->validateImageInsertUpdate($data);
        // Processing upload Image value 
        if (!empty($_FILES['data']['name'][$modelName]['value_image'])) {
            $data[$modelName]['value'] = $this->Image->uploadImage("{$modelName}.value_image");
        } else {
            unset($data[$modelName]['value']);
        }
    } else {
        $result = $model->validateInsertUpdate($data);
    }
    //unset other value
    unset($data[$modelName]['value_string']);
    unset($data[$modelName]['value_textarea']);
    unset($data[$modelName]['value_boolean']);
    unset($data[$modelName]['value_image']);
    if ($result) {
        $id = Api::call(Configure::read('API.url_settings_addupdate'), $data[$modelName]);
        if (Api::getError()) {
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        }
        $this->redirect("/{$this->controller}/update/{$id}");
    } else {
        AppLog::info('Can not update', __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
    