<?php

$modelName = $this->Followcategorylog->name;

// Get all department
$listCategory = MasterData::categories_all();
$listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');
// create breadcrumb
$pageTitle = __('Follow category log list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'category_id',
            'label' => __('Category'),
            'options' => $listCategory,
            'empty' => Configure::read('Config.StrChooseOne'),
            'autocomplete_combobox' => true,
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Username')
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname')
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email')
        ))
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'user_name-asc' => __('Name Asc'),
                'user_name-desc' => __('Name Desc'),
                'user_nickname-asc' => __('Nickname Asc'),
                'user_nickname-desc' => __('Nickname Desc'),
                'email-asc' => __('Email Asc'),
                'email-desc' => __('Email Desc'),
                'category_name-asc' => __('Category name Asc'),
                'category_name-desc' => __('Category name Desc'),
                'created-asc' => __('Created Asc'),
                'created-desc' => __('Created Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_followcategorylogs_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable->addColumn(array(
            'id' => 'user_name',
            'title' => __('Username'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'user_nickname',
            'title' => __('Nickname'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'category_name',
            'title' => __('Category name'),
        ))
        ->addColumn(array(
            'id' => 'created',
            'type' => 'date',
            'title' => __('Created'),
            'width' => '120'
        ))
        ->setDataset($data);
