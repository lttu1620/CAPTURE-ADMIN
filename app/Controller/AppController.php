<?php

App::uses('Controller', 'Controller');
App::uses('AppHelper', 'View/Helper');
App::uses('CakeEmail', 'Network/Email');
App::uses('MasterData', 'Model');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package	Controller
 * @version 1.0
 * @author Tuan Cao
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 * @copyright Oceanize INC
 */
class AppController extends Controller {

    /** @var object $AppUI Session infomation of user logged. */
    public $AppUI = null;

    /** @var object $controller Controller name. */
    public $controller = null;

    /** @var object $action Action name. */
    public $action = null;

    /** @var object $meta meta tag information . */
    public $meta = null;

    /** @var string $layout layout name . */
    public $layout = "template";

    /** @var array $helpers Helpers use in project. */
    public $helpers = array('Common', 'App');
    
    /** @var array $allowedActions list action can access not login. */
    public $allowedActions = array(
        'login',
        'fblogin',
        'logout',
        'register',
        'registerprofile',
        'resendforgetpassword',
        'resendregisteremail',
        'resendregistercompany',
        'approve',
        'active',
        'forgetpassword',
        'newpassword',
        'sendmailsuccess',
        'sendmailnotification',
        'lp',
        'contact',
        'privacypolicy',
        'postdetail'
    );
    
    /** @var array $components list components use in project */
    public $components = array(
        'App',
        'AppHtml',
        'Session',
        'Common',
        'Breadcrumb',
        'Video',
        'Image',
        'SimpleTable',
        'SimpleForm',
        'SearchForm',
        'UpdateForm',
        'Auth' => array(
            'loginRedirect' => false,
            'logoutRedirect' => false,
            'loginAction' => array(
                'controller' => 'pages',
                'action' => 'login',
                'plugin' => null
            ),
            'sessionKey' => 'Auth.Capture'
        ),
        'Cookie',
        'RequestHandler'
    );

    /**
     * Initializes components for AppController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Commont function to get params of actions in controller.
     *
     * @param array $default List parameter name. Default is array().
     *
     * @return array
     */
    public function getParams($default = array()) {
        $params = $this->request->query;
        if (!empty($default)) {
            foreach ($default as $paramName => $paramValue) {
                if (!isset($params[$paramName])) {
                    $params[$paramName] = $paramValue;
                }
            }
        }
        return $params;
    }

    /**
     * Commont function to set params to action of controller.
     *
     * @param array $params List key-value of parameter. Default is array().
     *
     * @return void
     */
    public function setParams($params = array()) {
        if (!empty($params)) {
            foreach ($params as $name => $value) {
                $this->request->query[$name] = $value;
            }
        }
    }

    /**
     * Commont function to get value of param by parameter name.
     *
     * @param string $name name of parameter to need get value.
     * @param string $defaultValue  Value default if not has paramter with name $name.
     *
     * @return string
     */
    public function getParam($name, $defaultValue = null) {
        return isset($this->request->query[$name]) ? $this->request->query[$name] : $defaultValue;
    }

    /**
     * Commont function to set value of param by parameter name.
     *
     * @param string $name name of parameter to need get value.
     * @param string $value  Value default if not set.
     *
     * @return string
     */
    public function setParam($name, $value = null) {
        $this->request->addParams(array($name => $value));
    }

    /**
     * Commont function before render html.
     *
     * @return void
     */
    public function beforeRender() {
        $this->set('moreCss', $this->AppHtml->getCss());
        $this->set('moreScript', $this->AppHtml->getScript());
        if (!empty($this->SearchForm->get())) {
            $this->set('searchForm', $this->SearchForm->get());
        }
        if (!empty($this->UpdateForm->get())) {
            $this->set('updateForm', $this->UpdateForm->get());
        }
        if (!empty($this->SimpleTable->get())) {
            $this->set('table', $this->SimpleTable->get());
        }
        if (!empty($this->Breadcrumb->get())) {
            $this->set('breadcrumbTitle', $this->Breadcrumb->getTitle());
            $this->set('breadcrumb', $this->Breadcrumb->get());
        }
        $siteList = MasterData::news_sites_all();
        if (!empty($siteList)) {
            $this->set('sitelist', $siteList);
        }
        if ($this->isMobile() && file_exists(APP . 'View' . DS .  'Mobile' . DS . $this->viewPath . DS . $this->view . $this->ext)) {
            $this->viewPath = 'Mobile' . DS . $this->viewPath;
        } elseif (!file_exists(APP . 'View' . DS . $this->viewPath . DS . $this->view . $this->ext) && file_exists(APP . 'View' . DS . 'Common' . DS . $this->view . $this->ext)) {
            $this->viewPath = 'Common';
        }
        $this->set('page', $this->getParam('page', 1));
        if (isset($this->Auth) && $this->isAuthorized()) {
            $this->set('AppUI', $this->Auth->user());
        }
    }

    /**
     * Commont function set layout for view.
     *
     * @return void
     */
    public function setLayout() {
        if ($this->controller == 'pages' && $this->action != 'display') {
            $this->layout = "blank";
        } elseif ($this->controller == 'ajax' || $this->request->isAjax()) {
            $this->layout = "ajax";
        } 
        if ($this->isMobile() && $this->Auth->admin_type != 1) {
            $this->layout = "mobile"; 
        }
    }

    /**
     * Commont function set layout for view
     *
     * 
     * @param object $checkEmpty infomation name to check.
     * @param object $userInfo Session userInfomation is logged.
     * @return boolean
     */
    public function checkEmpty($checkEmpty, $userInfo = false) {
        if (!empty($userInfo)) {
            foreach ($checkEmpty as $field) {
                if (empty($userInfo[$field])) {
                    return true;
                }
            }
            return false;
        }
        foreach ($checkEmpty as $field) {
            if (empty($this->AppUI->{$field})) {
                return true;
            }
        }
        return false;
    }

    /**
     * Commont function called before the controller action.
     * 
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        // allow some actions not require authorize
        $this->Auth->allow($this->allowedActions);
        $this->controller = $this->request->params['controller'];
        $this->action = $this->request->params['action'];
        $this->set('controller', $this->controller);
        $this->set('action', $this->action);
        $this->set('url', $this->request->url);
        $this->set('referer', Controller::referer());
        $this->setLayout();

        // load page title, meta tag, ...
        $seo = Configure::read('seo');
        $this->meta = $seo['default'];
        if (isset($seo[$this->controller][$this->action])) {
            $this->meta = array_merge($this->meta, $seo[$this->controller][$this->action]);
        }
        $this->set('meta', $this->meta);

        // change language
        if ($l = $this->Session->read('language')) {
            Configure::write('Config.language', $l);
        }

        if ($this->controller == 'posts' && $this->action == 'detail')
        {
            return;
        }
        
        // redirect landing page if the action difference with login
        if (!$this->isAuthorized() && !in_array($this->action, $this->allowedActions)
        ) {
            return $this->redirect('/lp');
        }

        if ($this->isAuthorized()) {
            if (!in_array($this->action, array('logout', 'approve', 'active'))) {
                if ($this->controller != 'images' && $this->controller != 'ajax') {
                    if ($this->action != 'registerprofile' && !empty($this->AppUI->redirect_register_profile)) {
                        return $this->redirect('/register/profile');
                    } elseif ($this->action != 'registercompany' && !empty($this->AppUI->redirect_register_company)) {
                        return $this->redirect('/register/company');
                    }
                }
            }
            // deny some action not require login
            if (in_array($this->action, array('login', 'fblogin', 'register', 'forgetpassword', 'newpassword'))) {
                return $this->redirect('/');
            }
        }
        // check access request link
        if ($this->isAuthorized()) {
            if (!$this->AppUI->is_admin) {
                $allows = Configure::read('ACL.recruiter.allow');
                $denies = Configure::read('ACL.recruiter.deny');
            } elseif ($this->AppUI->admin_type == 0) {
                $allows = Configure::read('ACL.ambassador.allow');
                $denies = Configure::read('ACL.ambassador.deny');
            }
            if (!empty($allows) || !empty($denies)) {
                $isAllow = false;
                if (!empty($allows)) {
                    foreach ($allows as $partten) {
                        preg_match("/{$partten}/i", $this->request->here(false), $matches);
                        if (!empty($matches[0])) {
                            $isAllow = true;
                        }
                    }
                }
                if ($isAllow == false && !empty($denies)) {
                    foreach ($denies as $partten) {
                        preg_match("/{$partten}/i", $this->request->here(false), $matches);
                        if (!empty($matches[0])) {
                            $this->redirect('/');
                        }
                    }
                }
            }
        }
    }

    /**
     * Commont function check user is Authorized..
     * 
     * 
     * @param object $user Session user logged.
     * @return boolean  If true is authorize, and false is unauthorize.
     */
    public function isAuthorized($user = null) {
        if (!isset($this->Auth)) {
            return false;
        }
        if (empty($user)) {
            $user = $this->Auth->user();
        }
        if (!empty($user)) {
            $this->AppUI = $user;
            return true;
        }
        return false;
    }

    /**
     * Commont function check user is Authorized..
     * 
     * @param object $param Information login of admin.
     * @return object Session informtion of admin or boolean result login action.
     */
    public function startAdminLogin($param = null) {
        $user = Api::call(Configure::read('API.url_admins_login'), $param);
        if (Api::getError()) {
            return $this->Common->setFlashErrorMessage(Api::getError());
        }
        return $this->createLoginSession($user);
    }

    /**
     * Commont function check user is Authorized..
     * 
     * @param object $param Information login of recruiter.
     * @return object Session informtion of recruiter or boolean result login action.
     */
    public function startLogin($param = null) {
        if (!empty($param['email']) && !empty($param['password'])) {
            $user = Api::call(Configure::read('API.url_userrecruiters_login'), $param);
        } elseif (!empty($param['id'])) {
            $user = Api::call(Configure::read('API.url_users_detail'), $param);
        }
        if (Api::getError()) {
            return $this->Common->setFlashErrorMessage(Api::getError());
        }
        return $this->createLoginSession($user);
    }

    /**
     * Commont function creater Session of user login.
     * 
     * @param object $userInfo User information is logged.
     * @return boolean
     */
    public function createLoginSession($userInfo = null) {
        if (empty($userInfo)) {
            return false;
        }
        if (isset($userInfo['password'])) {
            unset($userInfo['password']);
        }
        $userInfo['is_admin'] = isset($userInfo['login_id']) ? 1 : 0;
        if (!empty($userInfo['is_admin'])) {
            $userInfo['display_name'] = $userInfo['login_id'];
        } else {
            $userInfo['display_image'] = $userInfo['company_thumbnail_img'];
            if (!empty($userInfo['nickname'])) {
                $userInfo['display_name'] = $userInfo['nickname'];
            } elseif (!empty($userInfo['name'])) {
                $userInfo['display_name'] = $userInfo['name'];
            } else {
                $userInfo['display_name'] = $userInfo['email'];
            }
            if ($this->checkEmpty(array('name', 'nickname', 'sex_id'), $userInfo)) {
                $userInfo['redirect_register_profile'] = 1;
            } elseif ($this->checkEmpty(array('company_id'/* , 'company_thumbnail_img', 'company_address', 'company_description_short' */), $userInfo)) {
                $userInfo['redirect_register_company'] = 1;
            } elseif (!empty($userInfo['company_id']) && $userInfo['recruiter_admin'] == 0 && $userInfo['is_approved'] == 0) {
                $userInfo['redirect_register_company'] = 1;
            }
        }
        $userInfo = json_decode(json_encode($userInfo), false);
        $this->Auth->login($userInfo);
        return $userInfo;
    }

    /**
     * Commont function creater message notification.
     *
     * 
     * @param object $modelName Model name will notification message.
     * @return object 
     */
    public function doGeneralAction($modelName) {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId'])) {
                $data['items'] = array($data['actionId']);
            }
            if (!empty($data['action']) && !empty($data['items'])) {
                $action = $data['action'];
                $param['id'] = implode(',', $data['items']);
                switch ($action) {
                    default:
                        $param['disable'] = ($data['action'] == 'disable' ? 1 : 0);
                        if (!Api::call("{$this->request->params['controller']}/disable", $param)) {
                            AppLog::warning("Can not update", __METHOD__, $data);
                            $this->Common->setFlashErrorMessage(__("Can not update"));
                        }
                        $this->Common->deleteCacheAfterDisable($this->request->params['controller']);
                        $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));
                }
                return $this->redirect($this->request->here(false));
            }
        }
    }

    /**
     * Commont function split field sort - value.
     *
     * 
     * @param array $sort List field sorted. [fielname-sortType]
     * @return array
     */
    public function parseOrder($sort) {
        if (empty($sort))
            return array();
        $order = array();
        if (!empty($sort)) {
            $sortExplode = explode('-', $sort);
            if (count($sortExplode) == 2) {
                $order[$sortExplode[0]] = $sortExplode[1];
            }
        }
        return $order;
    }

    /**
     * Commont function get all data of view.
     *
     * 
     * @param array $modelName model name to get data.
     * @return array Data of view array($modelName => $data)
     */
    public function getData($modelName) {
        $data = array();
        foreach ($this->data[$modelName] as $field => $value) {
            if (preg_match('/_customid$/', $field)) {
                $id = str_replace('_customid', '_id', $field);
                $data[$id] = $value;
            } else {
                $data[$field] = $value;
            }
        }
        $data['model_name'] = $modelName;
        return array($modelName => $data);
    }

    /**
     * Commont function set title for page.
     *
     * 
     * @param array $title  title name will set to page.
     * @return void
     */
    public function setPageTitle($title) {
        $this->meta['title'] = $title;
        $this->set('meta', $this->meta);
    }

    public function dispatchEvent($callback, $param = array()) {
        $event = new CakeEvent($callback, $this, $param);
        $this->getEventManager()->dispatch($event);
    }
    
    public function isMobile() {
        return $this->RequestHandler->isMobile();
    }
    
}
