<?php

App::uses('AppController', 'Controller');

/**
 * Universitys controller
 * 
 * @package Controller
 * @created 2014-12-01
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class UniversitiesController extends AppController {

    public $uses = array('User', 'University');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     * 
     * @author Truongnn 
     * @return void
     */
    public function index() {
        include ('Universities/index.php');
    }

    /**
     * update action
     * 
     * @author Truongnn 
     * @param $id 
     * @return void
     */
    public function update($id = 0) {
        include ('Universities/update.php');
    }

}
