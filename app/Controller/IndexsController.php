<?php

App::uses('AppController', 'Controller');

/**
 * IndexsController class of Indexs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class IndexsController extends AppController {

    public $components = array('Chart');

    /**
     * Initializes components for IndexsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index IndexsController.
     * 
     * @return void
     */
    public function index() {
        include ('Indexs/newsfeedshare.php');
    }
    
    /**
     * Handles user interaction of view newsfeedshare IndexsController.
     * 
     * @return void
     */
    public function newsfeedshare() {
        include ('Indexs/newsfeedshare.php');
    }

    /**
     * Handles user interaction of view newscommentlike IndexsController.
     * 
     * @param object $id ID value of NewsComments. Default value is 0.
     * 
     * @return void
     */
    public function newscommentlike($id = 0) {
        include ('Indexs/newscommentlike.php');
    }

    /**
     * Handles user interaction of view newsfeedfavorites IndexsController.
     * 
     * @return void
     */
    public function newsfeedfavorites() {
        include ('Indexs/newsfeedfavorites.php');
    }

    /**
     * Handles user interaction of view newsfeedreads IndexsController.
     *
     * @return void
     */
    public function newsfeedreads()
    {
        include('Indexs/newsfeedreads.php');
    }

    /**
     * Handles user interaction of view newsfeedview IndexsController.
     * 
     * @return void
     */
    public function newsfeedview() {
        include ('Indexs/newsfeedview.php');
    }

}
