<?php

App::uses('AppController', 'Controller');

/**
 * NewssitesController class of Newssites Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class NewssitesController extends AppController {

    /**
     * Initializes components for NewssitesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

   /**
     * Handles user interaction of view index Newssites.
     * 
     * 
     * @return void
     */
    public function index() {
        include ('Newssites/index.php');
    }

   /**
     * Handles user interaction of view index Newssites.
     * 
    * @param object $id ID value of NewsSites. Default value is 0.
     * @return void
     */
    public function update($id = 0) {
        include ('Newssites/update.php');
    }

   /**
     * Update statistic for NewsSites.
     * 
    * @param object $modelName Model name is update.
     * @return void
     */
    public function doUpdateStatisticAction($modelName) {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId2'])) {
                $data['items'] = array($data['actionId2']);
            }
            if (!empty($data['items'])) {
                $param['news_site_id'] = implode(',', $data['items']); 
                if (!Api::call(Configure::read('API.url_newssites_updatecounter'), $param)) {
                    AppLog::warning("Can not update", __METHOD__, $data);
                    $this->Common->setFlashErrorMessage(__("Can not update"));
                }
                $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));

                if (empty($this->getParams())) {
                    return $this->redirect($this->request->here(false));
                }
                return $this->redirect($this->request->here . '?' . http_build_query($this->getParams(), null, '&'));
            }
        }
    }

}
