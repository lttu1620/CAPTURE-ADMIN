<?php

App::uses('AppController', 'Controller');

/**
 * Presetcomments controller
 * 
 * @package Controller
 * @created 2014-12-11
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class PresetcommentsController extends AppController {

    public $uses = array('User', 'Presetcomment');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
     /**
     * index action
      * 
     * @author Truongnn 
     * @return void 
     */
    public function index() {
        include ('Presetcomments/index.php');
    }

    /**
     * update action
     *
     * @author Truongnn 
     * @param int $id 
     * @return void
     */
    public function update($id = 0) {
        include ('Presetcomments/update.php');
    }
    
}