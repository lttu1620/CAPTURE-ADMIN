<?php

/**
 * FollowcompanylogsController class of Followcompanylogs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class FollowcompanylogsController extends AppController {

    /**
     * Initializes components for FollowcompanylogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Followcompanylogs.
     * 
     * @return void
     */
    public function index() {
        include ('Followcompanylogs/index.php');
    }

}
