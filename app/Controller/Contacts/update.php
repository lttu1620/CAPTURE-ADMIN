<?php
$modelName = $this->Contact->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add contact');
if (!empty($id)) {
    $pageTitle = __('Edit contact');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_contacts_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/contacts',
            'name' => __('Contact list'),
        ))
        ->add(array(
            'name' => $pageTitle,
        ));
//System will send mail if the value is_send_mail equal true, otherwise not send mail.
// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('ID'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => '名前',
            'required' => true
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'tel',
            'label' => __('Tel'),
        ))
        ->addElement(array(
            'id' => 'address',
            'type' => 'textarea',
            'escapse' => false,
            'rows' => '2',
            'label' => __('Address'),
        ))
        ->addElement(array(
            'id' => 'website',
            'label' => __('Website'),
        ))
        ->addElement(array(
            'id' => 'subject',
            'label' => __('Subject'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'content',
            'type' => 'textarea',
            'escapse' => false,
            'rows' => '5',
            'label' => __('Content'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => (!empty($id) ? __('Save') : __('Send')),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validate($this->getData($modelName))) {
        $model->data[$modelName]['user_id'] = $this->AppUI->id;
        $model->data[$modelName]['company_id'] = $this->AppUI->company_id;
        $model->data[$modelName]['company_name'] = $this->AppUI->company_name;
        $model->data[$modelName]['server_host'] = $_SERVER['HTTP_HOST'];
        $model->data[$modelName]['server_ip'] = $_SERVER['SERVER_ADDR'];
        $model->data[$modelName]['is_send_email'] = 1;
        $id = Api::call(Configure::read('API.url_contacts_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}