<?php

$modelName = $this->Contact->name;

// Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Contact list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email'),
        ))
        ->addElement(array(
            'id' => 'tel',
            'label' => __('Tel'),
        ))
        ->addElement(array(
            'id' => 'address',
            'label' => __('Address'),
        ))
        ->addElement(array(
            'id' => 'website',
            'label' => __('Website'),
        ))
        ->addElement(array(
            'id' => 'subject',
            'label' => __('Subject'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'id-desc' => __('ID Desc'),
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
                'created-asc' => __('Date Asc'),
                'created-desc' => __('Date Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_contacts_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '40'
        ))        
        ->addColumn(array(
            'id' => 'name',
            'type' => 'link',
            'title' => __('Name'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '200',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
            'width' => '200',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'tel',
            'title' => __('Tel'),
            'width' => '100',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'address',
            'title' => __('Address'),
            'width' => '250',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'website',
            'title' => __('Website'),
            'width' => '150',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'subject',
            'title' => __('Subject'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'created',
            'type' => 'date',
            'title' => __('Created'),
            'width' => 120,
            'empty' => ''
        ))
        ->setDataset($data);

