<?php

App::uses('AppController', 'Controller');

/**
 * InvitesController class of Invites Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class InvitesController extends AppController {

    /**
     * Initializes components for InvitesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Invites.
     *
     * @return void
     */
    public function index() {
        include ('Invites/index.php');
    }

    /**
     * Handles user interaction of view update Invites.
     *
     * @param integer $id ID value of Invites. Default value is 0.
     *
     * @return void
     */
    public function update($id = 0) {
        include ('Invites/update.php');
    }

}
