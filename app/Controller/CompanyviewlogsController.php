<?php

/**
 * CompanyviewlogsController class of Companyviewlogs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class CompanyviewlogsController extends AppController {

    /**
     * Initializes components for CompanyviewlogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Companyviewlogs.
     * 
     * @return void
     */
    public function index() {
        include ('Companyviewlogs/index.php');
    }

}
