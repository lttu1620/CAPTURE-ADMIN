<?php

App::uses('AppController', 'Controller');

/**
 * NewssitesController class of Newssites Controller
 *
 * @package Controller
 * @copyright Oceanize INC
 */
class NewssitesrssController extends AppController {
    
    /**
     * Initializes components for NewssitesrssController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

     /**
     * Handles user interaction of view index Newssitesrss.
     * 
     * @return void
     */
    public function index() {
        include ('Newssitesrss/index.php');
    }

    /**
     * Handles user interaction of view update Newssitesrss.
     * 
     * @param object $id ID value of NewsSiteRss. Default value is 0.
     * @return void
     */
    public function update($id = 0) {
        include ('Newssitesrss/update.php');
    }
    
}