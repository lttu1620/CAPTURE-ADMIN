<?php

App::uses('AppController', 'Controller');

/**
 * HelpcontentsController class of Helpcontents Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class HelpcontentsController extends AppController {

    
    /**
     * Initializes components for HelpcontentsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Helpcontents.
     * 
     * @return void
     */
    public function index() {
        include ('Helpcontents/index.php');
    }

    /**
     * Handles user interaction of view index Helpcontents.
     * 
     * @param object $id ID value of Helpcontents. Default value is 0.
     * @return void
     */
    public function update($id = 0) {
        include ('Helpcontents/update.php');
    }

}
