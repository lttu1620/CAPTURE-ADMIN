<?php

App::uses('AppController', 'Controller');

/**
 * Subcategories controller
 * 
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class SubcategoriesController extends AppController {

    public $uses = array('User', 'Subcategory');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

     /**
     * index action  
     * 
     * @author Truongnn
     * @return void
     */
    public function index() {
        include ('Subcategories/index.php');
    }

    /**
     * update action
     * 
     * @author Truongnn
     * @param int id 
     * @return void
     */
    public function update($id = 0) {
        include ('Subcategories/update.php');
    }
    
    /**
     * Action: follower   
     * 
     * @author Truongnn 
     * @param $id 
     * @return void
     */
    public function follower($id = 0) {
        include ('Subcategories/follower.php');
    }
}