<?php

/**
 * NewscommentlikesController class of Newscommentlikes Controller
 *
 * @package Controller  
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
App::uses('AppController', 'Controller');

class NewscommentlikesController extends AppController {
    
    /**
     * Initializes components for LoginlogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Loginlogs.
     * @param object $news_comment_id ID value of NewsComments. Default value is 0.
     * 
     * @return void
     */
    public function index($news_comment_id = 0) {
        include ('Newscommentlikes/index.php');
    }

    /**
     * Handles user interaction of view update Loginlogs.
     * 
     * @param object $feedId ID value of NewsFeeds. Default value is 0.
     * @param object $id ID value of Newscommentlikes. Default value is 0.
     * 
     * @return void
     */
    public function update($feedId = 0, $id = 0) {
        include ('Newscommentlikes/update.php');
    }

}
