<?php

App::uses('AppController', 'Controller');

/**
 * PostsController class of Post Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PostsController extends AppController {

    /**
     * Initializes components for PostsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Posts.
     *
     * @return void
     */
    public function index() {
        include ('Posts/index.php');
    }

    /**
     * Handles user interaction of view update Posts.
     *
     * @param integer $id ID value of Posts. Default value is 0.
     *
     * @return void
     */
    public function update($id = 0) {
        include ('Posts/update.php');
    }

    /**
     * Handles user interaction of view detail Posts.
     *
     * @param integer $id ID value of Posts. Default value is 0.
     *
     * @return void
     */
    public function detail($id = 0) {
        include ('Posts/detail.php');
    }

}
