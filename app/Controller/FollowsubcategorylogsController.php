<?php

/**
 * FollowsubcategorylogsController class of Followsubcategorylogs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class FollowsubcategorylogsController extends AppController {

    /**
     * Initializes components for FollowsubcategorylogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Followsubcategorylogs.
     * 
     * @return void
     */
    public function index() {
        include ('Followsubcategorylogs/index.php');
    }

}
