<?php

App::uses('AppController', 'Controller');

/**
 * CampusesController class of Campuses Controller
 *
 * @package	Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class CampusesController extends AppController {

    /**
     * Initializes components for CampusesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * Handles user interaction of view index Campuses.
     *
     * @return void
     */
    public function index() {
        include ('Campuses/index.php');
    }

    /**
     * Handles user interaction of view update Campuses.
     *
     * @param integer $id ID value of Campuses. Default value is 0.
     *
     * @return void
     */
    public function update($id = 0) {
        include ('Campuses/update.php');
    }
    
}