<?php

$modelName = $this->Tag->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Tag');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_tags_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Tag unavailable", __METHOD__, $param);
        throw new NotFoundException("Tag unavailable", __METHOD__, $param);
    }
    // set tile update 
    $pageTitle = __('Edit Tag');
}
$this->setPageTitle($pageTitle);
// Create Breadcrum
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/tags',
            'name' => __('Tag list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));
//Get all categories.
$listCategory = MasterData::categories_all();
$listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');

// Create Update form 
$this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('name'),
            'autocomplete' => 'off',
            'required' => true
        ))
        ->addElement(array(
            'id' => 'color',
            'label' => __('Color'),
            // 'options' => Configure::read('Config.searchColor'),
            // 'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'category_id',
            'label' => __('Categories'),
            'options' => $listCategory,
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
         ->addElement(array(
            'id' => 'type',
            'label' => __('Type'),
            'options' => Configure::read('Config.searchTagType')
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    // if case add new tag
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_tags_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError()); 
    } else {
        AppLog::info("Can not add new", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}