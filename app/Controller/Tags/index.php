<?php

$modelName = $this->Tag->name;

// Process disable/enable
$this->doGeneralAction($modelName);
// Process update statistics
$this->doUpdateStatisticAction($modelName);
// create breadcrumb
$pageTitle = __('Tag list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));
//Get all categories.
$listCategory = MasterData::categories_all();
$listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');

// Create search form 
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name')
        ))
        ->addElement(array(
            'id' => 'color',
            'label' => __('Color'),
            'options' => Configure::read('Config.searchColor'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
                'id' => 'category_id',
                'label' => __('Category'),
                'options' => $listCategory,
                'empty' => Configure::read('Config.StrChooseOne'),
                'autocomplete_combobox' => true,
        ))
        ->addElement(array(
            'id' => 'type',
            'label' => __('Type'),
            'options' => Configure::read('Config.searchTagType'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'id-desc' => __('ID Desc'),
                'name-asc' => __('Nickname Asc'),
                'name-desc' => __('Nickname Desc'),
                'created-asc' => __('Created Asc'),
                'created-desc' => __('Created Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
            'onchange' => 'javascript: $(\'#btnSearch\').click();',
        ))
        ->addElement(array(
            'type' => 'submit',
            'id' => 'btnSearch',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['login_id'] = $this->getParam('login', '');
list($total, $data) = Api::call(Configure::read('API.url_tags_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
foreach ($data as &$item) {
    $item['color'] = "<dt style=\"background-color: {$item['color']}; width:80px; color:white; \">{$item['color']}</dt>";
}
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'witd' => '20'
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'name',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'title' => __('Name'),
            'empty' => ''
        ))
         ->addColumn(array(
            'id' => 'category_name',
            'title' => __('Category'),
            'width' => '250',
            'empty' => ''
        ))
         ->addColumn(array(
            'id' => 'type',
            'title' => __('Type'),
            'rules' => array(
                '1' => __('Normal'),
                '2' => __('Company')
            ),
            'width' => '200',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'color',
            'title' => __('Color'),
            'width' => '120'
        ))
        ->addColumn(array(
            'id' => 'feed_count',
            'title' => __('Feed Count'),
            'width' => '120'
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Created'),
            'type' => 'date',
            'width' => 120
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 90,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Update statistic'),
            'class' => 'btn btn-primary btn-statistic',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ))
        ->addHidden(array(
            'type' => 'hidden',
            'id' => 'actionId2',
        ));
