<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (!$this->AppUI->is_admin && $id <= 0) {
    $param['id'] = $this->AppUI->id;
}else
$param['id'] = $id == 0 ? -1 : $id;
$param['full'] = true;
$data[$modelName] = Api::Call(Configure::read('API.url_users_detail'), $param);
if (empty($data[$modelName])) {
    AppLog::info("User unavailable", __METHOD__, $param);
    throw new NotFoundException("User unavailable", __METHOD__, $param);
}
if (!$this->AppUI->is_admin) {
    if ($this->AppUI->company_id != $data[$modelName]['company_id']) {
        AppLog::info("User not same company", __METHOD__, $data[$modelName]['company_id']);
        throw new NotFoundException("User not same company", __METHOD__, $data[$modelName]['company_id']);
    }
}

$pageTitle = __('Update recruiter profile');

// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));

$Profiletab[] = array(
    'name' => __('User information'),
    'link' =>  $this->request->base . "/users/profileinformation/" . ($this->AppUI->is_admin == 1|| $id > 0 ? "{$id}" : ""),
);
if (!empty($data[$modelName]['facebook_id'])) {
    $Profiletab[] = array(
        'name' => __('Facebook profile'),
        'link' => $this->AppUI->is_admin == 1|| $id > 0 ? "../facebookinformation/{$id}" : "./facebookinformation/",
    );
}
if (!empty($data[$modelName]['is_company']) && !empty($data[$modelName]['company_id'])) {
    $Profiletab[] = array(
        'name' => __('Recruiter profile'),       
        'class' => 'active'
    );
    $Profiletab[] = array(
        'name' => __('Company profile'),
       'link' => $this->request->base . "/users/companyinformation/" . ( $this->AppUI->is_admin == 1 || $id > 0 ? "{$data[$modelName]['company_id']}/{$id}" : "{$data[$modelName]['company_id']}"),
    );
}
$this->set('profileTab', $this->Common->renderProfileTab($Profiletab));

//Create form for addupdate user_recruiters
if (!empty($data[$modelName]['is_company']) && !empty($data[$modelName]['company_id'])) {
    $this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'user_id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'company_id',
            'type' => 'hidden',
            'label' => __('Company'),
        ))
        ->addElement(array(
            'id' => 'recruiter_introduction_text',
            'type' => 'textarea',
            'rows' => '10',
            'label' => __('Recruiter Introduction'),
        ))
        ->addElement(array(
            'id' => 'thumbnail_img',
            'type' => 'file',
            'image' => true,
            'label' => __('Recruiter thumbnail image'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
    ));
} else {
    $this->Common->setFlashErrorMessage(__('Recruiter not in company'));
    $this->redirect('/profileinformation/' . $id);
}

if ($this->request->is('post')) {
    $data = $this->getData($modelName);

    $valiadateMode = $model->validateRecruiterInsertUpdate($data);
    if ($valiadateMode) {
        // Processing upload Image 
        if (!empty($_FILES['data']['name'][$modelName]['thumbnail_img'])) {
            $resultImage = $this->Image->uploadImage("{$modelName}.thumbnail_img");
            $data[$modelName]['thumbnail_img'] = $resultImage;
        } else {
            unset($data[$modelName]['thumbnail_img']);
        }
        // set value introductionText for User_recruiter
        $data[$modelName]['introduction_text'] = $data[$modelName]['recruiter_introduction_text'];
        unset($data[$modelName]['recruiter_introduction_text']);
        if ($id = Api::call(Configure::read('API.url_recruiters_addupdate'), $data[$modelName])) {
            if (Api::getError()) {
                return $this->Common->setFlashErrorMessage(Api::getError());
            } else {
                $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                // Redirect to facebookinformation | companyinfomation
//                if (!empty($data[$modelName]['facebook_id'])) {
//                    $this->redirect('/facebookinfomation/' . $id);
//                }
//                if (!empty($data[$modelName]['is_company']) && !empty($data[$modelName]['company_id'])) {
//                    $this->redirect('/companyinfomation/' . $id);
//                }
//                $this->redirect('../profileinformation/' . $id);
            }
        } else {
            AppLog::info("Can not update", __METHOD__, $this->data);
            $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
    $this->redirect($this->request->here(false));
}