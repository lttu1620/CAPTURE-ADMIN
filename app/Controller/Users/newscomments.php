<?php
if (empty($user_id)) {
    $user_id = $this->AppUI->id;
}

$modelName = $this->User->name;

// create breadcrumb
$pageTitle = __('User comments');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/users',
    ))
    ->add(array(
        'name' => $pageTitle,
    ));

$userInfo = Api::Call(Configure::read('API.url_users_detail'), array('id' => $user_id, 'full' => 1), false, array());
$this->Common->handleException(Api::getError());

if(!$this->isMobile()){
    $this->set('profileTab', $this->Common->renderProfileTab($userInfo));
}

$param = $this->getParams(array(
        'page' => 1,
        'limit' => Configure::read('Config.pageSize'),
        'user_id' => $user_id,
        'sort' => 'created-desc',
    ));

list($total, $data) = Api::call(Configure::read('API.url_newscomments_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(array(
        'id' => 'display_image',
        'type' => 'image',
        'title' => __('Comment'),
        'src' => '{display_image}',
        'image_type' => 'user',
        'width' => 68
    ))
    ->addColumn(array(
        'id' => 'display_username',
        'type' => 'link',
        'href' => '/users/profileinformation/{user_id}',
        'target' => 'blank'
    ))
    ->addColumn(array(
        'id' => 'comment_short',
        'title' => __('Comment'),
        'hidden' => true
    ))
    ->addColumn(array(
        'id' => 'news_feed_title',
        'title' => __('Feed title'),
        'type' => 'link',
        'href' => '/newsfeeds/view/{news_feed_id}',
        'width' => 300,
        'target' => 'blank'
    ))
    ->addColumn(array(
        'id' => 'clap_count',
        'type' => 'link',
        'title' => __('Clap count'),
        'width' => 80,
        'button' => true,
    ))
    ->addColumn(array(
        'id' => 'created',
        'title' => __('Created'),
        'type' => 'date',
        'width' => 120,
        'hidden' => true
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'display_username' => array(
            array(
                'field' => 'comment_short',
            ),
            array(
                'field' => 'created',
                'before' => __('Created') . " : ",
                'after' => ''
            ),
        )
    ));
