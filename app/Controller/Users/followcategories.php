<?php

$modelName = $this->User->name;

// process disable / enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Following category list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

$userInfo = Api::Call(Configure::read('API.url_users_detail'), array('id' => $user_id, 'full' => 1), false, array());
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($userInfo));

$data = Api::call(Configure::read('API.url_mobile_followcategories_list'), array('user_id' => $user_id), false, array());
$this->Common->handleException(Api::getError());
$result = array();
foreach ($data as $item) {
    if (!empty($item['follow_category_id'])) {
        $item['id'] = $item['follow_category_id'];
        $result[] = $item;
    }
}
$this->SimpleTable->addColumn(array(
            'id' => 'name',
            'title' => __('Name'),
            'width' => '950'
        ))/*
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Follow'),
            'toggle' => true,            
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 80,
        ))*/
        ->setDataset($result);
