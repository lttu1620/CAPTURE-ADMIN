<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (!$this->AppUI->is_admin && $id <= 0) {
    $param['id'] = $this->AppUI->id;
} else
    $param['id'] = $id == 0 ? -1 : $id;
$param['full'] = true;
$data[$modelName] = Api::Call(Configure::read('API.url_users_detail'), $param);
$this->Common->handleException(Api::getError());

if (!$this->AppUI->is_admin) {
    if ($this->AppUI->company_id != $data[$modelName]['company_id']) {
        AppLog::info("User not same company", __METHOD__, $data[$modelName]['company_id']);
        throw new NotFoundException("User not same company", __METHOD__, $data[$modelName]['company_id']);
    }
}
$pageTitle = __('Update user information');
$listCompany = MasterData::companies_all(array('id', 'name'));

// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));

if(!$this->isMobile()){
    $this->set('profileTab', $this->Common->renderProfileTab($data[$modelName]));
}

// Create Update form user
$this->UpdateForm->reset()
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id' => 'app_id',
        'type' => 'text',
        'readonly' => true,
        'label' => __('App ID')
    ))
    ->addElement(array(
        'id' => 'email',
        'readonly' => true,
        'required' => true,
        'label' => __('Email')
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name'),
        'autocomplete' => 'off',
        'required' => true
    ))
    ->addElement(array(
        'id' => 'nickname',
        'label' => __('Nickname')
    ))
    ->addElement(array(
        'id' => 'sex_id',
        'label' => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty' => Configure::read('Config.StrChooseOne'),
    ));
/*
 * for User
 */
if (empty($data[$modelName]['is_company']) && empty($data[$modelName]['company_id'])) {

    $listCampus = array();
    $listDepartment = array();

    // If has university_id==> call ajax get data campuse
    if (isset($data[$modelName]['university_id'])) {
        try {
            $listCampus = Api::call(Configure::read('API.url_campuses_list'), array(
                    'university_id' => $data[$modelName]['university_id'])
            );
            $listCampus = $this->Common->arrayKeyValue($listCampus, 'id', 'name');

            // Get data for department
            $listDepartment = Api::call(Configure::read('API.url_departments_list'), array(
                    'university_id' => $data[$modelName]['university_id'])
            );
            $listDepartment = $this->Common->arrayKeyValue($listDepartment, 'id', 'name');
        } catch (Exception $exc) {
            //echo $exc->getTraceAsString();
        }
    }
    $this->UpdateForm
        ->addElement(array(
            'id' => 'university_id',
            'type' => 'hidden',
        ))
        ->addElement(array(
            'id' => 'university_name',
            'label' => __('University'),
            'options' => array(
                'url' => "/ajax/autocompleteuniversity",
                'callback' => "callbackUserProfile",
            ),
            'autocomplete_ajax' => true,
            'onblur' => "onblurUniversity(this,'university_id');",
            'required' => true
        ))
        ->addElement(array(
            'id' => 'campus_id',
            'label' => __('Campus '),
            'options' => $listCampus,
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'department_id',
            'label' => __('Department'),
            'options' => $listDepartment,
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'introduction_text',
            'type' => 'textarea',
            'rows' => '10',
            'label' => __('Introduction'),
        ))
        ->addElement(array(
            'id' => 'enrollment_year',
            'label' => __('Enrollment year'),
        ))
        ->addElement(array(
            'id' => 'additional_year',
            'label' => __('Additional year'),
            'maxlength' => 2
        ))
        ->addElement(array(
            'id' => 'image_url',
            'type' => 'file',
            'image' => true,
            'label' => __('Image'),
            'allowEmpty' => true,
            'crop' => !empty($data[$modelName]['id']) ? array('user_id' => $data[$modelName]['id'], 'field' => 'image_url') : false,
    ));
}
/*
 * for User Recruiter
 */
if (!empty($data[$modelName]['is_company']) && !empty($data[$modelName]['company_id'])) {
    $this->UpdateForm
        ->addElement(array(
            'id' => 'user_id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'company_id',
            'type' => 'hidden',
            'label' => __('Company'),
        ))
        ->addElement(array(
            'id' => 'is_admin',
            'type' => 'hidden',
        ))
        ->addElement(array(
            'id' => 'is_approved',
            'type' => 'hidden',
        ))
        ->addElement(array(
            'id' => 'recruiter_introduction_text',
            'type' => 'textarea',
            'rows' => '10',
            'label' => __('Recruiter Introduction'),
        ))
        ->addElement(array(
            'id' => 'thumbnail_img',
            'type' => 'file',
            'image' => true,
            'label' => __('Recruiter thumbnail image'),
            'allowEmpty' => true,
            'crop' => !empty($data[$modelName]['id']) ? array('user_id' => $data[$modelName]['id'], 'field' => 'thumbnail_img') : false,
    ));
}
$this->UpdateForm
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ));
// Process when submit form
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    $valiadateMode = false;
    $valiadateMode = $model->validateUserInsertUpdate($data);
    if ($valiadateMode) {

        // Processing upload Image
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $image_url = $this->Image->uploadImage("{$modelName}.image_url");
            $data[$modelName]['image_url'] = $image_url;
        } elseif (!empty($model->data[$modelName]['image_url']['remove'])) {
            $data[$modelName]['image_url'] = '';
        } else {
            unset($data[$modelName]['image_url']);
        }
        unset($data[$modelName]['password_confirm']);
        unset($data[$modelName]['app_id']);
        unset($data[$modelName]['email']);

        /*
         * For User recruiter
         */
        // Processing upload Image 
        if (!empty($_FILES['data']['name'][$modelName]['thumbnail_img'])) {
            $resultImage = $this->Image->uploadImage("{$modelName}.thumbnail_img");
            $data[$modelName]['thumbnail_img'] = $resultImage;
        } elseif (!empty($model->data[$modelName]['thumbnail_img']['remove'])) {
            $data[$modelName]['thumbnail_img'] = '';
        } else {
            unset($data[$modelName]['thumbnail_img']);
        }
        // set value introductionText for User_recruiter
        if (!empty($data[$modelName]['recruiter_introduction_text'])) {
            $data[$modelName]['introduction_text'] = $data[$modelName]['recruiter_introduction_text'];
            unset($data[$modelName]['recruiter_introduction_text']);
        }
        $id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/profileinformation/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error    
    $this->Common->setFlashErrorMessage($model->validationErrors);
}