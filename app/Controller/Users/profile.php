<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (!$this->AppUI->is_admin) {
    $id = $this->AppUI->id;
}
$this->redirect('profileinformation/' . $id);

$param['id'] = $id;
$param['full'] = true;
$data[$modelName] = Api::Call(Configure::read('API.url_users_detail'), $param);
if (empty($data[$modelName])) {
    AppLog::info("User unavailable", __METHOD__, $param);
    throw new NotFoundException("User unavailable", __METHOD__, $param);
}

$isAdminCompany = $data[$modelName]['recruiter_admin'] == 1 && $data[$modelName]['is_company'] == 1;
$pageTitle = __('Update user profile');

$listCompany = MasterData::companies_all();
$listCompany = $this->Common->arrayKeyValue($listCompany, 'id', 'name');

$listUniversity = MasterData::universities_all_key_value();
$listCampus = array();
$listDepartment = array();

// If has university_id==> call ajax get data campuse
if (isset($data[$modelName]['university_id'])) {
    try {
        $lstData = Api::call(Configure::read('API.url_campuses_list'), array(
                'university_id' => $data[$modelName]['university_id'])
        );
        foreach ($lstData as $item) {
            $listCampus[$item['id']] = (string) $item['name'];
        }

        // Get data for department
        $lstData = Api::call(Configure::read('API.url_departments_list'), array(
                'university_id' => $data[$modelName]['university_id'])
        );
        foreach ($lstData as $item) {
            $listDepartment[$item['id']] = (string) $item['name'];
        }
    } catch (Exception $exc) {
        //echo $exc->getTraceAsString();
    }
}

// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));

// Create Update form user
$userForm = $this->UpdateForm->reset()
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id' => 'submitType',
        'type' => 'hidden',
        'value' => 'UserProfile'
    ))
    ->addElement(array(
        'id' => 'app_id',
        'type' => 'text',
        'readonly' => true,
        'label' => __('App ID')
    ))
    ->addElement(array(
        'id' => 'email',
        'readonly' => true,
        'required' => true,
        'label' => __('Email')
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name'),
        'autocomplete' => 'off',
        'required' => true
    ))
    ->addElement(array(
        'id' => 'nickname',
        'label' => __('Nickname')
    ))
    ->addElement(array(
        'id' => 'sex_id',
        'label' => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'university_id',
        'label' => __('University'),
        'options' => $listUniversity,
        'empty' => Configure::read('Config.StrChooseOne'),
        'onchange' => 'return selectChangeUniversity(this,"campus_id","department_id",1);'
    ))
    ->addElement(array(
        'id' => 'campus_id',
        'label' => __('Campus '),
        'options' => $listCampus,
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'department_id',
        'label' => __('Department'),
        'options' => $listDepartment,
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'introduction_text',
        'type' => 'textarea',
        'rows' => '10',
        'label' => __('Introduction'),
    ))
    ->addElement(array(
        'id' => 'enrollment_year',
        'label' => __('Enrollment year'),
    ))
    ->addElement(array(
        'id' => 'additional_year',
        'label' => __('Additional year'),
        'maxlength' => 2
    ))
    ->addElement(array(
        'id' => 'image_url',
        'type' => 'file',
        'image' => true,
        'label' => __('Image')
    ))
    ->addElement(array(
    'type' => 'submit',
    'value' => __('Save'),
    'class' => 'btn btn-primary pull-left',
    ));
$this->set('userForm', $userForm->get());

//Create form for addupdate user_recruiters
if (!empty($data[$modelName]['is_company']) && !empty($data[$modelName]['company_id'])) {
    $this->UpdateForm->reset();
    $userRecruiterForm = $this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'submitType',
            'type' => 'hidden',
            'value' => 'UserRecruiterProfile'
        ))
        ->addElement(array(
            'id' => 'user_id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'company_id',
            'type' => 'hidden',
            'label' => __('Company'),
        ))
        ->addElement(array(
            'id' => 'recruiter_introduction_text',
            'type' => 'textarea',
            'rows' => '10',
            'label' => __('Recruiter Introduction'),
        ))
        ->addElement(array(
            'id' => 'is_admin',
            'label' => __('Is Admin'),
            'options' => Configure::read('Config.BooleanValue'),
        ))
        ->addElement(array(
            'id' => 'is_approved',
            'label' => __('Is approved'),
            'options' => Configure::read('Config.BooleanValue'),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'thumbnail_img',
            'type' => 'file',
            'image' => true,
            'label' => __('Recruiter thumbnail image'),
        ))
        ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ));
    $this->set('userRecruiterForm', $userRecruiterForm->get());
}

//Create form for user_facebook_information
if (!empty($data[$modelName]['facebook_id'])) {
    $this->UpdateForm->reset();
    $userFacebookForm = $this->UpdateForm
        ->setData($data)
        ->setModelName($modelName)
        ->addElement(array(
            'id' => 'submitType',
            'type' => 'hidden',
            'value' => 'UserFacebookProfile'
        ))
        ->addElement(array(
            'id' => 'user_id',
            'type' => 'hidden',
        ))
        ->addElement(array(
            'id' => 'facebook_id',
            'type' => 'text',
            'readonly' => true,
            'label' => __('Facebook ID')
        ))
        ->addElement(array(
            'id' => 'facebook_username',
            'type' => 'text',
            'readonly' => true,
            'label' => __('Facebook username')
        ))
        ->addElement(array(
            'id' => 'facebook_email',
            'type' => 'text',
            'readonly' => true,
            'label' => __('facebook email')
        ))
        ->addElement(array(
            'id' => 'facebook_image',
            'readonly' => true,
            'image' => true,
            'title' => __('Facebook image'),
        ))
        ->addElement(array(
            'id' => 'facebook_gender',
            'type' => 'text',
            'label' => __('Facebook gender'),
            'readonly' => true,
        ))
        ->addElement(array(
            'id' => 'facebook_name',
            'type' => 'text',
            'label' => __('Facebook name')
        ))
        ->addElement(array(
            'id' => 'facebook_first_name',
            'type' => 'text',
            'label' => __('Facebook first name')
        ))
        ->addElement(array(
            'id' => 'facebook_last_name',
            'type' => 'text',
            'label' => __('facebook last name')
        ))
        ->addElement(array(
            'id' => 'facebook_link',
            'type' => 'text',
            'label' => __('Facebook link')
        ))
        ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ));
    $this->set('userFacebookForm', $userFacebookForm->get());
}

//Create form for user company
if (!empty($data[$modelName]['is_company']) && !empty($data[$modelName]['company_id'])) {
    $this->UpdateForm->reset();
    $userCompanyForm = $this->UpdateForm
        ->setModelName($modelName)
        ->addElement(array(
            'id' => 'submitType',
            'type' => 'hidden',
            'value' => 'UserCompanyProfile'
        ))
        ->addElement(array(
            'id' => 'user_id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'company_id',
            'type' => 'hidden',
            'label' => __('Company id'),
            'readonly' => !$isAdminCompany,
        ))
        ->addElement(array(
            'id' => 'company_name',
            'type' => 'text',
            'label' => __('Company name'),
            'readonly' => !$isAdminCompany,
            'required' => true,
            'value' => isset($data[$modelName]['company_name']) ? $data[$modelName]['company_name'] : ''
        ))
        ->addElement(array(
            'id' => 'kana',
            'type' => 'text',
            'label' => __('Company kana'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_kana']) ? $data[$modelName]['company_kana'] : ''
        ))
        ->addElement(array(
            'id' => 'address',
            'type' => 'text',
            'label' => __('Company address'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_address']) ? $data[$modelName]['company_address'] : ''
        ))
        ->addElement(array(
            'id' => 'description_short',
            'type' => 'textarea',
            'rows' => '3',
            'label' => __('Company description short'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_description_short']) ? $data[$modelName]['company_description_short'] : ''
        ))
        ->addElement(array(
            'id' => 'description',
            'type' => 'textarea',
            'rows' => '10',
            'label' => __('Company description'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_description']) ? $data[$modelName]['company_description'] : ''
        ))
        ->addElement(array(
            'id' => 'thumbnail_img',
            'type' => 'file',
            'image' => true,
            'label' => __('Company thumbnail image'),
            'readonly' => !$isAdminCompany,
            'value' => !empty($data[$modelName]['company_thumbnail_img']) ? $data[$modelName]['company_thumbnail_img'] : '',
        ))
        ->addElement(array(
            'id' => 'background_img',
            'type' => 'file',
            'image' => true,
            'label' => __('Company background image'),
            'readonly' => !$isAdminCompany,
            'value' => !empty($data[$modelName]['company_background_img']) ? $data[$modelName]['company_background_img'] : '',
        ))
        ->addElement(array(
            'id' => 'introduction_movie',
            'type' => 'file',
            'video' => true,
            'label' => __('Company introduction movie'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_introduction_movie']) ? $data[$modelName]['company_introduction_movie'] : ''
        ))
        ->addElement(array(
            'id' => 'corporate_url',
            'type' => 'text',
            'label' => __('Company corporate url'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_corporate_url']) ? $data[$modelName]['company_corporate_url'] : ''
        ))
        ->addElement(array(
            'id' => 'corporate_facebook',
            'type' => 'text',
            'label' => __('Company corporate facebook'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_corporate_facebook']) ? $data[$modelName]['company_corporate_facebook'] : ''
        ))
        ->addElement(array(
            'id' => 'corporate_twitter',
            'type' => 'text',
            'label' => __('Company corporate twitter'),
            'readonly' => !$isAdminCompany,
            'value' => isset($data[$modelName]['company_corporate_twitter']) ? $data[$modelName]['company_corporate_twitter'] : ''
        ))
        ->addElement(array(
        'id' => 'corporate_entry',
        'type' => 'text',
        'label' => __('Company corporate entry'),
        'readonly' => !$isAdminCompany,
        'value' => isset($data[$modelName]['company_corporate_entry']) ? $data[$modelName]['company_corporate_entry'] : ''
    ));
    if ($isAdminCompany) {
        $userCompanyForm->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ));
    }
    $this->set('userCompanyForm', $userCompanyForm->get());
}
if ($this->request->is('post')) {
    // if case add new User
    $data = $this->getData($modelName);
    $submitType = $data[$modelName]['submitType'];
    unset($data[$modelName]['submitType']);
    $valiadateMode = false;
    switch ($submitType) {
        case "UserProfile":
            $valiadateMode = $model->validateUserInsertUpdate($data);
            if ($valiadateMode) {
                // Processing upload Image 
                if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
                    $image_url = $this->Image->uploadImage("{$modelName}.image_url");
                    $data[$modelName]['image_url'] = $image_url;
                } else {
                    unset($data[$modelName]['image_url']);
                }
                unset($data[$modelName]['password_confirm']);
                unset($data[$modelName]['app_id']);
                unset($data[$modelName]['email']);
                if ($id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName])) {
                    if (Api::getError()) {
                        return $this->Common->setFlashErrorMessage(Api::getError());
                    } else {
                        $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                    }
                } else {
                    AppLog::info("Can not update", __METHOD__, $this->data);
                    $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
                }
            } else {
                $this->Common->setFlashErrorMessage($model->validationErrors);
            }
            break;
        case "UserRecruiterProfile" :

            $valiadateMode = $model->validateRecruiterInsertUpdate($data);
            if ($valiadateMode) {
                // Processing upload Image 
                if (!empty($_FILES['data']['name'][$modelName]['thumbnail_img'])) {
                    $resultImage = $this->Image->uploadImage("{$modelName}.thumbnail_img");
                    $data[$modelName]['thumbnail_img'] = $resultImage;
                } else {
                    unset($data[$modelName]['thumbnail_img']);
                }
                // set value introductionText for User_recruiter
                $data[$modelName]['introduction_text'] = $data[$modelName]['recruiter_introduction_text'];
                unset($data[$modelName]['recruiter_introduction_text']);
                if ($id = Api::call(Configure::read('API.url_recruiters_addupdate'), $data[$modelName])) {
                    if (Api::getError()) {
                        return $this->Common->setFlashErrorMessage(Api::getError());
                    } else {
                        $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                    }
                } else {
                    AppLog::info("Can not update", __METHOD__, $this->data);
                    $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
                }
            } else {
                $this->Common->setFlashErrorMessage($model->validationErrors);
            }

            break;
        case "UserFacebookProfile":
            unset($data[$modelName]['facebook_image']);
            unset($data[$modelName]['facebook_email']);
            if ($id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName])) {
                $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                if (Api::getError()) {
                    return $this->Common->setFlashErrorMessage(Api::getError());
                } else {
                    $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                }
            } else {
                AppLog::info("Can not update", __METHOD__, $this->data);
                $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
            }
            break;
        case "UserCompanyProfile":
            if ($isAdminCompany) {

                $data[$modelName]['name'] = $data[$modelName]['company_name'];
                unset($data[$modelName]['company_name']);

                $valiadateMode = $model->validateCompanyInsertUpdate($data);
                if ($valiadateMode) {
                    // Processing upload Image and Video
                    if (!empty($_FILES['data']['name'][$modelName]['thumbnail_img'])) {
                        // $model->data[$modelName]['thumbnail_img'] = $this->Image->uploadImage(
                        $resultImage = $this->Image->uploadImage("{$modelName}.thumbnail_img");
                        $data[$modelName]['thumbnail_img'] = $resultImage;
                    } else {
                        unset($data[$modelName]['thumbnail_img']);
                    }
                    // processing upload background_img
                    if (!empty($_FILES['data']['name'][$modelName]['background_img'])) {
                        // $model->data[$modelName]['thumbnail_img'] = $this->Image->uploadImage(
                        $resultbackground = $this->Image->uploadImage("{$modelName}.background_img");
                        $data[$modelName]['background_img'] = $resultbackground;
                    } else {
                        unset($data[$modelName]['background_img']);
                    }

                    //processing upload introduction movie
                    if (!empty($_FILES['data']['name'][$modelName]['introduction_movie'])) {
                        $resultVideo = $this->Video->upload("{$modelName}.introduction_movie");
                        if ($this->Video->errorMsg) {
                            return $this->Common->setFlashErrorMessage($this->Video->errorMsg);
                        }
                        $data[$modelName]['introduction_movie'] = $resultVideo;
                    } else {
                        unset($data[$modelName]['introduction_movie']);
                    }
                    $data[$modelName]['id'] = $data[$modelName]['company_id'];
                    unset($data[$modelName]['company_id']);

                    // Update thong tin company                   
                    if ($id = Api::call(Configure::read('API.url_companies_addupdate'), $data[$modelName])) {
                        $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                        if (Api::getError()) {
                            return $this->Common->setFlashErrorMessage(Api::getError());
                        } else {
                            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                        }
                    } else {
                        AppLog::info("Can not update", __METHOD__, $this->data);
                        $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
                    }
                } else {
                    $this->Common->setFlashErrorMessage($model->validationErrors);
                }
            } else {
                $this->Common->setFlashErrorMessage(__('You have readonly infomation company'));
            }
            break;
    }
    $this->redirect($this->request->here(false));
} 