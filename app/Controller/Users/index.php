<?php

$modelName = $this->User->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create search conditions    
$app = $this->getParam('app', '');
$listUniversity = MasterData::universities_all_key_value();
$listCampus = MasterData::campuses_all(array('id', 'name'));

// create breadcrumb
$pageTitle = __('User list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'name',
        'label' => __('Username')
    ))
    ->addElement(array(
        'id' => 'nickname',
        'label' => __('Nickname'),
        'empty' => ''
    ))
    ->addElement(array(
        'id' => 'email',
        'label' => __('Email'),
        'empty' => ''
    ))
    ->addElement(array(
        'id' => 'sex_id',
        'label' => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty' => __('All'),
        'empty' => ''
    ))
    ->addElement(array(
        'id' => 'university_id',
        'type' => 'hidden',
    ))
    ->addElement(array(
        'id' => 'university_name',
        'label' => __('University'),
        'options' => array(
            'url' => "/ajax/autocompleteuniversity",
            'callback' => "callbackCampuse",
        ),
        'onblur' => "onblurUniversity(this,'university_id');",
        'autocomplete_ajax' => true,
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => __('All')
    ))
    /*
      ->addElement(array(
      'id' => 'campus_id',
      'label' => __('Campus'),
      'empty' => __('All')
      ))
     * 
     */
    ->addElement(array(
        'id' => 'app',
        'label' => __('Regist from'),
        'options' => array(
            0 => __('Android'),
            1 => __('Ios')
        ),
        'empty' => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => __('ID Asc'),
            'id-desc' => __('ID Desc'),
            'name-asc' => __('Name Asc'),
            'name-desc' => __('Name Desc'),
            'nickname-asc' => __('Nickname Asc'),
            'nickname-desc' => __('Nickname Desc'),
            'email-asc' => __('Email Asc'),
            'email-desc' => __('Email Desc'),
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['is_company'] = 0;
$param['is_ios'] = $app == '1' ? 1 : '';
$param['is_android'] = $app == '0' ? 1 : '';

list($total, $data) = Api::call(Configure::read('API.url_users_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id' => 'image_url',
        'type' => 'image',
        'title' => __('Image'),
        'src' => '{image_url}',
        'image_type' => 'user',
        'width' => '60'
    ))
    ->addColumn(array(
        'id' => 'id',
        'type' => 'link',
        'title' => __('ID'),
        'href' => '/' . $this->controller . '/profile/{id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id' => 'name',
        'type' => 'link',
        'href' => '/' . $this->controller . '/profile/{id}',
        'title' => __('Username'),
        'width' => 250,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'nickname',
        'type' => 'link',
        'href' => '/' . $this->controller . '/profile/{id}',
        'title' => __('Nickname'),
        'width' => 150,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'sex_id',
        'title' => __('Gender'),
        'rules' => Configure::read('Config.searchGender'),
        'empty' => '',
        'width' => 80,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'email',
        'title' => __('Email'),
        'empty' => '',
        'hidden'=> true
    ))
    ->addColumn(array(
        'id' => 'university_name',
        'title' => __('University'),
        'width' => 120,
        'empty' => ''
    )) 
    ->addColumn(array(
        'id' => 'app',
        'title' => __('Regist from'),
        'rules' => array(
            '1' => __('Ios'),
            '2' => __('Android'),
            '0' => __('PC'),
        ),
        'width' => 100
    ))
    ->addColumn(array(
        'id' => 'created',
        'type' => 'date',
        'title' => __('Created'),
        'width' => 120,
        'hidden'=>true
    ))
    ->addColumn(array(
        'type' => 'link',
        'th_title' => __('Password'),
        'title' => __('Change'),
        'href' => '/' . $this->controller . '/password/{id}',
        'button' => true,
        'width' => 100,
    ))
    ->addColumn(array(
        'type' => 'link',
        'title' => __('Setting'),
        'href' => '/usersettings/index/{id}',
        'button' => true,
        'width' => '80'
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'name' => array(
            array(
                'field' => 'email',
                'before' => __('Email') . " : ",
                'after' => ''
            ),
            array(
                'field' => 'created',
                'before' => __('Created') . " : ",
                'after' => ''
            ),
        )
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
