<?php

$modelName = $this->User->name;
$model = $this->{$modelName};

// Create breadcrumb 
$pageTitle = __('Register user');
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/users',
        'name' => __('User list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

$this->UpdateForm->setModelName($modelName)
    ->addElement(array(
        'id' => 'email',
        'label' => __('Email'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'sex_id',
        'label' => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty' => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'device',
        'label' => __('Regist from'),
        'options' => array(
            "android" => __('Android'),
            "ios" => __('Ios')
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'password',
        'type' => 'password',
        'label' => __('Password'),
        'autocomplete' => 'off',
        'required' => true
    ))
    ->addElement(array(
        'id' => 'password_confirm',
        'type' => 'password',
        'label' => __('Confirm password'),
        'autocomplete' => 'off',
        'required' => true
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ));

// Process when submit form
if ($this->request->is('post')) {
    $error = array(
        'email' => array(1011 => __('Email address have registered already'))
    );
    $data = $this->getData($modelName);
    if ($model->validateUserInsertUpdate($data)) {
        //$data = $this->getData($modelName);
        $data[$modelName]['is_company'] = 0;
        $id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/profileinformation/{$id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
