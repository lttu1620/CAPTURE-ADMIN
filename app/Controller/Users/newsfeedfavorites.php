<?php

$modelName = $this->User->name;

// create breadcrumb
$pageTitle = __('News feed favorites list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

$userInfo = Api::Call(Configure::read('API.url_users_detail'), array('id' => $user_id, 'full' => 1), false, array());
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($userInfo));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['user_id'] = $user_id;
$result = Api::call(Configure::read('API.url_mobile_newsfeedfavorites_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$data = $result['data'];
$total = $result['total'];
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
        ->addColumn(array(
            'id' => 'image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{image_url}',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'title',
            'title' => __('Title'),
            'type' => 'link',
            'href' => '/newsfeeds/view/{news_feed_id}',
            'target' =>'blank'
        ))
        ->addColumn(array(
            'id' => 'site_name',
            'title' => __('Site name'),
            'width' => '250'
        ))
        ->addColumn(array(
            'id' => 'news_feed_created',
            'title' => __('Created'),
            'type' => 'date',
            'empty' => '',
             'width' => '120'
        ))
        ->setDataset($data);

