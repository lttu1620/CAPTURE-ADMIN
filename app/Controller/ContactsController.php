<?php

App::uses('AppController', 'Controller');

/**
 * ContactsController class of Contacts Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class ContactsController extends AppController {

    /**
     * Initializes components for ContactsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

   /**
     * Handles user interaction of view index Contacts.
     * 
     * @return void
     */
    public function index() {
        include ('Contacts/index.php');
    }

    /**
     * Handles user interaction of view update Contacts.
     * 
     * @param integer $id ID value of Contacts. Default value is 0.
     * 
     * @return void
     */
    public function update($id = 0) {
        include ('Contacts/update.php');
    }

}
