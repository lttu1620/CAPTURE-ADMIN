<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};
$forgetPasswordForm = $this->UpdateForm->setModelName($modelName)
        ->setAttribute('type', 'post')
        ->addElement(array(
            'id' => 'email',
            'type' => 'input',
            'label' => __('Email'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Send Mail'),
            'class' => 'btn bg-olive btn-block',
        ));
$this->set('forgetPassword', $forgetPasswordForm->get());

if ($this->request->is('post')) {
    $error = array(
        'email' => array(1011 => __('Email has been sent')),
        'is_email' => array(1010 => __('Email address have not registered already')),
    );
    $data = $this->getData($modelName);
    if ($model->validateForgetPassword($data)) {
        $param['email'] = $data[$modelName]['email'];
        $resendLink = "resendRegister('" . Router::url('/ajax/resendforgetpassword') . "','{$param['email']}');return false;";
        $result = Api::call(Configure::read('API.url_users_forgetpassword'), $param);
        if (!empty($result) && !Api::getError()) {
            return $this->Common->setFlashSuccessMessage(
                __('We are to send your information password to email. Please check your email and create new password')
                . ".<br/><a href=\"#\" onclick=\"{$resendLink}\" class=\"resendUrl\">" . __("Resend email for me") . "</a>"
                . ".<br/><a href=\"../\">" . __("to go home.") . "</a> " 
            );
        }else{
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        AppLog::info("Can not update", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}