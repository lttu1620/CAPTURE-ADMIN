<?php
$error = array(
    'token' => array(1010 => __('The token does not exist')),
    'email' => array(1011 => __('This member have been already approved by other admmin'))
);
if (!empty($token)) {
    $param['token'] = $token;
} else {
    $param['token'] = $this->getParam('token');
}
$param['unauthorize'] = true;
$user = Api::call(Configure::read('API.url_users_registeractive'), $param);
if (Api::getError()) { 
    AppLog::warning('Validation error', __METHOD__, $error);
    return $this->Common->setFlashErrorMessage(Api::getError(), $error);
}
if (!empty($user['id'])) {      
    $this->Common->setFlashSuccessMessage(__('Member is approved'));
} else {
    AppLog::warning('Approved error', __METHOD__, $param);
    return $this->Common->setFlashSuccessMessage(__('System error, please try again'));
}
return;