<?php
$error = array(
    'token' => array(1010 => __('The request is already being processed by another user')),
    'email' => array(1011 => __('Email address is already registered'))
);
if (!empty($token)) {
    $param['token'] = $token;
} else {
    $param['token'] = $this->getParam('token');
}
$param['unauthorize'] = true;
$user = Api::call(Configure::read('API.url_users_registeractive'), $param);
if (Api::getError()) {
    AppLog::warning('Validation error', __METHOD__, Api::getError());
    return $this->Common->setFlashErrorMessage(Api::getError(), $error);
}
if (!empty($user)) {
    $this->Common->setFlashSuccessMessage(__('Your registration is activated'));
    $type = Configure::read('Config.UserActivationRegistType');
    if ($user['regist_type'] == $type['registerRecruiter']) {        
        if (!empty($user['id'])) {
            $param = array(
                'user_id' => $user['id'],
                'regist_type' => 'user',
                'unauthorize' => true
            );
            $user['token'] = Api::call(Configure::read('API.url_users_token'), $param);           
        }        
        $this->createLoginSession($user);       
        return $this->redirect('/');
    }   
} else {
    AppLog::warning('Active email error', __METHOD__, $param);
    return $this->Common->setFlashSuccessMessage(__('System error, please try again'));
}