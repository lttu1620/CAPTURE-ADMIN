<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};
$t = time();
// Check token is valid and expire date  ???
if (!empty($token)) {
    $param['token'] = $token;
} else {
    $param['token'] = $this->getParam('token');
}
$param['expire_date'] = $t;
$param['regist_type'] = Configure::read('Config.UserActivationRegistType')['forgetPassword'];
$activationDetail = Api::call(Configure::read('API.url_useractivations_check'), $param);

$newPasswordForm = $this->UpdateForm
        ->setModelName($modelName);
if ($token != "" && !empty($activationDetail)) {
    switch ($activationDetail) {
        case 3:// Check token and expire date is valid
            $newPasswordForm->setAttribute('type', 'post')
                    ->addElement(array(
                        'id' => 'password',
                        'type' => 'password',
                        'autocomplete' => 'off',
                        'label' => __('New password'),
                    ))
                    ->addElement(array(
                        'id' => 'password_confirm',
                        'type' => 'password',
                        'autocomplete' => 'off',
                        'label' => __('Confirm password'),
                    ))
                    ->addElement(array(
                        'type' => 'submit',
                        'value' => __('Submit'),
                        'class' => 'btn bg-olive btn-block',
            ));
            break;
        case 1: // Token invalid
            $newPasswordForm
                    ->addElement(array(
                        'type' => 'label',
                        'value' => 'Token invalid'
            ));
            break;
        case 2:// Token expire date
            $newPasswordForm
                    ->addElement(array(
                        'type' => 'label',
                        'value' => 'Token has expired Date.'
            ));
            break;
        case 4:// reset password Done
            $this->set('hasChanged', 4);
    }
} else {
    $newPasswordForm
            ->addElement(array(
                'type' => 'label',
                'value' => 'Token invalid or expired Date.'
    ));
}
$this->set('renewPassword', $newPasswordForm->get());
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateResetPassword($data)) {
        $param['password'] = $data[$modelName]['password'];
        unset($param['expire_date']);
        if ($token != "") {
            // Call API update password from user_activations.token
            if ($result = Api::call(Configure::read('API.url_userprofiles_updatepassword'), $param)) {
                $this->Common->setFlashSuccessMessage(__('Create new password successfuly'));
                $this->redirect('../login/');
            } else {
                AppLog::info("Can not update password", __METHOD__, $this->data);
                $this->Common->setFlashErrorMessage(__('Create new password not successfull'));
            }
        }
    }
}