<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};

$data = array();
if (!empty($this->AppUI->name) && !empty($this->AppUI->name)) {
    $data[$modelName]['name'] = $this->AppUI->name;
}
if (!empty($this->AppUI->nickname)) {
    $data[$modelName]['nickname'] = $this->AppUI->nickname;
}
if (!empty($this->AppUI->sex_id)) {
    $data[$modelName]['sex_id'] = $this->AppUI->sex_id;
}
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->setAttribute('type', 'post')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),           
            'required' => true
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname'),            
            'required' => true
        ))
        ->addElement(array(
            'id' => 'sex_id',
            'label' => __('Gender'),
            'options' => Configure::read('Config.searchGender'),
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true           
        ))        
        ->addElement(array(
            'type'      => 'select',
            'id'        => 'accept_policy',
            'label' => __('Accept Policy'),
            'options' => array("accept"=>"　規約に同意する"),
            'multiple'  => 'checkbox',
            'after'   => "<div>→ <a href=\"../files/capture_recruiter_policy.pdf\" target=\"_blank\">利用規約を読む</a></div>",
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Update profile'),
            'class' => 'btn bg-capture-red btn-block',
            'style' => 'width:320px;',
        ));

if ($this->request->is('post')) {    
    if ($model->validateRegisterProfile($this->getData($modelName))) {
        $param['id'] = $this->AppUI->id;
        $param['name'] = $model->data[$modelName]['name'];                
        $param['nickname'] = $model->data[$modelName]['nickname'];                
        $param['sex_id'] = $model->data[$modelName]['sex_id'];
        $param['accept_policy'] = $model->data[$modelName]['accept_policy'];
        $userId = Api::call(Configure::read('API.url_users_addupdate'), $param);
        $this->AppUI->redirect_register_profile = 0;
        $this->AppUI->redirect_register_company = 0;
        $this->Auth->login($this->AppUI);           
        return $this->redirect('/');
    }
    return $this->Common->setFlashErrorMessage($model->validationErrors);   
}