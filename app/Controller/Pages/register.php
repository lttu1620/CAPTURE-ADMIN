<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};

$this->UpdateForm->setModelName($modelName)
    ->setAttribute('type', 'post');
$token = $this->getParam('token');
if (!empty($token)) {
    $checkToken = Api::call(Configure::read('API.url_user_activation_check_token_invite'), array('token' => $token));
    if (Api::getError()) {
        $error = array(
            'token' => array(1010 => __('The token does not exist')),
            'token_disable' => array(1021 => __('Token has been disabled')),
            'expire_date' => array(1021 => __('Token has expired date')),
        );
        $this->Common->setFlashErrorMessage(Api::getError(), $error);
    } else {
        $this->UpdateForm->addElement(
            array(
                'id'       => 'email_mask',
                'label'    => __('Email'),
                'value'    => $checkToken['email'],
                'disabled' => true,
            )
        )
            ->addElement(
                array(
                    'id'       => 'email',
                    'label'    => __('Email'),
                    'value'    => $checkToken['email'],
                    'type'  => 'hidden',
                )
            )
            ->addElement(
                array(
                    'id'    => 'regist_type',
                    'label' => __('Regist type'),
                    'value' => $checkToken['regist_type'],
                    'type'  => 'hidden',
                )
            )
            ->addElement(
                array(
                    'id'    => 'company_id',
                    'label' => __('Company Id'),
                    'value' => $checkToken['company_id'],
                    'type'  => 'hidden',
                )
            )
            ->addElement(
                array(
                    'id'    => 'token',
                    'label' => __('Token'),
                    'value' => $checkToken['token'],
                    'type'  => 'hidden',
                )
            );
    }
} else {
    $this->UpdateForm->addElement(
        array(
            'id'    => 'email',
            'label' => __('Email'),
            'value' => isset($this->data[$modelName]['email']) ? $this->data[$modelName]['email'] : '',
        )
    )
        ->addElement(
            array(
                'id'    => 'regist_type',
                'label' => __('Regist type'),
                'value' => 'register_recruiter',
                'type'  => 'hidden',
            )
        );
}
$this->UpdateForm->addElement(
    array(
        'id'    => 'password',
        'type'  => 'password',
        'label' => __('Password'),
        'value' => '',
    )
)
    ->addElement(
        array(
            'id'    => 'password_confirm',
            'type'  => 'password',
            'label' => __('Confirm password'),
            'value' => '',
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Register'),
            'class' => 'btn bg-capture-red btn-block',
            'style' => 'width:320px;',
        )
    );

if ($this->request->is('post')) {
    if ($model->validateRegister($this->getData($modelName))) {
        $param['email'] = $model->data[$modelName]['email'];
        $param['password'] = $model->data[$modelName]['password'];
        $param['regist_type'] = $model->data[$modelName]['regist_type'];
        if ($param['regist_type'] == 'recruiter_admin' || $param['regist_type'] == 'recruiter_member') {
            // register by invite with token
            $param['token'] = $model->data[$modelName]['token'];
            $param['company_id'] = $model->data[$modelName]['company_id'];
            $user = Api::call(Configure::read('API.url_user_register_by_invite'), $param);
            if (Api::getError()) {
                return $this->Common->setFlashErrorMessage(Api::getError(), $error);
            }
            if (!empty($user)) {
                $this->Common->setFlashSuccessMessage(__('Your registration is activated'));
                if (!empty($user['id'])) {
                    $param = array(
                        'user_id' => $user['id'],
                        'regist_type' => 'user',
                        'unauthorize' => true
                    );
                    $user['token'] = Api::call(Configure::read('API.url_users_token'), $param);
                }
                $this->createLoginSession($user);
                return $this->redirect('/');
            } else {
                AppLog::warning('Active email error', __METHOD__, $param);
                return $this->Common->setFlashSuccessMessage(__('System error, please try again'));
            }

        } else { // register normal
            $resendLink = "resendRegister('".Router::url(
                    '/ajax/resendregisteremail'
                )."','{$param['email']}');return false;";
            Api::call(Configure::read('API.url_users_registeremail'), $param);
            if (Api::getError()) {
                if (isset(Api::getError()['email'][1021])) {
                    $error = array(
                        'email' => array(
                            1021 => __(
                                    'Email address is already registered and waiting activation'
                                )."<br/><a href=\"#\" onclick=\"{$resendLink}\" class=\"resendUrl\">".__(
                                    "Resend email for me"
                                )."</a>",
                        ),
                    );
                } else {
                    $error = array(
                        'email' => array(
                            1011 => __('Email address is already registered'),
                        ),
                    );
                }
                return $this->Common->setFlashErrorMessage(Api::getError(), $error);
            }
            return $this->Common->setFlashSuccessMessage(
                __('Please check email to activate account')
                .".<br/><a href=\"#\" onclick=\"{$resendLink}\" class=\"resendUrl\">".__("Resend email for me")."</a>"
            );
        }
    }
    return $this->Common->setFlashErrorMessage($model->validationErrors);
}