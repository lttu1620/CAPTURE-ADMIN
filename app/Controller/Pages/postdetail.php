<?php
$this->layout = 'other';
if (!empty($id)) {
    $param['id'] = $id;
    $data = Api::call(Configure::read('API.url_posts_detail'), $param);
    $this->Common->handleException(Api::getError());
    $this->setPageTitle($data['post_title']);
    $this->set('data', $data);
} else {
    return $this->redirect("/{$this->controller}/");
}
