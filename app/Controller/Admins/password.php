<?php

if (!$this->AppUI->is_admin) {
    $this->redirect('/');
}
$modelName = $this->Admin->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Change password');
// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle
        ));
// Create Update form user
$this->UpdateForm
        ->setModelName($modelName)
        //Create form for addupdate user
        ->addElement(array(
            'id' => 'password',
            'type' => 'password',
            'label' => __('New password'),
            'autocomplete' => 'off'
        ))
        ->addElement(array(
            'id' => 'password_confirm',
            'type' => 'password',
            'label' => __('Confirm password'),
            'autocomplete' => 'off'
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    if ($model->validate_passwords_admin($this->getData($modelName))) {
        $param = array(
            'id' => $id,
            'password' => $model->data[$modelName]['password']
        );
        Api::call(Configure::read('API.url_admins_password'), $param);
        if (Api::getError()) {
            AppLog::info("Can not update ", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        }
        $this->redirect($this->request->here(false));
    } else {
        AppLog::info("Can not update password", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}