<?php

$modelName = $this->Admin->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Admin');
if (!empty($id)) {
    $pageTitle = __('Edit Admin');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_admins_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/admins',
            'name' => __('Admin list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// Create Update form 
$this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'login_id',
            'type' => 'input',
            'readonly' => !empty($id),
            'label' => __('Login ID')
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => 'name',
            'autocomplete' => 'off'
        ))
        ->addElement(array(
            'id' => 'admin_type',
            'label' => __('Admin Type'),
            'options' => Configure::read('Config.searchAdminType'),
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ));

// case add new admin
if (empty($id)) {
    $this->UpdateForm
            ->addElement(array(
                'id' => 'password',
                'type' => 'password',
                'autocomplete' => 'off',
                'label' => __('Password')
            ))
            ->addElement(array(
                'id' => 'password_confirm',
                'type' => 'password',
                'label' => __('Confirm password')
    ));
}
$this->UpdateForm->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {    
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_admins_addupdate'), $model->data[$modelName]);        
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error    
    $this->Common->setFlashErrorMessage($model->validationErrors);
}