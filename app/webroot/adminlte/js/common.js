function paging(url, div) {
    $.get(url, {}, function (result) {
        if (result) {
            $('#' + div).html(result);
        }
    });
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}
function isValidDate(dateStr, format) {
    if (format == null) {
        format = "MDY";
    }
    format = format.toUpperCase();
    if (format.length != 3) {
        format = "MDY";
    }
    if ((format.indexOf("M") == -1) || (format.indexOf("D") == -1) || (format.indexOf("Y") == -1)) {
        format = "MDY";
    }
    if (format.substring(0, 1) == "Y") {
        var reg1 = /^\d{2}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
        var reg2 = /^\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
    } else if (format.substring(1, 2) == "Y") {
        var reg1 = /^\d{1,2}(\-|\/|\.)\d{2}\1\d{1,2}$/
        var reg2 = /^\d{1,2}(\-|\/|\.)\d{4}\1\d{1,2}$/
    } else {
        var reg1 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{2}$/
        var reg2 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/
    }
    if ((reg1.test(dateStr) == false) && (reg2.test(dateStr) == false)) {
        return false;
    }
    var parts = dateStr.split(RegExp.$1);
    if (format.substring(0, 1) == "M") {
        var mm = parts[0];
    } else if (format.substring(1, 2) == "M") {
        var mm = parts[1];
    } else {
        var mm = parts[2];
    }
    if (format.substring(0, 1) == "D") {
        var dd = parts[0];
    } else if (format.substring(1, 2) == "D") {
        var dd = parts[1];
    } else {
        var dd = parts[2];
    }
    if (format.substring(0, 1) == "Y") {
        var yy = parts[0];
    } else if (format.substring(1, 2) == "Y") {
        var yy = parts[1];
    } else {
        var yy = parts[2];
    }
    if (parseFloat(yy) <= 50) {
        yy = (parseFloat(yy) + 2000).toString();
    }
    if (parseFloat(yy) <= 99) {
        yy = (parseFloat(yy) + 1900).toString();
    }
    var dt = new Date(parseFloat(yy), parseFloat(mm) - 1, parseFloat(dd), 0, 0, 0, 0);
    if (parseFloat(dd) != dt.getDate()) {
        return false;
    }
    if (parseFloat(mm) - 1 != dt.getMonth()) {
        return false;
    }
    return true;
}
function isPhoneNumber(str) {
    var alphaExp = /^((\(\+?84\)[\-\.\s]?)|(\+?84[\-\.\s]?)|(0))((\d{3}[\-\.\s]?\d{6})|(\d{2}[\-\.\s]?\d{8}))$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isAlphabet(str) {
    var alphaExp = /^[a-zA-Z]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isAlphabetAndNumber(str) {
    var alphaExp = /^[a-zA-Z0-9_]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isUserName(str) {
    var alphaExp = /^[a-zA-Z0-9_.\-]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isEmail(email) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(email);
}
;
function isNumber(str) {
    var alphaExp = /^[0-9]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function strip_tags(str) {
    str = str.replace(/&nbsp;/g, '');
    str = jQuery.trim(str);
    allowed_tags = '';
    var key = '', allowed = false;
    var matches = [];
    var allowed_array = [];
    var allowed_tag = '';
    var i = 0;
    var k = '';
    var html = '';
    var replacer = function (search, replace, str) {
        return str.split(search).join(replace);
    };
    // Build allowes tags associative array
    if (allowed_tags) {
        allowed_array = allowed_tags.match(/([a-zA-Z]+)/gi);
    }
    str += '';
    // Match tags
    matches = str.match(/(<\/?[\S][^>]*>)/gi);
    // Go through all HTML tags
    for (key in matches) {
        if (isNaN(key)) {
// IE7 Hack
            continue;
        }
// Save HTML tag
        html = matches[key].toString();
        // Is tag not in allowed list? Remove from str!
        allowed = false;
        // Go through all allowed tags
        for (k in allowed_array) {
// Init
            allowed_tag = allowed_array[k];
            i = -1;
            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + '>');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + ' ');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('</' + allowed_tag);
            }

// Determine
            if (i == 0) {
                allowed = true;
                break;
            }
        }
        if (!allowed) {
            str = replacer(html, "", str); // Custom replace. No regexing
        }
    }
    return str;
}
function checkAll(strItemName, value) {
    var x = document.getElementsByName(strItemName);
    for (var i = 0; i < x.length; i++) {
        if (value == 1 && !x[i].disabled) {
            if (!x[i].checked)
                x[i].checked = 'checked';
        } else {
            if (x[i].checked)
                x[i].checked = '';
        }
    }
}
function getItemsChecked(strItemName, sep) {
    var x = document.getElementsByName(strItemName);
    var p = "";
    for (var i = 0; i < x.length; i++) {
        if (x[i].checked) {
            p += x[i].value + sep;
        }
    }
    var result = (p != '' ? p.substr(0, p.length - 1) : '');
    return result;
}
deleteItem = function (id) {
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    $("#action").val('delete');
    $("#actionId").val(id);
    $("#dataForm").submit();
}
disableItem = function (id, disable) {
    if (typeof disable == "undefined") {
        return false;
    }
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    $("#action").val(disable == 0 ? 'disable' : 'enable');
    $("#actionId").val(id);
    $("#dataForm").submit();
}
enableItem = function (id) {
    if (confirm('Are you sure') == false && $("#item_" + id).length > 0) {
        return false;
    }
    $("#action").val('enable');
    $("#actionId").val(id);
    $("#dataForm").submit();
}
back = function () {
    if (referer.indexOf(url) === -1) {
        location.href = referer;
    } else {
        location.href = '/' + controller;
    }
    return false;
}

selectChangeUniversity = function (_element, _campus, _deparment, _mode) {
    // Remove all old elementnot first element;
    $('#' + _campus + ' :not(:eq(0)), #' + _deparment + " :not(:eq(0))").remove();
    id = _element.value;
    // ajax

    loading_ajax('/Ajax/university/' + id,
        "",
        function () {// function on before send
        },
        function (msg) {// action when successfull
            if (_campus !== undefined && _campus !== "") {
                var _lstCampus = msg.Campus;
                $.each(_lstCampus, function (idx, obj) {
                    $('#' + _campus).append($("<option>").val(obj.id).append(obj.value));
                });
            }
            //
            if (_deparment !== undefined && _deparment !== "") {
                var _lstDepartment = msg.Department;
                $.each(_lstDepartment, function (idx, obj) {
                    $('#' + _deparment).append($("<option>").val(obj.id).append(obj.value));
                });
            }
        },
        function () { // function on error
        }, true
    );
    return false;
}

likeComment = function (commentId, userId, feedId, is_like, page, limit) {
    $.ajax({
        type: "POST",
        url: '/Ajax/likecomment/' + commentId + "/" + userId,
        data: {page: page, limit: limit, status: is_like, feedId: feedId},
        success: function (msg) {
            $('#chat-box').html(msg);
        }
    });
    return false;
}
onblurUniversity = function (_element, _refElementID) {
    if (_element.value.trim() == "")
        $('#' + _refElementID).val('');
    return false;
}
function ChangeDataType(obj) {
    $('.DivEffect').switchClass('Show', 'Hidden', 500, 'easeInOutQuad'); //.removeClass('Show').addClass('Hidden');
    if (obj.value != "")
        $(".div" + obj.value).switchClass('Hidden', 'Show', 500, 'easeInOutQuad'); //.addClass('Show');
    else
        $(".divnumber").switchClass('Hidden', 'Show', 500, 'easeInOutQuad');
    return false;
}
var base_ajax_time_out = 120000;
var base_ajax_time_out_message = "システムタイムアウトを発生しました。";
var base_url_system_error = "/Error/400";
// Common Ajax function
function loading_ajax(url, data_post, before_send, on_success, on_error, is_async) {
    if (is_async == undefined || is_async == null) {
        is_async = true;
    }
    $.ajax({
        url: url,
        data: data_post,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        async: is_async,
        timeout: base_ajax_time_out,
        beforeSend: function (xhr) {
            before_send();
        },
        success: function (msg) {
            if (msg != null && msg != "") {
                on_success(msg)
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            if (xhr.status == 0) {
                return false;
            }
            if (on_error == null || on_error == undefined) {
                return false;
            }
            if (textStatus === "timeout") {
                alert(base_ajax_time_out_message);
                return false;
            }
            //window.location.href = base_url_system_error;
        }
    });
}

$(function () {
    $(".btn-disable").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        $("#action").val('disable');
        return true;
    });
    $(".btn-enable").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        $("#action").val('enable');
        return true;
    });
    $(".btn-select-kill").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        if (confirm('Are you sure?') == false) {
            return false;
        }
        location.href = baseUrl + controller + '/ps/id=' + items;
        return false;
    });
    $(".btn-search").click(function () {
        var action = $("#searchForm").attr('action');
    });
    $(".btn-addnew").click(function () {
        location.href = baseUrl + controller + '/update';
        return false;
    });
    $(".btn-invite").click(function () {
        location.href = baseUrl + 'invites/update';
        return false;
    });
    
    if ($('#pages_login').length > 0) {
        var hash = window.location.hash.substr(1);
        var thisRegex = new RegExp(/access_token=*/);
        if (thisRegex.test(hash)) {
            $('#login-box').html('<div style="width:100%;text-align:center;margin-top:50%;">' +
                '<img src="'+baseUrl+'img/grid.svg"/>' +
                '</div>');  
            var value = hash.split('=');
            var value = value[1].split('&');
            var params = {
                token: value[0]
            };           
            var url = baseUrl + 'ajax/fblogin';
            $.ajax({
                cache: false,
                async: true,
                data: params,
                url: url,
                success: function (json) {
                    response = jQuery.parseJSON(json); 
                    if (response.error !== 0) {
                        alert(response.message);
                    } else {
                        location.href = baseUrl;
                    }
                }
            });  
        }
    }
    
    
    $("#fblogin").click(function () {
        window.open('https://www.facebook.com/dialog/oauth?client_id='+fb_app_id+'&redirect_uri='+ document.location.href + '&scope=email,public_profile&response_type=token', '', null);
        return false;
        FB.login(function (response) {
            if (response.authResponse) {
                FB.api('/me', function (response) {
                    console.log(response);
                    return false;
                    var params = response;
                    var url = baseUrl + 'ajax/fblogin';
                    $.ajax({
                        cache: false,
                        async: true,
                        data: params,
                        url: url,
                        success: function (redirectUrl) {
                            if (redirectUrl) {
                                location.href = redirectUrl;
                            }
                        }
                    });
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: 'email,user_likes'});
    });
   
   
    if ($('.form-body .js-thumb').length > 0) {
        $('.form-body .js-thumb').lightBox();
    }
    var collapse = getCookie('collapse');
    if (collapse == '') {
        collapse = '1';
    }
    if (collapse == '0') {
        $('.search-body').show();
        if ($(".search-collapse").length > 0) {
            //$("[data-widget='collapse']").click();
            $(".search-collapse").click();
        }
    }
    $(".search-collapse").click(function () {
        if (collapse == '1') {
            collapse = '0';
        } else {
            collapse = '1';
        }
        setCookie('collapse', collapse, 1);
        return true;
    });
    $(".dialog").click(function () {
        $('body').append("<div id=\"dialog\"></div>");
        var dialogTitle = $(this).attr('alt');
        var url = $(this).attr('href');
        var data = {};
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response) {
                $("#dialog").html(response);
                $("#dialog").dialog({
                    title: dialogTitle,
                    minWidth: 600,
                    minHeight: 200,
                    modal: true,
                    draggable: true,
                    /*
                     buttons: [{
                     text: "Close",
                     click: function () {
                     $(this).dialog("close");
                     }
                     }] */
                });
            }
        });
        return false;
    });
    $(".btn-statistic").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        return true;
    });
    $(".crop-img").click(function (e) {
        e.preventDefault();
        var url = '/images/get_image?img=' + urldecode($('a.js-thumb').attr('href'));
        $('.resize-image').attr('src', url);
        resizeableImage($('.resize-image'));
        $("#modal-resize").modal('show');
    });
    $('.toggle').show();
    $(".toggle-group").click(function () {
         var _selector = $(this).closest("div.toggle").find('input[data-toggle=toggle]'); 
         _selector.prop('checked', !_selector.prop('checked')).change();
        var id  = _selector.val();  
           
        var data = null;
        var _url = "";

        if (_selector.hasClass('toggle-is_public')) {
            var disable = _selector.prop('checked') ? 1 : 0;
            _url = baseUrl + 'ajax/ispublic';
            data = {
                controller: controller,
                id: id,
                value: disable
            }
        } else if (_selector.hasClass('post_public')) {
            var disable = _selector.prop('checked') ? 1 : 0;
            _url = baseUrl + 'ajax/postpublic';
            data = {
                controller: controller,
                id: id,
                value: disable
            }
        } else if (_selector.hasClass('is_tadacopy')) {
            var disable = _selector.prop('checked') ? 1 : 0;
            _url = baseUrl + 'ajax/istadacopy';
            data = {
                controller: controller,
                id: id,
                value: disable
            }
        } else {
            _url = baseUrl + 'ajax/disable';
            var disable = _selector.prop('checked') ? 0 : 1;
            data = {
                controller: controller,
                action: action,
                id: id,
                disable: disable
            };
        }
        // console.log(data);
        // return true;
        $.ajax({
            type: "POST",
            url: _url,
            data: data,
            success: function (response) {
                if (response) {
                    response=  $.parseJSON(response);  
                    if(response['message'] !="")
                    {
                        // revert status for button
                        var _parent = _selector.closest("div.toggle");  
                        var _class = ""; 
                        if(_parent.hasClass('off')){
                            _class='off';
                        }else{
                             _class ='on';
                        }
                        setTimeout( function() {
                               _selector.prop('checked', !_selector.prop('checked')).change();
                        },200);                       
                        
                        alert(response['message']);
                    }
                }
            }
        });
        return false;
    });
    $('.btn-recruiter-disable').click(function (e) {
        e.preventDefault();
        if (confirm("Are you sure?")) {
            $.ajax({
                type: "POST",
                url: e.currentTarget.href,
                data: {
                    controller: controller,
                    action: action,
                    id: $(e.currentTarget).attr('data-id'),
                    disable: 1
                }
            }).done(function (response) {
                location.reload();
            });
        }
    });

    //admin | approved toggle click
    $(".toggle input.admin,input.approved").change(function () {
        var _class = $(this).attr('class');
        var id = $(this).val();
        var _value = $(this).prop('checked') ? 1 : 0;

        var data = {
            controller: controller,
            id: id,
            value: _value,
        };
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/' + _class,
            data: data,
            success: function (response) {
                if (response) {
                    alert(response);
                }
            }
        });
        return false;
    });

    // Handler error load image 
    $.each($("div.form-group ").find('img'), function (_index, _object) {
        // check load image error
        $("<img/>")
            .error(function () {
                $(_object).closest('div').css({'max-width': ''}).html($('<span>').css({'color': 'red'}).append($(_object).attr("src") + ': image url not exists.'));
            })
            .attr("src", $(_object).attr("src"))
        ;
    });
});
changeAdminApprovedItem = function (id, value, option) {
    if (typeof value == "undefined") {
        return false;
    }
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    if (option == 0) {
        $("#action2").val(value == 0 ? 'is_admin' : 'user');
    } else if (option == 1) {
        $("#action2").val(value == 0 ? 'is_approved' : 'user');
    } else {
        return false;
    }
    $("#actionId2").val(id);
    $("#optionId2").val(option);
    $("#dataForm").submit();
}
DrawChart = function (_element, _data, _xkey, _ykeys, _labels, _lineColors, _type, _mode) {
    if (_type === undefined || _type == null) {
        _type = 'line-chart';
    }
// Reset html of element before Draw chart
    $('#' + _element).html();
    switch (_type) {
        case "bar_chart":
            var bar = new Morris.Bar({
                element: _element,
                resize: false,
                data: _data,
                barColors: _lineColors,
                xkey: _xkey,
                ykeys: _ykeys,
                labels: _labels,
                hideHover: 'auto',
                xLabelFormat: function (d) {
                    return d.src.labels;
                },
            });
            break;
        case "sales_chart":
            // Sales chart
            var area = new Morris.Area({
                element: _element,
                resize: false,
                data: _data,
                xkey: _xkey,
                ykeys: _ykeys,
                labels: _labels,
                lineColors: _lineColors,
                hideHover: 'auto',
                xLabelFormat: function (d) {
                    var xlable = MonthChart(_mode, d);
                    return xlable
                },
            });
            break;
        case "line-chart":
        default:
            //Line Chart
            var line = new Morris.Line({
                element: _element,
                resize: false,
                data: _data,
                xkey: _xkey,
                ykeys: _ykeys,
                labels: _labels,
                lineColors: _lineColors,
                hideHover: 'auto',
                xLabelFormat: function (d) {
                    console.log(d);
                    var xlable = MonthChart(_mode, d);
                    return xlable
                },
            });
            break;
    }
}
function MonthChart(_mode, _date) {

    switch (_mode) {
        case "week":
        case "day":
            return $.datepicker.formatDate('dd/mm/yy', _date);
//            var to = _date.setTime(_date.getTime() - (_date.getDay() ? _date.getDay() : 7) * 24 * 60 * 60 * 1000);
//            var from = to + 6 * 24 * 60 * 60 * 1000;
//            return  $.datepicker.formatDate('dd/mm',new Date(to))    + "->" +
//                    $.datepicker.formatDate('dd/mm/yy',new Date(from));
            break;
        case "month":
            var _lastdatmonth = new Date(_date.getFullYear(), _date.getMonth() + 1, _date.getDate() - 1);
            return $.datepicker.formatDate('dd/mm', (_date < new Date(document.getElementById('date_from').value) ? new Date(document.getElementById('date_from').value) : _date))
                + "->" +
                $.datepicker.formatDate('dd/mm/yy',
                    _lastdatmonth > new Date(document.getElementById('date_to').value) ? new Date(document.getElementById('date_to').value) : _lastdatmonth);
            break;
    }
    return;
}

DrawChartNew = function (_element, _data, _type, _mode, _categories) {
    // Reset html of element before Draw chart
    $('#' + _element).html();
    switch (_type) {
        case "sales_chart":
            $('#' + _element).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'area'
                },
                title: {
                    text: (_mode == "day" ? 'Daily' : _mode == 'week' ? 'Weekly' : 'Mothly') + ' Average Newsfeed view'
                },
                subtitle: {
                    //text: 'Source: http://www.oceanize.jp'
                },
                xAxis: {
                    categories: JSON.parse(_categories)
                },
                yAxis: {
                    title: {
                        //text: 'Temperature (°C)'
                    }
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black, 0 0 3px black'
                            }
                        }
                    }
                },
                series: _data
            });

            break;
        case "bar_chart":
            $('#' + _element).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: (_mode == "day" ? 'Daily' : _mode == 'week' ? 'Weekly' : 'Mothly') + ' Average Newsfeed view'
                },
                subtitle: {
                    //text: 'Source: http://www.oceanize.jp'
                },
                xAxis: {
                    categories: JSON.parse(_categories)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total fruit consumption'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black, 0 0 3px black'
                            }
                        }
                    }
                },
                series: _data
            });
            break;
        case "line_chart":
            $('#' + _element).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'line'
                },
                title: {
                    text: (_mode == "day" ? 'Daily' : _mode == 'week' ? 'Weekly' : 'Mothly') + ' Average Newsfeed view'
                },
                subtitle: {
                    //text: 'Source: http://www.oceanize.jp'
                },
                xAxis: {
                    categories: JSON.parse(_categories)
                },
                yAxis: {
                    title: {
                        text: 'Temperature (°C)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + ' ngày ' + this.x + '</b>: ' + Highcharts.numberFormat(this.y, 0, ',', '.');
                    },
                    backgroundColor: '#FCFFC5',
                    borderColor: 'black',
                    borderRadius: 10,
                    borderWidth: 2,
                    valueDecimals: 0
                },
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    borderWidth: 0
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: _data
            });
            break;
    }


}
callbackCampuse = function (item) {
    $('#university_id').val(item.id);
}

callbackUserProfile = function (item) {
    $('#university_id').val(item.id);
    selectChangeUniversity(document.getElementById('university_id'), 'campus_id', 'department_id', 1);
}

autocomplete = function (id, url, func) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: url,
                dataType: "jsonp",
                data: {
                    q: request.term
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            func(ui.item);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
}
updateStatistic = function (id) {
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    $("#actionId2").val(id);
    $("#dataForm").submit();
}
function tagDelete(tagId, feedId) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/deletefeedtag',
        data: {tag_id: tagId, news_feed_id: feedId},
        success: function (msg) {
            if (msg == 'OK') {
                $('.tag-' + feedId + '-' + tagId).remove();
            }
        }
    });
    return false;
}

function showListTag(feedId, tagType) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/showlisttag',
        data: {feedId: feedId, tagType: tagType},
        success: function (response) {
            if (response != '') {
                $('.tag-select-' + feedId).html(response);
                $('.tag-select-' + feedId).show();
                $('#select-tag-id-' + feedId).chosen({});
            }
        }
    });

    return false;
}

function addfeedtag(feedId, tagId) {
    val_arr = tagId.split('-');
    tag_id = val_arr[0];
    tag_color = val_arr[1];
    style = "";
    if (tag_color != '') {
        style = "style='background-color:" + tag_color + "'";
    }
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/addfeedtag',
        data: {tag_id: tag_id, news_feed_id: feedId},
        success: function (msg) {
            if (msg == 'OK') {
                if ($('.tag-' + feedId + '-' + tag_id).length == 0) {
                    tagname = $('#select-tag-id-' + feedId + ' option:selected').text();
                    build_html = "<span class='post-wrap tag-" + feedId + "-" + tag_id + "' " + style + ">";
                    build_html += tagname + "&nbsp&nbsp&nbsp&nbsp&nbsp";
                    build_html += "<div class='delete-button' " + style + ">";
                    build_html += "<a onclick='tagDelete(" + tag_id + "," + feedId + ");' class='button-delete' " + style + "><i class='fa fa-fw fa-times'></i></a></div></span>";
                    $(build_html).insertBefore(".tag-select-" + feedId);
                }
            }
            $('.tag-select-' + feedId).hide();
        }
    });
    return false;

}

function closeDiaglog(_imgsrc, _element) {
    $.each($("#dialog"), function (index, value) {
        $(this).remove();
    });
    var _imageEffect = $('#' + _element).closest('div.form-group').find('img');
    d = new Date();
    _imageEffect.attr('src', _imgsrc + "?" + d.getTime());
   $('#' + _element).lightBox();
}

resendRegister = function (url, email) {
    var data = {
        email: email
    };
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (response) {
            alert(response);
        }
    });
    return false;
}

runbatch = function () {
    if (confirm('Are you sure?') == false) {
        return false;
    }
    alert('Batch is running...');
    var data = {};
    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: baseUrl + '/ajax/runbatch',
        data: data,
        success: function (response) {
            alert(response);
        }
    });
    return false;
}

killProcess = function (id) {
    if (confirm('Are you sure?') == false) {
        return false;
    }
    location.href = baseUrl + controller + '/ps?id=' + id;
    return false;
}

invite = function (email, user_id, admin_id, company_id, regist_type, disable) {
    if (disable == '0') {
        if (confirm("Are you sure?")) {
            $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/resendinvite',
                data: {
                    email: email,
                    user_id: user_id,
                    admin_id: admin_id,
                    company_id: company_id,
                    regist_type: regist_type
                }
            }).done(function (response) {
                document.location.reload();
            });
        }
    } else {
        return false;
    }
}


/**
 * Recuiter news feed ajax without reload page
 */
$(".quick-comment").click(function (e) {
    e.preventDefault();
    if (!$(this).hasClass("disabled")) {
        var id = $(this).attr("data-id");
        $("button[data-id='"+id+"']").addClass('disabled');
        $.ajax({
            cache: false,
            type: "POST",
            url: baseUrl + 'ajax/quick_comment',
            dataType: "json",
            data: {
                news_feed_id: id
            }
        }).done(function(response) {
            alert(response.message);
            if (response.success) {
                //window.location.reload();
            }
        });
    }
});

/**
 * Recuiter news feed ajax reload page
 */
$(".quick-comment-reload").click(function (e) {
    e.preventDefault();
    if (!$(this).hasClass("disabled")) {
        var id = $(this).attr("data-id");
        $("button[data-id='"+id+"']").addClass('disabled');
        $.ajax({
            cache: false,
            type: "POST",
            url: baseUrl + 'ajax/quick_comment',
            dataType: "json",
            data: {
                news_feed_id: id
            }
        }).done(function(response) {
            alert(response.message);
            if (response.success) {
                window.location.reload();
            }
        });
    }
});

