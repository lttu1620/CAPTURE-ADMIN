 $(function () {
        $size='standart';
        switch ($size) {
            case 'large':
                $('#myModal').attr('class', 'modal fade bs-example-modal-lg')
                    .attr('aria-labelledby','myLargeModalLabel');
                $('.modal-dialog').attr('class','modal-dialog modal-lg modal-dialog-custom');
                break;
            case 'standart':
                $('#myModal').attr('class', 'modal fade')
                    .attr('aria-labelledby','myModalLabel');
                $('.modal-dialog').attr('class','modal-dialog modal-dialog-custom');
                break;
            case 'small':
                $('#myModal').attr('class', 'modal fade bs-example-modal-sm')
                    .attr('aria-labelledby','mySmallModalLabel');
                $('.modal-dialog').attr('class','modal-dialog modal-sm modal-dialog-custom');
                break;
        }
        $('.schedule').datetimepicker(
            {
                   format: 'YYYY年MM月DD日 HH時mm分',
                    stepping:10,
                    locale: 'ja',
                    useCurrent: false,
                    minDate: new Date([moment().subtract('minute',10)]),
                    maxDate: new Date([moment().subtract('years',-10)]),
                    showTodayButton:true,
                    showClear:true,
                    showClose:true
            });
        $("#start_date").on("dp.change", function (e) {
            if(e.date != null && e.date != undefined){
                $('#finish_date').data("DateTimePicker").minDate(e.date);
            }
        });
        $("#finish_date").on("dp.change", function (e) {
            
            if(e.date != null && e.date != undefined){
                $('#start_date').data("DateTimePicker").maxDate(e.date);
            }

        });
        $('.schedule > input').click(function() {
            $(this).next().click();
        });

        // action ajax save time to DB
        $("#btnModalSave").bind('click', function () {

            var _sd  = $('#start_date').data('date') !== undefined ?  
                                date_change_formart($('#start_date').data('date').trim(),1) :"";
            var _fd = $('#finish_date').data('date') !== undefined ? 
                                date_change_formart($('#finish_date').data('date').trim(),1): ""
            if(_sd + _fd == "")
            {
                alert('No set schedule !');
                $('#myModal').modal('hide');
                return false;
            }

            var _startDate  = _sd;//_sd == "" ? "" : _sd[0] + "-" + _sd[1] +"-" + _sd[2] + " " + _sd[3] + ":" + _sd[4];
            var _finishDate = _fd;//_fd == "" ? "" : _fd[0] + "-" + _fd[1] +"-" + _fd[2] + " " + _fd[3] + ":" + _fd[4];
            var id      = $('#myModal').data('news_feed_id');  

            var data    = {
                            news_feed_id : id,
                            start_date   : _startDate ,
                            finish_date  : _finishDate

                        };   
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/newsfeedschedule',
            data: data,
            dataType: "json",
            success: function (response) {
                if (!response.success) {
                    alert(response.message);
                } else {
                    //$("."+id+"___date_time_rank").addClass('hidden'); 
                    $("." + id + "___date_time_rank").next().remove();
                    $("." + id + "___date_time_rank").after($("<div>").append(
                            ($('#start_date').data('date') !== undefined && $('#start_date').data('date') !== '' ? $('<div>').addClass('mt5 start_date').append(_textStart + " : "
                                    + $('#start_date').data('date')) : ""),
                            ($('#finish_date').data('date') !== undefined && $('#finish_date').data('date') !== '' ? $('<div>').addClass('mt5 finish_date').append(_textFinish + " : "
                                    + $('#finish_date').data('date')) : "")));
                }
            }
        });

            $('#myModal').modal('hide');
            return false
        });
    });

    function showdatetimerank(selector) {
        //$("#reservationtime").val('');
        var _news_feed_id = $(selector).attr('class').substring(0,$(selector).attr('class').indexOf('___'));

        var _startDate = date_change_formart($(selector).next().find('div.start_date').text().trim(),2);//.replace(/\D+/g,'@').replace(/^@/,'');
        var _finish_date = date_change_formart($(selector).next().find('div.finish_date').text().trim(),2);//.replace(/\D+/g,'@').replace(/^@/,'');

        $('#start_date').data('DateTimePicker').minDate(false);
        $('#start_date').data('DateTimePicker').maxDate(false);
        $('#finish_date').data('DateTimePicker').minDate(false);
        $('#finish_date').data('DateTimePicker').maxDate(false);
        if( _finish_date != ""){
            $('#finish_date').data('DateTimePicker').date(_finish_date).change;            
            if (new Date(_finish_date) < moment()) {
                $('#finish_date').data('DateTimePicker').disable();
                $('#start_date').data('DateTimePicker').disable();
            } else {
                $('#start_date').data('DateTimePicker').maxDate(_finish_date);
                $('#finish_date').data('DateTimePicker').enable();
                $('#finish_date').data('DateTimePicker').minDate(new Date([moment().subtract('minute',10)]));
            }
        } else {
            $('#finish_date').data('DateTimePicker').date(null).change;            
            $('#finish_date').data('DateTimePicker').minDate(new Date([moment().subtract('minute',10)])).enable();
        }

        // Set Start date and finish date to datetimepicker;
        if( _startDate != "")
        {
            $('#start_date').data('DateTimePicker').minDate(_startDate);
            // check to disable
            if (new Date(_startDate) < moment()) {
                $('#start_date').data('DateTimePicker').disable();
            } else {
                $('#start_date').data('DateTimePicker').enable();
            }
            $('#start_date').data('DateTimePicker').date(_startDate).change;
        } else {
            $('#start_date').data('DateTimePicker').date(null).change;            
            $('#start_date').data('DateTimePicker').minDate(new Date([moment().subtract('minute',10)])).enable();
        }

        $('#myModal').data('news_feed_id', _news_feed_id);

        $('#myModal').modal('show');
        return false;
    }

    function date_change_formart(_string ,_type)
    {
        _type       =  _type === undefined ? 1 : 2;// 1: to english; 2: to japanese
        _pattern    = null;
        _replace    = "";
        switch(_type)
        {
            case 1 : 
                     _pattern   = /(\d{4})\/(\d{2})\/(\d{2})(\s)*(\d{2})\:(\d{2})/;
                    _replace    = '$1年$2月$3日 $5時$6分';
                break;
            case 2: 
                     _pattern   = /(\W)*(\d{4})\年(\d{2})\月(\d{2})\日(\s)*(\d{2})\時(\d{2})\分/;
                    _replace    = '$2/$3/$4 $6:$7';
                break;
        }
        
        return _string.replace(_pattern, _replace);
    }

