 $.extend($.imgAreaSelect.prototype, {
        animateSelection: function (x1, y1, x2, y2, duration) {
            var fx = $.extend($('<div/>')[0], {
                ias: this,
                start: this.getSelection(),
                end: { x1: x1, y1: y1, x2: x2, y2: y2 }
            });
            
            $(fx).animate({
                cur: 1
            },
            {
                duration: duration,
                step: function (now, fx) {
                    var start =fx.elem.start, 
                    end =fx.elem.end,
                    curX1 = Math.round(start.x1 + (end.x1 - start.x1) * now),
                    curY1 = Math.round(start.y1 + (end.y1 - start.y1) * now),
                    curX2 = Math.round(start.x2 + (end.x2 - start.x2) * now),
                    curY2 = Math.round(start.y2 + (end.y2 - start.y2) * now);
                    fx.elem.ias.setSelection(curX1, curY1, curX2, curY2);
                    fx.elem.ias.update();
                }
            });
        }
    });

    $(document).ready(function () {
        $('#myModal').attr('class', 'modal fade bs-example-modal-lg')
                    .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg modal-dialog-custom');

        $('#myModal').modal('show');
        
        $('#myModal').on('hide.bs.modal', function () {
          // do something…
            $('div.imgareaselect-outer').prev().hide();
            $('div.imgareaselect-outer').hide();
        })
        var x1 = 0;
        var y1 = 0;
        var x2 = 0;
        var y2 = 0;
        var width = 0;
        var height = 0;
        
        var pic_real_width = 1, pic_real_height = 1, pic_width = 1, pic_height = 1;
        
        $("<img/>")  // Make in memory copy of image to avoid css issues
        .attr("src", $("#image").attr("src"))
        .load(function () {
                    pic_real_width = this.width; // Note: $(this).width() will not
                    pic_real_height = this.height; // work for in memory images.
                    $("div.imageDescription span.width").text( _textW + ": " + pic_real_width + "px");
                    $("div.imageDescription span.height").text( _textH + ": " + pic_real_height + "px");
                });
        setTimeout(function () {
            pic_width = $("#image").width();
            pic_height = $("#image").height();
        }, 200);

        // Init for imgAreaSelect
        var ias = $('#image').imgAreaSelect({
            handles: true,
            instance: true,
            //enable: true,            
            fadeSpeed : 400,
            //show : true,
        });
         //Flat red color scheme for iCheck
         $('div#myModal input.minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
        })
        .on('ifChanged', function(event){
             if($(this).hasClass('scale_image')){
                $('div#myModal input.background_color').iCheck('uncheck');
                if(!$('div#myModal input.background_color').prop('checked')) {
                    if($(this).prop('checked')) // Scale by 16:9
                     {
                        ias.setOptions({
                            aspectRatio : '16:9',
                            show : true
                        });
                        // If nothing's selected, start with a tiny area in the center
                        if (!ias.getSelection().width)
                            ias.setOptions({ show: true,
                                            x1: Math.round(pic_width/2) - 1,
                                            y1: Math.round(pic_height/2)  -1,
                                            x2: Math.round(pic_width/2),
                                            y2: Math.round(pic_height/2 )
                       });
                        ias.animateSelection(
                                    0,//Math.round(pic_width/4), 
                                    0,//Math.round(pic_height/4),
                                    pic_width,//Math.round(pic_width/4) + Math.round(pic_height * 16 /18), 
                                    Math.round(pic_width * 9 / 16),//Math.round(pic_height/4) + Math.round(pic_height /2), 
                                    'slow');
                    }else{
                        ias.setOptions({
                            aspectRatio : '',
                            show : true
                        });
                         if (!ias.getSelection().width)
                        //ias.setOptions({ show: true, x1: 199, y1: 149, x2: 200, y2: 150 });
                        ias.setOptions({ show: true,
                                        x1: Math.round(pic_width/2) - 1,
                                        y1: Math.round(pic_height/2)  -1,
                                        x2: Math.round(pic_width/2),
                                        y2: Math.round(pic_height/2 )
                        });
                        ias.animateSelection(
                                        Math.round(pic_width/4), 
                                        Math.round(pic_height/4),
                                        Math.round(pic_width/4) + Math.round(pic_width/2), 
                                        Math.round(pic_height/4)+ Math.round(pic_height/2), 
                                        'slow');
                    }  
                }
                 
            }else if($(this).hasClass('background_color')){
                 if($(this).prop('checked')) // Scale by 16:9
                 {
                    $('div#myModal input.scale_image').iCheck('uncheck');
                    ias.setOptions({ hide: true,
                        x1: 0,
                        y1: 0,
                        x2: pic_width,
                        y2: pic_height });
                    ias.update();
                }
            }
        });

        //color picker with addon
        $("div#myModal .my-colorpicker2").colorpicker();

        $("div#myModal #txtColor").click(function() {
            $('div.input-group-addon').click();
        });

        $("div#myModal #btncrop").click(function () {
            
            if($('div#myModal input.background_color').prop('checked') && $("#txtColor").val().trim() == "")
            {
                alert(_msg2);
                $("#txtColor").focus();
                return false;
            }

            var _elem = ias.getSelection();
            width = ! isNaN(_elem.width) ? _elem.width : 0;
            height = ! isNaN(_elem.height) ? _elem.height : 0;
            x1 = ! isNaN(_elem.x1) ? _elem.x1 : 0;
            y1 = ! isNaN(_elem.y1) ? _elem.y1 : 0;
            x2 = ! isNaN(_elem.x2) ? _elem.x2 : 0;
            y2 = ! isNaN(_elem.y2) ? _elem.y2 : 0;
            if (width == 0 && height == 0) {
                alert(_msg1);
                return false;
            }
          /*  console.log(x1 + "," + y1 + "," + x2 + "," +y2 );
            return false;
            ////////////////////////////////////*/
            var data = {
                info: _info,
                x1: pic_real_width > pic_width ? Math.round(x1 * pic_real_width / pic_width) : x1,
                y1: pic_real_height > pic_height ? Math.round(y1 * pic_real_height / pic_height) : y1,
                x2: pic_real_width > pic_width ? Math.round(x2 * pic_real_width / pic_width) : x2,
                y2: pic_real_height > pic_height ? Math.round(y2 * pic_real_height / pic_height) : y2,
                background_color: $("#txtColor").val().trim()
            };
            $.ajax({
                cache: false,
                async: true,
                type: "POST",
                url: baseUrl + '/images/crop',
                data: data,
                dataType: 'jsonp',
                jsonp: 'callback',
                success: function (response) {
                    if (response['status'] == '200') {
                        var _imgsrc = response['message'];
                        var _element = response['element'];
                        // Call method from parent    
                        closeDiaglog(_imgsrc, _element);                
                        $('#myModal').modal('hide');
                        //window.parent.closeDiaglog(_imgsrc, _element);
                    } else {
                        alert(response['message']);
                    }
                }
            });
            return false;
        });
});