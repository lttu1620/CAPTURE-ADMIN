<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Capture: 404 page</title>
        <meta name="keywords" content="404">
        <link href="<?php echo $baseUrl?>404/style.css" rel="stylesheet" type="text/css" media="all">
    </head>
    <body>
        <div class="wrap">           
            <div class="header">
                <div class="logo">
                    <h1><a href="<?php echo $baseUrl?>">Capture</a></h1>
                </div>
            </div>
            <div class="content">
                <img src="<?php echo $baseUrl?>404/error-img.png" title="error">
                <p>
                    <span></span>
                    <?php echo __('You Requested the page that is no longer there.')?>                    
                </p>
                <a href="<?php echo $baseUrl?>"><?php echo __('Home')?></a>
                <a href="#" onclick="history.back();return false;"><?php echo __('Back')?></a>
                <div class="copy-right">
                    
                </div>
            </div>
        </div>        
    </body>
</html>