﻿function paging(url, div) {	
	$.get(url,{},function(result){if(result){$('#'+div).html(result);}});
}
function urldecode(str) {
   return decodeURIComponent((str+'').replace(/\+/g, '%20'));
}
function isValidDate(dateStr, format) {
	if (format == null) { format = "MDY"; }
	format = format.toUpperCase();
	if (format.length != 3) { format = "MDY"; }
	if ( (format.indexOf("M") == -1) || (format.indexOf("D") == -1) || (format.indexOf("Y") == -1) ) { format = "MDY"; }
	if (format.substring(0, 1) == "Y") {
	  var reg1 = /^\d{2}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
	  var reg2 = /^\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
	} else if (format.substring(1, 2) == "Y") {
	  var reg1 = /^\d{1,2}(\-|\/|\.)\d{2}\1\d{1,2}$/
	  var reg2 = /^\d{1,2}(\-|\/|\.)\d{4}\1\d{1,2}$/
	} else {
	  var reg1 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{2}$/
	  var reg2 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/
	}
	if ( (reg1.test(dateStr) == false) && (reg2.test(dateStr) == false) ) { return false; }
	var parts = dateStr.split(RegExp.$1);
	if (format.substring(0, 1) == "M") { var mm = parts[0]; } else if (format.substring(1, 2) == "M") { var mm = parts[1]; } else { var mm = parts[2]; }
	if (format.substring(0, 1) == "D") { var dd = parts[0]; } else if (format.substring(1, 2) == "D") { var dd = parts[1]; } else { var dd = parts[2]; }
	if (format.substring(0, 1) == "Y") { var yy = parts[0]; } else if (format.substring(1, 2) == "Y") { var yy = parts[1]; } else { var yy = parts[2]; }
	if (parseFloat(yy) <= 50) { yy = (parseFloat(yy) + 2000).toString(); }
	if (parseFloat(yy) <= 99) { yy = (parseFloat(yy) + 1900).toString(); }
	var dt = new Date(parseFloat(yy), parseFloat(mm)-1, parseFloat(dd), 0, 0, 0, 0);
	if (parseFloat(dd) != dt.getDate()) { return false; }
	if (parseFloat(mm)-1 != dt.getMonth()) { return false; }
	return true;
}
function isPhoneNumber(str){
	var alphaExp = /^((\(\+?84\)[\-\.\s]?)|(\+?84[\-\.\s]?)|(0))((\d{3}[\-\.\s]?\d{6})|(\d{2}[\-\.\s]?\d{8}))$/;
	if(str.match(alphaExp)){
		return true;
	}
	return false;
}
function isAlphabet(str){
	var alphaExp = /^[a-zA-Z]+$/;
	if(str.match(alphaExp)){
		return true;
	}
	return false;
}
function isAlphabetAndNumber(str) {
	var alphaExp = /^[a-zA-Z0-9_]+$/;
	if (str.match(alphaExp)){
		return true;
	}
	return false;
}
function isUserName(str) {
	var alphaExp = /^[a-zA-Z0-9_.\-]+$/;
	if (str.match(alphaExp)){
		return true;
	}
	return false;
}
function isEmail(email) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(email);
};
function isNumber(str) {
	var alphaExp = /^[0-9]+$/;
	if(str.match(alphaExp)){
		return true;
	}
	return false;
}
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57))	return false;
	return true;
}
function strip_tags(str) {
    str = str.replace(/&nbsp;/g,'');
	str = jQuery.trim(str);
	allowed_tags = '';
	var key = '', allowed = false;
    var matches = [];
    var allowed_array = [];
    var allowed_tag = '';
    var i = 0;
    var k = '';
    var html = '';
    var replacer = function(search, replace, str) {
        return str.split(search).join(replace);
    };
    // Build allowes tags associative array
    if (allowed_tags) {
        allowed_array = allowed_tags.match(/([a-zA-Z]+)/gi);
    }
    str += '';
    // Match tags
    matches = str.match(/(<\/?[\S][^>]*>)/gi);
    // Go through all HTML tags
    for (key in matches) {
        if (isNaN(key)) {
            // IE7 Hack
            continue;
        }
        // Save HTML tag
        html = matches[key].toString();
        // Is tag not in allowed list? Remove from str!
        allowed = false;
        // Go through all allowed tags
        for (k in allowed_array) {
            // Init
            allowed_tag = allowed_array[k];
            i = -1;
            if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+'>');}
            if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+' ');}
            if (i != 0) { i = html.toLowerCase().indexOf('</'+allowed_tag)   ;}

            // Determine
            if (i == 0) {
                allowed = true;
                break;
            }
        }
        if (!allowed) {
            str = replacer(html, "", str); // Custom replace. No regexing
        }
    }
    return str;
}
function checkAll(strItemName, value) {
	var x=document.getElementsByName(strItemName);
	for (var i=0; i<x.length; i++) {
	  	if (value == 1 && !x[i].disabled) {
			if( !x[i].checked ) x[i].checked = 'checked';
		} else {
			if(x[i].checked) x[i].checked = '';
		}
	}
}
function getItemsChecked(strItemName, sep) {
	var x=document.getElementsByName(strItemName);
	var p="";
	for(var i=0; i<x.length; i++) {
		if(x[i].checked) {
			p += x[i].value + sep;
		}
	}
	var result = (p != '' ? p.substr(0, p.length - 1) : '');
	return result;
}
deleteItem = function(id) {
	if (!id || $("#actionId").length <= 0) {
		return false;
	}
	if (confirm('Are you sure') == false) {
		return false;
	}
	$("#action").val('delete');
	$("#actionId").val(id);
	$("#dataForm").submit();
} 
activeItem = function(id, is_active) {	
	if (typeof is_active == "undefined") { alert(1);
		return false;
	}
	if (!id || $("#actionId").length <= 0) {
		return false;
	}
	if (confirm('Are you sure') == false) {
		return false;
	}	
	$("#action").val(is_active == 0 ? 'active' : 'inactive');
	$("#actionId").val(id);
	$("#dataForm").submit();
} 
inActiveItem = function(id) {
	if (confirm('Are you sure') == false && $("#item_"+id).length > 0) {
		return false;
	}
	$("#action").val('inactive');
	$("#item_"+id).prop("checked", true);
	$("#dataForm").submit();
} 
$(function() {	 //alert(window.location.search);
	$(".btn-delete").click(function(){
		var items = getItemsChecked('items[]', ','); 
		if (items == '') {
			alert('Please select a item');
			return false;
		}
		$("#action").val('delete');			
		return true;		
	});
	$(".btn-inactive").click(function(){
		var items = getItemsChecked('items[]', ',');
		if (items == '') {
			alert('Please select a item');
			return false;
		}
		$("#action").val('inactive');	
		return true;		
	});
	$(".btn-active").click(function(){
		var items = getItemsChecked('items[]', ',');
		if (items == '') {
			alert('Please select a item');
			return false;
		}
		$("#action").val('active');	
		return true;
	});
	$(".btn-search").click(function(){
		var action = $("#searchForm").attr('action');
		//alert(action);
		//return false;
	});
});